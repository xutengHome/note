# HAP入门

## 开发环境准备

### 后端开发环境准备

#### 开发工具

JDK 1.8 及以上

```java
java -version // java version "1.8.0_211"
```

redis 3.0 及以上

```java
cd D:\software\Redis-x64-3.2.100 // 进入Redis安装路径
redis-cli -v // redis-cli 3.2.100
```

maven 3.3 及以上

```java
mvn version // Apache Maven 3.6.1
```

Tomcat 7+ (不支持 Tomcat 6)

```java
catalina version // Server version: Apache Tomcat/9.0.10
```

数据库（任选），这里选择Mysql

```java
mysql -uroot -p123456 // Server version: 5.7.17 MySQL Community Server (GPL)
/*
Mysql server 5.6 及以上
SqlServer 2012 及以上
Oracle 9 以上
Navicat Premium（或其他）
*/ 
```

Git

```java
git --version // git version 2.22.0.windows.1
```

#### 软件安装

##### Java安装

- 在 Oracle 官网 下载对应平台的 JDK 1.8 以上的环境。
- 本地执行安装文件，安装 JDK 环境。
- Win 在环境变量系统变量中 path 配置 JDK 的环境变量指向 JDK 安装目录下 JDK/bin。
- 配置完成后打开 cmd 执行 javac，有提示则说明环境安装成功。

##### Git安装

- 在 Git 官网 下载对应平台的 Git。
- 本地执行安装文件， 安装 Git 环境。
- Win 在环境变量中系统变量的 path 配置 Git 的环境变量指向 Git 安装目录下的 /bin。
- 配置完成后打开 cmd 执行 git，有提示则说明环境安装成功

##### Maven安装

- 在 Maven 官网 下载对应平台的合适的 maven 版本的压缩包。
- 本地解压压缩包。
- Win 在环境变量中系统变量的 path 配置 maven 的环境变量指向 maven 解压目录下的 /bin。
- 配置完成后打开 cmd 执行 mvn -v，有提示则说明环境安装成功。

##### Redis 安装

>  开发需要依赖 Redis 环境，所以在启动后台程序时，请确保 Redis 已经启动。

- 在 Redis win 官网 下载最新的 redis 版本的压缩包。
- 本地解压压缩包。
- Win 在解压目录下打开 cmd 执行 **redis-server.exe redis.windows.conf**，有提示则说明 Redis 已经启动。
- Win 平台下如果启动失败，修改 redis.windows.conf 文件中的 maxheap 为 maxheap 1024000000。
- redis GUI 客户端下载 ：http://redisdesktop.com/
- redis 命令手册：http://doc.redisfans.com/
- redis 清空缓存命令：flushall
- redis服务器启动命令: `redis-server.exe  redis.windows.conf`

## 本地环境搭建

### 确定项目信息

- groupId 本项目的代号，比如汉得的 BI 产品，代号为 hbi**(可以自己修改)**
- artifactId 本项目的顶层目录名称，使用项目代号(第一个字母大写) + Parent，如 HbiParent**(可以自己修改)**
- package 包名称，使用项目代号 + core ,如 hbi.core **(可以自己修改)**
- archetypeVersion 是指模板项目的版本

### 新建项目

> 使用maven新建项目

```java
mvn org.apache.maven.plugins:maven-archetype-plugin:2.4:generate -D archetypeGroupId=hap -D archetypeArtifactId=hap-webapp-archetype -D archetypeVersion=3.5-SNAPSHOT -D groupId=hbi -D artifactId=HbiTemplate -D package=hbi.core -D archetypeRepository=http://nexus.saas.hand-china.com/content/repositories/rdcsnapshot
```

#### 版本输入

> 默认是1.0-SNAPSHOT, 这里输入4.0-SNAPSHOT(后来发现4.0版本有问题后来重新生成项目这里输入：3.5-SNAPSHOT)

![image-20211108162443876](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211108162443876.png)

### Maven配置

#### 配置本地仓库的存储位置

修改Maven的配置文件：Maven的安装路径下的conf/settings.xml

![image-20211108164159989](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211108164159989.png)

#### 配置镜像

修改Maven的配置文件：Maven的安装路径下的conf/settings.xml

![image-20211108164444561](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211108164444561.png)	

```xml
<mirror>      
  <id>nexus-aliyun</id>    
  <name>nexus-aliyun</name>  
  <url>http://maven.aliyun.com/nexus/content/groups/public</url>    
  <mirrorOf>central</mirrorOf>      
</mirror>  
```

### 编译项目

> 初次执行会下载hap相应的依赖，需要下载一段时间,如果因为网络因素停顿,可以Ctrl+C,重新执行命令
>
> Install的目的是将工程下的core及core-db子工程安装到本地仓库,因为在初始化数据库时依赖core-db的jar包.
>
> 注意:每次更新了core-db中的文件都需要重新执行上述步骤并重新初始化数据库

在artifactId目录下执行命令

```java
mvn clean install -D maven.test.skip=true
```

### 初始化数据库表结构及基础数据

> HAP框架支持三种常用数据库,Mysql,Oracle,SqlServer.下面以Mysql为示例.

#### 创建数据库

```mysql
# 创建schema create database hap_dev
create schema hap_dev default character set utf8;
```

#### 创建用户

```mysql
# 进入mysql数据库
use mysql;
# 创建用户hap_dev,并将密码设置为hap_dev
create user hap_dev@'%' identified by 'hap_dev';
create user hap_dev@'localhost' identified by 'hap_dev';
# 将上面新建的hap_dev的权限全部赋予用户hap_dev
grant all privileges on hap_dev.* to hap_dev@'%';
grant all privileges on hap_dev.* to hap_dev@'localhost';
flush privileges;
# 删除用户
drop user hap_dev@'%';
drop user hap_dev@'localhost';
flush privileges;
```

#### 初始化数据库

> 通过maven命令初始化数据库

```mysql
# 进入项目根目录
cd HbiTemplate;
# 初始化数据库
mvn process-resources -D skipLiquibaseRun=false -D db.driver=com.mysql.jdbc.Driver -D db.url=jdbc:mysql://127.0.0.1:3306/hap_dev -Ddb.user=hap_dev -Ddb.password=hap_dev
```

#### 其它数据库初始化

> 没有测试过

SqlServer

```java
mvn process-resources -D skipLiquibaseRun=false  -Ddb.user=hap -Ddb.password=handhapdev -D db.driver=com.microsoft.sqlserver.jdbc.SQLServerDriver -D db.url="jdbc:sqlserver://10.211.55.6:1433; DatabaseName=hap_dev"
```

Oracle

```java
mvn process-resources -D skipLiquibaseRun=false -D db.driver=oracle.jdbc.driver.OracleDriver -D db.url=jdbc:oracle:thin:@192.168.115.136:1521:HAP -D db.user=hap_dev -D db.password=hap_dev
```

### 配置JNDI数据源

> HAP项目通过JNDI(Java Naming and Directory Interface)获取数据源的配置信息.在运行项目前需要配置JNDI数据源
>
> 修改tomcat中conf/context.xml, 添加如下代码
>
> driverClassName，url，name，username，password属性可以根据需要修改
>
> name属性与hap中的config.properties有关联.如无特殊需要,可不做修改

```xml
<Resource auth="Container" driverClassName="com.mysql.jdbc.Driver" url=" jdbc:mysql://localhost:3306/hap_dev?useUnicode=true&amp;characterEncoding=UTF-8" name="jdbc/hap_dev" type="javax.sql.DataSource" username="hap_dev" password="hap_dev"/>
```

### 运行项目

```mysql
# 进入项目根目录
cd HbiTemplate;
# IntelliJ IDEA 用户可以跳过此步骤
mvn eclipse:eclipse;	
```

#### 导入项目

![image-20211109153542024](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109153542024.png)	

#### 配置服务器

![image-20211109154026949](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109154026949.png)	

![image-20211109153719512](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109153719512.png)	

#### 运行项目

> 运行成功后会自动跳转

![image-20211109153758163](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109153758163.png)	

> 用户名/密码：admin/admin

![image-20211109154303545](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109154303545.png)	

## 开发规范

> 在浏览其他文档时，也顺便在自己的文档中记录一下。

### 后端开发规范

#### Package规约

> 主要讲述HAP工程中代码存放的位置和工程，大致上分为三层Package。

##### 一级目录

> 沿用了ERP开发模块界定,主要考虑package与后台建表的规范统一; 同时保证多人开发尽量减少代码冲突;

![image-20211109155713157](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109155713157.png)	

| **模块功能** | **模块包**         |
| :----------- | ------------------ |
| xxpub        | 公共模块           |
| xxws         | 发布给外围系统接口 |
| xxwip        | 完工生产相关       |
| xxom         | 销售模块           |
| xxpo         | 采购               |
| xxinv        | 库存               |
| xxar         | 应收               |
| xxap         | 应付               |
| xxgl         | 总账               |
| xxmd         | 主数据模块         |
| xxevent      | 事件执行模块       |
| xxreport     | 报表模块           |
| xxsn         | 序列管理模块       |

##### 二级目录

> 按照具体作用进行分层，方便代码索引到具体位置

| 功能界定     | **包名** |
| ------------ | -------- |
| 基础设置功能 | basic    |
| 业务功能     | biz      |
| 集成功能     | intg     |
| 工作流       | wf       |
| 通用功能     | common   |
| 工具包       | utils    |

![image-20211109155923096](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109155923096.png)	

> 以xxar模块为例：
>
> - 底下分为了basic和intg两个文件夹；
> - basic为HAP基础的功能（与Cloud无关），如收款预测放置的地方。
> - intg为与Cloud相关的同步程序放置的地方，包括为了同步接口设计的接口表dto/mapper，同步task等程序。

##### 三级目录

> 具体类存放的位置

| 定时/其他任务   | task       |
| --------------- | ---------- |
| 队列消费者等    | components |
| 定时任务        | job        |
| 常量包          | constant   |
| 实体类          | dto        |
| Mybatis查询方法 | mapper     |
| 具体处理逻辑    | service    |
| Controller      | controller |

#### 阿里巴巴规范检查

##### 安装插件Alibaba Java Coding Guidelines

> File -> Setting

![image-20211109160605811](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109160605811.png)	

##### 使用插件分析代码

![image-20211109160813889](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109160813889.png)	

> 扫描之后可以看到对应的分析结果以及对应的说明

![image-20211109161016410](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109161016410.png)

##### 代码规约

> 总体规约以《阿里巴巴Java开发手册》为主，请开发人员至少阅读一遍该手册。
>
> http://hzerodoc0-11.saas.hand-china.com/files/docs/development-specification/backend-development-specification/basic/阿里巴巴Java开发手册.pdf
>
> 总体要求
>
> - 代码提交之前使用阿里巴巴规约插件以及SonarQube插件扫描，可以开启阿里巴巴规约实时扫描。
> - 保持代码整洁性，格式化代码。
> - 代码命名规范，无论是包、类、方法、变量，见名知意。
> - 多使用 jdk，自有库和被验证的第三方库的类和函数。

#### 代码注释说明

> 良好的注释不仅可以帮助自己和其他开发者理解代码结构，也可以用在项目编译时生成javadoc，避免重复工作。
>
> 现代IDE可以通过模板的方式自动生成一些格式化的注释，在此我们提供模板的规范。请不要使用非Javadoc标准的注解，如@date。
>
> 方法注解使用自动生成的即可（输入“/**”+回车键）

##### 类注释

> 所有的类都必须使用 Javadoc，***添加创建者和创建日期及描述信息***，不得使用 // xxx 方式。

```java
/**
  * <p>类描述</p>
  * @author ${USER} ${DATE} ${TIME} 
  * @version 1.0
  */
```

![image-20211109172611909](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211109172611909.png)	

##### 方法注释

> 所有的抽象类、接口中的方法必须要用 Javadoc 注释、除了***返回值、参数、异常说明外，还必须指出该方法做什么事情，实现什么功能。对子类的实现要求，或者调用注意事项***，请一并说明。
>
> 这里使用idea默认的方法注释

> 方法内部单行注释，在被注释语句上方另起一行，使用//注释。方法内部多行注释使用/* */注释，注意与代码对齐。

```java
public void test(){
    // 单行注释
    String single = "";

    /*
     * 多行注释
     * 多行注释
     */
    String multi = "";
}

```

> 私有方法可以使用 // xxx 注释，也可以使用Javadoc注释

```java
// description
private void test3 () {
    // ...

    /* ... */
}

```

##### 字段注释

> 实体属性使用Javadoc注释，标明字段含义，这样getter/setter会含有注释。
>
> public 属性必须使用Javadoc注释

```java
/**
  * 静态字段描述
  */
public static final String STATIC_FIELD = "DEMO";

/**
  * 姓名
  */
private String name;

```

##### 特殊注释标记

> 待办事宜（TODO）:（ 标记人，标记时间，[预计处理时间]）表示需要实现，但目前还未实现的功能。
>
> 错误（FIXME）:（标记人，标记时间，[预计处理时间]）在注释中用 FIXME 标记某代码是错误的，而且不能工作，需要及时纠正的情况。

```java
public void test3 () {
    // TODO 待完成 [author, time]

    // FIXME 待修复 [author, time]
}
```

##### 分隔符

> 有时需要划分区块，可以使用分隔符进行分隔

```Java
//
// 说明
// ------------------------------------------------------------------------------

//========================================================
//  说明
//========================================================

```

##### 其它

> 同一个类多个人开发，如有必要请在类、方法上加上***负责人、时间信息***
>
> 代码修改的同时，注释也要进行相应的修改，尤其是参数、返回值、异常、核心逻辑等的修改
>
> 对于注释的要求：第一、能够准确反应设计思想和代码逻辑；第二、能够描述业务含义，使别的程序员能够迅速了解到代码背后的信息。
>
> 好的命名、代码结构是自解释的，注释力求精简准确、表达到位。避免出现注释的一个极端：过多过滥的注释，代码的逻辑一旦修改，修改注释是相当大的负担。
>
> 大段代码注释请注意格式化，可以使用<pre>、<ul>、<li>、<b>、<strong>等常用标签美化下注释。

#### 异常规范

> 后端异常规范，在开发使用中，异常应该能够很好地帮助我们定位到问题的所在。如果使用一种错误的方式，则bug很难被找到

##### 异常捕获

> 我们捕获异常的目的是要记录异常，把错误的消息变成格式化的数据可以让用户或者运维人员在具体的数据界面可以看得到，比如：
>
> 每一Phase 都要进行异常的throw，由外部调用捕获

- Job和Task运行过程中出现的异常，应该被记录到执行历史记录的执行概要里；
- 数据推送异常，比如往ERP推送一条数据产生的异常，应该记录到推送接口表里的“错误消息”字段。
- 数据接收异常，比如接收外围系统WMS的一次接口访问，无法通过我们的校验，那错误和异常应该记录到接收数据的接口表里的“错误消息”字段。
- 一些数据处理出错需要回滚的场景，产生了异常，需要整个业务正式表数据的事务回滚，此时的异常需要记录到处理接口表（保证接口表的事物不回滚正常提交）

##### 异常的分类

> JAVA中有三种一般类型的可抛类: 检查性异常(checked exceptions)、非检查性异常(unchecked Exceptions) 和 错误(errors)。

- Checked exceptions：必须通过方法进行声明。这些异常都继承自Exception类。一个Checked exception声明了一些预期可能发生的异常。
- Unchecked exceptions：不需要声明的异常。大多继承自RuntimeException。例如NullPointerException，ArrayOutOfBoundsException。同时这样的异常不应该捕获，而应该打印出堆栈信息。
- Errors：大多是一些运行环境的问题，这些问题可能会导致系统无法运行。例如OutOfMemoryError，StackOverflowError。

##### 用户自定义异常

- 当应用程序出现问题时，直接抛出自定义异常。

  ```java
  throw new DaoObjectNotFoundException("Couldn't find dao with id " + id);
  ```

- 将自定义异常中的原始异常包装并抛出。

  ```java
  catch (NoSuchMethodException e) {
    throw new DaoObjectNotFoundException("Couldn't find dao with id " + id, e);
  }
  ```

##### 错误的做法

- 不要吞下catch的异常。这样的捕获毫无意义。我们应该使用一定的日志输出来定位到问题。

  ```java
  try {
    System.out.println("Never do that!");
  } catch (AnyException exception) {
    // Do nothing
  }
  ```

- 方法上应该抛出具体的异常。而不是Exception。

  ```java
  public void foo() throws Exception { //错误方式
  
  }
  
  public void foo() throws SQLException { //正确方式
  
  }
  ```

- 要捕获异常的子类，而不是直接捕获Exception。

  ```java
  catch (Exception e) { //错误方式
  
  }
  ```

- 永远不要捕获Throwable类。

- 不要只是抛出一个新的异常，而应该包含堆栈信息。错误的做法

  ```java
  try {
    // Do the logic
  } catch (BankAccountNotFoundException exception) {
    throw new BusinessException();
    // or
    throw new BusinessException("Some information: " + e.getMessage());
  }
  try {
    // Do the logic
  } catch (BankAccountNotFoundException exception) {
    throw new BusinessException(exception);
    // or
    throw new BusinessException("Some information: " ,exception);
  }
  ```

- 要么记录异常要么抛出异常，但不要一起执行。

  ```java
  catch (NoSuchMethodException e) { 
    //错误方式 
    LOGGER.error("Some information", e);
    throw e;
  }
  ```

- 不要在finally中再抛出异常。

  ```java
  try {
   someMethod(); //Throws exceptionOne
  } finally {
   cleanUp();  //如果finally还抛出异常，那么exceptionOne将永远丢失
  }
  // 如果someMethod 和 cleanUp 都抛出异常，那么程序只会把第二个异常抛出来，原来的第一个异常（正确的原因）将永远丢失。
  ```

- 始终只捕获实际可处理的异常。不要为了捕捉异常而捕捉，只有在想要处理异常时才捕捉异常。

  ```java
  catch (NoSuchMethodException e) {
    throw e; //避免这种情况，因为它没有任何帮助
  }
  ```

- 不要使用printStackTrace()语句或类似的方法。

- 如果你不打算处理异常，请使用finally块而不是catch块。

- 应该尽快抛出(throw)异常，并尽可能晚地捕获(catch)它。你应该做两件事：分装你的异常在最外层进行捕获，并且处理异常。

- 在捕获异常之后，需要通过finally 进行收尾。在使用io或者数据库连接等，最终需要去关闭并释放它。

- 不要使用if-else 来控制异常的捕获。

- 一个异常只能包含在一个日志中。

  ```java
  // 错误
  LOGGER.debug("Using cache sector A");
  LOGGER.debug("Using retry sector B");
  // 正确
  LOGGER.debug("Using cache sector A, using retry sector B");
  ```

- 将所有相关信息尽可能地传递给异常。有用且信息丰富的异常消息和堆栈跟踪也非常重要。

- 在JavaDoc中记录应用程序中的所有异常。应该用javadoc来记录为什么定义这样一个异常。

- 异常应该有具体的层次结构。如果异常没有层次的话，则很难管理系统中异常的依赖关系。

  ```java
  // 类似这样
  class Exception {}
  class BusinessException extends Exception {}
  class AccountingException extends BusinessException {}
  class BillingCodeNotFoundException extends AccountingException {}
  class HumanResourcesException extends BusinessException {}
  class EmployeeNotFoundException extends HumanResourcesException {}
  ```

#### 代码事务规范

##### 事务基本内容

> 数据库事务(Database Transaction) ，是指作为单个逻辑工作单元执行的一系列操作，要么完全地执行，要么完全地不执行。事务，就必须具备ACID特性，即原子性（Atomicity）、一致性(Consistency)、隔离性(Isolation)和持久性(Durability)。
>
> 不允许使用编程式事务（即在在代码中显式调用beginTransaction()、commit()、rollback()等事务管理相关的方法），
>
> 推荐使用声明式事务，Spring 的声明式事务管理在底层是建立在 AOP 的基础之上的，其本质是对方法前后进行拦截，然后在目标方法开始之前创建或者加入一个事务，在执行完目标方法之后根据执行情况提交或者回滚事务。事务增强也是AOP的一大用武之处。

##### 声明式事务基础用法

- 在类加入注解@Transactional，此时该注解作用于这个类的所有方法

  ![image-20211110104248691](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104248691.png)	

- 在方法加入注解@Transactional，此该方法的注解优先于类的注解，如果一个类下方法都不一致，建议把类注解删掉，减少AOP代码执行

  ![image-20211110104346577](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104346577.png)	

##### 事务线程安全

> spring 的事务管理是线程安全的。
>
> 在相同线程中进行相互嵌套调用的事务方法工作于相同的事务中。
>
> 如果这些相互嵌套调用的方法工作在不同的线程中，不同线程下的事务方法工作在独立的事务中

##### 注解的继承关系和优先级

> 注解并没有继承这种说法，但此处用“继承关系”来形容父类@Transactional和子类方法之间的关系最恰当不过了；
>
> 父类Service声明了@Transactional，子类继承父类，父类的声明的@Transactional会对子类的所有方法进行事务增强；
>
> 如果子类的方法重写了父类的方法并且声明了@Transactional，那么子类的事务声明会优先于父类的事务声明。

##### 注解方法的可见度

> @Transactional 可以作用于接口、接口方法、类以及类方法上。但是 Spring 小组建议不要在接口或者接口方法上使用该注解，因为这只有在使用基于接口的代理时它才会生效。
>
> 但在日常开发中，我们的同一个类的不同方法经常需要用到不同的事务传播方式，所以此时我们会在方法上面声明事务注解，通过代理走AOP的方式令其生效，但需要注意的是不要在 protected、private 或者默认可见性的方法上使用 @Transactional 注解，这将被忽略，也不会抛出任何异常。

![image-20211110104550215](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104550215.png)	

![image-20211110104601650](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104601650.png)	

##### @Transactional属性

> @Transactional注解支持10个属性的设置，可根据实际情况使用，比较常见和重要的是Propagation属性。

![image-20211110104640212](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104640212.png)	

##### Propagation属性详解

> ropagation属性用来枚举事务的传播行为。所谓事务传播行为就是多个事务方法相互调用时，事务如何在这些方法间传播。Spring 支持 7 种事务传播行为，分别如下图所示。

![image-20211110104708431](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104708431.png)	

- REQUIRED是常用的事务传播行为，也是默认的值，如果当前没有事务，就新建一个事务，如果已经存在一个事务中，加入到这个事务中。
- SUPPORTS： 如果业务方法在某个事务范围内被调用,则方法成为该事务的一部分,如果业务方法在事务范围外被调用,则方法在没有事务的环境下执行。
-  MANDATORY： 只能在一个已存在事务中执行,业务方法不能发起自己的事务,如果业务方法在没有事务的环境下调用,就抛异常
-  REQUIRES_NEW：业务方法总是会为自己发起一个新的事务,如果方法已运行在一个事务中,则原有事务被挂起,新的事务被创建,直到方法结束,新事务才结束,原先的事务才会恢复执行.
-  NOT_SUPPORTED： 声明方法需要事务,如果方法没有关联到一个事务,容器不会为它开启事务.如果方法在一个事务中被调用,该事务会被挂起,在方法调用结束后,原先的事务便会恢复执行.
- NEVER： 声明方法绝对不能在事务范围内执行,如果方法在某个事务范围内执行,容器就抛异常.只有没关联到事务,才正常执行.
-  NESTED： 如果一个活动的事务存在,则运行在一个嵌套的事务中.如果没有活动的事务,则按REQUIRED属性执行.它使用了一个单独的事务, 这个事务拥有多个可以回滚的保证点.内部事务回滚不会对外部事务造成影响, 它只DataSourceTransactionManager 事务管理器起效.

##### value属性详解

> value主要用来指定不同的事务管理器；用来满足在同一个系统中，存在不同的事务管理器。比如在Spring中，声明了两种事务管理器txManager1, txManager2。然后，用户可以根据这个参数来根据需要指定特定的txManager。
>
> 什么情况下会存在多个事务管理器的情况呢？ 比如在一个系统中，需要访问多个数据源或者多个数据库，则必然会配置多个事务管理器的

##### 事务处理代码分层例子

> 这里举个例子，帮助大家理解一下理解一下事务的使用，数据流程如下：

- 需要接入外围系统传入HAP的销售订单数据；
- 数据首先要记录到接口表（不管成功失败都要求接口表有调用记录）
- 接口表数据校验成功之后，需要记录到正式表（如果在此过程中发生错误，正式表需要回滚，接口表不需要回滚）

###### 方式1：调用的各个方法，采用事物传播方式REQUIRES+NESTED

![image-20211110104955836](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110104955836.png)	

###### 方式2：采用队列，利用队列进行事务隔离

![image-20211110105023289](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105023289.png)	

#### 代码编写规范

##### Dao层

- Mapper层只允许使用select方法，不允许使用insert和update方法，常见的错误如下

  ![image-20211110105230343](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105230343.png)	

- Mapper层只允许编写多表关联的select方法，不允许自定义update方法，常见的错误如下

  ![image-20211110105242667](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105242667.png)	

- insert和update方法需使用service层的方法，且走AOP

##### Job和Task

- 不允许出现业务逻辑，业务逻辑代码封装在service方法里，Job和Task只做简单的调用；

- 不允许并发的Job或者Task，需要防止并发的情况出现，需要加Redis锁或者注解@DisallowConcurrentExecution；

- 如果有报错，需要setExecutionSummary，注意字段长度不要超长。

  ![image-20211110105336023](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105336023.png)	

##### 队列

- 使用队列进行解耦和提高数据处理效率；
- 默认的队列1个消费者参数为prefetchCount = 3, concurrentConsumers = 1, maxConcurrentConsumers = 1，可根据实际需求改写框架组源代码；
- 队列里不允许出现复杂的业务代码，所有业务代码必须封装在service里，且考虑失败的情况；
- 队列最外层必须try catch包围，避免队列失败一直重复执行消耗系统资源；
- 队列的5个who审计字段不允许断层，常见的做法是将队列的dto和IRequuest放进一个Object里传进队列，队列里再设置一下IRequest和操作dto；
- 设置IRequest的时候需要注意语言环境，不允许跟随服务器语言，一般设置为中文。

##### 开发迁移

- 涉及到多个环境之间的设置迁移，优先考虑Liquibase脚本；
- 如果是手工同步，需要维护迁移清单，记录所有设置项；
- 迁移的内容包括数据库变更脚本（建表脚本、更新脚本）和系统配置（如描述维护、编码、快码、接口配置、菜单配置等）；
- 迁移到生产环境的设置，必须是经过UAT或者QA环境验证的。

##### 接口文档

- 当调试外围系统接口，如Oracle Fusion、WMS等系统时，使用SoapUI或者PostMan进行调试时，需要记录请求报文、正常响应报文、错误响应报文、字段含义，形成一份可供后续技术运维使用和检查报错的接口文档

  ![image-20211110105709626](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105709626.png)	

- 当开放接口给其它系统使用时，需要提供接口文档，记录详细的调用步骤、报文demo、字段信息、每个字段值列表限制等信息。

  ![image-20211110105731829](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105731829.png)	

##### 压力测试

- 当访问并发量较大时，需要做压力测试，根据实际需求设置压测场景；

- 压力测试应该包含测试脚本和测试报告；

- 根据压力测试结果，动态调整服务器参数或者扩容。

- 严格意义上来说，生产环境一旦正式投入使用不允许进行压测，但是在预备上线初期可以进行压测。

  ![image-20211110105846769](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110105846769.png)	

##### 程序发版

- 上线需要记录发版步骤和分支合并，需要有Excel登记发版的历史记录，记录每一个commit id，方便环境版本回退；

- 涉及到发版需要修改的数据库脚本，需要统一登记，明细到天；

- 涉及到数据修复，执行每一个脚本前，需要对影响的数据范围做数据导出备份，明细到天。

  ![image-20211110110017885](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110110017885.png)	

##### 自动部署

- 使用Jenkins或者Gitlab CI等自动部署工具；
- 不要使用人工上传Jar、War包的方式。

#### 数据库规范

> 据库建表，HAP我们需要用表设计脚本统一新建，保持文档最全和最新

##### DB规范

> 表字符集默认使用utf8，必要时候使用utf8mb4

- 通用，无乱码风险，汉字3字节，英文1字节
- utf8mb4是utf8的超集，有存储4字节例如表情符号时，使用它

> 禁止使用存储过程，视图，触发器，Event

- 对数据库性能影响较大，互联网业务，能让站点层和服务层干的事情，不要交到数据库层
- 调试，排错，迁移都比较困难，扩展性较差

> 禁止在数据库中存储大文件，例如照片，可以将大文件存储在对象存储系统，数据库中存储路径
>
> 禁止在线上环境做数据库压力测试
>
> 测试，开发，线上数据库环境必须隔离

##### 命名规范

> 所有的数据库对象命名都是使用XX开头，全部统一大写

| 类型                    |        | 命名规则                                     | 备注                  |
| ----------------------- | ------ | -------------------------------------------- | --------------------- |
| TABLE                   |        | 模块+MEANING, 多语言要_B/_TL                 |                       |
| VIEW                    |        | 模块+Meaning+V                               |                       |
| FUNCTION/PROCEDURE      |        | 模块+Meaning+ACTION(get/fetch/update/insert) | XXPM_CONTRACT_AMT_GET |
| CLUMN+MODEL+MEANING.sql | 增加列 | 用于增量迁移                                 |                       |
| INDEX+MODEL+MEANING.sql | INDEX  | 用于增量迁移                                 |                       |

##### 表设计规范

- 表必须有主键
- 禁止使用外键，如果要保证完整性，应由应用程式实现
  - 外键使得表之间相互耦合，影响update/delete等SQL性能，有可能造成死锁，高并发情况下容易成为数据库瓶颈
- 建议将大字段，访问频度低的字段拆分到单独的表中存储，分离冷热数据
- 表名长度不要超过26个字符，否则Oracle建相关对象可能会报错

##### 列设计规范

> 建表时字段的信息起码需要包含以下几部分：

- 索引字段，比如主键ID、BATCH_ID等；
- 业务字段，比如业务实体ID、单号、单据日期等；
- 数据追溯字段，比如数据从哪里来的来源字段（比如SOURCE_TABLE、SOURCE_TABLE_ID），数据到哪里去的去向字段（比如xx_id保存目标表的主键），逆向数据的正向字段（比如退货订单关联的正向订单ID），子数据的父字段（比如parent_id订单复制自另外一张订单需要记录、订单拆分自父订单也需要记录父订单的ID）
- 数据处理字段，比如PROCESS_GROUP_ID、PROCESS_STATUS、PROCESS_MESSAGE；
- 标准字段，比如版本号字段、who字段、请求字段、弹性域字段等。

##### 建表规范

- 根据业务区分使用tinyint/int/bigint，分别会占用1/4/8字节

- 根据业务区分使用char/varchar

  - 字段长度固定，或者长度近似的业务场景，适合使用char，能够减少碎片，查询性能高
  - 字段长度相差较大，或者更新较少的业务场景，适合使用varchar，能够减少空间

- 存储年使用year，存储日期使用date，存储时间使用datetime

  - datetime使用DEFAULT CURRENT_TIMESTAMP 和 ON UPDATE CURRENT_TIMESTAMP定义默认值和默认更新

- 必须把字段定义为NOT NULL并设默认值

  - NULL的列使用索引，索引统计，值都更加复杂，MySQL更难优化

  - NULL需要更多的存储空间

  - NULL只能采用IS NULL或者IS NOT NULL，而在=/!=/in/not in时有大坑

- 使用INT UNSIGNED存储IPv4，不要用char(15)
- 使用varchar(20)存储手机号，不要使用整数
  - 牵扯到国家代号，可能出现+/-/()等字符，例如+86
  - 手机号不会用来做数学运算
  - varchar可以模糊查询，例如like ‘138%’
- 使用TINYINT来代替ENUM
  - ENUM增加新值要进行DDL操作
- 列名称长度不允许超过30个字符，否则Oracle建相关对象可能会报错

##### 索引规范

- 主键索引名为 表名_pk； 唯一索引名为 表名_u1..5； 普通索引名则为 表名_n1..5。
  - _pk 即 primary key； _u1 即 unique key； _n1 即 index 的简称
- 单张表索引数量建议控制在5个以内
  - 互联网高并发业务，太多索引会影响写性能
  - 生成执行计划时，如果索引太多，会降低性能，并可能导致MySQL选择不到最优索引
  - 异常复杂的查询需求，可以选择ES等更为适合的方式存储
- 组合索引字段数不建议超过5个
  - 如果5个字段还不能极大缩小row范围，八成是设计有问题
- 不建议在频繁更新的字段上建立索引
- 非必要不要进行JOIN查询，如果要进行JOIN查询，被JOIN的字段必须类型相同，并建立索引
  - 因为JOIN字段类型不一致，而导致全表扫描
- 理解组合索引最左前缀原则，避免重复建设索引，如果建立了(a,b,c)，相当于建立了(a), (a,b), (a,b,c)

##### 表设计自检

- 唯一性索引是否有，是否合理
- 字段是否可以为空，为空的理由是什么
- 后缀为_num表示为编码规则生成的代码编号；_code为代码
- 用Tinyint类型时，一般要默认必输，默认值1=启用，比如enabled_flag；性别gender，1男、0女

##### SQL规范

- 禁止使用select *，只获取必要字段

  - select *会增加cpu/io/内存/带宽的消耗

  - 指定字段能有效利用索引覆盖

  - 指定字段查询，在表结构变更时，能保证对应用程序无影响

- insert必须指定字段，禁止使用insert into T values()。指定字段插入，在表结构变更时，能保证对应用程序无影响
- 手写update语句，禁止使用别名（update T t SET t.xx = 1），以免出现数据库不兼容问题
- 手写update语句更新时间，使用CURRENT_TIMESTAMP，不要使用特定数据库方言函数
- 隐式类型转换会使索引失效，导致全表扫描
- 禁止在where条件列使用函数或者表达式， 导致不能命中索引，全表扫描
- 禁止负向查询以及%开头的模糊查询，会导致不能命中索引，全表扫描
- 禁止大表JOIN和子查询
- 同一个字段上的OR必须改写成IN，IN的值必须少于50个
- 应用程序必须捕获SQL异常，方便定位线上问题

### 前端开发规范

## 代码生成器的使用

> 本功能可根据数据表生成相关的dto，mapper， impl，service，controller与html，包含相关的增删改查功能。

### 创建表

> 使用工具导入,或者在mysql数据库中直接运行创建也行

```mysql
-- ----------------------------
-- Table structure for XXAP_INVOICE_ALL
-- ----------------------------
DROP TABLE IF EXISTS `XXAP_INVOICE_ALL`;
CREATE TABLE `XXAP_INVOICE_ALL` (
  `INVOICE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INVOICE_NUM` varchar(50) NOT NULL COMMENT '发票编号',
  `VENDOR_ID` bigint(20) DEFAULT NULL COMMENT '供应商id',
  `VENDOR_NAME` varchar(240) NOT NULL COMMENT '供应商名称',
  `INVOICE_CURRENCY_CODE` varchar(15) DEFAULT NULL COMMENT '发票币种',
  `INVOICE_AMOUNT` bigint(20) DEFAULT 0 COMMENT '发票金额',
  `VENDOR_SITE_ID` varchar(50) DEFAULT NULL COMMENT '供应商地址',
  `VENDOR_SITE_NAME` varchar(240) DEFAULT NULL COMMENT '供应商地址名称',
  `AMOUNT_PAID` bigint(20) DEFAULT 0 COMMENT '付款金额',
  `INVOICE_DATE` date DEFAULT NULL COMMENT '发票日期',
  `INVOICE_TYPE_LOOKUP_CODE` varchar(25) NOT NULL COMMENT '发票类别',
  `DESCRIPTION` varchar(240) DEFAULT NULL COMMENT '发票描述',
  `OBJECT_VERSION_NUMBER` bigint(20) DEFAULT '1',
  `REQUEST_ID` bigint(20) DEFAULT '-1',
  `PROGRAM_ID` bigint(20) DEFAULT '-1',
  `CREATED_BY` bigint(20) DEFAULT '-1',
  `CREATION_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATED_BY` bigint(20) DEFAULT '-1',
  `LAST_UPDATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_LOGIN` bigint(20) DEFAULT '-1',
  `ATTRIBUTE1` varchar(240) DEFAULT NULL,
  `ATTRIBUTE2` varchar(240) DEFAULT NULL,
  `ATTRIBUTE3` varchar(240) DEFAULT NULL,
  `ATTRIBUTE4` varchar(240) DEFAULT NULL,
  `ATTRIBUTE5` varchar(240) DEFAULT NULL,
  `ATTRIBUTE6` varchar(240) DEFAULT NULL,
  `ATTRIBUTE7` varchar(240) DEFAULT NULL,
  `ATTRIBUTE8` varchar(240) DEFAULT NULL,
  `ATTRIBUTE9` varchar(240) DEFAULT NULL,
  `ATTRIBUTE10` varchar(240) DEFAULT NULL,
  `ATTRIBUTE11` varchar(240) DEFAULT NULL,
  `ATTRIBUTE12` varchar(240) DEFAULT NULL,
  `ATTRIBUTE13` varchar(240) DEFAULT NULL,
  `ATTRIBUTE14` varchar(240) DEFAULT NULL,
  `ATTRIBUTE15` varchar(240) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_ID`),
  UNIQUE KEY `XXAP_INVOICE_U1` (`INVOICE_NUM`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;
```

### 使用代码生成器

> HAP中自带的有代码生成器，为了简便开发这里我们使用代码生成器在地址栏中输入：http://localhost:8080/core_war_exploded/generator/generator.html

- 项目路径:   指向本机hap。
- 包父路径:  指向hap的包路径。
- 包路径:  为所要生成的包名。
- 选择表:  选中所要维护的表。
- 文件名:  将自动根据表名生成对应名称（修改dto可选择是否根据dto修改其余文件，也可单独修改其余文件）（ 文件创建之前会判断项目中是否有同名文件 ） 。
- 是否创建:  勾选创建文件。
- 是否覆盖:  勾选则可以覆盖同路径下同名文件。
- 选择ftl模板:  可根据选择的ftl模板生成对应的html页面。

![image-20211112102920048](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211112102920048.png)

点击创建过后，刷新项目 就会生成相应的Dto，Controller，Mapper，MapperXml，Service，Impl，如下图:

![image-20211110155458099](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110155458099.png)	

### 生成的页面

#### 查看生成的HTML页面

> [Hand Application Platform](http://localhost:8080/core_war_exploded/basis/invoice_all.html)

![image-20211110155809787](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110155809787.png)	

![image-20211110160804418](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110160804418.png)

#### 设置时间字段

![image-20211111170859652](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111170859652.png)	

![image-20211111170910849](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111170910849.png)	

#### 处理编译器报错

> 几乎都是对应的jar包下没有对应的package
>
> 后来发现是maven仓库的配置原因, 配置一下maven仓库

![image-20211110161141766](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110161141766.png)![image-20211110161202331](HAP%E6%8A%80%E6%9C%AF%E5%AD%A6%E4%B9%A0.assets/image-20211110161202331.png)

#### 重新导入依赖

![image-20211110163316177](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110163316177.png)

#### 设置Maven仓库

> 上面的问题不是对应的版本中没有对应的包，而是这里没有配置好

![image-20211110165916508](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110165916508.png)	

## 描述维护

### 页面标题维护

> 上面生成的页面的标题需要修改成对应的语言的展示

![image-20211110174606919](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110174606919.png)

![image-20211111151039991](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111151039991.png)

## 模糊查询

> 新创建的功能增删查改都有, 这里做了一个新增查询条件和模糊查询

### 新增查询条件

> 模仿前面的查询条件，复制出来一个并做响应的修改

![image-20211110175227680](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211110175227680.png)

### 模糊查询

> 修改代码将新增的查询条件做成模糊查询

#### 重写对应的service和mapper

![image-20211111105926240](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111105926240.png)	

![image-20211111105947851](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111105947851.png)

![image-20211111110003706](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111110003706.png)	

#### 编写mapper对应的xml文件

> 编写完后重启项目生效

![image-20211111144317222](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211111144317222.png)

```xml
<!-- 自定义的select方法
    1. 重启后生效
    2. 别名需要和类中字段名称一致
-->
<select id="select" parameterType="xxar.basis.dto.InvoiceAll"
        resultType="xxar.basis.dto.InvoiceAll">
    SELECT xia.invoice_id invoiceId
    ,xia.invoice_num invoiceNum
    ,xia.vendor_id vendorId
    ,xia.vendor_name vendorName
    ,xia.invoice_currency_code invoiceCurrencyCode
    ,xia.invoice_amount invoiceAmount
    ,xia.vendor_site_id vendorSiteId
    ,xia.vendor_site_name vendorSiteName
    ,xia.amount_paid amountPaid
    ,xia.invoice_date invoiceDate
    ,xia.invoice_type_lookup_code invoiceTypeLookupCode
    ,xia.description description
    FROM xxap_invoice_all xia
    WHERE 1 = 1
    <if test="vendorName!=null">
        and xia.vendor_name = #{vendorName}
    </if>
    <if test="invoiceCurrencyCode!=null">
        and xia.invoice_currency_code = #{invoiceCurrencyCode}
    </if>
    <if test="vendorSiteName!=null">
        and xia.vendor_site_name = #{vendorSiteName}
    </if>
    <if test="invoiceTypeLookupCode!=null">
        and xia.invoice_type_lookup_code = #{invoiceTypeLookupCode}
    </if>
    <if test="invoiceNum!=null">
        and xia.invoice_num like concat(#{invoiceNum},'%')
    </if>
    order by xia.invoice_num
</select>
```

#### 通过@Condition实现

![image-20211117105559673](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117105559673.png)		

## 头行界面

> 在上面头表的基础上创建行表

### 创建行表

```mysql
-- ----------------------------
-- Table structure for XXAP_INVOICE_LINES_ALL
-- ----------------------------
DROP TABLE IF EXISTS `XXAP_INVOICE_LINES_ALL`;
CREATE TABLE `XXAP_INVOICE_LINES_ALL` (
  `INVOICE_LINE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '发票行id',
  `INVOICE_ID` bigint(20) NOT NULL COMMENT '发票id',
  `INVOICE_LINE_AMOUNT` bigint(20) DEFAULT 0 COMMENT '发票行金额',
  `INVOICE_LINE_NUMBER` int(5) NOT NULL COMMENT '发票行号',
  `DESCRIPTION` varchar(240) DEFAULT NULL COMMENT '发票行描述',
  `LINE_TYPE_LOOKUP_CODE` varchar(25) NOT NULL COMMENT '行类型',
  `TAX_CODE` varchar(240) NOT NULL COMMENT '税码',
  `OBJECT_VERSION_NUMBER` bigint(20) DEFAULT '1',
  `REQUEST_ID` bigint(20) DEFAULT '-1',
  `PROGRAM_ID` bigint(20) DEFAULT '-1',
  `CREATED_BY` bigint(20) DEFAULT '-1',
  `CREATION_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATED_BY` bigint(20) DEFAULT '-1',
  `LAST_UPDATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_LOGIN` bigint(20) DEFAULT '-1',
  `ATTRIBUTE1` varchar(240) DEFAULT NULL,
  `ATTRIBUTE2` varchar(240) DEFAULT NULL,
  `ATTRIBUTE3` varchar(240) DEFAULT NULL,
  `ATTRIBUTE4` varchar(240) DEFAULT NULL,
  `ATTRIBUTE5` varchar(240) DEFAULT NULL,
  `ATTRIBUTE6` varchar(240) DEFAULT NULL,
  `ATTRIBUTE7` varchar(240) DEFAULT NULL,
  `ATTRIBUTE8` varchar(240) DEFAULT NULL,
  `ATTRIBUTE9` varchar(240) DEFAULT NULL,
  `ATTRIBUTE10` varchar(240) DEFAULT NULL,
  `ATTRIBUTE11` varchar(240) DEFAULT NULL,
  `ATTRIBUTE12` varchar(240) DEFAULT NULL,
  `ATTRIBUTE13` varchar(240) DEFAULT NULL,
  `ATTRIBUTE14` varchar(240) DEFAULT NULL,
  `ATTRIBUTE15` varchar(240) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_LINE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;
```

### 页面注册

> 使用页面生成器生成行页面，为了避免每次手工输入url，所以这里先进行[菜单配置](#菜单配置)

### 修改头相关信息

#### 修改头界面

> 为发票号加上链接

```html
{
	field: "invoiceNum",
	title: '<@spring.message "invoiceall.invoicenum"/>',
	width: 120,
	template: function(rowdata) {
		return '<a id = "' +rowdata.invoiceId + '" style = "color: #240fff" href="javascript:void(0);" onclick="detailInfo(this)">' + rowdata.invoiceNum + '</a>'
	}
},
```

```javascript
// 打开新的TAB页
function detailInfo(e) {
	var id = e.getAttribute("id");
	var param1 = "XXAP_INVOICE" + id;
	var param2 = "应付发票修改界面"+;
	var param3 = "${base.contextPath}/basis/ap_invoice_edit.html?invoiceId=" + id;
	// $('#' + id).attr('href', param3);
	parent.openTab(param1, param2, param3);
}
```

#### 修改controller

> 添加新的方法

```java
@Autowired
private IInvoiceLinesAllService lineService;

// 行界面中获取头信息
@RequestMapping(value = "/xxap/invoice/edit/query")
@ResponseBody
public ResponseData editApInvoice(@RequestBody InvoiceAll dto, HttpServletRequest request) {
	IRequest requestContext = createRequestContext(request);
	return new ResponseData(service.editApInvoice(requestContext, dto));
}

// 更新头(在更新行界面时也可以更新头界面)
@RequestMapping(value = "/xxap/invoice/update")
@ResponseBody
public void updateInvoice(@RequestBody InvoiceAll dto, HttpServletRequest request) {
	System.out.println("request InvoiceAmount:" + dto.getInvoiceAmount());
	IRequest requestContext = createRequestContext(request);
	InvoiceAll invoice = service.updateByPrimaryKey(requestContext, dto);
	System.out.println("InvoiceId: " + invoice.getInvoiceId());
	System.out.println("InvoiceAmount:" + invoice.getInvoiceAmount());
}

// 修改删除头的方法，加入删除行信息的代码
@RequestMapping(value = "/xxap/invoice/all/remove")
@ResponseBody
public ResponseData delete(HttpServletRequest request, @RequestBody List<InvoiceAll> dto) {
	service.batchDelete(dto);
	// 删除对应的行
	for (InvoiceAll invoice : dto) {
		List<InvoiceLinesAll> lists = service.selectInvoiceLinesALl(invoice);
		System.out.println("count:" + lists.size());
		lineService.batchDelete(lists);
	}
	return new ResponseData();
}
```

#### 修改service

> 添加新的方法

```java
// 获取头信息
List<InvoiceAll> editApInvoice(IRequest requestContext, InvoiceAll dto);

/**
 * 查询发票下面的所有发票行
 *
 * @param dto 发票
 * @return 发票行
 */
List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto);
```

#### 修改serviceImpl

> 添加方法并且在mapper对应的xml文件中加入对invoiceId的查询

```java
// 获取头信息
@Override
public List<InvoiceAll> editApInvoice(IRequest requestContext, InvoiceAll dto) {
	return mapper.select(dto);
}

/**
 * 查询发票下面的所有发票行
 *
 * @param dto 发票
 * @return 发票行
 */
@Override
public List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto) {
	return mapper.selectInvoiceLinesALl(dto);
}
```

#### 修改mapper

```java
/**
 * 查询发票下面的所有发票行
 *
 * @param dto 发票
 * @return 发票行
 */
List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto);
```

#### 修改mapper.xml

```xml
<select id="selectInvoiceLinesALl" parameterType="xxap.basis.dto.InvoiceAll"
		resultType="xxap.basis.dto.InvoiceLinesAll">
	SELECT xilb.invoice_line_id       invoiceLineId
		 , xilb.invoice_id            invoiceId
		 , xilb.invoice_line_amount   invoiceLineAmount
		 , xilb.invoice_line_number   invoiceLineNumber
		 , xilb.description           description
		 , xilb.line_type_lookup_code lineTypeLookupCode
		 , xilb.tax_code              taxCode
	FROM xxap_invoice_lines_b xilb
	where 1 = 1
	  and xilb.invoice_id = #{invoiceId}
</select>
```

### 修改行信息

#### 添加头信息

```html
<!-- 添加头信息 -->
<div id="header">
	<div class="panel-body">
		<form class="form-horizontal" role="form">
			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="col-sm-4 control-label">
							发票编号
						</label>
						<div class="col-sm-8">
							<input type="text" class="k-textbox" id="invoiceId"
								   style="width: 100%;background-color:#f0f0f0;"
								   data-bind="value:parentModel.invoiceId" readonly="readonly">
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="col-sm-4 control-label">
							发票号
						</label>
						<div class="col-sm-8">
							<input type="text" class="k-textbox" id="invoiceNum"
								   style="width: 100%;background-color:#f0f0f0;"
								   data-bind="value:parentModel.invoiceNum" readonly="readonly">
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="col-sm-4 control-label">
							发票金额
						</label>
						<div class="col-sm-8">
							<input type="text" class="k-textbox" id="invoiceAmount"
								   style="width: 100%;background-color:#f0f0f0;"
								   data-bind="value:parentModel.invoiceAmount" readonly="readonly">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>kendo.bind($('#header'), viewModel);</script>
```

#### 获取头信息

```javascript
<script type="text/javascript">
    var viewModel = Hap.createGridViewModel("#grid");
    // 获取参数
    var paramApInvoiceId = '${RequestParameters.invoiceId!}';
    // 参数有值
    if (paramApInvoiceId) {
        // 创建一个新的键来存储头信息
        viewModel.set("parentModel", {});
        $.ajax({
            type: 'post',
            url: "${base.contextPath}/xxap/invoice/edit/query",
            data: kendo.stringify({"invoiceId": paramApInvoiceId}),
            dataType: "json",
            contentType: "application/json",
            success: function (args) {
                var a0 = args.rows[0] || {};
                for (var k in a0) {
                    viewModel.parentModel.set(k, a0[k]);
                }
            }
        });
    }
</script>
```

#### 修改头信息

> 在保存按钮上假如onclick触发方法

![image-20211117173111460](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117173111460.png)

```javascript
function updateInvoice(e) {
	console.log("更新头");
	var invoiceAmount = $("#invoiceAmount").val();
	viewModel.parentModel.set("invoiceAmount",invoiceAmount);
	$.ajax({
		type: 'post',
		url: "${base.contextPath}/xxap/invoice/update",
		// data: kendo.stringify({"invoiceId": paramApInvoiceId,"invoiceAmount": invoiceAmount}),
		data: kendo.stringify(viewModel.parentModel),
		dataType: "json",
		contentType: "application/json"
	});
}
```

#### 添加参数

![image-20211112170738469](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211112170738469.png)	

```javascript
parameterMap: function (options, type) {
                if (type !== "read" && options.models) {
                    var datas = Hap.prepareSubmitParameter(options, type)
                    return kendo.stringify(datas);
                } else if (type === "read") {
                    var param = Hap.prepareQueryParameter(viewModel.model.toJSON(), options);
                    if (paramApInvoiceId) {
                        param.invoiceId = paramApInvoiceId;
                    }
                    return param
                }
            }
```

#### 注释掉一些不用的信息

> 注释查询

![image-20211112170710839](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211112170710839.png)	

![image-20211112170723873](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211112170723873.png)	

### 效果

> 这里注意启用一下标签页
>
> ![image-20211117173616935](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117173616935.png)	

![image-20211117173529236](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117173529236.png)

![image-20211117173543094](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117173543094.png)

## 菜单配置

> hap提供功能管理的服务，用户可以将url页面挂到菜单栏上，这样就不需要每次手工单独输入url网址；除此之外，还可以实现权限控制，对没有配置该菜单权限的用户实行屏蔽

### 资源管理

> 系统管理 -> 功能管理 -> 资源管理
>
> 新增一个HTML资源，查询应付发票头界面

![image-20211117142730630](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117142730630.png)

### 功能维护

> 系统管理 -> 功能管理 -> 功能维护

![image-20211117142911615](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117142911615.png)

> 应付管理是菜单不需要维护功能资源，所以这里需要维护应付发票查询功能的功能资源

![image-20211117143052268](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117143052268.png)

### 功能分配

> 系统管理 -> 功能管理 -> 功能分配

![image-20211117143214558](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211117143214558.png)	

## 多语言

### 界面多语言

> 这里以新增的页面为例，在描述中新增英文展示

#### 描述中维护多语言

![image-20211115111704423](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211115111704423.png)

#### 修改首选项

> 设置为英文

![image-20211115111734167](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211115111734167.png)	

#### 查看结果

![image-20211115111800786](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211115111800786.png)

### 数据库多语言

> 多语言的实现需要基于 _B结尾的基表和 _TL结尾的多语言表，我们之前建立的XXAP_INVOICE_LINES_ALL不符合要求，所以，需要新建相应的表对象；这里我不再对XXAP_INVOICE_LINES_ALL结构进行修改，直接新建XXAP_INVOICE_LINES_B和XXAP_INVOICE_LINES_TL两张表。

#### 创建_B表和 _TL表

> _b表和原来的表一样只是表名不同， _tl表保留invoice_line_id,who字段和弹性域字段以及可能会有多语言的字段，最后新增lang字段

```mysql
-- ----------------------------
-- Table structure for XXAP_INVOICE_LINES_B
-- ----------------------------
DROP TABLE IF EXISTS `XXAP_INVOICE_LINES_B`;
CREATE TABLE `XXAP_INVOICE_LINES_B` (
  `INVOICE_LINE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '发票行id',
  `INVOICE_ID` bigint(20) NOT NULL COMMENT '发票id',
  `INVOICE_LINE_AMOUNT` bigint(20) DEFAULT 0 COMMENT '发票行金额',
  `INVOICE_LINE_NUMBER` int(5) NOT NULL COMMENT '发票行号',
  `DESCRIPTION` varchar(240) DEFAULT NULL COMMENT '发票行描述',
  `LINE_TYPE_LOOKUP_CODE` varchar(25) NOT NULL COMMENT '行类型',
  `TAX_CODE` varchar(240) NOT NULL COMMENT '税码',
  `OBJECT_VERSION_NUMBER` bigint(20) DEFAULT '1',
  `REQUEST_ID` bigint(20) DEFAULT '-1',
  `PROGRAM_ID` bigint(20) DEFAULT '-1',
  `CREATED_BY` bigint(20) DEFAULT '-1',
  `CREATION_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATED_BY` bigint(20) DEFAULT '-1',
  `LAST_UPDATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_LOGIN` bigint(20) DEFAULT '-1',
  `ATTRIBUTE1` varchar(240) DEFAULT NULL,
  `ATTRIBUTE2` varchar(240) DEFAULT NULL,
  `ATTRIBUTE3` varchar(240) DEFAULT NULL,
  `ATTRIBUTE4` varchar(240) DEFAULT NULL,
  `ATTRIBUTE5` varchar(240) DEFAULT NULL,
  `ATTRIBUTE6` varchar(240) DEFAULT NULL,
  `ATTRIBUTE7` varchar(240) DEFAULT NULL,
  `ATTRIBUTE8` varchar(240) DEFAULT NULL,
  `ATTRIBUTE9` varchar(240) DEFAULT NULL,
  `ATTRIBUTE10` varchar(240) DEFAULT NULL,
  `ATTRIBUTE11` varchar(240) DEFAULT NULL,
  `ATTRIBUTE12` varchar(240) DEFAULT NULL,
  `ATTRIBUTE13` varchar(240) DEFAULT NULL,
  `ATTRIBUTE14` varchar(240) DEFAULT NULL,
  `ATTRIBUTE15` varchar(240) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_LINE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for XXAP_INVOICE_LINES_TL
-- ----------------------------
DROP TABLE IF EXISTS `XXAP_INVOICE_LINES_TL`;
CREATE TABLE `XXAP_INVOICE_LINES_TL` (
  `INVOICE_LINE_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(240) DEFAULT NULL COMMENT '发票行描述',
  `LANG` varchar(10) NOT NULL,
  `OBJECT_VERSION_NUMBER` bigint(20) DEFAULT '1',
  `REQUEST_ID` bigint(20) DEFAULT '-1',
  `PROGRAM_ID` bigint(20) DEFAULT '-1',
  `CREATED_BY` bigint(20) DEFAULT '-1',
  `CREATION_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATED_BY` bigint(20) DEFAULT '-1',
  `LAST_UPDATE_DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_LOGIN` bigint(20) DEFAULT '-1',
  `ATTRIBUTE1` varchar(240) DEFAULT NULL,
  `ATTRIBUTE2` varchar(240) DEFAULT NULL,
  `ATTRIBUTE3` varchar(240) DEFAULT NULL,
  `ATTRIBUTE4` varchar(240) DEFAULT NULL,
  `ATTRIBUTE5` varchar(240) DEFAULT NULL,
  `ATTRIBUTE6` varchar(240) DEFAULT NULL,
  `ATTRIBUTE7` varchar(240) DEFAULT NULL,
  `ATTRIBUTE8` varchar(240) DEFAULT NULL,
  `ATTRIBUTE9` varchar(240) DEFAULT NULL,
  `ATTRIBUTE10` varchar(240) DEFAULT NULL,
  `ATTRIBUTE11` varchar(240) DEFAULT NULL,
  `ATTRIBUTE12` varchar(240) DEFAULT NULL,
  `ATTRIBUTE13` varchar(240) DEFAULT NULL,
  `ATTRIBUTE14` varchar(240) DEFAULT NULL,
  `ATTRIBUTE15` varchar(240) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

#### 修改DTO

> 修改表名为xxap_invoice_lines_b，并添加多语言注解：@MultiLanguage和@MultiLanguageField；

![image-20211116160112521](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116160112521.png)	

![image-20211116160122601](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116160122601.png)	

#### 修改html界面

![image-20211116161449379](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116161449379.png)	

```html
{
	field: "description",
	title: '<@spring.message "invoicelinesall.description"/>',
	width: 120,
	editor: function(container, options) {
		$('<input name="' + options.field + '"/>').appendTo(container).kendoTLEdit({
			idField: 'INVOICE_LINE_ID',
			field: 'description',
			dto: 'xxap.basis.dto.InvoiceLinesAll',
			model: options.model
		});
	}
}
```

#### 效果

![image-20211116161335714](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116161335714.png)	

## LOV的定义和使用

> 为了LOV的数据源, 我先创建一个快码，中英文都维护

![image-20211116095857330](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116095857330.png)	![image-20211116095918888](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116095918888.png)	

### LOV定义

> 使用自定义sql
>

![image-20211124174546237](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124174546237.png)

```mysql
SELECT scvb.value t_value
      ,scvt.meaning t_meaning
      ,scvt.description t_description
FROM   sys_code_b        scb
      ,sys_code_value_b  scvb
      ,sys_code_value_tl scvt
WHERE  1 = 1
AND    scb.code_id = scvb.code_id
AND    scvb.code_value_id = scvt.code_value_id
AND    scb.code = 'XXAP.INVOICE_LINE_TYPE'
# 使用当前语言查询
AND    scvt.lang =  #{request.locale,jdbcType=VARCHAR,javaType=java.lang.String} 
<if test="meaning !=null">
AND    scvt.meaning like concat("%",#{meaning},"%")
</if>
<if test="description !=null">
AND    scvt.description like concat("%",#{description},"%")
</if>
```

### 表单元素的含义介绍

![image-20211124175653462](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124175653462.png)

- 代码：Lov代码，对应LOV的ViewModel中的code字段；
- 描述：Lov描述，对应description字段；
- 标题：Lov弹出窗口的标题，对应title字段；
- 提示：Lov输入框的提示信息，对应placeholder字段；
- ValueField：值域，此处的字段名应该使用驼峰命名法；
- 可编辑：Lov输入框是否可以编辑，默认为否，对应ViewModel中的editableFlag字段（数据库中为EDITABLE）。当编辑状态为“否”时，则Lov的输入框不能进行输入，只能点击后面的查询按钮来进行相应操作；
- 高度：LOV弹出窗口的高度；
- 宽度：LOV弹出窗口的宽度；
- 列数：Lov弹出窗口中，查询字段输入框布局的列数。对应queryColumns字段，值应该大于0并小于等于4，其他值无效，大于4的默认为4，小于等于0的默认为1； 高度：LOV弹出窗口的高度；
- TextFiled：文本域，即Lov输入框最终显示的值所对应的字段，此处的字段名应该使用驼峰命名法；
- Sql类型：LOV用来查询数据的SQL语句的类型；分为Sql Id、自定义Sql和url三种类型；sqlId类型即**Mapper.xml文件中对应的sql操作的ID；自定义sql即自己定义的sql语句；url对应一个可以查询出数据的url，例如controller中的某个url路径。
- Sql Id/自定义Sql/URL：根据“Sql类型”的选择来判断此处的表单元素类型；

![image-20211124175726180](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124175726180.png)

- 显示：描述字段;
- 字段名：查询字段名；
- 列宽度：LOV弹出窗口里值列表中列的宽度
- 表格列：是否是LOV弹出窗口里值列表中的列
- 列对齐方式：：LOV弹出窗口里值列表中列的对齐方式
- 查询字段：是否是LOV弹出窗口里可进行查询的字段
- 列序号：LOV弹出窗口里值列表中列的排列序号
- 查询配置：
  - 字段类型：查询字段的类型；
  - 查询描述宽度：查询字段的label的宽度，基数为12，图中的值
  - 为3，则代表label占该行的3/12，即1/4；为0时代表使用系统默
  - 认的宽度，若已经设置需要取消,可将其值设为0；
  - 查询字段宽度：查询字段的input框的宽度，基数为12，其值的
  - 意义与描述宽度相同；同一行中各个字段的描述宽度与字段宽度
  - 总和不得超过12，否则会换行或者超过部分无效。
  - 查询字段序号：查询字段的排列序号

### 创建lov规范

> 提示：当lov的可编辑被勾选后，则lov的input框允许输入数据，此时lov的input会生成成为autoComplete框，当有数据输入时，lov会根据输入的数据查询数据库中是否包含了输入字段的数据，如果有，则以下拉的列表的形式展示其中10条，此时可对数据进行选择；如果没有选择，那么此时input框中的数据有两种可能：该数据在数据库中存在或者不存在；当输入数据不存在于数据库中时，lov的value和text都会被设置为输入的数据。
>
> 例如：
>
> - 打开LOV_RESOURCE的可编辑，输入用户，lov会提示包含了‘用户’两字的前10条数据；.
>
>   ![image-20211124175152642](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124175152642.png)	
>
> - 选择第三条"删除用户"，用控制台查看lov的value和text，分别是lov定义时的valueField的值和textFiled的值
>
>   ![image-20211124175405499](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124175405499.png)
>
> - 输入“删除”，不选择提示中的数据，数据库中此时没有这条数据；用控制台查看lov的value和text，它们的值都是输入的数据
>
>   ![image-20211124175607176](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124175607176.png)

- 凡是需要使用到数据库中查出数据的字段名的地方，其字段名的命名方式应采用驼峰命名的方式，本页面中有三处地方使用到字段名：textField表单、valueFiled表单以及grid中的“字段名”；
- 使用自定义sql的方式来获取数据时，字段名与数据库中的相对应，sql语句的末尾不能加分号

### grid表格字段使用LOV

> 在发票界面中的发票行类型引用上面定义的LOV

![image-20211116100808991](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116100808991.png)	

```html
{
	field: "lineTypeLookupCode",
	title: '<@spring.message "invoicelinesall.linetypelookupcode"/>',
	width: 120,
	editor: function (container, options) {
		$('<input name="' + options.field + '"/>')
			.appendTo(container)
			.kendoLov($.extend(<@lov "XXAP_LOV_INVOICE_LINE_TYPE"/>,{
			model    : options.model,
			textField: 'tValue',
			select: function(e) {
				console.log(e.item);
				options.model.set('lineTypeLookupCode', e.item.tValue);
			}
		}));
	}
}
```

> 效果展示

![image-20211116101202854](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116101202854.png)	

![image-20211116101219443](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116101219443.png)

### 查询字段使用lov

> 按照上述步骤新建发票类型LOV以便在查询发票时使用

#### 界面调整

> 加上LOV发现输入框显示异常

![image-20211116105942003](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116105942003.png)		

![image-20211116110044040](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116110044040.png)

![image-20211116110125871](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116110125871.png)		

> 添加lov代码

```html
<script>
	// lov
	$("#invoiceTypeLookupCode").kendoLov(<@lov "XXAP_LOV_INVOICE_TYPE" />);
</script>
```

![image-20211116150212112](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116150212112.png)	

### ValueField和TextFiled

> 将发票类型的TextField修改成描述

![image-20211116150810563](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116150810563.png)

> 查看LOV变化，返回值是TextField对应的值，用于查询的值(实际值)是ValueField对应的值

![image-20211116150925358](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211116150925358.png)

## Mybatis的基本操作和开发方法

### Mybatis的开发

> HAP中调用是通过service 调用 mapper, 所以我们除了在controller中添加新的方法，还要修改对应的serivice、serviceImpl和mapper以及对应的xml文件。
>
> 以添加查询发票头的所有行为例

#### 查询所有行

> 找到对应的mapper添加方法，方法添加完之后再文件中添加对应的方法

```java
List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto);
```

![image-20211126142910152](https://gitee.com/image_bed/image-bed1/raw/master/img/202111261429413.png)

> 修改生成的方法，到此mapper层的修改就结束了

```xml
<select id="selectInvoiceLinesALl" parameterType="xxap.basis.dto.InvoiceAll"
		resultType="xxap.basis.dto.InvoiceLinesAll">
	SELECT xilb.invoice_line_id       invoiceLineId
		 , xilb.invoice_id            invoiceId
		 , xilb.invoice_line_amount   invoiceLineAmount
		 , xilb.invoice_line_number   invoiceLineNumber
		 , xilb.description           description
		 , xilb.line_type_lookup_code lineTypeLookupCode
		 , xilb.tax_code              taxCode
	FROM xxap_invoice_lines_b xilb
	where 1 = 1
	  and xilb.invoice_id = #{invoiceId}
</select>
```

> 之后就是在对应的controller，service,serviceImpl中补充对应的方法以便前端调用

##### IInvoiceAllService

```java
List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto);
```

##### InvoiceAllServiceImpl

> 这里需要加上InvoiceAllMapper，否则继承而来的mapper没有selectInvoiceLinesALl方法

```java
@Autowired
protected InvoiceAllMapper mapper;

public List<InvoiceLinesAll> selectInvoiceLinesALl(InvoiceAll dto) {
        return mapper.selectInvoiceLinesALl(dto);
    }
```

##### InvoiceAllController

> 修改删除操作，在删除头的时候也删除行

```java
@Autowired
private IInvoiceLinesAllService lineService;

@RequestMapping(value = "/xxap/invoice/all/remove")
@ResponseBody
public ResponseData delete(HttpServletRequest request, @RequestBody List<InvoiceAll> dto) {
	service.batchDelete(dto);
	// 删除对应的行
	for (InvoiceAll invoice : dto) {
		List<InvoiceLinesAll> lists = service.selectInvoiceLinesALl(invoice);
		System.out.println("count:" + lists.size());
		// 查看行数据
		for(InvoiceLinesAll line:lists) {
			System.out.println(line.getInvoiceLineId());
		}
		lineService.batchDelete(lists);
	}
	return new ResponseData();
}
```

> 至此修改就结束了

### Mybatis基本操作

##### 自动化CRUD

> Mapper 接口继承 通用 Mapper< T >，可以自动完成基本的增删改查操作

```java
public interface InvoiceAllMapper extends Mapper<InvoiceAll> {}
```

##### 动态sql

> MyBatis 可以用 一些内置的标签来动态的组织 SQL 语句，常用的标签有

- if 当 test 语句通过时，if 内包含的 SQL 会起作用

```mysql
WHERE 1 = 1
<if test="invoiceId!=null">
	and xia.invoice_id = #{invoiceId}
</if>
```

- where 以 where 开头，将一组标签的返回值拼接起来，自动去除首尾多余的  AND   OR  专门用于生成动态 WHERE 条件

```mysql
<where>
<if test="invoiceId!=null">
  xia.invoice_id = #{invoiceId}
</if>
</where>
```

- set 以 set 开头，将一组标签的返回值拼接起来，自动去除首尾多余的逗号  ,  专门用于生成动态的 set 语句(update)

```mysql
<update id="updateInvoice">
  update xxap_invoice_all
    <set>
      <if test="invoice_amount != null">invoice_amount=#{invoiceAmount},</if>
      <if test="invoice_currency_code != null">invoice_currency_code=#{invoiceCurrencyCode},</if>
    </set>
  where invoice_id=#{invoiceId}
</update>
```

- foreach：对集合进行遍历（尤其是在构建 IN 条件语句的时候）

```mysql
<select id="selectPostIn" resultType="domain.blog.Post">
  SELECT *
  FROM POST P
  WHERE ID in
  <foreach item="item" index="index" collection="list"
      open="(" separator="," close=")">
        #{item}
  </foreach>
</select>
```

## KendoUI组件的使用

> 详细的标签使用可以参考对应的标准文档
>
> [标准文档](https://docs.telerik.com/kendo-ui/api/javascript/ui/datepicker/events/change)

### Button

> [按钮](https://demos.telerik.com/kendo-ui/button/index?_ga=2.28409404.1033050038.1637635441-1155103513.1636618525)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <div>
                <h4>Basic Button</h4>
                <p>
                    <button id="primaryTextButton" class="k-primary">Primary Button</button>
                    <button id="textButton">Button</button>
                </p>
            </div>
            <div>
                <h4>Disabled buttons</h4>
                <p>
                    <a id="primaryDisabledButton" class="k-primary">Disabled Primary Button</a>
                    <a id="disabledButton">Disabled Button</a>
                </p>
            </div>
            <div>
                <h4>Buttons with icons</h4>
                <p>
                    <a id="iconTextButton">Filter</a>
                    <a id="kendoIconTextButton">Clear Filter</a>
                    <em id="iconButton"></em>
                </p>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#primaryTextButton").kendoButton();
                $("#textButton").kendoButton();
                $("#primaryDisabledButton").kendoButton({
                    enable: false
                });
                $("#disabledButton").kendoButton({
                    enable: false
                });
                $("#iconTextButton").kendoButton({
                    icon: "filter"
                });
                $("#kendoIconTextButton").kendoButton({
                    icon: "filter-clear"
                });
                $("#iconButton").kendoButton({
                    icon: "refresh"
                });
                // image
                $("#iconButton2").kendoButton({
                    spriteCssClass: "k-icon netherlandsFlag"
                });
                $("#kendoIconButton").kendoButton({
                    icon: "filter"
                });
            });
        </script>
        <style>
            .demo-section p {
                margin: 0 0 30px;
                line-height: 50px;
            }

            .demo-section p .k-button {
                margin: 0 10px 0 0;
            }

            .k-primary {
                min-width: 150px;
            }
        </style>
    </div>
</div>
</body>
</html>

```

### CheckBox

> [复选框](https://demos.telerik.com/kendo-ui/checkbox/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <h4>Choose Extra Equipment</h4>
            <ul class="fieldlist">
                <li>
                    <input type="checkbox" id="eq1" class="k-checkbox" checked="checked">
                    <label class="k-checkbox-label" for="eq1">Rear side airbags</label>
                </li>
                <li>
                    <input type="checkbox" id="eq2" class="k-checkbox" checked="checked" disabled="disabled">
                    <label class="k-checkbox-label" for="eq2">Leather trim</label>
                </li>
                <li>
                    <input type="checkbox" id="eq3" class="k-checkbox">
                    <label class="k-checkbox-label" for="eq3">Luggage compartment cover</label>
                </li>
                <li>
                    <input type="checkbox" id="eq4" class="k-checkbox">
                    <label class="k-checkbox-label" for="eq4">Heated front and rear seats</label>
                </li>
                <li>
                    <input type="checkbox" id="eq5" class="k-checkbox">
                    <label class="k-checkbox-label" for="eq5">Dual-zone air conditioning</label>
                </li>
                <li>
                    <input type="checkbox" id="eq6" class="k-checkbox">
                    <label class="k-checkbox-label" for="eq6">Rain sensor</label>
                </li>
                <li>
                    <input type="checkbox" id="eq7" class="k-checkbox" disabled="disabled">
                    <label class="k-checkbox-label" for="eq7">Towbar preparation</label>
                </li>
            </ul>
        </div>
        <style>
            .fieldlist {
                margin: 0 0 -1em;
                padding: 0;
            }

            .fieldlist li {
                list-style: none;
                padding-bottom: 1em;
            }
        </style>
    </div>
</div>
</body>
</html>
```

### ComboBox

> [下拉列表](https://demos.telerik.com/kendo-ui/combobox/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example" role="application">
        <div id="tshirt-view" class="demo-section k-content">
            <h4>Customize your Kendo T-shirt</h4>
            <img id="tshirt" alt="T-shirt image" src="../../resources/images/tshirt.png"/>
            <h4><label for="fabric">T-shirt Fabric</label></h4>
            <input id="fabric" placeholder="Select fabric..." style="width: 100%;"/>

            <h4 style="margin-top: 2em;"><label for="size">T-shirt Size</label></h4>
            <select id="size" placeholder="Select size..." style="width: 100%;">
                <option>X-Small</option>
                <option>Small</option>
                <option>Medium</option>
                <option>Large</option>
                <option>X-Large</option>
                <option>2X-Large</option>
            </select>

            <button class="k-button k-primary" id="get" style="margin-top: 2em; float: right;">Customize</button>
        </div>
        <style>
            #tshirt {
                display: block;
                margin: 2em auto;
            }

            .k-readonly {
                color: gray;
            }
        </style>
        <script>
            $(document).ready(function () {
                // create ComboBox from input HTML element
                $("#fabric").kendoComboBox({
                    dataTextField: "text",
                    dataValueField: "value",
                    dataSource: [
                        {text: "Cotton", value: "1"},
                        {text: "Polyester", value: "2"},
                        {text: "Cotton/Polyester", value: "3"},
                        {text: "Rib Knit", value: "4"}
                    ],
                    filter: "contains",
                    suggest: true,
                    index: 3
                });

                // create ComboBox from select HTML element
                $("#size").kendoComboBox(); // 这个如果不加, 下面select.value()会报错

                var fabric = $("#fabric").data("kendoComboBox");
                var select = $("#size").data("kendoComboBox");

                $("#get").click(function () {
                    alert('Thank you! Your Choice is:\n\nFabric ID: ' + fabric.value() + ' and Size: ' + select.value());
                });
            });
        </script>
    </div>
</div>
</body>
</html>
```

### DataSource

> [数据源](https://demos.telerik.com/kendo-ui/datasource/local-operations)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example" class="k-content">

        <div class="demo-section k-content wide">
            <div id="products"></div>
        </div>

        <script type="text/x-kendo-template" id="template">
            <div class="product">
                <img src="../../resources/images/foods/#= ProductID #.jpg" alt="#: ProductName # image"/>
                <h3>#:ProductName#</h3>
                <p>#:kendo.toString(UnitPrice, "c")#</p>
            </div>
        </script>

        <script>
            $(function () {
                var template = kendo.template($("#template").html());

                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: "https://demos.telerik.com/kendo-ui/service/Products",
                            dataType: "jsonp"
                        }
                    },
                    requestStart: function () {
                        kendo.ui.progress($("#products"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("#products"), false);
                    },
                    change: function () {
                        $("#products").html(kendo.render(template, this.view()));
                    }
                });
                dataSource.read();
            });
        </script>

        <style>
            #products {
                padding: 10px;
                margin-bottom: -1px;
                /*            height: 510px;
                            overflow: auto;*/
            }

            .product {
                float: left;
                position: relative;
                width: 111px;
                height: 170px;
                margin: 0;
                padding: 0;
            }

            .product img {
                width: 110px;
                height: 110px;
            }

            .product h3 {
                margin: 0;
                padding: 3px 5px 0 0;
                max-width: 96px;
                overflow: hidden;
                line-height: 1.1em;
                font-size: .9em;
                font-weight: normal;
                text-transform: uppercase;
                color: #999;
            }

            .product p {
                visibility: hidden;
            }

            .product:hover p {
                visibility: visible;
                position: absolute;
                width: 110px;
                height: 110px;
                top: 0;
                margin: 0;
                padding: 0;
                line-height: 110px;
                vertical-align: middle;
                text-align: center;
                color: #fff;
                background-color: rgba(0, 0, 0, 0.75);
                transition: background .2s linear, color .2s linear;
                -moz-transition: background .2s linear, color .2s linear;
                -webkit-transition: background .2s linear, color .2s linear;
                -o-transition: background .2s linear, color .2s linear;
            }

            .k-listview:after {
                content: ".";
                display: block;
                height: 0;
                clear: both;
                visibility: hidden;
            }
        </style>
    </div>
</body>
</html>
```

> 由于这个测试案例中涉及到的图片有点多，我这里使用python下了一下

```python
# 下载图片
from urllib.request import urlretrieve

for n in range(1, 78):
    imageLocation = "https://demos.telerik.com/kendo-ui/content/web/foods/{}.jpg".format(n)
    imageName = "{}.jpg".format(n)
    urlretrieve(imageLocation, imageName)
```

### DatePicker

> [DatePicker](https://demos.telerik.com/kendo-ui/datepicker/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">

            <h4>Show e-mails from:</h4>
            <input id="datepicker" value="10/10/2011" title="datepicker" style="width: 100%"/>

            <h4 style="margin-top: 2em;">Add to archive mail from:</h4>
            <input id="monthpicker" value="November 2011" title="monthpicker" style="width: 100%"/>
        </div>
        <script>
            $(document).ready(function () {
                // create DatePicker from input HTML element
                $("#datepicker").kendoDatePicker();

                $("#monthpicker").kendoDatePicker({
                    // defines the start view
                    start: "year",
                    // defines when the calendar should return date
                    depth: "year",
                    // display month and year in the input
                    format: "MMMM yyyy",
                    // specifies that DateInput is used for masking the input element
                    dateInput: true
                });
            });
        </script>
    </div>
</div>
</body>
</html>
```

### DataTimePicker

> [DateTimePicker](https://demos.telerik.com/kendo-ui/datetimepicker/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <h4>Remind me on</h4>
            <input id="datetimepicker" title="datetimepicker" style="width: 100%;"/>
        </div>
        <script>
            $(document).ready(function () {
                // create DateTimePicker from input HTML element
                $("#datetimepicker").kendoDateTimePicker({
                    value: new Date(),
                    dateInput: true
                });
            });
        </script>
    </div>
</div>
</body>
</html>
```

### DropDownList

> [下拉列表](https://demos.telerik.com/kendo-ui/dropdownlist/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <h4>Categories:</h4>
            <input id="categories" style="width: 100%;"/>

            <h4 style="margin-top: 2em;">Products:</h4>
            <input id="products" disabled="disabled" style="width: 100%;"/>

            <h4 style="margin-top: 2em;">Products:</h4>
            <input id="shipTo" disabled="disabled" style="width: 100%;"/>

            <script>
                $(document).ready(function () {
                    var categories = $("#categories").kendoDropDownList({
                        optionLabel: "Select category...",
                        dataTextField: "CategoryName",
                        dataValueField: "CategoryID",
                        height: 310,
                        template: '<span class="k-state-default" style="background-image: url(\'../../resources/images/dropdownlist/#:data.CategoryID#.jpg\')" ></span><span class=\"k-state-default\" style=\"padding-left: 15px;\"><h3>#: data.CategoryName #</h3></span>',
                        valueTemplate: '<span class="selected-value" style="background-image: url(\'../../resources/images/dropdownlist/#:data.CategoryID#.jpg\')"></span><span>#:data.CategoryName#</span>',
                        footerTemplate: 'Total number of <strong>#: instance.dataSource.total() #</strong> categories found',
                        dataSource: {
                            type: "odata",
                            serverFiltering: true,
                            transport: {
                                read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories"
                            },
                            change: function () {
                                console.log("categories: ");
                                console.log(this.view())
                            }
                        }
                    }).data("kendoDropDownList");

                    var products = $("#products").kendoDropDownList({
                        autoBind: false,
                        cascadeFrom: "categories", // 必须category有值，这里才可以选择
                        optionLabel: "Select product...",
                        dataTextField: "ProductName",
                        dataValueField: "ProductID",
                        change: onChange,
                        dataSource: {
                            type: "odata",
                            serverFiltering: true,
                            transport: {
                                read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Products",
                            },
                            change: function () {
                                console.log("products: ");
                                console.log(this.view())
                            }
                        }
                    }).data("kendoDropDownList");

                    $("#shipTo").kendoDropDownList({
                        dataTextField: "ShipAddress",
                        dataValueField: "OrderID",
                        optionLabel: "Select address...",
                        autoBind: false,
                        virtual: {
                            itemHeight: 26,
                            valueMapper: function (options) {
                                $.ajax({
                                    // 这个url没有返回值，会报错
                                    url: "https://demos.telerik.com/kendo-ui/service/Orders/ValueMapper",
                                    type: "GET",
                                    dataType: "jsonp",
                                    data: convertValues(options.value),
                                    success: function (data) {
                                        // console.log(options);
                                        // console.log(data);
                                        // console.log(options.success(data));
                                        options.success(data);
                                    }
                                })
                            }
                        },
                        height: 290,
                        dataSource: {
                            type: "odata",
                            transport: {
                                read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                            },
                            schema: {
                                model: {
                                    fields: {
                                        OrderID: {type: "number"},
                                        Freight: {type: "number"},
                                        ShipName: {type: "string"},
                                        OrderDate: {type: "date"},
                                        ShipCity: {type: "string"}
                                    }
                                }
                            },
                            pageSize: 80,
                            serverPaging: true,
                            serverFiltering: true
                        }
                    });
                });

                function onChange(e) {
                    var orders = $("#shipTo").data("kendoDropDownList");
                    orders.value("");

                    if (e.sender.value() == "") {
                        orders.enable(false);
                    } else {
                        orders.enable(true);
                    }
                    console.log("onChange: " + e.sender.value());
                }
            </script>
        </div>
    </div>
    <script>
        function convertValues(value) {
            var data = {};
            value = $.isArray(value) ? value : [value];

            for (var idx = 0; idx < value.length; idx++) {
                data["values[" + idx + "]"] = value[idx];
            }

            return data;
        }
    </script>
    <style>
        .k-readonly {
            color: gray;
        }

        .selected-value {
            display: inline-block;
            vertical-align: middle;
            width: 24px;
            height: 24px;
            background-size: 100%;
            margin-right: 5px;
            border-radius: 50%;
        }

        #categories-list .k-item {
            line-height: 1em;
            min-width: 300px;
        }

        /* Material Theme padding adjustment*/
        .k-material #categories-list .k-item,
        .k-material #categories-list .k-item.k-state-hover,
        .k-materialblack #categories-list .k-item,
        .k-materialblack #categories-list .k-item.k-state-hover {
            padding-left: 5px;
            border-left: 0;
        }

        #categories-list .k-item > span {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            vertical-align: middle;
            display: table-cell;
            vertical-align: central;
            margin: 10px 10px 10px 5px;
        }

        #categories-list .k-item > span:first-child {
            -moz-box-shadow: inset 0 0 30px rgba(0, 0, 0, .3);
            -webkit-box-shadow: inset 0 0 30px rgba(0, 0, 0, .3);
            box-shadow: inset 0 0 30px rgba(0, 0, 0, .3);
            margin: 10px;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            background-size: 100%;
            background-repeat: no-repeat;
        }

        #categories-list h3 {
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 1px 0;
            padding: 0;
        }

        #categories-list p {
            margin: 0;
            padding: 0;
            font-size: .8em;
        }
    </style>
</div>
</body>
</html>
```

### Grid

> [Grid](https://demos.telerik.com/kendo-ui/grid/remote-data-binding)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div id="grid"></div>
        <script>
            $(document).ready(function () {
                $("#grid").kendoGrid({
                    dataSource: {
                        type: "odata",
                        transport: {
                            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                        },
                        schema: {
                            model: {
                                fields: {
                                    OrderID: {type: "number"},
                                    Freight: {type: "number"},
                                    ShipName: {type: "string"},
                                    OrderDate: {type: "date"},
                                    ShipCity: {type: "string"}
                                }
                            }
                        },
                        pageSize: 20,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    height: 805, // 高度和pageSize决定了一页显示多少行
                    filterable: true,
                    sortable: true,
                    pageable: true,
                    columns: [{
                        field: "OrderID",
                        filterable: false
                    },
                        "Freight",
                        {
                            field: "OrderDate",
                            title: "Order Date",
                            format: "{0:MM/dd/yyyy}"
                        }, {
                            field: "ShipName",
                            title: "Ship Name"
                        }, {
                            field: "ShipCity",
                            title: "Ship City"
                        }
                    ]
                });
            });
        </script>
    </div>
</div>
</body>
</html>
```

### MaskedTextBox

> [带掩码的文本框](https://demos.telerik.com/kendo-ui/maskedtextbox/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <ul id="fieldlist">
                <li>
                    <h4 for="phone_number">Phone number:</h4>
                    <input id="phone_number" value="555 123 4567"/>
                </li>
                <li>
                    <h4 for="credit_card">Credit Card number:</h4>
                    <input id="credit_card" value="1234 1234 1234 1234"/>
                </li>
                <li>
                    <h4 for="ssn">Social security number:</h4>
                    <input id="ssn" value="003-12-3456"/>
                </li>
                <li>
                    <h4 for="postcode">UK postcode:</h4>
                    <input id="postcode" value="W1N 1AC"/>
                </li>
            </ul>
        </div>

        <script>
            $(document).ready(function () {
                $("#phone_number").kendoMaskedTextBox({
                    mask: "(999) 000-0000"
                });

                $("#credit_card").kendoMaskedTextBox({
                    mask: "0000 0000 0000 0000"
                });

                $("#ssn").kendoMaskedTextBox({
                    mask: "000-00-0000"
                });

                $("#postcode").kendoMaskedTextBox({
                    mask: "L0L 0LL"
                });
            });
        </script>
        <style>
            #fieldlist {
                margin: 0 0 -2em;
                padding: 0;
            }

            #fieldlist li {
                list-style: none;
                padding-bottom: 2em;
            }

            #fieldlist .k-maskedtextbox {
                width: 100%;
            }
        </style>
    </div>
</div>
</body>
</html>
```

### Raido

> [单选框](https://demos.telerik.com/kendo-ui/radiobutton/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <h4>Select Car Engine</h4>
            <ul class="fieldlist">
                <!--name一样表示只能选择一个-->
                <li>
                    <input type="radio" name="engine" id="engine1" class="k-radio" checked="checked">
                    <label class="k-radio-label" for="engine1">1.4 Petrol, 92kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine2" class="k-radio">
                    <label class="k-radio-label" for="engine2">1.8 Petrol, 118kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine3" class="k-radio">
                    <label class="k-radio-label" for="engine3">2.0 Petrol, 147kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine4" class="k-radio" disabled="disabled">
                    <label class="k-radio-label" for="engine4">3.6 Petrol, 191kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine5" class="k-radio">
                    <label class="k-radio-label" for="engine5">1.6 Diesel, 77kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine6" class="k-radio">
                    <label class="k-radio-label" for="engine6">2.0 Diesel, 103kW</label>
                </li>
                <li>
                    <input type="radio" name="engine" id="engine7" class="k-radio" disabled="disabled">
                    <label class="k-radio-label" for="engine7">2.0 Diesel, 125kW</label>
                </li>
            </ul>
        </div>
        <style>
            .fieldlist {
                margin: 0 0 -1em;
                padding: 0;
            }

            .fieldlist li {
                list-style: none;
                padding-bottom: 1em;
            }
        </style>
    </div>
</div>
</body>
</html>
```

### TabStrip

> [选项卡](https://demos.telerik.com/kendo-ui/tabstrip/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <div id="tabstrip">
                <ul>
                    <li class="k-state-active">
                        Paris
                    </li>
                    <li>
                        New York
                    </li>
                    <li>
                        London
                    </li>
                    <li>
                        Moscow
                    </li>
                </ul>
                <div>
                    <span class="rainy">&nbsp;</span>
                    <div class="weather">
                        <h2>17<span>&ordm;C</span></h2>
                        <p>Rainy weather in Paris.</p>
                    </div>
                </div>
                <div>
                    <span class="sunny">&nbsp;</span>
                    <div class="weather">
                        <h2>29<span>&ordm;C</span></h2>
                        <p>Sunny weather in New York.</p>
                    </div>
                </div>
                <div>
                    <span class="sunny">&nbsp;</span>
                    <div class="weather">
                        <h2>21<span>&ordm;C</span></h2>
                        <p>Sunny weather in London.</p>
                    </div>
                </div>
                <div>
                    <span class="cloudy">&nbsp;</span>
                    <div class="weather">
                        <h2>16<span>&ordm;C</span></h2>
                        <p>Cloudy weather in Moscow.</p>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $("#tabstrip").kendoTabStrip({
                    animation: {
                        open: {
                            effects: "fadeIn"
                        }
                    }
                });
            });
        </script>

        <style>
            .sunny, .cloudy, .rainy {
                display: block;
                margin: 30px auto 10px;
                width: 128px;
                height: 128px;
                background: url('../../resources/images/tabstrip/weather.png') transparent no-repeat 0 0;
            }

            .cloudy {
                background-position: -128px 0;
            }

            .rainy {
                background-position: -256px 0;
            }

            .weather {
                margin: 0 auto 30px;
                text-align: center;
            }

            #tabstrip h2 {
                font-weight: lighter;
                font-size: 5em;
                line-height: 1;
                padding: 0 0 0 30px;
                margin: 0;
            }

            #tabstrip h2 span {
                background: none;
                padding-left: 5px;
                font-size: .3em;
                vertical-align: top;
            }

            #tabstrip p {
                margin: 0;
                padding: 0;
            }
        </style>
    </div>
</div>
</body>
</html>
```

### TreeView

> [TreeView](https://demos.telerik.com/kendo-ui/treeview/basic-usage)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <ul id="treeview">
                <li data-expanded="true">
                    <span class="k-sprite folder"></span>
                    My Web Site
                    <ul>
                        <li data-expanded="true">
                            <span class="k-sprite folder"></span>images
                            <ul>
                                <li><span class="k-sprite image"></span>logo.png</li>
                                <li><span class="k-sprite image"></span>body-back.png</li>
                                <li><span class="k-sprite image"></span>my-photo.jpg</li>
                            </ul>
                        </li>
                        <li data-expanded="true">
                            <span class="k-sprite folder"></span>resources
                            <ul>
                                <li data-expanded="true">
                                    <span class="k-sprite folder"></span>pdf
                                    <ul>
                                        <li><span class="k-sprite pdf"></span>brochure.pdf</li>
                                        <li><span class="k-sprite pdf"></span>prices.pdf</li>
                                    </ul>
                                </li>
                                <li><span class="k-sprite folder"></span>zip</li>
                            </ul>
                        </li>
                        <li><span class="k-sprite html"></span>about.html</li>
                        <li><span class="k-sprite html"></span>contacts.html</li>
                        <li><span class="k-sprite html"></span>index.html</li>
                        <li><span class="k-sprite html"></span>portfolio.html</li>
                    </ul>
                </li>
            </ul>
        </div>

        <script>
            $(document).ready(function () {
                $("#treeview").kendoTreeView();
            });
        </script>

        <style>
            #treeview .k-sprite {
                background-image: url("../content/web/treeview/coloricons-sprite.png");
            }

            .rootfolder {
                background-position: 0 0;
            }

            .folder {
                background-position: 0 -16px;
            }

            .pdf {
                background-position: 0 -32px;
            }

            .html {
                background-position: 0 -48px;
            }

            .image {
                background-position: 0 -64px;
            }
        </style>
    </div>
</div>
</body>
</html>
```

### TreeList

> [TreeList](https://demos.telerik.com/kendo-ui/treelist/remote-data-binding)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div id="treelist"></div>

        <script>
            $(document).ready(function () {
                var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service";

                var dataSource = new kendo.data.TreeListDataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/EmployeeDirectory",
                            dataType: "jsonp"
                        }
                    },
                    schema: {
                        model: {
                            id: "EmployeeId",
                            parentId: "ReportsTo",
                            fields: {
                                EmployeeId: {type: "number", nullable: false},
                                ReportsTo: {field: "ReportsTo", nullable: true}
                            }
                        }
                    }
                });
                $("#treelist").kendoTreeList({
                    dataSource: dataSource,
                    columns: [
                        {field: "FirstName", expandable: true, title: "First Name", width: 250},
                        {field: "LastName", title: "Last Name"},
                        {field: "Position"},
                        {field: "Extension", title: "Ext", format: "{0:#}"}
                    ]
                });
            });
        </script>
    </div>
</div>
</body>
</html>
```

### Lov

> 官方文档目前没有Lov的使用，见上述的 [LOV的定义和使用](#LOV的定义和使用)

### QueryPanel

> 官方文档没有找到这个标签的使用

### Form

> kendoui的表单部分常用的有:
>
> - textbox
> - combobox
> - lov
> - datapicker 当组件被加载时，首先通过data-role去判断当前组件是什么，然后在根据data-bind去绑定资源

#### textbox

> 注意：kendoui中textbox的样式：.k-textbox data-role ="maskedtextbox",如果data-role不存在，会默认为文本框。
>
> 例如下面的代码，标记maskedtextbox的就是按照[MaskedTextBox](#MaskedTextBox)显示

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <input type="text" placeholder='...' data-bind="value:model.url" class="k-textbox">

    <div class="form-group">
        <label class="col-sm-2 control-label">用户名</label>
        <div class="col-sm-10">
            <input id="id1" type="text" class="k-textbox" data-bind="value:model.placeholder">
        </div>
    </div>

    <script>
        $("#id1").kendoMaskedTextBox({
            mask: "(999) 000-0000"
        });
    </script>
</div>
</body>
</html>
```

#### combobox

```html
<div class="col-sm-3">
	<div class="form-group">
		<label class="col-sm-4 control-label">*<@spring.message "xxar.PrjInvoiceHeaders.headerStatus"/></label>
		<div class="col-sm-8">
			<input  type="text"  id="headerStatus"  data-bind="value:model.headerStatus"
					 style="width: 100%;background-color:#f0f0f0" readonly="readonly">
		</div>
		<script>
			$("#headerStatus").kendoComboBox({
				dataSource:headerStatus,
				valuePrimitive: true,
				dataTextField: "meaning",
				dataValueField: "value"
			});
		</script>
	</div>
</div>
```

#### lov

> kendoui并没有实现lov,使用见[LOV的定义和使用](#LOV的定义和使用)

#### datapicker

> [DatePicker](#DatePicker)

### Validator

> [Validator](https://demos.telerik.com/kendo-ui/validator/index)

```html
<#include "../../include/header.html">
<body xmlns:h="http://www.w3.org/1999/html">
<div id="page-content">
    <div id="example">
        <div class="demo-section k-content">
            <div id="validation-summary">
            </div>
            <form id="ticketsForm" class="k-form k-form-vertical">
                <ul class="k-form-group">
                    <li class="k-form-field">
                        <label for="fullname" class="k-form-label">Your Name</label>
                        <span class="k-form-field-wrap">
                        <input type="text" id="fullname" name="fullname" class="k-textbox" placeholder="Full name"
                               required validationMessage="Enter {0}"/>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="search" class="k-form-label">Movie</label>
                        <span class="k-form-field-wrap">
                        <input type="search" id="search" name="search" required validationMessage="Select movie"/>
                        <span class="k-invalid-msg" data-for="search"></span>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="date" class="k-form-label">Date</label>
                        <span class="k-form-field-wrap">
                        <input type="text" id="date" name="date"
                               min="5/6/2017" data-max-msg="Enter date after '5/6/2017''"
                               pattern="\d+\/\d+\/\d+" validationMessage="Enter full date"/>
                        <span class="k-invalid-msg" data-for="date"></span>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="time" class="k-form-label">Start time</label>
                        <span class="k-form-field-wrap">
                        <select name="time" id="time" required data-required-msg="Select start time">
                            <option>14:00</option>
                            <option>15:30</option>
                            <option>17:00</option>
                            <option>20:15</option>
                        </select>
                        <span class="k-invalid-msg" data-for="time"></span>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="amount" class="k-form-label">Amount</label>
                        <span class="k-form-field-wrap">
                        <input id="amount" name="amount" type="text" min="1" max="10" value="1" required
                               data-max-msg="Enter value between 1 and 10"/>
                        <span class="k-invalid-msg" data-for="amount"></span>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="email" class="k-form-label">Email</label>
                        <span class="k-form-field-wrap">
                        <input type="email" id="email" name="email" class="k-textbox"
                               placeholder="e.g. myname@example.net" required
                               data-email-msg="Email format is not valid"/>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="tel" class="k-form-label">Phone</label>
                        <span class="k-form-field-wrap">
                        <input type="tel" id="tel" name="tel" class="k-textbox" pattern="\d{10}"
                               placeholder="Enter a ten-digit number" required
                               validationMessage="Enter a ten-digit number"/>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <label for="rating" class="k-form-label">Rating</label>
                        <span class="k-form-field-wrap">
                        <input id="rating" name="rating" required validationMessage="Select a rating"/>
                        <span class="k-invalid-msg" data-for="rating"></span>
                    </span>
                    </li>
                    <li class="k-form-field">
                        <span class="k-form-label">Terms of Service</span>
                        <span class="k-form-field-wrap">
                        <label>
                            <input type="checkbox" name="Accept" required validationMessage="Acceptance is required"/>
                            I accept the terms of service.
                        </label>
                    </span>
                    </li>
                    <li class="k-form-buttons">
                        <button class="k-button k-primary" type="submit">Submit</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var data = [
                "12 Angry Men",
                "Il buono, il brutto, il cattivo.",
                "Inception",
                "One Flew Over the Cuckoo's Nest",
                "Pulp Fiction",
                "Schindler's List",
                "The Dark Knight",
                "The Godfather",
                "The Godfather: Part II",
                "The Shawshank Redemption"
            ];

            $("#search").kendoAutoComplete({
                dataSource: data,
                separator: ", "
            });

            $("#time").kendoDropDownList({
                optionLabel: "--Start time--"
            });

            $("#amount").kendoNumericTextBox();

            $("#date").kendoDateInput();

            $("#rating").kendoRating();

            var validator = $("#ticketsForm").kendoValidator().data("kendoValidator");
            var validationSummary = $("#validation-summary");

            $("form").submit(function (event) {
                event.preventDefault();

                if (validator.validate()) {
                    validationSummary.html("<div class='k-messagebox k-messagebox-success'>Hooray! Your tickets has been booked!</div>");
                } else {
                    validationSummary.html("<div class='k-messagebox k-messagebox-error'>Oops! There is invalid data in the form.</div>");
                }
            });
        });
    </script>
</div>
</body>
</html>
```

### 测试截图

> 图片需要放在webapp/resources/images/下

![image-20211124163625488](https://gitee.com/image_bed/image-bed1/raw/master/img/image-20211124163625488.png)

# HAP进阶

## 简易WebService开发

> 开发一个简单的Soap WS

### 编写WebService服务端

> 前文提到过WebService分为服务提供者、服务请求者和服务中介者，服务端就是服务的提供者，我们需要在服务端定义相关的方法，实现服务请求者的各种需求，然后我们将服务端发布注册到服务中介者那，服务请求者就可以根据相关信息去调用我们服务端的方法。\

#### 创建interface

> @WebService：标记表示该接口是一个WebService服务。
>
> @WebMethod：标记表示WebService中的方法。
>
> @WebParam(name="paramName")表示方法中的参数，name属性限制了参数的名称，若没有指定该属性，参数将会被重命名

```java
package xxitf.service;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * <p>HelloWorld接口</p>
 *
 * @author hyatt 2021/11/29 14:57
 * @version 1.0
 */
@WebService
public interface IHelloWorldService {
    /**
     * 测试方法
     * @param text
     * @return
     */
    public String sayHi(@WebParam(name="text") String text);
}
```

#### 编写ServiceImpl层，实现该WebService接口

> @WebService(endpointInterface=”**对应的WebService接口**”，serviceName=”WebService的名字，自己定义”)

```java
package xxitf.service.impl;

import xxitf.service.IHelloWorldService;

import javax.jws.WebService;

/**
 * <p>HelloWorld接口的实现类</p>
 *
 * @author hyatt 2021/11/29 14:55
 * @version 1.0
 */
@WebService(endpointInterface = "xxitf.service.IHelloWorldService", serviceName = "HelloService")
public class HelloWorldServiceImpl implements IHelloWorldService {
    @Override
    public String sayHi(String text) {
        return "say hello: " + text;
    }
}
```

#### 编写用于发布WebService的类

> 使用Endpoint.publish(url,WebService)方法，该方法需要两个参数，一个是服务的发布地址，即最后你在浏览器上访问你发布的WebServcie时用的Url，另一个参数就是你要发布的WebService的实现类。右键——Run as——Java Application，服务发布成功后，访问http://localhost:9999/helloworld?wsdl进行测试，看到如下图效果即为成功。（必须带上?wsdl,不然是不被识别的，它代表的是对这个WebService的相关描述）

```java
package xxitf;

import xxitf.service.impl.HelloWorldServiceImpl;

import javax.xml.ws.Endpoint;

/**
 * <p>发布WebService的类</p>
 *
 * @author hyatt 2021/11/29 15:02
 * @version 1.0
 */
public class WebserviceApp {
    public static void main(String[] args) {
        System.out.println("start service");
        HelloWorldServiceImpl helloWorldService = new HelloWorldServiceImpl();
        String address = "http://localhost:9999/helloworld";
        Endpoint.publish(address, helloWorldService);
        System.out.println("end service");
    }
}
```

![image-20211129153209444](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291532859.png)	

#### soapui调用

> 不需要登陆

![image-20211129153254692](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291533683.png)	

### 编写WebService的客户端

> 服务发布成功之后，在服务请求者方，我们需要编写相应的客户端来调用我们的WebService(这里需要导入一个依赖,见下方的问题)

```java
package xxitf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import xxitf.service.IHelloWorldService;

/**
 * <p>客户端调用接口</p>
 *
 * @author hyatt 2021/11/29 15:09
 * @version 1.0
 */
public class HelloWorldClient {
    public static void main(String[] args) {
        // Spring 方式
        System.out.println("Start WebService");
        ApplicationContext factory = new ClassPathXmlApplicationContext("spring/cxf-beans-commonservice.xml");
        IHelloWorldService client = (IHelloWorldService) factory.getBean("client");
        String result = client.sayHi("你好, Spring");
        System.out.println(result);

//        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
//        jaxWsProxyFactoryBean.getInInterceptors().add(new LoggingInInterceptor());
//        jaxWsProxyFactoryBean.getOutInterceptors().add(new LoggingOutInterceptor());
//        jaxWsProxyFactoryBean.setServiceClass(IHelloWorldService.class);
//        jaxWsProxyFactoryBean.setAddress("http://localhost:9999/helloworld");
//        IHelloWorldService client = (IHelloWorldService) jaxWsProxyFactoryBean.create();
//        String reply = client.sayHi("Hello World!!");
//        System.out.println(reply);
    }
}
```

> 用JaxWsProxyFactoryBean方法创建一个客户端厂；jaxWsProxyFactoryBean.getInInterceptors().add()和jaxWsProxyFactoryBean.getOutInterceptors().add()方法，是为了打印控制台日志的。重点是jaxWsProxyFactoryBean.setServiceClass(IHelloWorldService.class)，表明你的WebService接口是哪一个，然后jaxWsProxyFactoryBean.setAddress()表明你的服务地址是什么，最后进行客户端client的生成，调用相应的方法。日志有很多下面只截取了一部分。效果如下：

![image-20211129155417320](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291554502.png)

![image-20211129155437636](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291554739.png)

### WebService与Spring结合

> 有的WebService服务需要我们根据需求去自主发布，但是有些公用的服务，我们可以在启动Tomact启动时就同时发布上去，在Tomact启动完成后，我们马上就可以进行使用，那么为了实现这个想法，我们就需要用到WebService与Spring的结合

#### 定义服务接口

> 同上

#### 编译服务实现层

> 同上

#### 在web.xml中增加监听

> HAP已经配置好了

![image-20211129155856885](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291559333.png)	

#### 配置application文件

> 新建名为cxf-beans-commonservice.xml的配置文件.
>
> 注意：命名问题和路径问题(需要根据applicationContext-cxf.xml中的设置来做)
>
> ![image-20211129164229217](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291642485.png)

![image-20211129174435032](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291744830.png)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:jaxws="http://cxf.apache.org/jaxws"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://cxf.apache.org/jaxws
       http://cxf.apache.org/schemas/jaxws.xsd">

    <!-- 在Tomcat启动后就自动发布 -->
    <jaxws:endpoint id="helloworld" implementor="xxitf.service.impl.HelloWorldServiceImpl" address="/helloworld"/>

    <!-- 为了实现spring的客户端调用接口配置 -->
    <bean id="clientFactory" class="org.apache.cxf.jaxws.JaxWsProxyFactoryBean">
        <property name="serviceClass" value="xxitf.service.IHelloWorldService"></property>
        <property name="address" value="http://localhost:8080/core_war_exploded/ws/helloworld"></property>
    </bean>
    <bean id="client" class="xxitf.client.HelloWorldClient" factory-bean="clientFactory" factory-method="create"></bean>
</beans>
```

> 配置完成后启动项目，访问：[localhost:8080/core_war_exploded/ws/helloworld?wsdl](http://localhost:8080/core_war_exploded/ws/helloworld?wsdl)

![image-20211129164353916](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291721759.png)	

#### SoapUI调用

> 不需要登陆

![image-20211129174628378](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291746237.png)	

#### 问题

> 客户端运行后报错：org.apache.cxf.jaxws.JaxWsProxyFactoryBean ClassNotFoundException

![image-20211129153722069](https://gitee.com/image_bed/image-bed1/raw/master/img/202111291537941.png)	

> 在maven中添加依赖， 添加完依赖后重新运行HAP项目发现报错，所以需要排除掉一些包，这里我把spring相关的包都排除掉了，之后两个都可以运行了
>
> 版本需要3.0以上的

```xml
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-bundle</artifactId>
	<version>3.0.0-milestone1</version>
	<exclusions>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jms</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-expression</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-asm</artifactId>
		</exclusion>
		<exclusion>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
		</exclusion>
		<exclusion>
			<artifactId>jaxb-impl</artifactId>
			<groupId>com.sun.xml.bind</groupId>
		</exclusion>
	</exclusions>
</dependency>	
```

## 基于HAP的WebService开发

### 开发前准备

#### 引入依赖

> HAP框架已经集成好了，但是需要补充一些依赖

![image-20211130095320888](https://gitee.com/image_bed/image-bed1/raw/master/img/202111300956156.png)	

> 由于之前在做简易ws开发是引入了包cxf-bundle，里面已经包含了一些，还有已经存在的包，所以这里就不引入了

> 其他笔记中需要引入的包，这里做一个记录(版本可以做相应的修改)

```xml
<!-- 开发webservice核心包 -->
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-rt-frontend-jaxws</artifactId>
	<version>3.1.6</version>
	<exclusions>
		<exclusion>
			<artifactId>jaxb-impl</artifactId>
			<groupId>com.sun.xml.bind</groupId>
		</exclusion>
		<exclusion>
			<artifactId>jaxb-core</artifactId>
			<groupId>com.sun.xml.bind</groupId>
		</exclusion>
	</exclusions>
</dependency>
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-rt-transports-http</artifactId>
	<version>3.1.6</version>
</dependency>
<!-- 在jetty环境下需要的包 -->
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-rt-transports-http-jetty</artifactId>
	<version>3.1.6</version>
</dependency>
<!-- 开发rest接口需要的包 -->
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-rt-frontend-jaxrs</artifactId>
	<version>3.1.6</version>
</dependency>
<!-- rest客户端调用需要的包，不推荐使用 -->
<dependency>
	<groupId>org.apache.cxf</groupId>
	<artifactId>cxf-rt-rs-client</artifactId>
	<version>3.1.6</version>
</dependency>
```

#### 在web.xml文件中加入WS的监听

> 这一步HAP也是配置好的

![image-20211130095609258](https://gitee.com/image_bed/image-bed1/raw/master/img/202111300956995.png)	

#### 接口的权限控制

> 此处的控制基于开发本地的WebService，并不针对外部系统的接口服务。
>
> 在这里我们是用oauth2.0来进行WebService服务的接口控制，oauth的原理在于access_token的使用，access_token讲白了就是一个临时的权限令牌，获取了这个令牌后，我们就可以去访问一些被保护起来的资源。详情参考《HAP框架-SpringSecurity入门手册》

##### 开发规范

> 现在cxf配置的地址是/ws开头，例如前面在服务器启动时就发布的接口
>
> 所以需要修改一下web.xml

![image-20211201102706103](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011027613.png)		

> Hap框架中的oauth2Security.xml文件专门对一些对外开放的接口服务做了限制。
>
> /api/public/*下的url没有任何权限控制
>
> /api/*下url有权限控制
>
> 我们可以根据自己要写的接口是否需要权限控制，自行发布在两个URL之下。
>

![image-20211130111620732](https://gitee.com/image_bed/image-bed1/raw/master/img/202111301116950.png)	

### Restful风格的WS

#### 简单的Restful WS

##### 编写WebService接口

```java
package xxitf.service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * <p>Restful类型的ws</p>
 *
 * @author hyatt 2021/11/30 10:09
 * @version 1.0
 */
@Path("")
@Produces("*/*")
public interface IHelloRestfulService {
    /**
     * Restful风格的WebMethod与Soap风格的有比较大的区别,它整体和Controller的结构很类似
     * * @GET(对应获取数据),@POST(对应发送数据),@PUT(对应更新数据),@DELDETE(对应删除数据)四种注解
     * * Produces()表示可处理的数据类型
     * * @Path()访问该方法需要的Url
     *
     * @param name 参数
     * @return 结果
     */
    @GET
    @Path("/say/{name}")
    // @Produces(MediaType.TEXT_PLAIN) 会发生500报错，可以修改成@Produces("*/*")，为了避免所有方法都加入这个，所以把produce属性放在类上表示所有方法的Produces一致
    public String say(@PathParam(value = "name") String name);
}
```

##### 编写服务实现层

```java
package xxitf.service.impl;

import org.springframework.stereotype.Component;
import xxitf.service.IHelloRestfulService;

/**
 * <p>Restful类型的ws的实现类</p>
 *
 * @author hyatt 2021/11/30 10:16
 * @version 1.0
 */

@Component("helloRestfulImpl")
public class HelloRestfulImpl implements IHelloRestfulService {
    @Override
    public String say(String name) {
        System.out.println(name + ", welcome");
        return name + ", welcome you.";
    }
}
```

##### 修改cxf-beans-commonservice.xml文件

```xml
<!-- 基于HAP的webservice开发 -->
<bean id="helloRestfulImpl" class="xxitf.service.impl.HelloRestfulImpl"></bean>
<jaxrs:server id="helloRestful" address="/public/helloRestful">
	<jaxrs:serviceBeans>
		<ref bean="helloRestfulImpl"/>
	</jaxrs:serviceBeans>
	<jaxrs:extensionMappings>
		<entry key="json" value="application/json"/>
		<entry key="xml" value="application/xml"/>
	</jaxrs:extensionMappings>
	<jaxrs:languageMappings>
		<entry key="en" value="en-gb"/>
	</jaxrs:languageMappings>
</jaxrs:server>
```

##### 测试

> 输入地址：[http://localhost:8080/core_war_exploded/api/public/helloRestful/say/name](http://localhost:8080/core_war_exploded/api/public/helloRestful/say/name)

> 500错误，但是看后台日志发现是调用成功了

![image-20211130172025504](https://gitee.com/image_bed/image-bed1/raw/master/img/202111301720565.png)	

> 修改@Produces属性为@Produces("*/*")后，就可以看到返回值了

![image-20211201111932982](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011119059.png)	

#### 以报文为交互内容的WS

> 就是将java中的对象转化为了xml文件格式的信息

##### 创建对象

> 新建Student类
>
> - @XmlRootElement(name = "Student")：代表xml文件中的根元素，就是最外层的元素
> - @XmlElement(name = "NAME")：根对象包含的属性
> - @XmlElement(name = "StudentAddress")：根对象中包含的一个子对象
> - @XmlElementWrapper(name = "StudentPhones")：根对象中包含的一个子对象的集合
> - @XmlElement(name = "StudentPhone")：根对象中包含的一个子对象

```java
package xxitf.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <p>学生类</p>
 *
 * @author hyatt 2021/12/1 9:53
 * @version 1.0
 */

@XmlRootElement(name = "Student")
public class Student {
    private String name;
    private StudentAddress studentAddress;
    private List<StudentPhone> studentPhones;

    @XmlElement(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "StudentAddress")
    public StudentAddress getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(StudentAddress studentAddress) {
        this.studentAddress = studentAddress;
    }

    @XmlElementWrapper(name = "StudentPhones")
    @XmlElement(name = "StudentPhone")
    public List<StudentPhone> getStudentPhones() {
        return studentPhones;
    }

    public void setStudentPhones(List<StudentPhone> studentPhones) {
        this.studentPhones = studentPhones;
    }
}
```

> 创建在Student类中引用的其它类

```java
package xxitf.dto;

import javax.xml.bind.annotation.XmlElement;

/**
 * <p>学生地址类</p>
 *
 * @author hyatt 2021/12/1 9:54
 * @version 1.0
 */
public class StudentAddress {
    /**
     * 家庭住址
     */
    private String homeAddress;
    /**
     * 公司地址
     */
    private String workAddress;

    @XmlElement(name = "HomeAddress")
    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    @XmlElement(name = "WorkAddress")
    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }
}
```

> 在根对象中进行了声明的，子对象中可以不添加注解

```java
package xxitf.dto;

/**
 * <p>学生通讯录</p>
 *
 * @author hyatt 2021/12/1 9:57
 * @version 1.0
 */
public class StudentPhone {
    /**
     * 电话号码类型
     */
    private String type;
    /**
     * 电话号码
     */
    private String num;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
```

##### 报文

> 根据上面的标签整理出一个报文

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Student>
    <NAME>SAM-SHO</NAME>
    <StudentPhones>
        <StudentPhone>
            <num>13612345678</num>
            <type>移动</type>
        </StudentPhone>
        <StudentPhone>
            <num>13798765432</num>
            <type>联通</type>
        </StudentPhone>
    </StudentPhones>
    <StudentAddress>
        <HomeAddress>苏州高新区</HomeAddress>
        <WorkAddress>苏州园区</WorkAddress>
    </StudentAddress>
</Student>
```

##### 解析报文信息

> 创建一个类来解析报文

```java
package xxitf;

import org.xml.sax.InputSource;
import xxitf.dto.Student;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * <p>解析报文信息</p>
 *
 * @author hyatt 2021/12/1 10:03
 * @version 1.0
 */
public class ObjectAndXmlHandle {

    public static Student parseXmlToUserBean(String xml) {
        try {
            JAXBContext context = JAXBContext.newInstance(Student.class);
            InputSource inputSource = new InputSource();
            StringReader stringReader = new StringReader(xml);
            inputSource.setCharacterStream(stringReader);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Student student = (Student) unmarshaller.unmarshal(inputSource);
            return student;
        } catch (JAXBException e) {
            System.out.println("异常：" + e.getMessage());
        }
        return null;
    }
}
```

##### 注意

- 报文的根标签必须和@XmlRootElement(name = "Student")中的name一样，否则解析报文信息的时候会出现异常(例如我将报文的根标签改为Student)

![image-20211201142144660](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011421377.png)	

- 除了根标签其他的标签也要和@XmlElement(name = "NAME")中的name值一致，虽然不会报错但是会获取不到值(以name为例。我将标签改为Name)

![image-20211201142500335](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011425178.png)

- 没有标签标志的默认和成员变量名称一致

##### 接口层添加方法

> sendXml

```java
package xxitf.service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * <p>Restful类型的ws</p>
 *
 * @author hyatt 2021/11/30 10:09
 * @version 1.0
 */
@Path("")
@Produces("*/*")
public interface IHelloRestfulService {
    /**
     * Restful风格的WebMethod与Soap风格的有比较大的区别,它整体和Controller的结构很类似
     * * @GET(对应获取数据),@POST(对应发送数据),@PUT(对应更新数据),@DELDETE(对应删除数据)四种注解
     * * Produces()表示可处理的数据类型
     * * @Path()访问该方法需要的Url
     *
     * @param name 参数
     * @return 结果
     */
    @GET
    @Path("/say/{name}")
    public String say(@PathParam(value = "name") String name);

    @POST
    @Path("/sendXml")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String sendXml(String requestXml);
}
```

##### 接口实现层添加方法

> sendXml

```java
package xxitf.service.impl;

import org.springframework.stereotype.Component;
import xxitf.ObjectAndXmlHandle;
import xxitf.dto.Student;
import xxitf.dto.StudentAddress;
import xxitf.service.IHelloRestfulService;

/**
 * <p>Restful类型的ws的实现类</p>
 *
 * @author hyatt 2021/11/30 10:16
 * @version 1.0
 */

@Component("helloRestfulImpl")
public class HelloRestfulImpl implements IHelloRestfulService {
    @Override
    public String say(String name) {
        System.out.println(name + ", welcome");
        return name + ", welcome you.";
    }

    @Override
    public String sendXml(String requestXml) {
        System.out.println("参数：" + requestXml);
        Student student = ObjectAndXmlHandle.parseXmlToUserBean(requestXml);
        String message = student.getName() + "你好, 你的请求成功了.";
        StudentAddress studentAddress = student.getStudentAddress();
        String homeAddress = studentAddress.getHomeAddress();
        System.out.println(message);
        System.out.println("homeAddress: " + homeAddress);
        return message;
    }
}
```

> 测试结果

![image-20211201143546899](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011435433.png)

##### 通过代码获取报文

> 创建一个解析xml/json报文和通过java对象生成xml/json报文的类和对应的测试方法。
>
> json解析和使用需要添加依赖
>
> ```xml
> <dependency>
>     <groupId>com.google.code.gson</groupId>
>     <artifactId>gson</artifactId>
>     <version>2.8.5</version>
> </dependency>
> ```
>
> [Gson类 - Gson教程](https://www.yiibai.com/gson/gson_class.html)

```java
package xxitf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.http.converter.json.GsonFactoryBean;
import org.xml.sax.InputSource;
import xxitf.dto.Student;
import xxitf.dto.StudentAddress;
import xxitf.dto.StudentPhone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>解析报文信息</p>
 *
 * @author hyatt 2021/12/1 10:03
 * @version 1.0
 */
public class ObjectAndXmlHandle {

    /**
     * 解析报文，转换为java独享
     *
     * @param xml 报文
     * @return java中的对象
     */
    public static Student parseXmlToUserBean(String xml) {
        try {
            JAXBContext context = JAXBContext.newInstance(Student.class);
            InputSource inputSource = new InputSource();
            StringReader stringReader = new StringReader(xml);
            inputSource.setCharacterStream(stringReader);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Student student = (Student) unmarshaller.unmarshal(inputSource);
            return student;
        } catch (JAXBException e) {
            System.out.println("异常：" + e.getMessage());
        }
        return null;
    }

    /**
     * 将java对象封装为报文
     *
     * @param object java中对象
     */
    public static void objectToXml(Object object) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty("jaxb.encoding", "UTF-8");
            marshaller.marshal(object, System.out);
        } catch (JAXBException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 将对象转换为json格式的字符串
     *
     * @param object java对象
     */
    public static String objectToJson(Object object) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        String jsonString = gson.toJson(object);
        return jsonString;
    }

    /**
     * 将json字符串转换为Student类
     *
     * @param jsonString json字符串
     * @return Student类
     */
    public static Student jsonToObject(String jsonString) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        builder.setLenient();
        Student student = gson.fromJson(jsonString, Student.class);
        return student;
    }

    /**
     * 生成xml报文
     */
    @Test
    public void testObjectToXml() {
        Student student = new Student();
        // 设置学生名称
        student.setName("邓超");
        // 创建学生地址
        StudentAddress studentAddress = new StudentAddress();
        studentAddress.setHomeAddress("上海浦东");
        studentAddress.setWorkAddress("上海浦东");
        // 创建通讯信息
        StudentPhone studentPhone = new StudentPhone();
        studentPhone.setType("移动");
        studentPhone.setNum("13612345678");
        StudentPhone studentPhone2 = new StudentPhone();
        studentPhone2.setType("联通");
        studentPhone2.setNum("13798765432");
        List<StudentPhone> studentPhones = new ArrayList<StudentPhone>();
        studentPhones.add(studentPhone);
        studentPhones.add(studentPhone2);
        // 将地址信息和通讯信息添加到学生中
        student.setStudentAddress(studentAddress);
        student.setStudentPhones(studentPhones);
        // 查看xml报文
        objectToXml(student);
    }

    @Test
    public void testObjectToJson() {
        Student student = new Student();
        // 设置学生名称
        student.setName("邓超");
        // 创建学生地址
        StudentAddress studentAddress = new StudentAddress();
        studentAddress.setHomeAddress("上海浦东");
        studentAddress.setWorkAddress("上海浦东");
        // 创建通讯信息
        StudentPhone studentPhone = new StudentPhone();
        studentPhone.setType("移动");
        studentPhone.setNum("13612345678");
        StudentPhone studentPhone2 = new StudentPhone();
        studentPhone2.setType("联通");
        studentPhone2.setNum("13798765432");
        List<StudentPhone> studentPhones = new ArrayList<StudentPhone>();
        studentPhones.add(studentPhone);
        studentPhones.add(studentPhone2);
        // 将地址信息和通讯信息添加到学生中
        student.setStudentAddress(studentAddress);
        student.setStudentPhones(studentPhones);
        // 查看xml报文
        String jsonString = objectToJson(student);
        System.out.println(jsonString);
    }

    @Test
    public void testJsonToObject() {
        // 这个字符串是通过上面的测试方法获取的
        String jsonString = "{\n" +
                "    \"name\":\"邓超\",\n" +
                "    \"studentAddress\":{\n" +
                "        \"homeAddress\":\"上海浦东\",\n" +
                "        \"workAddress\":\"上海浦东\"\n" +
                "    },\n" +
                "    \"studentPhones\":[\n" +
                "        {\n" +
                "            \"type\":\"移动\",\n" +
                "            \"num\":\"13612345678\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\":\"联通\",\n" +
                "            \"num\":\"13798765432\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        Student student = jsonToObject(jsonString);
        System.out.println("name: " + student.getName());
    }
}
```

> 运行测试代码

![image-20211201150255498](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011502342.png)	

![image-20211201160004988](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011600240.png)	

##### 添加json方法

> 添加接口层方法

```java
@POST
@Path("/sendJson")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public String sendJson(String jsonString);
```

> 添加实现层方法

```java
@Override
public String sendJson(String jsonString) {
    System.out.println("参数： " + jsonString);
    Student student = ObjectAndXmlHandle.jsonToObject(jsonString);
    String message = student.getName() + "你好, 你的请求成功了.";
    return message;
}
```

> 测试并查看结果

![image-20211201161158321](https://gitee.com/image_bed/image-bed1/raw/master/img/202112011612375.png)	

#### 以JavaBean为交互内容的WS

> xml格式和json格式的

##### 添加接收返回信息的类

```java
package xxitf.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>返回的信息类</p>
 *
 * @author hyatt 2021/12/1 16:17
 * @version 1.0
 */
@XmlRootElement(name = "RETURN")
public class ReturnDto {

    private String code;
    private String message;

    @XmlElement(name = "Code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(name = "Message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
```

##### 添加接口方法

```java
@POST
@Path("/sendBean")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public ReturnDto sendBean(Student student);
```

##### 添加实现类方法

```java
@Override
public ReturnDto sendBean(Student student) {
	// ObjectAndXmlHandle.objectToXml(student);
	System.out.println("name: " + student.getName());
	ReturnDto returnDto = new ReturnDto();
	returnDto.setCode("0");
	returnDto.setMessage(student.getName() + ", 请求成功");
	return returnDto;
}
```

> 最后调用的时候，接口没有自动返回xml格式的数据(报500错误)，这个问题暂时没有找到原因(所以暂时只能通过字符串来返回和传入)

> 修改之前的一报文为交互内容的ws，将返回信息修改成对应的xml和json字符串

##### 修改解析报文信息

> 之前获取xml是打印在输出台，这里修改成返回字符串

```java
/**
 * 将java对象封装为报文并返回
 *
 * @param object 对象
 * @return 返回报文
 */
public static String objectToXmlString(Object object) {
    try {
        StringWriter stringWriter = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty("jaxb.encoding", "UTF-8");
        marshaller.marshal(object, stringWriter);
        return new String(stringWriter.getBuffer());
    } catch (JAXBException e) {
        System.out.println(e.getMessage());
    }
    return null;
}
```

##### 修改对应的方法

```java
@Override
public String sendXml(String requestXml) {
	System.out.println("参数：" + requestXml);
	Student student = ObjectAndXmlHandle.parseXmlToUserBean(requestXml);
//        StudentAddress studentAddress = student.getStudentAddress();
//        String homeAddress = studentAddress.getHomeAddress();
//        System.out.println("homeAddress: " + homeAddress);
	ReturnDto returnDto = new ReturnDto();
	returnDto.setCode("0");
	returnDto.setMessage(student.getName() + ", 请求成功");
	String message = ObjectAndXmlHandle.objectToXmlString(returnDto);
	return message;
}

@Override
public String sendJson(String jsonString) {
	System.out.println("参数： " + jsonString);
	Student student = ObjectAndXmlHandle.jsonToObject(jsonString);
	// String message = student.getName() + "你好, 你的请求成功了.";
	// 返回一个对象组成的json字符串
	ReturnDto returnDto = new ReturnDto();
	returnDto.setCode("0");
	returnDto.setMessage(student.getName() + ", 请求成功");
	String message = ObjectAndXmlHandle.objectToJson(returnDto);
	return message;
}
```

![image-20211202141958671](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021420808.png)	

![image-20211202142023901](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021420164.png)	

#### 使用Spring框架的Rest

> 基于之前创建Invoice的类

> 上面在使用cxf开发的时候，遇到过许多问题，例如修改web.xml，和本身spring框架冲突等问题，所以下面用Spring来实现(将web.xml修改回来)

##### 创建响应类

```java
package xxap.basis.dto;

import com.hand.hap.system.dto.ResponseData;

import java.util.List;

/**
 * <p>用于返回信息</p>
 *
 * @author hyatt 2021/12/2 14:37
 * @version 1.0
 */
public class InvoiceResponse extends ResponseData {

    private List<InvoiceAll> list;
    private Status status = new Status();

    public InvoiceResponse() {
    }

    public List<InvoiceAll> getList() {
        return list;
    }

    public void setList(List<InvoiceAll> list) {
        this.list = list;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * 创建Status内部类
     */
    public static class Status {
        private int code = 0;
        private String message = "success";

        public Status() {

        }

        public Status(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
```

##### 添加接口方法

> 在对应的发票service类中添加方法，一个获取一个更新

```java
InvoiceResponse selectAllInvoice(IRequest request) throws InvoiceException;
InvoiceResponse updateInvoice(IRequest request, List<InvoiceAll> invoiceAllList) throws InvoiceException;
```

##### 添加接口实现类方法

```java
@Override
public InvoiceResponse selectAllInvoice(IRequest request) throws InvoiceException {
    InvoiceResponse invoiceResponse = new InvoiceResponse();
    List<InvoiceAll> invoiceAllList = mapper.selectAll();
    System.out.println("invoiceAllList: " + invoiceAllList.size());
    invoiceResponse.setList(invoiceAllList);
    invoiceResponse.setStatus(new InvoiceResponse.Status(0, "获取成功"));
    return invoiceResponse;
}

@Override
public InvoiceResponse updateInvoice(IRequest request, List<InvoiceAll> invoiceAllList) throws InvoiceException {
	InvoiceResponse invoiceResponse = new InvoiceResponse();
	List<InvoiceAll> invoices = new ArrayList<InvoiceAll>();
	for (InvoiceAll invoiceAll : invoiceAllList) {
		int result = mapper.updateByPrimaryKey(invoiceAll);
		System.out.println("更新结果： " + result);
		// 获取刚才更新的数据
		invoices.add(mapper.selectByPrimaryKey(invoiceAll));
		// 已付金额大于发票金额
		if (invoiceAll.getAmountPaid() > invoiceAll.getInvoiceAmount()) {
			throw new InvoiceException("已付金额大于发票金额", 2, "发票编号" + invoiceAll.getInvoiceNum() + "已付金额大于发票金额");
		}
	}
	invoiceResponse.setList(invoices);
	invoiceResponse.setStatus(new InvoiceResponse.Status(0, "更新成功"));
	return invoiceResponse;
}
```

##### 创建Controller

> 单独创建一个ws的controller
>
> /api/public：表示不用验证

```java
package xxap.basis.controllers;

import com.hand.hap.core.IRequest;
import com.hand.hap.system.controllers.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import xxap.basis.dto.InvoiceAll;
import xxap.basis.dto.InvoiceException;
import xxap.basis.dto.InvoiceResponse;
import xxap.basis.service.IInvoiceAllService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>发票接口</p>
 *
 * @author hyatt 2021/12/2 14:59
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/api/public")
public class InvoiceWebServiceController extends BaseController {

    @Autowired
    private IInvoiceAllService service;

    @RequestMapping(value = "/invoices")
    @ResponseBody
    public InvoiceResponse getAllInvoice(HttpServletRequest request) {
        IRequest requestContext = createRequestContext(request);
        InvoiceResponse invoiceResponse = new InvoiceResponse();
        try {
            invoiceResponse = service.selectAllInvoice(requestContext);
        } catch (InvoiceException e) {
            invoiceResponse.setSuccess(false);
            invoiceResponse.setStatus(new InvoiceResponse.Status(2, "查询报错"));
            e.printStackTrace();
        }
        return invoiceResponse;
    }

    @RequestMapping(value = "/invoices/update", method = RequestMethod.PUT)
    @ResponseBody
    public InvoiceResponse updateInvoice(HttpServletRequest request, @RequestBody List<InvoiceAll> invoiceAllList) {
        IRequest requestContext = createRequestContext(request);
        InvoiceResponse invoiceResponse = new InvoiceResponse();
        try {
            invoiceResponse = service.updateInvoice(requestContext, invoiceAllList);
        } catch (InvoiceException e) {
            invoiceResponse.setSuccess(false);
            invoiceResponse.setStatus(new InvoiceResponse.Status(2, "更新报错"));
            invoiceResponse.setMessage(e.getValue());
            e.printStackTrace();
        }
        return invoiceResponse;
    }
}

```

##### 测试结果

![image-20211202154632035](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021546637.png)	

![image-20211202154915129](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021549959.png)

![image-20211202160127834](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021601128.png)	

> 发票更新失败后，发现都没有更新成功	

##### 权限验证

> 已查询为例。首先把public去掉

![image-20211202160731584](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021607808.png)	

> 直接查询报错

![image-20211202161231165](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021612061.png)	

##### 获取token

> 首页地址+/oauth/token+参数

![image-20211202161426616](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021614125.png)

##### 添加token

> 添加token之后获取成功

![image-20211202161506656](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021615559.png)	

> 过期之后报错，重新获取token

![image-20211202171201493](https://gitee.com/image_bed/image-bed1/raw/master/img/202112021712775.png)	

## 接口管理平台的使用

### 入站请求和出站请求

> 添加标签@HapInbound和@HapOutbound
>
> 以之前的发票查询为例

![image-20211203141841441](https://gitee.com/image_bed/image-bed1/raw/master/img/202112031418883.png)	

> 调用成功后，查看界面

![image-20211203150056946](https://gitee.com/image_bed/image-bed1/raw/master/img/202112031501156.png)	

![image-20211203150116790](https://gitee.com/image_bed/image-bed1/raw/master/img/202112031501543.png)	

### 接口定义

#### REST 接口类型

> 这里以获取项目为例

![image-20211215143944526](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151439277.png)

> 定义好之后，通过HAP平台调用

![image-20211215144019754](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151440174.png)	

![image-20211215144053509](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151440143.png)	

#### SOAP接口

> 以获取报表信息为例

![image-20211215144411667](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151444486.png)

> 报表调用

![image-20211215144517136](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151445015.png)	

> 输入参数需要转换成json格式的

![image-20211215144548976](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151445013.png)

> 通过日志可以看到最后的报文被转换成xml格式然后拼接的

```xml
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:pub="http://xmlns.oracle.com/oxp/service/PublicReportService">
	<soap:Header/>
	<soap:Body>
		<pub:runReport>
			<pub:reportRequest>
				<pub:attributeFormat>xml</pub:attributeFormat>
				<pub:byPassCache>false</pub:byPassCache>
				<pub:flattenXML>false</pub:flattenXML>
				<pub:parameterNameValues>
					<pub:item>
						<pub:name>from_date</pub:name>
						<pub:values>
							<pub:item>2021-12-10 3:00:00</pub:item>
						</pub:values>
					</pub:item>
				</pub:parameterNameValues>
				<pub:reportAbsolutePath>/A_PaaS/Sync/SyncCustomerXML.xdo</pub:reportAbsolutePath>
				<pub:sizeOfDataChunkDownload>-1</pub:sizeOfDataChunkDownload>
			</pub:reportRequest>
			<!-- 登录Oracle Cloud的账户密码，注意替换 -->
			<pub:userID>PAAS_SYNC</pub:userID>
			<pub:password>PHPrd_123</pub:password>
			<!-- End -->
		</pub:runReport>
	</soap:Body>
</soap:Envelope>
```

> 也可以在调用记录中看到

![image-20211215144908830](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151449507.png)	

### 对接口定义中的接口进行客户化

#### REST接口

> 创建一个类并继承HapTransferDataMapper

```java
package xxap.basis.controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hand.hap.intergration.beans.HapTransferDataMapper;
import net.sf.json.JSONObject;

/**
 * <p>修改项目信息同步的返回值</p>
 *
 * @author hyatt 2021/12/15 15:02
 * @version 1.0
 */
public class RestProjSyncMapper extends HapTransferDataMapper {
    @Override
    public String requestDataMap(JSONObject params) {
        return null;
    }

    @Override
    public String responseDataMap(String params) {
        String header = "{\n" +
                "    \"items\": [";
        String tail = "    ]\n" +
                "}";
        String content = "";
        JsonParser parser = new JsonParser();
        JsonElement rootNode = parser.parse(params);
        if (rootNode.isJsonObject()) {
            JsonObject details = rootNode.getAsJsonObject();
            JsonArray items = details.getAsJsonArray("items");
            for (int i = 0; i < items.size(); i++) {
                JsonObject projectNode = items.get(i).getAsJsonObject();
                JsonElement projectNumber = projectNode.get("ProjectNumber");
                JsonElement projectName = projectNode.get("ProjectName");
                JsonElement projectId = projectNode.get("ProjectId");
                System.out.println("projectNumber:" + projectNumber.getAsString());
                System.out.println("projectName:" + projectName.getAsString());
                System.out.println("projectId:" + projectId.getAsString());
                System.out.println("============================================");
                // 拼接json字符串
                if (!content.equals("")) {
                    content += ",";
                }
                content += "{ \"projectNumber\":" + projectNumber + ",\"projectName\":" + projectName + ",\"projectId\":" + projectId + "}";
            }
        }
        String jsonString = header + content + tail;
        return jsonString;
    }
}
```

> 在接口定义中添加映射类,包括包

![image-20211215151646907](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151516365.png)

> 标准的返回值有很多，例如我们只需要返回项目编号，id和名称

![image-20211215160017938](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151600451.png)	

## Excel文件的导入和导出

### HAP默认方法

> 在前端页面添加上下面的代码即可，导出查询到的所有的数据到Excel中

```html
<!-- 创建一个excel导出按钮-->
<span class="btn btn-primary " style="float:left;margin-right:5px;" data-bind="click:exportExcel"><i class="fa fa-file-excel-o" style="margin-right:3px;"></i><@spring.message "hap.exportexcel"/></span>
```

![image-20211214160253730](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141602197.png)

![image-20211214160645679](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141606872.png)

![image-20211214160406785](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141604344.png)

### 使用kendoUI的grid组件实现导出

> 这个可以给文件命名

```html
toolbar: ["excel"],
excel: {
	fileName: "Invoices.xlsx",
	filterable: true
}
```

![image-20211214165606668](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141656194.png)	

![image-20211214165634817](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141656271.png)

### Excel导入

> 添加按钮

```html
 <!-- 创建一个导入按钮 Hap.importExcel("xxap_invoice_all")参数我表的名称-->
<span class="btn btn-primary  k-grid-excel" style="float:left;" onclick='Hap.importExcel("xxap_invoice_all")' ><i class="fa fa-arrow-circle-up" style="margin-right:3px;"></i><@spring.message "excel.import"/></span>
   
```

> 下载模板，然后填上数据，保存之后上传

![image-20211214171927407](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141719606.png)

![image-20211214171629347](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141716269.png)	

> 查看数据

![image-20211214171844626](https://gitee.com/image_bed/image-bed1/raw/master/img/202112141718662.png)	

# HAP大作业

> 记录一下上面没有做过的一些笔记

## Lov级联

> 客户名称，LOV，显示客户名称，基于客户ID进行关联查询，其中如果选择了公司，客户LOV必须使用公司选定值进行限定

> 公司LOV定义好之后，定义客户的LOV代码

```mysql
SELECT
  hac.CUSTOMER_ID,
  hac.CUSTOMER_NUMBER,
  hac.CUSTOMER_NAME
FROM hap_ar_customers hac,
  hap_org_companys hoc
WHERE 1 = 1
    AND hac.COMPANY_ID = hoc.COMPANY_ID
    <if test="companyId != null">
	AND hac.COMPANY_ID = #{companyId}
    </if>
```

> 在表单中引用lov

```javascript
<script>
	$("#customerId").kendoLov($.extend(<@lov "XXOM_LOV_CUSTOMER"/>, {
		query:function (e) {
			// 查询的时候将companyId取出来作为客户的查询参数
			var companyId = $("#companyId").data("kendoLov").value();
			console.log("companyId: " + companyId);
			e.param['companyId'] = companyId;
		}
	}
	));
</script>
```

> 效果

![image-20211223163530482](https://gitee.com/image_bed/image-bed1/raw/master/img/202112231635356.png)	

![image-20211223163616830](https://gitee.com/image_bed/image-bed1/raw/master/img/202112231636431.png)	

## 代码的引用

> 引用维护的代码作为数据源

```javascript
<!--代码导入 快码-->
<script src="${base.contextPath}/common/code?orderStatus=XXOM_ORDER_STATUS" type="text/javascript"></script>

<script>
	$("#orderStatus").kendoComboBox({
		dataSource: orderStatus,
		valuePrimitive: true,
		dataTextField:"description",
		dataTextValue:"value"
	});
</script>
```

## Tab页签功能

> 注意: 参考HAP的组织架构->组织管理这个页面，然后也简单学习了bootstrap教程
>
> 需要导入bootstrap包

```javascript
<script src="${base.contextPath}/lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

<div id="line" style="clear:both">
	<ul class="nav nav-tabs" id="mytab" style="clear:both">
		<li class="active"><a href="#grid" data-toggle="tab">主要</a></li>
		<li class=""><a href="#other" data-toggle="tab">其他</a></li>
	</ul>
	<div style="clear:both" class="tab-content">
		<div id="grid" class="tab-pane fade in active"></div>
		<div id="other" class="tab-pane fade"></div>
	</div>
</div>
// 添加其他tab页的内容
$("#other").kendoGrid({
	dataSource: dataSource,
	resizable: true,
	scrollable: true,
	navigatable: false,
	selectable: 'multiple, rowbox',
	dataBound: function () {
		if (parent.autoResizeIframe) {
			parent.autoResizeIframe('${RequestParameters.functionCode!}')
		}
	},
	pageable: {
		pageSizes: [5, 10, 20, 50],
		refresh: true,
		buttonCount: 5
	},
	columns: [
		{
			field: "lineNumber",
			title: '<@spring.message "omorderlines.linenumber"/>',
			width: 120
		},
		{
			field: "addition1",
			title: '<@spring.message "omorderlines.addition1"/>',
			width: 120
		},
		{
			field: "addition2",
			title: '<@spring.message "omorderlines.addition2"/>',
			width: 120
		},
		{
			field: "addition3",
			title: '<@spring.message "omorderlines.addition3"/>',
			width: 120
		},
		{
			field: "addition4",
			title: '<@spring.message "omorderlines.addition4"/>',
			width: 120
		},
		{
			field: "addition5",
			title: '<@spring.message "omorderlines.addition5"/>',
			width: 120
		},
	],
	editable: true
});
```

## 表单验证

> 个性化

```javascript
// 所有input输入框为必输
$('#header_form').kendoValidator({
	rules: {
		requiredRule: function(input) {
			return $.trim(input.val())!=="";
		}
	},
	messages: {
		requiredRule: "为必填项"
	}
});
```

## kendoui Grid用法整理

> [(8条消息) Kendo UI Grid 用法详细整理_yzh的博客-CSDN博客_kendogrid](https://blog.csdn.net/qwerdfcv/article/details/103479584)

### 失效字段

```javascript
// 失效lov
$("#customerId").data("kendoLov").enable(false);
// 失效时间组件
$("#orderDate").data("kendoDatePicker").enable(false);
//
$("#rateType").data("kendoComboBox").enable(false);
// .. 其它类似
```

## 列不可编辑

> dataSource -> schema -> model -> fields

```javascript
itemDescription: {
    validation: {
        disabled: true
    }
}
// 这个和 fields 并列
// 这个不可编辑，LOV改变也不会赋值了(后端不允许赋值)
// editable: function (col) {
//     if(col=="itemDescription"){
//         return false
//     }
//     return true;
// }
```

## 行号自动加1

> 在$("#grid").kendoGrid中edit属性中添加

```javascript
edit: function(e) {
    if(e.model.isNew()) {
        var grid = $("#grid").data("kendoGrid");
        var count = grid.dataSource.total();
        console.log('count:' + count);
        $("#grid").find('tr').eq(1).find('td').eq(1).text(count); // 前端给表格列复制
        grid._data[0].lineNumber = count; // 给对应的列赋值，
    }
}
```

## 头行结构保存

> 重新定义了viewModel中的save功能

### 前端相关代码

> 之后的提交按钮，由于执行更改状态，所以功能和保存一样，只要在Hap.submitForm执行修改parentModel.orderStatus的值
>
> ```javascript
> viewModel.parentModel["orderStatus"] = "SUBMITED"
> ```

```javascript
var viewModel = kendo.observable({
            model: {},
            parentModel: {},
            create: function () {
                $("#grid").data('kendoGrid').addRow();
            },
            save: function () {
                if(validator.validate()){
                    if(headerId) {
                        viewModel.parentModel.__status = 'update';
                    } else {
                        viewModel.parentModel.__status = 'add';
                    }
                    Hap.submitForm({
                        url: '${base.contextPath}/hap/om/order/submit',
                        formModel: viewModel.parentModel,
                        grid: {"orderLineList": $("#grid")},
                        success: function (data) {
                            if (data["success"] == true) {
                                var a0 = data.rows[0] || {};
                                kendo.ui.showInfoDialog({
                                    message: $l('hap.tip.success')
                                }).done(function(e) {
                                    // 刷新
                                    $('#grid').data('kendoGrid').dataSource.read();
                                    // 重新获取头信息
                                    // 调用获取数据
                                    $.ajax({
                                        type: 'post',
                                        url: "${base.contextPath}/hap/om/order/headers/edit/query",
                                        data: kendo.stringify({"headerId": headerId}),
                                        dataType: "json",
                                        contentType: "application/json",
                                        success: function (args) {
                                            var a0 = args.rows[0] || {};
                                            // console.log(args);
                                            for (var k in a0) {
                                                viewModel.parentModel.set(k, a0[k]);
                                            }
                                        }
                                    });
                                    viewModel.refresh();
                                });
                            } else {
                                kendo.ui.showInfoDialog({
                                    message: data["message"]
                                });
                            }
                        }
                    });
                }

                // $("#grid").data('kendoGrid').saveChanges();
            },
            query: function (e) {
                $("#grid").data('kendoGrid').dataSource.page(1);
            },
            refresh: function(){
                $("#grid").data('kendoGrid').dataSource.read();
            },
            remove: function () {
                Hap.deleteGridSelection({
                    grid: $("#grid")
                });
            },
            reset: function(){
                var formData = this.model.toJSON();
                for (var k in formData) {
                    this.model.set(k, null);
                }
            },
            cancel:function(){
                $("#grid").data('kendoGrid').cancelChanges();
            },
            exportExcel:function () {
                $("#grid").data('kendoGrid').saveAsExcel();
            },
            exportPDF:function(){
                $("#grid").data('kendoGrid').saveAsPDF();
            },
            orderSave: function() {
                if(headerId) {
                    viewModel.parentModel.__status = 'update';
                } else {
                    viewModel.parentModel.__status = 'add';
                }
                Hap.submitForm({
                    url: '${base.contextPath}/hap/om/order/headers/submit',
                    formModel: viewModel.parentModel,
                    // grid: {"prjInvoiceLinesList": $("#grid")},
                    success: function (data) {
                        if (data["success"] == true) {
                            var a0 = data.rows[0] || {};
                            // console.log(a0);
                            kendo.ui.showInfoDialog({
                                message: $l('hap.tip.success')
                            });
                        } else {
                            kendo.ui.showInfoDialog({
                                message: data["message"]
                            });
                        }
                    }
                });
            }
        });

// 表单验证
var validator = $("#header").kendoValidator({
	valid: function (e) {
	},
	invalidMessageType: "tooltip"
}).data("kendoValidator");
```

### 后端相关代码

> OmOrderHeadersController.java

```java
@Autowired
private IOmOrderHeadersService service;

@RequestMapping(value = "/hap/om/order/submit")
@ResponseBody
public ResponseData orderUpdate(@RequestBody List<OmOrderHeaders> omOrderHeadersList, HttpServletRequest request) {
    IRequest requestCtx = createRequestContext(request);
    try {
        return new ResponseData(service.saveOrder(requestCtx, omOrderHeadersList));
    }catch (Exception e) {
        e.printStackTrace();
    }
    // 异常类暂时没有添加
    return null;
}
```

> 对应的接口和接口实现类

```java
List<OmOrderHeaders> saveOrder(IRequest request, @StdWho List<OmOrderHeaders> omOrderHeadersList) throws Exception;

@Override
public List<OmOrderHeaders> saveOrder(IRequest request, @StdWho List<OmOrderHeaders> omOrderHeadersList) throws Exception {
	for (OmOrderHeaders omOrderHeaders : omOrderHeadersList) {
		if (omOrderHeaders.getHeaderId() == null) {
			insertOrder(request, omOrderHeaders);
			continue;
		}
		updateOrder(request, omOrderHeaders);
	}
	return omOrderHeadersList;
}

/**
 * 更新
 *
 * @param request
 * @param omOrderHeaders
 */
private OmOrderHeaders updateOrder(IRequest request, OmOrderHeaders omOrderHeaders) {
	updateByPrimaryKey(request, omOrderHeaders);
	if (omOrderHeaders.getOrderLineList() != null) {
		saveOrderLines(request, omOrderHeaders);
	}
	return omOrderHeaders;
}

/**
 * @param request
 * @param omOrderHeader
 */
private OmOrderHeaders insertOrder(IRequest request, OmOrderHeaders omOrderHeader) {
	insertSelective(request, omOrderHeader);
	if (omOrderHeader.getOrderLineList() != null) {
		saveOrderLines(request, omOrderHeader);
	}
	return omOrderHeader;
}

/**
 * 保存订单行
 *
 * @param request
 * @param omOrderHeader
 */
private void saveOrderLines(IRequest request, OmOrderHeaders omOrderHeader) {
	for (OmOrderLines omOrderLines : omOrderHeader.getOrderLineList()) {
		if (omOrderLines.getLineId() == null) {
			omOrderLines.setHeaderId(omOrderHeader.getHeaderId());
			omOrderLines.setCompanyId(omOrderHeader.getCompanyId());
			this.linesService.insertSelective(request, omOrderLines);
		} else {
			this.linesService.updateByPrimaryKey(request, omOrderLines);
		}
	}
}
```

## PDF预览

> 参考：[(8条消息) vue文件流转换成pdf预览(pdf.js+iframe)&&使用vue-pdf实现pdf预览_乐YB的博客-CSDN博客](https://blog.csdn.net/m0_48469964/article/details/117566371?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-1.pc_relevant_paycolumn_v2&spm=1001.2101.3001.4242.2&utm_relevant_index=4)

> 直接返回pdf流没有做成功
>
> 现在服务器上生成文件，然后用服务器上地址去打开

### 后端

```java
@RequestMapping(value = "/hap/om/order/lines/print", method = RequestMethod.GET)
@ResponseBody
public ResponseData pagePrint(HttpServletRequest request, @RequestParam String headerId, HttpServletResponse response) {
	System.out.println("headerId: " + headerId);

	ResponseData rd = new ResponseData();
	rd.setSuccess(true);
	DataSource sqlDataSource = ds.getDataSource();

	IRequest requestContext = createRequestContext(request);
	String rootPath = profileService.getProfileValue(requestContext, "REPORT_ROOT_DIR");
	// String imageRoot = profileService.getProfileValue(requestContext, "IMAGE_ROOT_DIR");
	// String filePath = rootPath + "\\reports\\xxom\\deliveryBillReport2.jasper";
	String filePath = "D:\\hand\\work\\HAP_Study\\HAP_Projects\\HbiTemplate\\core\\src\\main\\java\\xxom\\biz\\reports\\PO_PRINT.jasper";
	System.out.println("filePath: " + filePath);
	filePath = filePath.replace("\\", File.separator);
	System.out.println("filePath: " + filePath);
//        String filePath1 = rootPath + "\\reports\\xxom\\deliveryBillReport3.jasper";
//        filePath1 = filePath1.replace("\\", File.separator);

//        String imagePath = imageRoot + "\\SinexcelLogo.png";
//        imagePath = imagePath.replace("\\", File.separator);
	String errMsg = "";
	try {
		// String serialNumber = sysCodeRuleProcessService.getRuleCode("XXPCG_CONT_SERIAL_NUMBER");
		File repFile = new File(filePath);
		String absolutePath = repFile.getAbsolutePath();
		logger.info("absolutePath: ", absolutePath);
		InputStream reportStream = new FileInputStream(repFile);
		Map<String, Object> jasperParameter = new HashMap();

		jasperParameter.put("headerId", Long.parseLong(headerId));
		// jasperParameter.put("imagePath", imagePath);
		// jasperParameter.put("serialNumber", serialNumber);
		JasperPrint jasperPrint = JasperFillManager.fillReport(reportStream, jasperParameter, sqlDataSource.getConnection());
//            response.setContentType("application/pdf");
//            // response.setHeader("Content-disposition", "inline; filename=order.pdf");
//            response.setHeader("Access-Control-Allow-Origin", "*");
//            OutputStream outStream = response.getOutputStream();
//            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

		// 生成pdf文件
		byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
		String realPath = request.getServletContext().getRealPath("/resources");
		String pdfPath = realPath +"/tmp.pdf";
		System.out.println(realPath);
		FileOutputStream fos = new FileOutputStream(pdfPath);
		fos.write(bytes);
		fos.flush();
		fos.close();
		// FileWriter fw = new FileWriter(new FileOutputStream("tmp.pdf"));

//            outStream.flush();
//            outStream.close();
	} catch (Exception e) {
		e.printStackTrace();
		errMsg = e.getMessage();
		rd.setMessage("打印失败");
		rd.setSuccess(false);
		return rd;
	}

	return rd;
}
```

> 获取dataSource

```java
@Autowired
private HapDBDataSource ds;
```

> HapDBDataSource

```java
package hap.common.components;

import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2021/12/30 14:18
 * @version 1.0
 */

@Component
public class HapDBDataSource {
    private static final Logger logger = LoggerFactory.getLogger(hap.common.components.HapDBDataSource.class);

    private static final String DATABASE_MYSQL = "mysql";

    private static final String DATABASE_ORACLE = "oracle";

    private static final String DATABASE_MSSQL = "mssql";

    private String dbType = "mysql";

    private DataSource dataSource;

    @Autowired
    @Qualifier("sqlSessionFactory")
    SqlSessionFactory sqlSessionFactory;

    @Autowired
    public HapDBDataSource(@Qualifier("dataSource") DataSource dataSource) {
        this.dataSource = dataSource;
        try (Connection conn = DataSourceUtils.getConnection(dataSource)) {
            DatabaseMetaData meta = conn.getMetaData();
            this.dbType = meta.getDatabaseProductName().toLowerCase();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public DataSource getDataSource() {
        return this.dataSource;
    }

    public SqlSessionFactory getSqlSessionFactory() {
        return this.sqlSessionFactory;
    }
}
```

### 前端

```javascript
span class="btn btn-default" id="print" onclick="print(this)" style="float:left;margin-right:5px;"><@spring.message "omorderheaders.print"/></span>

/**
 * pdf打印
 * @param e
 */
function print(e) {
	$.ajax({
		type: 'get',
		url: "${base.contextPath}/hap/om/order/lines/print",
		data: "headerId="+headerId,
		success: function (res) {
			// console.log(new Blob([res]);
			var urlPdf = "${base.contextPath}/resources/tmp.pdf";
			// console.log(urlPdf);
			// console.log(encodeURIComponent(urlPdf));

			var onClose = function () {
				$("#file_preview").empty();
			}

			$("#file_preview").kendoWindow({
				actions: ["Close"],
				title: "PDF预览",
				draggable: true,
				height: "600px",
				width: "800px",
				close: onClose,
				content: '${base.contextPath}/resources/js/pdfJs/build/generic/web/viewer.html?file=' + encodeURIComponent(urlPdf),
				iframe: true,
				modal: true
			});
			var win = $("#file_preview").data("kendoWindow");
			win.center().open();
		}
	});
}
```

### 展示

![image-20211231172342737](https://gitee.com/image_bed/image-bed1/raw/master/img/202112311723996.png)	

## PDF预览优化

> 主要修改两个地方：
>
> 1. 报表地址通过配置文件获取
>    1. 之后可以添加一个上传按钮，上传模板到配置文件指定的路径下，然后再用配置文件获取
> 2. 生成的报表文件tmp.pdf的地址，回传到前端，然后解析

### 配置文件定义

![image-20220104154001061](https://gitee.com/image_bed/image-bed1/raw/master/img/202201041540256.png)	

### 后端

```java
@RequestMapping(value = "/hap/om/order/lines/print", method = RequestMethod.GET)
@ResponseBody
public ResponseData pagePrint(HttpServletRequest request, @RequestParam String headerId, HttpServletResponse response) {
    System.out.println("headerId: " + headerId);

    ResponseData rd = new ResponseData();
    rd.setSuccess(true);
    DataSource sqlDataSource = ds.getDataSource();
    // 获取模板文件
    IRequest requestContext = createRequestContext(request);
    String rootPath = profileService.getProfileValue(requestContext, "REPORT_ROOT_DIR");
    String realRootPath = request.getServletContext().getRealPath(rootPath);
    String tmplatePath = realRootPath + "\\xxom\\templates\\PO_PRINT.jasper";
    System.out.println("tmplatePath: " + tmplatePath);
    String errMsg = "";
    try {
        /**
         * 获取模板文件
         */
        File repFile = new File(tmplatePath);
        String absolutePath = repFile.getAbsolutePath();
        logger.info("absolutePath: ", absolutePath);
        InputStream reportStream = new FileInputStream(repFile);
        /**
         * 调用报表模板
         */
        Map<String, Object> jasperParameter = new HashMap();
        jasperParameter.put("headerId", Long.parseLong(headerId));
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportStream, jasperParameter, sqlDataSource.getConnection());
        /**
         * 生成pdf报表文件, 并写入临时文件
         */
        byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        String pdfPath = realRootPath +"\\xxom\\results\\tmp.pdf";
        System.out.println(pdfPath);
        FileOutputStream fos = new FileOutputStream(pdfPath);
        fos.write(bytes);
        fos.flush();
        fos.close();
        // 将地址回传给前端
        response.setHeader("Access-Control-Allow-Origin", "*");
        OutputStream outputStream = response.getOutputStream();
        // 相对路径
        String pdfPathRelative = rootPath + "\\xxom\\results\\tmp.pdf";
        // 相对路径回传给前端
        outputStream.write(pdfPathRelative.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    } catch (Exception e) {
        e.printStackTrace();
        errMsg = e.getMessage();
        rd.setMessage("打印失败");
        rd.setSuccess(false);
        return rd;
    }

    return rd;
}
```

### 前端

```javascript
/**
 * pdf打印
 * @param e
 */
function print(e) {
    $.ajax({
        type: 'get',
        url: "${base.contextPath}/hap/om/order/lines/print",
        data: "headerId="+headerId,
        success: function (res) {
            console.log(res);
            // var urlPdf = "${base.contextPath}/resources/reports/xxom/results/tmp.pdf";
            var urlPdf = "${base.contextPath}" + res;
            console.log(urlPdf);
            var onClose = function () {
                $("#file_preview").empty();
            }
            $("#file_preview").kendoWindow({
                actions: ["Close"],
                title: "PDF预览",
                draggable: true,
                height: "600px",
                width: "800px",
                close: onClose,
                content: '${base.contextPath}/resources/js/pdfJs/build/generic/web/viewer.html?file=' + encodeURIComponent(urlPdf),
                iframe: true,
                modal: true
            });
            var win = $("#file_preview").data("kendoWindow");
            win.center().open();
        }
    });
}
```

## 创建角色

> 配置文件设置层级值的时候需要提供角色
>
> 系统管理 -> 账户管理 -> 角色管理

![image-20220106095216750](https://gitee.com/image_bed/image-bed1/raw/master/img/202201060954199.png)	

## 整单删除

> 这里通过将头行建立级联关系来实现在删除头的时候，自动删除行

### 创建级联关系

> 在创建外键的时候，发现问题，所以这种只能在删除头的时候再去调用行的删除方法

![image-20220106103517809](https://gitee.com/image_bed/image-bed1/raw/master/img/202201061035743.png)	

## 获取配置文件的值

> 获取配置文件的时候在pdf预览的时候遇到过，但是那个方法不能用在一个用户有多个角色的情况下

```mysql
-- profileService.getProfileValue(requestContext, "REPORT_ROOT_DIR");
-- 后台查询
-- 通过sql可以看到这个用户如果有多个角色的话，是有问题的
SELECT SPV.PROFILE_VALUE_ID,
   SPV.PROFILE_ID,
   SPV.LEVEL_ID,
   SPV.LEVEL_VALUE,
   SPV.PROFILE_VALUE
FROM SYS_PROFILE_VALUE SPV,
   SYS_PROFILE SP
WHERE SPV.PROFILE_ID = SP.PROFILE_ID
AND SP.PROFILE_NAME =  'HAP_OM_ORDER_SUBMIT_CTL'
/*
AND ((SPV.LEVEL_ID = '30' AND SPV.LEVEL_VALUE = #{request.userId,jdbcType=DECIMAL,javaType=java.lang.Long})
   OR (SPV.LEVEL_ID = '20' AND SPV.LEVEL_VALUE = #{request.roleId,jdbcType=DECIMAL,javaType=java.lang.Long})
   OR (SPV.LEVEL_ID = '10')) */
ORDER BY SPV.LEVEL_ID DESC;

```

> 换一个方法

```mysql
/**
 * 根据配置文件的名字/用户，查找用户在该配置文件下的值. 优先顺序 用户>角色>全局 若当前用户 在 用户、角色、全局三层 均没有值，返回 null
 *
 * @param userId 用戶Id
 * @param profileName 配置文件名字
 * @return 配置文件值
 */
String getValueByUserIdAndName(Long userId, String profileName);
-- profileService.getValueByUserIdAndName(userId, ProfileResults.HAP_OM_ORDER_SUBMIT_CTL);
-- 后台查询
SELECT
<include refid="Base_Column_List"/>
FROM SYS_PROFILE_VALUE T
WHERE PROFILE_ID = #{profileId,jdbcType=DECIMAL}
AND ((LEVEL_ID='30' AND LEVEL_VALUE = #{userId,jdbcType=DECIMAL}) OR
(LEVEL_ID='20' AND LEVEL_VALUE IN (
    SELECT ROLE_ID FROM SYS_USER_ROLE
     WHERE USER_ID = #{userId,jdbcType=DECIMAL})) OR LEVEL_ID='10' )
ORDER BY LEVEL_ID DESC,LEVEL_VALUE
```

## 前端判断

> 1. 格式失效
> 2. 功能失效
>    1. 因为是span类型的，加上disabled属性并不会失效这个span标签的按钮功能(所以可以直接用button标签)
>    2. 暂时是在功能上也加入类似的判断然后终止功能
> 3. 由于配置文件的值是通过ajax获取的，所以需要等待ajax执行完成后在进行判断

```javascript
/**
 * 判断按钮是否有效
 */
function getProfiles() {
    // 获取配置文件的值, 然后判断按钮是否有效
    $.ajax({
        type: 'get',
        url: "${base.contextPath}/hap/om/order/profiles",
        // data: kendo.stringify({"headerId": headerId}),
        dataType: "json",
        contentType: "application/json",
        success: function (args) {
            console.log(args);
            var a0 = args.rows[0] || {};
            for (var k in a0) {
                profiles[k] = a0[k];
            }
            console.log("getProfiles: "+ profiles);
        }
    }).done(function (){
        // 根据配置文件设置css属性
        isActive("submit");
        isActive("approve");
        isActive("reject");
    });
}
```

> 主要是更改disabled的属性和给功能一个判断结果

```javascript
/**
 * 按钮是否可用
 */
function isActive(button) {
    console.log(profiles);
    if(!headerId) {
        console.log("什么都不做");
        return false;
    } else {
        if(button==="submit") {
            if(profiles["orderSubmitCtl"]!=='Y') {
                $('#submit').attr("disabled", true);
                console.log("失效提交");
                return false;
            }
        }
        //
        if(button==="approve") {
            if(profiles["orderApproveCtl"]!=='Y') {
                $('#approve').attr("disabled", true);
                return false;
            }
        }
    }
    return true;
}
```

> 例如在提交执行之前，假如如下代码

```javascript
var bool = isActive("submit");
if(!bool) {
    return;
}
```

# 未结问题

### 配置JNDI数据源

> 多个HAP项目在本地运行时如何配置JNDI数据源

# 已结问题

### 查看tomcat版本报错

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/clip_image002.jpg)	

> 问题解决

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/clip_image002-16371165717891.jpg)	



