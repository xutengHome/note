# Gitee的使用

## IDEA上传代码至Gitee

> [idea上传项目到gitee（码云）超详细_『愚』的博客-CSDN博客_idea上传项目到gitee](https://blog.csdn.net/weixin_43883917/article/details/111085602)

> 在上述博客中遇到一个问题

![在这里插入图片描述](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181756222.png)	

> 博客上是输入两个指令：
>
> **git pull origin master –allow-unrelated-histories**
>
> **git push -u origin master -f**
>
> 我自己在使用的过程中发现
>
> 执行：git pull origin master –allow-unrelated-histories 报错
>
> ![image-20220118175720801](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181757876.png)	
>
> 后来执行代码：git pull --rebase origin master之后，再次去push就可以了