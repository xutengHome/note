

# SSM

> Spring + SpringMVC + Mybatis

## Spring

> Spring是分层的 Java SE/EE应用 full-stack 轻量级开源框架，以 IoC（Inverse Of Control：反转控制）和AOP（Aspect Oriented Programming：面向切面编程）为内核。
>
> 提供了**展现层 SpringMVC** 和**持久层 Spring JDBCTemplate** 以及**业务层事务管理**等众多的企业级应用技术，还能整合开源世界众多著名的第三方框架和类库，逐渐成为使用最多的Java EE 企业应用开源框架

### Spring的优势

- 方便解耦，简化开发
  - 通过 Spring 提供的 IOC容器，可以将对象间的依赖关系交由 Spring 进行控制，避免硬编码所造成的过度耦合。用户也不必再为单例模式类、属性文件解析等这些很底层的需求编写代码，可以更专注于上层的应用
- AOP 编程的支持
  - 通过 Spring的 AOP 功能，方便进行面向切面编程，许多不容易用传统 OOP 实现的功能可以通过 AOP 轻松实现
- 声明式事务的支持
  - 可以将我们从单调烦闷的事务管理代码中解脱出来，通过声明式方式灵活的进行事务管理，提高开发效率和质量。
- 方便程序的测试
  - 可以用非容器依赖的编程方式进行几乎所有的测试工作，测试不再是昂贵的操作，而是随手可做的事情。

- 方便集成各种优秀框架
  - Spring对各种优秀框架（Struts、Hibernate、Hessian、Quartz等）的支持。
- 降低 JavaEE API 的使用难度
  - Spring对 JavaEE API（如 JDBC、JavaMail、远程调用等）进行了薄薄的封装层，使这些 API 的使用难度大为降低
- Java 源码是经典学习范例
  - Spring的源代码设计精妙、结构清晰、匠心独用，处处体现着大师对Java 设计模式灵活运用以及对 Java技术的高深造诣。它的源代码无意是 Java 技术的最佳实践的范例。

### Spring的体系结构

![image-20220303162102993](https://gitee.com/image_bed/image-bed1/raw/master/img/202203031621800.png)	

### Spring快速入门

#### Spring程序开发步骤

> 1. 导入 Spring 开发的基本包坐标
> 2. 编写 Dao 接口和实现类
> 3. 创建 Spring 核心配置文件
> 4. 在 Spring 配置文件中配置 UserDaoImpl
> 5. 使用 Spring 的 API 获得 Bean 实例

##### 使用maven创建一个空项目

![image-20220303162510647](https://gitee.com/image_bed/image-bed1/raw/master/img/202203031625825.png)	

##### 导入 Spring 开发的基本包坐标

```xml
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <spring.version>5.0.5.RELEASE</spring.version>
</properties>

<dependencies>
    <!--导入spring的context坐标，context依赖core、beans、expression-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>${spring.version}</version>
    </dependency>
</dependencies>
```

##### 编写 Dao 接口和实现类

```java
package com.itcast.dao;

/**
 * <p>接口描述</p>
 *
 * @author hyatt 2022/3/3 16:32
 * @version 1.0
 */
public interface UserDao {
    void save();
}
```

```java
package com.itcast.dao.impl;

import com.itcast.dao.UserDao;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/3/3 16:33
 * @version 1.0
 */
public class UserDaoImpl implements UserDao {
    @Override
    public void save() {
        System.out.println("save .....");
    }
}
```

##### 创建 Spring 核心配置文件

> 文件名一般为：applicationContext.xml

![image-20220303163419057](https://gitee.com/image_bed/image-bed1/raw/master/img/202203031634878.png)	

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    
</beans>
```

##### 在 Spring 配置文件中配置 UserDaoImpl

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl"></bean>
</beans>
```

##### 使用 Spring 的 API 获得 Bean 实例

```java
@Test
public void testUserDao() {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    UserDao userDao = (UserDao) applicationContext.getBean("userDao");
    userDao.save();
}
```

### Spring配置文件

#### Bean标签基本配置

> 默认情况下它调用的是类中的无参构造函数，如果没有无参构造函数则不能创建成功。

##### 基本属性

- id：Bean实例在Spring容器中的唯一标识
- class：Bean的全限定名称

#### Bean标签范围配置

> scope：指对象的作用范围

| 取值范围       | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| singleton      | 默认值，单例的                                               |
| prototype      | 多例的                                                       |
| request        | WEB 项目中，Spring 创建一个 Bean 的对象，将对象存入到 request 域中 |
| session        | WEB 项目中，Spring 创建一个 Bean 的对象，将对象存入到 session 域中 |
| global session | WEB 项目中，应用在 Portlet 环境，如果没有 Portlet 环境那么globalSession 相当于 session |

##### scope的取值为singleton

- Bean的实例化个数：1个(**可以通过打印两个对象的地址可以看到**)
- Bean的实例化时机：当Spring核心文件被加载时，实例化配置的Bean实例(**可以通过debug看到**)
- Bean的生命周期：
  - 对象创建：当应用加载，创建容器时，对象就被创建了
  - 对象运行：只要容器在，对象一直活着
  - 对象销毁：当应用卸载，销毁容器时，对象就被销毁了

##### scope的取值为prototype

- Bean的实例化个数：多个(**可以通过打印两个对象的地址可以看到**)
- Bean的实例化时机：当调用getBean()方法时实例化Bean(**可以通过debug看到**)
  - 对象创建：当使用对象时，创建新的对象实例
  - 对象运行：只要对象在使用中，就一直活着
  - 对象销毁：当对象长时间不用时，被 Java 的垃圾回收器回收了

#### Bean生命周期配置

- init-method：指定类中的初始化方法名称
- destroy-method：指定类中销毁方法名称

```java
@Test
public void testUserDao2() {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    UserDao userDao = (UserDao) applicationContext.getBean("userDao");
    applicationContext.close();
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>
</beans>
```

#### Bean实例化三种方式

> - 无参构造方法实例化
> - 工厂静态方法实例化
> - 工厂实例方法实例化

##### 无参构造方法实例化

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 无参构造实例化-->
    <bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>
</beans>
```

##### 工厂静态方法实例化

```java
package com.itcast.factory;

import com.itcast.dao.UserDao;
import com.itcast.dao.impl.UserDaoImpl;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/3/3 16:52
 * @version 1.0
 */
public class UseDaoStaticFactory {
    public static UserDao getUserDao() {
        return new UserDaoImpl();
    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 无参构造实例化-->
    <!-- <bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>-->
    <!-- 工厂静态方法实例化 -->
    <bean id="userDao" class="com.itcast.factory.UseDaoStaticFactory" factory-method="getUserDao"></bean>
</beans>
```

##### 工厂实例方法实例化

```java
package com.itcast.factory;

import com.itcast.dao.UserDao;
import com.itcast.dao.impl.UserDaoImpl;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/3/3 16:52
 * @version 1.0
 */
public class UseDaoFactory {
    /**
     * 不是静态方法
     * @return
     */
    public UserDao getUserDao() {
        return new UserDaoImpl();
    }
}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 无参构造实例化-->
    <!-- <bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>-->
    <!-- 工厂静态方法实例化 -->
    <!-- <bean id="userDao" class="com.itcast.factory.UseDaoStaticFactory" factory-method="getUserDao"></bean>-->
    <!-- 工厂实例方法实例化-->
    <bean id="factory" class="com.itcast.factory.UseDaoFactory"></bean>
    <bean id="userDao" factory-bean="factory" factory-method="getUserDao"></bean>
</beans>
```

#### Bean的依赖注入

> - 创建 UserService，UserService 内部在调用 UserDao的save() 方法
> - 将 UserServiceImpl 的创建权交给 Spring
> - 从 Spring 容器中获得 UserService 进行操作

> 创建 UserService，UserService 内部在调用 UserDao的save() 方法

```java
public class UserServiceImpl implements UserService {
    @Override
    public void save() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = (UserDao) applicationContext.getBean("userDao");
        userDao.save();
    }
}
```

> 将 UserServiceImpl 的创建权交给 Spring

```xml
<bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>
<bean id="userService" class="com.itcast.service.impl.UserServiceImpl"></bean>
```

> 从 Spring 容器中获得 UserService 进行操作

```java
@Test
public void testUserService() {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    UserService userService = (UserService) applicationContext.getBean("userService");
    userService.save();
}
```

> 注意：这里其实是加载了两次配置文件

#### Bean的依赖注入分析

> 目前UserService实例和UserDao实例都存在与Spring容器中，当前的做法是在容器外部获得UserService实例和UserDao实例，然后在程序中进行结合。

![image-20220304111802096](https://gitee.com/image_bed/image-bed1/raw/master/img/202203041118045.png)	

> 因为UserService和UserDao都在Spring容器中，而最终程序直接使用的是UserService，所以可以在Spring容器中，将UserDao设置到UserService内部

![image-20220304111824942](https://gitee.com/image_bed/image-bed1/raw/master/img/202203041118603.png)	

#### Bean的依赖注入概念

> 依赖注入（Dependency Injection）：它是 Spring 框架核心 IOC 的具体实现。
>
> 在编写程序时，通过控制反转，把对象的创建交给了 Spring，但是代码中不可能出现没有依赖的情况。
> IOC 解耦只是降低他们的依赖关系，但不会消除。例如：业务层仍会调用持久层的方法。
> 那这种业务层和持久层的依赖关系，在使用 Spring 之后，就让 Spring 来维护了。
> 简单的说，就是坐等框架把持久层对象传入业务层，而不用我们自己去获取。

#### Bean的依赖注入方式

> - 构造方法
> - set方法

##### Set方法注入

> 在UserServiceImpl中添加setUserDao方法

```java
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void save() {
       this.userDao.save();
    }
}
```

> 配置Spring容器调用set方法进行注入
>
> 属性说明：
>
> - property-name: 表示set后面的名称，然后第一个字符变小写(**注意不是定义的变量名称**)
> - property-ref: 表示需要注入的bean的id名称

```xml
<!--依赖注入-->
<bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl"></bean>
<bean id="userService" class="com.itcast.service.impl.UserServiceImpl">
    <property name="userDao" ref="userDao"></property>
</bean>
```

> 测试代码不变，直接获取UserService的bean去调用save即可。
>
> 注意：**这里直接通过构造方法获取会报空指针异常，因为没有给UserDao创建对象引用**

##### P命名空间注入

> P命名空间注入本质也是set方法注入，但比起上述的set方法注入更加方便，主要体现在配置文件中

> 首先，需要引入P命名空间

```xml
xmlns:p="http://www.springframework.org/schema/p"
```

> 其次，需要修改注入方式

```xml
<!--依赖注入-->
<bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl"></bean>
<bean id="userService" class="com.itcast.service.impl.UserServiceImpl" p:userDao-ref="userDao"/>
```

##### 构造方法注入

> 创建有参构造，并注释掉set方法

```java
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void save() {
       this.userDao.save();
    }
}
```

> 配置Spring容器调用有参构造时进行注入
>
> 属性说明
>
> - name: 构造方法中参数的名称
> - ref：bean的id

```xml
<!--依赖注入-->
<bean id="userDao" class="com.itcast.dao.impl.UserDaoImpl"></bean>
<!--构造方法注入-->
<bean id="userService" class="com.itcast.service.impl.UserServiceImpl">
    <constructor-arg name="userDao" ref="userDao"></constructor-arg>
</bean>
```

#### Bean的依赖注入的数据类型

> - 普通数据类型
> - 引用数据类型
> - 集合数据类型

##### 普通数据类型

> 创建User类

```java
public class User {
    public String name;
    public int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
```

> 配置Spring

```xml
<bean id="user" class="com.itcast.domain.User">
    <property name="name" value="zhangsan"></property>
    <property name="age" value="20"></property>
</bean>
```

> 测试

```java
@Test
public void testUser() {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    User user = (User) applicationContext.getBean("user");
    System.out.println(user); // User{name='zhangsan', age=20}
}
```

##### 集合数据类型

> 修改User类

```java
package com.itcast.domain;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/3/4 15:38
 * @version 1.0
 */
public class User {
    private String name;
    private int age;
    private List<String> hobbies;
    private List<User> userList;
    private Map<String, User> userMap;
    private Properties properties;

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public void setUserMap(Map<String, User> userMap) {
        this.userMap = userMap;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", hobbies=" + hobbies +
                ", userList=" + userList +
                ", userMap=" + userMap +
                ", properties=" + properties +
                '}';
    }
}
```

> 配置Spring上下文文件

```xml
<bean id="user1" class="com.itcast.domain.User">
    <property name="name" value="zhangsan"></property>
    <property name="age" value="20"></property>
</bean>

<bean id="user2" class="com.itcast.domain.User">
    <property name="name" value="lisi"></property>
    <property name="age" value="30"></property>
</bean>

<bean id="user" class="com.itcast.domain.User">
    <property name="name" value="wangwu"></property>
    <property name="age" value="20"></property>
    <property name="hobbies">
        <list>
            <value>math</value>
            <value>sing</value>
            <value>game</value>
        </list>
    </property>
    <property name="userList">
        <list>
            <!--创建两个空的User对象-->
            <bean class="com.itcast.domain.User"></bean>
            <bean class="com.itcast.domain.User"></bean>
            <!--将user1和2添加至列表中-->
            <ref bean="user1"></ref>
            <ref bean="user2"></ref>
        </list>
    </property>
    <property name="userMap">
        <map>
            <entry key="student" value-ref="user1"></entry>
            <entry key="teacher" value-ref="user2"></entry>
        </map>
    </property>
    <property name="properties">
        <props>
            <prop key="p1">aaa</prop>
            <prop key="p2">bbb</prop>
            <prop key="p3">ccc</prop>
        </props>
    </property>
</bean>
```

> 测试代码不变，输入User

#### 引入其他配置文件

> 实际开发中，Spring的配置内容非常多，这就导致Spring配置很繁杂且体积很大，所以，可以将部分配置拆解到其他配置文件中，而在Spring主配置文件通过import标签进行加载

```xml
<import resource="applicationContext-xxx.xml"/>
```

### Spring相关API

#### ApplicationContext的继承体系

> applicationContext：接口类型，代表应用上下文，可以通过其实例获得 Spring 容器中的 Bean 对象

![image-20220304165559037](https://gitee.com/image_bed/image-bed1/raw/master/img/202203041656290.png)	

#### ApplicationContext的实现类

- ClassPathXmlApplicationContext
  - 它是从类的根路径下加载配置文件 推荐使用这种
- FileSystemXmlApplicationContext
  - 它是从磁盘路径上加载配置文件，配置文件可以在磁盘的任意位置。
- AnnotationConfigApplicationContext
  - 当使用注解配置容器对象时，需要使用此类来创建 spring 容器。它用来读取注解。

#### getBean()方法使用

> - 当参数的数据类型是字符串时，表示根据Bean的id从容器中获得Bean实例，返回是Object，需要强转
> - 当参数的数据类型是Class类型时，表示根据类型从容器中匹配Bean实例，当容器中相同类型的Bean有多个时，则此方法会报错

```java
public Object getBean(String name) throws BeansException {
    assertBeanFactoryActive();
    return getBeanFactory().getBean(name);
}
public <T> T getBean(Class<T> requiredType) throws BeansException {
    assertBeanFactoryActive();
    return getBeanFactory().getBean(requiredType);
}
```

```java
ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
UserService userService1 = (UserService) applicationContext.getBean("userService");
UserService userService2 = applicationContext.getBean(UserService.class);
```

### Spring配置数据源

#### 开发步骤

> - 导入数据源的坐标和数据库驱动坐标
> - 创建数据源对象
> - 设置数据源的基本连接数据
> - 使用数据源获取连接资源和归还连接资源

##### 导入数据源的坐标和数据库驱动坐标

```xml
<dependencies>
    <!--导入c3p0数据源-->
    <dependency>
        <groupId>c3p0</groupId>
        <artifactId>c3p0</artifactId>
        <version>0.9.1.1</version>
    </dependency>
    <!--导入Druid数据源-->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.1.10</version>
    </dependency>
    <!--mysql驱动-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>
    <!--测试使用-->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.11</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

##### 创建数据源对象

```java
/**
 * C3p0测试
 */
@Test
public void test() throws Exception {
    // 创建数据源
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    // 设置数据库连接参数
    comboPooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
    comboPooledDataSource.setJdbcUrl("jdbc:mysql://localhost:3306/db1");
    comboPooledDataSource.setUser("root");
    comboPooledDataSource.setPassword("123456");
    // 获取连接对象
    Connection connection = comboPooledDataSource.getConnection();
    System.out.println(connection);

}

/**
 * druid测试
 */
@Test
public void test2() throws Exception {
    // 创建数据源
    DruidDataSource druidDataSource = new DruidDataSource();
    // 设置数据库连接参数
    druidDataSource.setDriverClassName("com.mysql.jdbc.Driver");
    druidDataSource.setUrl("jdbc:mysql://localhost:3306/db1");
    druidDataSource.setUsername("root");
    druidDataSource.setPassword("123456");
    // 获取连接对象
    Connection connection = druidDataSource.getConnection();
    System.out.println(connection);
}
```

##### 设置数据源的基本连接数据

> 提取jdbc.properties配置文件

```properties
jdbc.driver=com.mysql.jdbc.Driver
# WARN: Establishing SSL connection without server's identity verification is not recommended.
jdbc.url=jdbc:mysql://localhost:3306/db1?useSSL=false
jdbc.username=root
jdbc.password=123456
```

> 测试代码

```java
/**
 * C3p0测试 - 引用配置文件
 */
@Test
public void test3() throws Exception {
    // 加载类路径下的jdbc.properties
    ResourceBundle resourceBundle = ResourceBundle.getBundle("jdbc");
    // 创建数据源
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    // 获取配置文件中的值,并设置连接参数
    comboPooledDataSource.setDriverClass(resourceBundle.getString("jdbc.driver"));
    comboPooledDataSource.setJdbcUrl(resourceBundle.getString("jdbc.url"));
    comboPooledDataSource.setUser(resourceBundle.getString("jdbc.username"));
    comboPooledDataSource.setPassword(resourceBundle.getString("jdbc.password"));
    // 获取连接对象
    Connection connection = comboPooledDataSource.getConnection();
    System.out.println(connection);
}
```

#### Spring容器配置数据源

> - DataSource有无参构造方法，而Spring默认就是通过无参构造方法实例化对象的
> - DataSource要想使用需要通过set方法设置数据库连接信息，而Spring可以通过set方法进行字符串注入

> 导入包

```xml
<properties>
	<maven.compiler.source>8</maven.compiler.source>
	<maven.compiler.target>8</maven.compiler.target>
	<spring.version>5.0.5.RELEASE</spring.version>
</properties>
<!--导入spring的context坐标，context依赖core、beans、expression-->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${spring.version}</version>
</dependency>
```

> 创建配置文件：applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="driverClass" value="com.mysql.jdbc.Driver"></property>
        <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/db1"></property>
        <property name="user" value="root"></property>
        <property name="password" value="123456"></property>
    </bean>
</beans>
```

> 测试代码

```java
/**
 * Spring测试
 * @throws Exception
 */
@Test
public void test4() throws Exception {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    DataSource dataSource = applicationContext.getBean(DataSource.class);
    // 获取连接对象
    Connection connection = dataSource.getConnection();
    System.out.println(connection);
}
```

#### 将Spring配置信息和数据库信息分离

> 引入context命名空间和约束路径

```xml
命名空间：xmlns:context="http://www.springframework.org/schema/context"
约束路径：http://www.springframework.org/schema/context
		http://www.springframework.org/schema/context/spring-context.xsd
```

> 导入数据库配置文件

```xml
<context:property-placeholder location="classpath:jdbc.properties"/>
```

> 最终文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                            http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
    <context:property-placeholder location="classpath:jdbc.properties"/>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="driverClass" value="${jdbc.driver}"></property>
        <property name="jdbcUrl" value="${jdbc.url}"></property>
        <property name="user" value="${jdbc.username}"></property>
        <property name="password" value="${jdbc.password}"></property>
    </bean>
</beans>
```

### Spring注解开发

>Spring是**轻代码而重配置**的框架，配置比较繁重，影响开发效率，所以注解开发是一种趋势，注解**代替xml配置文件**可以简化配置，提高开发效率

#### Spring原始注解

> 注意：
>
> - @Controller、@Service、@Repository和@Component的功能是一致的，只是分层开发时，前面三个的可阅读性更高
> - @Autowired在有多个相同类型的时候自动注入会报错，和上面根据类获取bean时一样
> - 使用注解进行开发时，需要在applicationContext.xml中配置组件扫描，作用是指定哪个包及其子包下的Bean需要进行扫描以便识别使用注解配置的类、字段和方法

| 注解           | 说明                                           |
| -------------- | ---------------------------------------------- |
| @Component     | 使用在类上用于实例化Bean                       |
| @Controller    | 使用在web层类上用于实例化Bean                  |
| @Service       | 使用在service层类上用于实例化Bean              |
| @Repository    | 使用在dao层类上用于实例化Bean                  |
| @Autowired     | 使用在字段上用于**根据类型**依赖注入           |
| @Qualifier     | 结合@Autowired一起使用用于根据名称进行依赖注入 |
| @Resource      | 相当于@Autowired+@Qualifier，按照名称进行注入  |
| @Value         | 注入普通属性                                   |
| @Scope         | 标注Bean的作用范围                             |
| @PostConstruct | 使用在方法上标注该方法是Bean的初始化方法       |
| @PreDestroy    | 使用在方法上标注该方法是Bean的销毁方法         |

> 使用@Compont或@Repository标识UserDaoImpl需要Spring进行实例化。

```java
//@Component("userDao")
@Repository("userDao")
public class UserDaoImpl implements UserDao {

    @Override
    public void save() {
        System.out.println("save running...........");
    }
}
```

> - 使用@Compont或@Service标识UserServiceImpl需要Spring进行实例化
> - 使用@Autowired或者@Autowired+@Qulifier或者@Resource进行userDao的注入
> - 使用@Value进行字符串的注入
> - 使用@Scope标注Bean的范围
> - 使用@PostConstruct标注初始化方法，使用@PreDestroy标注销毁方法

```java
@Service("userService")
@Scope("singleton")
public class UserServiceImpl implements UserService {

    @Value("注入普通数据")
    private String str;
    @Value("${jdbc.driver}")
    private String driver;
//    @Autowired
//    @Qualifier("userDao")
    @Resource(name = "userDao")
    private UserDao userDao;

    @Override
    public void save() {
        System.out.println(str);
        System.out.println(driver);
        userDao.save();
    }

    @PostConstruct
    public void init() {
        System.out.println("Service的初始化方法");
    }

    @PreDestroy
    public void destory() {
        System.out.println("Service的销毁方法");
    }
}
```

> 测试代码

```java
@Test
public void test5() throws Exception {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    UserService userService = applicationContext.getBean(UserService.class);
    userService.save();
    applicationContext.close();
}
```

#### Spring新注解

> 使用上面的注解还不能全部替代xml配置文件,故而使用新的注解来实现全注解开发。
>
> - 非自定义的Bean的配置：<bean>
> - 加载properties文件的配置：<context:property-placeholder>
> - 组件扫描的配置：<context:component-scan>
> - 引入其他文件：<import>

| 注解            | 说明                                                         |
| --------------- | ------------------------------------------------------------ |
| @Configuration  | 用于指定当前类是一个 Spring 配置类，当创建容器时会从该类上加载注解 |
| @ComponentScan  | 用于指定 Spring 在初始化容器时要扫描的包。作用和在 Spring 的 xml 配置文件中的<context:component-scan base-package="com.itcast"/>一样 |
| @Bean           | 使用在方法上，标注将该方法的返回值存储到 Spring 容器中       |
| @PropertySource | 用于加载.properties 文件中的配置                             |
| @Import         | 用于导入其他配置类                                           |

> 核心配置类
>
> - @Configuration
> - @ComponentScan
> - @Import

```java
@Configuration
@ComponentScan("com.itcast")
@Import({DataSourceConfiguration.class})
public class SpringConfiguration {
}
```

> 需要导入的数据类
>
> - @PropertySource
> - @value
> - @Bean

```java
@PropertySource("classpath:jdbc.properties")
public class DataSourceConfiguration {
    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    @Bean("dataSource")
    public DataSource getDataSource() {
        // 创建数据源
        DruidDataSource druidDataSource = new DruidDataSource();
        // 设置数据库连接参数
        druidDataSource.setDriverClassName(driver);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);

        return druidDataSource;
    }
}
```

> 测试代码：加载核心配置类

```java
@Test
public void test6() throws Exception {
    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
    DataSource dataSource = applicationContext.getBean(DataSource.class);
    // 获取连接对象
    Connection connection = dataSource.getConnection();
    System.out.println(connection);
}
```

### Spring集成Junit

> 不需要每次都加載配置文件或者配置类

> - 导入spring集成Junit的坐标
> - 使用@Runwith注解替换原来的运行期
> - 使用@ContextConfiguration指定配置文件或配置类
> - 使用@Autowired注入需要测试的对象
> - 创建测试方法进行测试

> 导入spring集成Junit的坐标

```xml
<!--此处需要注意的是，spring5 及以上版本要求 junit 的版本必须是 4.12 及以上-->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-test</artifactId>
    <version>${spring.version}</version>
</dependency>
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<version>4.13.1</version>
	<scope>test</scope>
</dependency>
```

> - 使用@Runwith注解替换原来的运行期
> - 使用@ContextConfiguration指定配置文件或配置类
> - 使用@Autowired注入需要测试的对象
> - 创建测试方法进行测试

```java
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:applicationContext.xml")
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringJunitTest {
    @Autowired
    private UserService userService;

    @Autowired
    private DataSource dataSource;

    @Test
    public void test() throws SQLException {
        userService.save();
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
    }
}
```

## SpringMVC

### 概述

> SpringMVC 是一种基于 Java 的实现 MVC 设计模型的请求驱动类型的轻量级 Web 框架，属于SpringFrameWork 的后续产品，已经融合在 Spring Web Flow 中.
>
> SpringMVC 已经成为目前最主流的MVC框架之一，并且随着Spring3.0 的发布，全面超越 Struts2，成为最优秀的 MVC 框架。它通过一套注解，让一个简单的 Java 类成为处理请求的控制器，而无须实现任何接口。同时它还支持 RESTful 编程风格的请求。

### 快速入门

> 需求：客户端发起请求，服务器端接收请求，执行逻辑并进行视图跳转
>
> 开发步骤：
>
> - 导入SpringMVC相关坐标
> - 配置SpringMVC核心控制器DispathcerServlet
> - 创建Controller类和视图页面
> - 使用注解配置Controller类中业务方法的映射地址
> - 配置SpringMVC核心文件 spring-mvc.xml
> - 客户端发起请求测试

#### 导入SpringMVC相关坐标

```xml
<!--导入spring的context坐标，context依赖core、beans、expression-->
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>${spring.version}</version>
</dependency>
<!--SpringMVC-->
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-webmvc</artifactId>
  <version>${spring.version}</version>
</dependency>
<!--Servlet坐标-->
<dependency>
  <groupId>javax.servlet</groupId>
  <artifactId>servlet-api</artifactId>
  <version>2.5</version>
  <scope>provided</scope>
</dependency>
<!--jsp坐标-->
<dependency>
  <groupId>javax.servlet.jsp</groupId>
  <artifactId>jsp-api</artifactId>
  <version>2.2</version>
</dependency>
```

#### 配置SpringMVC核心控制器DispathcerServlet

> web.xml

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
  <display-name>Archetype Created Web Application</display-name>
  <!--spring-mvc配置-->
  <servlet>
    <servlet-name>DispatcherServlet</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:spring-mvc.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>DispatcherServlet</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>
</web-app>
```

#### 创建Controller类和视图页面和映射地址

```java
@Controller
public class QuickController {

    @RequestMapping("/quick")
    public String quickMethod() {
        System.out.println("quickMethod running .....");
        return "success.jsp";
    }
}
```

#### 配置SpringMVC核心文件

> springmvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                            http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
    <!--注解的组件扫描-->
    <context:component-scan base-package="com.itcast.controller"></context:component-scan>
</beans>
```

#### 客户端发起请求测试

![image-20220307175532874](https://gitee.com/image_bed/image-bed1/raw/master/img/202203071755078.png)	

### SpringMVC组件解析

#### SpringMVC的执行流程

![image-20220308150339040](https://gitee.com/image_bed/image-bed1/raw/master/img/202203081503246.png)

> 语言描述

1. 用户发送请求至前端控制器DispatcherServlet。
2. DispatcherServlet收到请求调用HandlerMapping处理器映射器。
3. 处理器映射器找到具体的处理器(可以根据xml配置、注解进行查找)，生成处理器对象及处理器拦截器(如果有则生成)一并返回给DispatcherServlet。
4. DispatcherServlet调用HandlerAdapter处理器适配器。
5. HandlerAdapter经过适配调用具体的处理器(Controller，也叫后端控制器)。
6. Controller执行完成返回ModelAndView。
7. HandlerAdapter将controller执行结果ModelAndView返回给DispatcherServlet。
8. DispatcherServlet将ModelAndView传给ViewReslover视图解析器。
9. ViewReslover解析后返回具体View。
10. DispatcherServlet根据View进行渲染视图（即将模型数据填充至视图中）。DispatcherServlet响应用户。

#### SpringMVC组件解析

##### 前端控制器：DispatcherServlet

> 用户请求到达前端控制器，它就相当于 MVC 模式中的 C，DispatcherServlet 是整个流程控制的中心，由它调用其它组件处理用户的请求，DispatcherServlet 的存在降低了组件之间的耦合性。

##### 处理器映射器：HandlerMapping

> HandlerMapping 负责根据用户请求找到 Handler 即处理器，SpringMVC 提供了不同的映射器实现不同的映射方式，例如：配置文件方式，实现接口方式，注解方式等。

##### 处理器适配器：HandlerAdapter

> 通过 HandlerAdapter 对处理器进行执行，这是适配器模式的应用，通过扩展适配器可以对更多类型的处理器进行执行。

##### 处理器：Handler

> 它就是我们开发中要编写的具体业务控制器。由 DispatcherServlet 把用户请求转发到 Handler。由Handler 对具体的用户请求进行处理

#####  视图解析器：View Resolver

> View Resolver 负责将处理结果生成 View 视图，View Resolver 首先根据逻辑视图名解析成物理视图名，即具体的页面地址，再生成 View 视图对象，最后对 View 进行渲染将处理结果通过页面展示给用户。

##### 视图：View

> SpringMVC 框架提供了很多的 View 视图类型的支持，包括：jstlView、freemarkerView、pdfView等。最常用的视图就是 jsp。一般情况下需要通过页面标签或页面模版技术将模型数据通过页面展示给用户，需要由程序员根据业务需求开发具体的页面

##### @RequestMapping

- 作用：用于建立请求 URL 和处理请求方法之间的对应关系
- 位置
  - 类上，请求URL 的第一级访问目录。此处不写的话，就相当于应用的根目录
  - 方法上，请求 URL 的第二级访问目录，与类上的使用@ReqquestMapping标注的一级目录一起组成访问虚拟路径属性
  - value：用于指定请求的URL。它和path属性的作用是一样的
  - method：用于指定请求的方式
  - params：用于指定限制请求参数的条件。它支持简单的表达式。要求请求参数的key和value必须和配置的一模一样
    - params = {"accountName"}，表示请求参数必须有accountName
    - params = {"moeny!100"}，表示请求参数中money不能是100

#### SpringMVC注解解析

##### mvc命名空间引入

> 命名空间：xmlns:context="http://www.springframework.org/schema/context"
> 				   xmlns:mvc="http://www.springframework.org/schema/mvc"
>
> 约束地址：http://www.springframework.org/schema/context
> 				   http://www.springframework.org/schema/context/spring-context.xsd
>                    http://www.springframework.org/schema/mvc 
>                    http://www.springframework.org/schema/mvc/spring-mvc.xsd

##### 组件扫描

> SpringMVC基于Spring容器，所以在进行SpringMVC操作时，需要将Controller存储到Spring容器中，如果使用@Controller注解标注的话，就需要使用<context:component-scan base-package=“com.itcast.controller"/>进行组件扫描

#### SpringMVC的XML配置解析

##### 视图解析器

> SpringMVC有默认组件配置，默认组件都是DispatcherServlet.properties配置文件中配置的，该配置文件地址org/springframework/web/servlet/DispatcherServlet.properties，该文件中配置了默认的视图解析器

```properties
org.springframework.web.servlet.ViewResolver=org.springframework.web.servlet.view.InternalResourceViewResolver
```

> 翻看该解析器源码，可以看到该解析器的默认设置

```java
REDIRECT_URL_PREFIX = "redirect:"  --重定向前缀
FORWARD_URL_PREFIX = "forward:"    --转发前缀（默认值）
prefix = "";     --视图名称前缀
suffix = "";     --视图名称后缀
```

> 我们可以通过属性注入的方式修改视图的的前后缀

```xml
<!--配置内部资源视图解析器-->
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <property name="prefix" value="/view/"></property>
    <property name="suffix" value=".jsp"></property>
</bean>
```

> idea帮助我们跳转

![image-20220308161645134](https://gitee.com/image_bed/image-bed1/raw/master/img/202203081616825.png)	

```java
@Controller
public class QuickController {

    @RequestMapping("/quick")
    public String quickMethod() {
        System.out.println("quickMethod running .....");
        // return "redirect:success"; 期望：http://localhost:8080/view/success.jsp 结果：http://localhost:8080/success
        return "success";
    }
}
```

> **注意：这个时候加上redirect和forward都会报错，好像前缀和后缀的配置无效**

### SpringMVC的数据响应

#### 响应方式

> - 页面跳转
>   - 直接返回字符串
>   - 通过ModelAndView对象返回
> - 回写数据
>   - 直接返回字符串
>   - 返回对象或集合

#### 页面跳转

##### 返回字符串形式

> 直接返回字符串：此种方式会将返回的字符串与视图解析器的前后缀拼接后跳转（这个可以参考上面的笔记）

##### 返回ModelAndView对象

```java
@RequestMapping("/quick2")
public ModelAndView quickMethod2() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("success");
    return modelAndView;
}
```

##### 向request域存储数据

> 通过SpringMVC框架注入的request对象setAttribute()方法设置

```java
@RequestMapping("/quick3")
public String quickMethod3(HttpServletRequest request) {
    System.out.println("quickMethod3 running .....");
    request.setAttribute("name","zhangsan");
    return "success";
}
```

```jsp
<%@page isELIgnored="false" %>

<html>
<body>
<h2>View Success! ${name}</h2>
</body>
</html>
```

> 通过ModelAndView的addObject()方法设置

```java
@RequestMapping("/quick4")
public ModelAndView quickMethod4() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("name","lisi");
    modelAndView.setViewName("success");
    return modelAndView;
}
```

#### 回写数据

##### 直接返回字符串

> 通过SpringMVC框架注入的response对象，使用response.getWriter().print(“hello world”) 回写数据，此时不需要视图跳转，业务方法返回值为void

```java
@RequestMapping("/quick5")
public void quickMethod5(HttpServletResponse response) throws IOException {
    response.getWriter().print("hello World!!");
}
```

> 将需要回写的字符串直接返回，但此时需要通过**@ResponseBody**注解告知SpringMVC框架，方法返回的**字符串不是跳转**是直接在http响应体中返回

```java
@RequestMapping("/quick6")
@ResponseBody
public String quickMethod6() {
    return "success";
}
```

##### json

> 导入坐标

```xml
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-core</artifactId>
  <version>2.9.0</version>
</dependency>
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
  <version>2.9.0</version>
</dependency>
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-annotations</artifactId>
  <version>2.9.0</version>
</dependency>
```

> 通过jackson转换json格式字符串，回写字符串。

```java
@RequestMapping("/quick7")
@ResponseBody
public String quickMethod7() throws JsonProcessingException {
    User user = new User();
    user.setAge(20);
    user.setName("zhangsan");
    ObjectMapper objectMapper = new ObjectMapper();
    String u = objectMapper.writeValueAsString(user);
    return u;
}
```

> 返回对象或者集合：通过SpringMVC帮助我们对**对象或集合进行json字符串的转换并回写**，为处理器适配器配置消息转换参数**，指定使用jackson进行对象或集合的转换**，因此需要在spring-mvc.xml中进行如下配置：

```xml
<!--json配置-->
<bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">
    <property name="messageConverters">
        <list>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter"></bean>
        </list>
    </property>
</bean>
```

```java
@RequestMapping("/quick8")
@ResponseBody
public User quickMethod8() {
    User user = new User();
    user.setAge(20);
    user.setName("zhangsan");
    return user;
}
```

> 在方法上添加@ResponseBody就可以返回json格式的字符串，但是这样配置比较麻烦，配置的代码比较多，因此，我们可以使用mvc的注解驱动代替上述配置，之前需要导入mvc的命名空间

```xml
<mvc:annotation-driven/>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                            http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                            http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd">
    <!--注解的组件扫描-->
    <context:component-scan base-package="com.itcast.controller"></context:component-scan>

    <!--配置内部资源视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/view/"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>

    <!--json配置-->
    <!--<bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">-->
    <!--    <property name="messageConverters">-->
    <!--        <list>-->
    <!--            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter"></bean>-->
    <!--        </list>-->
    <!--    </property>-->
    <!--</bean>-->
    <mvc:annotation-driven/>
</beans>
```

> 在 SpringMVC 的各个组件中，**处理器映射器、处理器适配器、视图解析器**称为 SpringMVC 的三大组件。使用<mvc:annotation-driven>自动加载RequestMappingHandlerMapping（处理映射器）和RequestMappingHandlerAdapter（ 处 理 适 配 器 ），可用在Spring-xml.xml配置文件中使用<mvc:annotation-driven>替代注解处理器和适配器的配置。同时使用<mvc:annotation-driven>**默认底层就会集成jackson进行对象或集合的json格式字符串的转换**

### SpringMVC获取请求数据

#### 获得基本类型参数

> Controller中的业务方法的参数名称要与请求参数的name一致，参数值会自动映射匹配
>
> [localhost:8080/quick9?name=zhangsan&age=40](http://localhost:8080/quick9?name=zhangsan&age=40)

```java
@RequestMapping("/quick9")
@ResponseBody
public void quickMethod9(String name, int age) {
    System.out.println(name);
    System.out.println(age);
}
```

#### 获得POJO类型参数

> Controller中的业务方法的POJO参数的属性名与请求参数的name一致，参数值会自动映射匹配
>
> http://localhost:8080/quick10?name=zhangsan&age=40

```java
@RequestMapping("/quick10")
@ResponseBody
public void quickMethod10(User user) {
    System.out.println(user.getAge());
    System.out.println(user.getName());
}
```

#### 获得数组类型参数

> Controller中的业务方法数组名称与请求参数的name一致，参数值会自动映射匹配
>
> [localhost:8080/quick11?strs=111&strs=222&strs=333](http://localhost:8080/quick11?strs=111&strs=222&strs=333)

```java
@RequestMapping("/quick11")
@ResponseBody
public void quickMethod11(String[] strs) {
    for (String s:strs) {
        System.out.println(s);
    }
}
```

#### 获得集合类型参数

> 获得集合参数时，要将集合参数包装到一个POJO中才可以

```java
public class Vo {
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public String toString() {
        return "Vo{" +
                "userList=" + userList +
                '}';
    }
}
```

```java
@RequestMapping("/quick12")
@ResponseBody
public void quickMethod11(Vo vo) {
    System.out.println(vo);
}
```

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <form action="/quick12"method="post">
        <input type="text" name="userList[0].name"><br/>
        <input type="text" name="userList[0].age"><br/>
        <input type="text" name="userList[1].name"><br/>
        <input type="text" name="userList[1].age"><br/>
        <input type="submit" value="提交">
    </form>
</body>
</html>
```

> 使用ajax提交时，可以指定contentType为json形式，那么在方法参数位置使用@RequestBody可以直接接收集合数据而无需使用POJO进行包装

```java
@RequestMapping("/quick13")
@ResponseBody
public void quickMethod13(@RequestBody List<User> userList) {
    System.out.println(userList);
}
```

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
</head>
<body>
    <script>
        var userList = new Array();
        userList.push({"name": "zhangsan", "age": "20"});
        userList.push({"name": "lisi", "age": "30"});
        $.ajax({
            type: "POST",
            url: "/quick13",
            data: JSON.stringify(userList),
            contentType: "application/json;charset=utf-8"
        });
    </script>
</body>
</html>
```

> 问题：没有加载到jquery文件
>
> 原因：SpringMVC的前端控制器DispatcherServlet的url-pattern配置的是/,代表对所有的资源都进行过滤操作

> 静态资源的设置

```xml
<!--<mvc:resources mapping="/js/**" location="/js/"/>-->
<mvc:default-servlet-handler/>
```

#### 请求数据乱码问题

> 通过设置过滤器来进行编码的过滤

```xml
<filter>
  <filter-name>CharacterEncodingFilter</filter-name>
  <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
  <init-param>
    <param-name>encoding</param-name>
    <param-value>UTF-8</param-value>
  </init-param>
</filter>
<filter-mapping>
  <filter-name>CharacterEncodingFilter</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>
```

#### 参数绑定注解@requestParam

> 当请求的参数名称与Controller的业务方法参数名称不一致时，就需要通过@RequestParam注解显示的绑定
>
> 注解@RequestParam参数：
>
> - value：与请求参数名称
> - required：此在指定的请求参数是否必须包括，默认是true，提交时如果没有此参数则报错
> - defaultValue：当没有指定请求参数时，则使用指定的默认值赋值

```html
<form action="${pageContext.request.contextPath}/quick14" method="post">
	<input type="text" name="name"><br>
	<input type="submit" value="提交"><br>
</form>
```

```java
@RequestMapping("/quick14")
@ResponseBody
public void quickMethod14(@RequestParam(value="name",required = false,defaultValue = "itcast") String username) throws IOException {
	System.out.println(username);
}
```

#### 获得Restful风格的参数

> 在SpringMVC中可以使用占位符进行参数绑定。地址/user/1可以写成/user/{id}，占位符{id}对应的就是1的值。在业务方法中我们可以使用@PathVariable注解进行占位符的匹配获取工作

```java
// http://localhost:8080/quick19/zhangsan
@RequestMapping("/quick19/{name}")
@ResponseBody
public void quickMethod19(@PathVariable(value = "name",required = true) String name){
	System.out.println(name);
}
```

#### 自定义类型转换器

> - SpringMVC 默认已经提供了一些常用的类型转换器，例如客户端提交的字符串转换成int型进行参数设置
> - 不是所有的数据类型都提供了转换器，没有提供的就需要自定义转换器，例如：日期类型的数据就需要自定义转换器

##### 开发步骤

> 定义转换器类实现Converter接口

```java
public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
```

> 在配置文件中声明转换器

```xml
<!--配置转换器-->
<bean id="conversionService" class="org.springframework.context.support.ConversionServiceFactoryBean">
    <property name="converters">
        <list>
            <bean class="com.itcast.converter.DateConverter"></bean>
        </list>
    </property>
</bean>
```

> 在<annotation-driven>中引用转换器

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
```

#### 获取请求头

> 使用@RequestHeader可以获得请求头信息
>
> - value：请求头的名称
> - required：是否必须携带此请求头

```java
@RequestMapping("/quick17")
@ResponseBody
public void quickMethod17(@RequestHeader(value = "User-Agent",required = false) String headerValue){
	System.out.println(headerValue);
}
```

> 使用@CookieValue可以获得指定Cookie的值
>
> - value：指定cookie的名称
> - required：是否必须携带此cookie

```java
@RequestMapping("/quick18")
@ResponseBody
public void quickMethod18(@CookieValue(value = "JSESSIONID",required = false) String jsessionid){
	System.out.println(jsessionid);
}
```

#### 文件上传

##### 文件上传客户端三要素

> - 表单项type=“file”
> - 表单的提交方式是post
> - 表单的enctype属性是多部分表单形式，及enctype=“multipart/form-data”

```jsp
<form action="${pageContext.request.contextPath}/quick15" method="post" enctype="multipart/form-data">
    名称：<input type="text" name="name"><br>
    文件：<input type="file" name="uploadFile"><br>
    <input type="submit" value="提交"><br>
</form>
```

##### 文件上传原理

- 当form表单修改为多部分表单时，request.getParameter()将失效
- enctype=“application/x-www-form-urlencoded”时，form表单的正文内容格式是:key=value&key=value&key=value
- 当form表单的enctype取值为Mutilpart/form-data时，请求正文内容就变成多部分形式

##### 单文件上传步骤

> - 导入fileupload和io坐标
> - 配置文件上传解析器
> - 编写文件上传代码

> 导入fileupload和io坐标

```xml
<!--文件上传-->
<dependency>
  <groupId>commons-fileupload</groupId>
  <artifactId>commons-fileupload</artifactId>
  <version>1.2.2</version>
</dependency>
<dependency>
  <groupId>commons-io</groupId>
  <artifactId>commons-io</artifactId>
  <version>2.4</version>
</dependency>
```

> 配置文件上传解析器

```xml
<!--上传解析器-->
<bean class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
    <!--上传文件总大小-->
    <property name="maxUploadSize" value="5242800"></property>
    <!--上传单个文件的大小-->
    <property name="maxUploadSizePerFile" value="5242800"></property>
    <!--上传文件的编码类型-->
    <property name="defaultEncoding" value="UTF-8"></property>
</bean>
```

> 编写文件上传代码

```java
@RequestMapping("/quick15")
@ResponseBody
public void quickMethod15(String name, MultipartFile uploadFile) throws IOException {
    // 获取文件名称
    String filename = uploadFile.getOriginalFilename();
    // 保存文件
    uploadFile.transferTo(new File("D:\\upload\\"+filename));
}
```

##### 多文件上传实现

```jsp
<h1>多文件上传测试</h1>
<form action="${pageContext.request.contextPath}/quick16" method="post" enctype="multipart/form-data">
    名称：<input type="text" name="name"><br>
    文件1：<input type="file" name="uploadFiles"><br>
    文件2：<input type="file" name="uploadFiles"><br>
    文件3：<input type="file" name="uploadFiles"><br>
    <input type="submit" value="提交"><br>
</form>
```

```java
@RequestMapping("/quick16")
@ResponseBody
public void quickMethod16(String name, MultipartFile[] uploadFiles) throws IOException {
    for (MultipartFile uploadFile : uploadFiles) {
        // 获取文件名称
        String filename = uploadFile.getOriginalFilename();
        // 保存文件
        uploadFile.transferTo(new File("D:\\upload\\"+filename));
    }
}
```

## ClassPath

[(23条消息) ClassPathXmlApplicationContext路径与classpath之间的关系_lizhen1114的博客-CSDN博客_applicationcontext classpath](https://blog.csdn.net/lizhen1114/article/details/80317068)

## 未结问题

### SpringMVC

> 在配置完前缀和后缀之后，加上redirect和forward都会报错[视图解析器](#####视图解析器)





