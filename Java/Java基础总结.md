# Java基础总结

## Java概述

### 计算机概述

#### 计算机

> [百度百科-计算机](https://baike.baidu.com/item/计算机/140338?fr=aladdin)

#### 计算机硬件

> [百度百科-计算机硬件](https://baike.baidu.com/item/硬件?fromtitle=计算机硬件&fromid=5459592)

#### 计算机软件(理解)

- 系统软件:Window,Linux,Mac
- 应用软件: QQ等

#### 软件开发

- 软件：有数据和指令组成的(计算器).
- 开发: 将软件做出来.
- 如何使用软件开发：使用开发工具和计算机语言做出东西.

#### 语言

- 自然语言: 人与人交流沟通的语言
- 计算机语言:人与计算机交流沟通的语言(C, C++, **Java**等)

#### 人机交互

- 图形界面：操作方便
- Dos命令: 需要记忆常见的命令

### Dos命令

> 常见的需要掌握

#### 常见的Dos命令(掌握)

```
- 盘符的切换：d: 回车
- 目录的进入： cd d:\(进入D盘根目录)
- 目录的回退：
  - cd..(回退到上一级目录)
  - cd\(回退到根目录)
- 清屏：cls
- 退出：exit
```

#### 其他Dos命令(了解)

```
创建目录: md aaa
删除目录: rd aaa
创建文件: touch a.txt
删除文件:del a.txt
显示目录下的内容: dir
删除带内容的目录: rd /s aa
```

### Java语言概述

#### Java语言的发展史

> [百度百科-Java语言的发展历史](https://baike.baidu.com/item/Java/85979?fr=aladdin)

#### Java语言的特点

- 开源
- 跨平台

#### Java语言跨平台的实现

> 通过针对不同的操作系统, 提供不同的JVM来实现

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082120064.jpg)		

#### Java语言的平台

- JavaSE
- JavaME – 安卓
- JavaEE

### JDK、JRE和JVM的作用和关系(掌握)

#### 作用

- JVM: 保证Java语言的跨平台
- JRE: Java语言的运行环境
- JDK: Java程序的开发环境

#### 关系

- JDK:JRE+工具
- JRE:JVM+类库

### 配置环境变量

#### 配置JAVA_HOME变量

- 进入计算机→右键“属性”→高级系统设置→高级→环境变量
- 点击系统变量下面的“【**新建**】”，弹出一个新建系统变量对话框，首先在变量名写上JAVA_HOME，顾名思义，JAVA_HOME的含义就是JDK的安装路径，，然后在变量值写JDK的安装路径，如这里设置的变量值是"**D:\Program Files (x86)\Java\jdk1.7.0**"，设置好变量值之后，点击【确定】按钮，JAVA_HOME环境变量就设置完成，如下图所示：系统变量中多了一个"JAVA_HOME"变量

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082127278.gif)	

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082127012.gif)	

#### 配置path环境变量

> 找到系统变量中的Path变量，点击【编辑】按钮，弹出编辑系统变量的对话框，可以看到，Path变量中设置有很多的目录，每个目录之间使用;(分号)隔开,将%JAVA_HOME%\bin;添加到Path变量的变量值中，点击【确定】按钮，Path环境变量的就设置完成了，如下图所示：
>
> **目的：这个配置是为了，我们可以在任意目录下使用javac和java命令，正如我们可以在任意目录下打开文本文件一样**

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082129990.gif)	

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082129237.gif)	

#### 配置ClassPath变量(之后使用IDE就不用配置classpath了)

> 设置Classpath的目的，在于告诉Java执行环境，在哪些目录下可以找到您所要执行的Java程序(.class文件)，关于这个ClassPath变量，其实可以不用配置了，在网上经常看到Classpath=.;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\toos.jar，但学习java这么久发现，不配置ClassPath也不影响java项目的开发和运行的.

#### 验证结果

> cmd进入命令执行界面执行 javac出现东西则配置成功

## Java基础语法

### 关键字(掌握)

> - Java中赋予特定含义的单词，例如 class等，这个名称不可以使用来声明变量
> - 特点：全部小写
> - 注意事项::goto和const作为保留字存在

### 标识符 (掌握)

> 为类、接口、方法、变量等命名的字符序列

#### 组成规则

- 标识符由字母、下划线(_)、美元符($)或者数字组成
- 标识符应该由字母、下划线(_)、美元符($)开头。
- Java标识符大小写敏感, 长度没有限制

#### 注意事项

- 不能以数字开头
- 不能是Java中的关键字
- 区分大小写

#### 常见的命名规则

- 大小写敏感
- 类名定义：首字母应该大写，如：HelloWord, Student
- 包名定义：包名应该全部小写，如：com.hyatt.test
- 方法名或者变量定义：方法名首字母应该小写，后面的单词首字母应该大写， 如：myFirstMethod()
- 常量：全部大写。多个单词之间必须要用_隔开，例如：PI, STUDENT_MAX_AGE.

### 注释

> 对程序进行解释说明的文字。
>
> 主要有两个作用：
>
> - 解释说明程序, 提高了代码的阅读性
> - 帮助我们调试程序

```java
// 单行注释
/*
	多行注释
*/
/**
* 文档注释, 用来生成API文档
*/
```

### 常量

> 在程序执行的过程中值不发生改变的量.

#### 分类

- 字面值常量
- 自定义常量

#### 字面值常量

- 字符串常量：“hello”
- 整数常量：12,23
- 小数常量：12.2345
- 字符常量：’a’, ‘A’,’1’
- 布尔常量: true, false
- 空常量: null

#### 在Java中针对整数常量提供了四种表现形式

- 二进制：由0和1组成。以0b开头
- 八进制：由0到7组成。以0开头
- 十进制：由0到9组成。整数默认是十进制
- 十六进制：有0,1…9,a,b,c,d,e,f(大小写均可)组成。以0x开头

### 进制转换(了解)

#### 其他进制到十进制

- 系数：每一个位上的数值
- 基数：x进制的基数就是x
- 权：对每一位上的数据，从右, 并且从0开始编号, 对应的编号就是该数的权
- 结果：基数^权次幂 * 系数 之和

> 例如：二进制：1001  转换为十进制为：2^0*1+2^1*0+2^2*0+2^3*1 = 1 + 8 = 9

#### 十进制到其他进制

> 除基数取余，直到商为0，余数反转

例如：十进制：9转换为二进制:

- 9/2=4 余数为1
- 4/2 = 2 余数为0
- 2/2 = 1余数为0
- 1/2 = 0 余数为1

> 直到商为0停止.最后的值从最后的一个余数朝前,所以最后的值为 1001

#### 进制转换的快速转换

> 例如我想将二进制转换为八进制

##### 第一种方法: 先转成十进制再转成八进制

- 1001 = 2^3 * 1 + 2^0*1 = 8 + 1 = 9
- 然后按照上面的方法转换成8进制
- 9 / 8 = 1 余数为 1
- 1 / 8 = 0 余数为 1
- 所以最后的值为:11

##### 第二种方法：拆分组合法(8进制3位一组，16进制4位一组)

- 1001 可以拆分为  001 001 (从右向左拆分，不够三位的左面补0)
- 然后将拆分出来的数分别转换为十进制：1  1
- 所以最后的结果是：11.

### 原码, 反码和补码(了解)

> 计算机中的二进制的数都是 补码
>
> **byte范围：-128 到 127 原因 ：**
>
> 在计算机内，定点数有3种表示法：原码、反码和补码
>
> - 原码 ：二进制定点表示法，即最高位为符号位，“0”表示正，“1”表示负，其余位表示数值的大小。
> - 反码 ：正数的反码与其原码相同；负数的反码是对其原码逐位取反，但符号位除外。
> - 补码 ：正数的补码与其原码相同；负数的补码是在其反码的末位加1。
>
> Java中用补码表示二进制数，补码的最高位是符号位，最高位为“0”表示正数，最高位为“1”表示负数。
>
> 正数补码为其本身；
>
> 负数补码为其绝对值各位取反加1；
>
> 例如：
>
> +21，其二进制表示形式是00010101，则其补码同样为00010101
>
> -21，按照概念其绝对值为00010101，各位取反为11101010，再加1为11101011，即-21的二进制表示形式为11101011
>
> 步骤：
>
> - byte为一字节8位，最高位是符号位，即最大值是01111111，因正数的补码是其本身，即此正数为01111111，十进制表示形式为127 
> - 最大正数是01111111，那么最小负是10000000(最大的负数是11111111，即-1)
> - 10000000是最小负数的补码表示形式，我们把补码计算步骤倒过来就即可。10000000减1得01111111然后取反10000000，因为负数的补码是其绝对值取反，即10000000为最小负数的绝对值，而10000000的十进制表示是128，所以最小负数是-128
> - 由此可以得出byte的取值范围是-128到+127

- 原码 ：二进制定点表示法，即最高位为符号位，“0”表示正，“1”表示负，其余位表示数值的大小。
- 反码 ：正数的反码与其原码相同；负数的反码是对其原码逐位取反，但符号位除外。
- 补码 ：正数的补码与其原码相同；负数的补码是在其反码的末位加1。

| 示例 | 符号位 | 数值位  | 原码     | 反码     | 补码(计算机的二进制表示) |
| ---- | ------ | ------- | -------- | -------- | ------------------------ |
| +7   | 0      | 0000111 | 00000111 | 00000111 | 00000111                 |
| -7   | 1      | 0000111 | 10000111 | 11111000 | 11111001                 |

### 变量(掌握)

> 在程序的执行过程中值在某个范围内可以发生变化的量

#### 变量的定义格式

- 数据类型 变量名 = 初始化值; int a = 10;
- 数据类型 变量名; 变量名 = 初始化值;  int a; a = 10;

### 数据类型(掌握)

> Java是一种强类型语言, 针对每种数据都提供了对应的数据类型

#### 分类

- 基本数据类型: 4类八种.
- 引用数据类型: 类、接口、数组.

#### 基本数据类型

- 整数默认是int类型
- 浮点数默认是double类型

| 数据类型 | 描述         | 内存             | 默认值                                              | 取值范围             |
| -------- | ------------ | ---------------- | --------------------------------------------------- | -------------------- |
| byte     | 字节         | 1 bit->8位二进制 | 0                                                   | -128 ～  127         |
| short    | 短整型       | 2 bits           | 0                                                   | -2^15 ~ 2^15-1       |
| int      | 整型         | 4 bits           | 0                                                   | -2^31 ~ 2^31-1       |
| long     | 长整型       | 8 bits           | 0L(这里L或者l都可以，但是l容易认成1，所以建议使用L) | -2^63 ~ 2^63 - 1     |
| float    | 单精度浮点型 | 4 bits           | 0.0f(加上f或者F)                                    | -3.403E38~3.403E38   |
| double   | 双精度浮点型 | 8 bits           | 0.0d                                                | -1.798E308~1.798E308 |
| char     | 字符型       | 2 bits           | \u0000                                              |                      |
| boolean  | 布尔型       | 1 bits           | false                                               | true/false           |

### 数据类型的转换(掌握)

#### 默认转换

- 内存小的，自动转换成大的
- 大小排序: byte, short, char -> int->long->float->double
- byte,short,char之间不会互相转换，**他们三者在计算的时候会首先转换成int**
- float占4个字节, long占8个字节, 之所以float的范围大于long，是由于float和long的底层存储结构不同.

#### 强制转换

- 类型大的，强制转换成类型小的
- **注意：不建议这么做，因为会损失进度或者溢出。**

#### 数据类型相关的面试题和思考题

> 下面的两种方式有区别吗

```java
float f1 = 12.345f;
float f2 = (float) 12.345;
// f2其实是通过double类型转换过来的, 可能会损失进度
// f1本身就是float类型的变量.
```

> 下面的哪个语句编译失败

```java
byte b1 = 3;
byte b2 = 4;
byte b3 = b1 + b2;
// 这句是有问题的, 变量在计算的时候会先转换成int类型参与运算, 所以最后需要进行强转
byte b4 = 3 + 4;
// 这句没有问题. 常量在计算的时候, 先计算结果, 如果结果再范围内则不报错
```

> 数据的值溢出计算

```
byte b5 = (byte) 130; // -126
```

| 示例                 | 130                                 |
| -------------------- | ----------------------------------- |
| 符号位               | 0                                   |
| 数值位               | 0000000 00000000 00000000 10000010  |
| 原码                 | 00000000 00000000 00000000 10000010 |
| 反码                 | 00000000 00000000 00000000 10000010 |
| 补码                 | 00000000 00000000 00000000 10000010 |
| 强转后(截取一个字节) | 10000010(补码,  计算其的原码)       |
| 符号位               | 1                                   |
| 数值位               | 0000010                             |
| 反码                 | 10000001                            |
| 原码                 | 11111110                            |
| 结果                 | -126                                |

> 字符的计算:
>
> 通过查询ASCII里面的值(记住三个值即可)
>
> ‘a’=97, ‘A’ = 65 , ‘0’ = 48

> 字符串的计算(拼接)

```java
System.out.println("hello" + 'a' + 1); // helloa1
System.out.println('a' + 1 + "hello"); // 98hello
System.out.println("5+5=" + 5 + 5); // 5+5=55
System.out.println(5 + 5 + "=5+5"); // 10 = 5+5
```

### 运算符(掌握)

#### 算术运算符

| 运算符说明 | 符号 |
| ---------- | ---- |
| 加         | +    |
| 减         | -    |
| 乘         | *    |
| 除         | /    |
| 取余       | %    |

#### 自增、自减运算符

```java
int a = 4;
int b = a ++; // b = 4  先执行赋值语句，再执行自增
b = ++a; // b = 6  先执行自增，再执行赋值
b = a--; // b = 6
b = --a; // b = 4
```

#### 赋值运算符

> **隐含了自动强制转换**

| 运算符 | 用法举例 | 等效的表达式 |
| ------ | -------- | ------------ |
| +=     | a+=b     | a=a+b        |
| -=     | a-=b     | a=a-b        |
| *=     | a*=b     | a=a*b        |
| /=     | a/=b     | a=a/b        |
| %=     | a%=b     | a=a&b        |

> 面试题：下面的哪个代码有问题

```java
short s = 1;
s = s + 1; // 这个有问题, 需要类型的强制转换
short s = 1;
s += 1;  // 这个没有问题,这个自动做了强制转换
```

#### 比较运算符

> **最终结果是boolean类型**

| 运算符 | 说明                     |
| ------ | ------------------------ |
| >      | 比较左面是否大于右面     |
| <      | 比较左边是否小于右面     |
| >=     | 比较左边是否大于等于右面 |
| <=     | 比较左边是否小于等于右面 |
| ==     | 比较左边是否等于右边     |
| !=     | 比较左边是否不等于右边   |

#### 逻辑运算符

##### && 和 &的区别

> && 在执行过程中会出现短路现象，即若符号"&&"前的表达式如果为假, 则后面的表达式不会被执行，直接返回false。

##### || 和 | 的区别

> 和上面的类似.
>
> || 在执行过程中会出现短路现象，即若符号"||"前的表达式如果为真, 则后面的表达式不会被执行，直接返回true。

| 运算符 | 说明     | 计算方法                                      |
| ------ | -------- | --------------------------------------------- |
| &&     | 短路与   | 只要有一个为false，结果即为false              |
| \|\|   | 短路或   | 只要有一个为true，结果即为true                |
| !      | 逻辑非   | 取反                                          |
| &      | 逻辑与   | 同 &&                                         |
| \|     | 逻辑或   | 同 \|                                         |
| ^      | 逻辑异或 | 当两个的结果一样的时候,即为false,不一样为true |

| a     | b     | a&b   | a&&b  | a\|b  | a\|\|b | !a    | a^b   |
| ----- | ----- | ----- | ----- | ----- | ------ | ----- | ----- |
| true  | true  | true  | true  | true  | true   | false | false |
| true  | false | false | false | true  | true   | false | true  |
| false | true  | false | false | true  | true   | true  | true  |
| false | false | false | false | false | false  | true  | false |

#### 按位运算符(了解)

> 如果进行比较的双方是数字的话，那么比较就会变为按位计算

| 运算符 | 说明 |
| ------ | ---- |
| ~      | 非   |
| &      | 与   |
| \|     | 或   |
| ^      | 异或 |

> 计算方法：(可以将1看成true，0看成false)

> 与：转换成二进制后，对应的位都为1，结果才为1 ，否则为0

```java
4 & 5 = 4
4 = 00000100
5 = 00000101
4 & 5 = 00000100 = 4
```

> 或：转换成二进制后，对应的位只有有一个为1，结果就为1 ，否则为0

```java
4 | 5 = 5
4 = 00000100
5 = 00000101
4 | 5 = 00000101 = 5
```

> 非：取反，如果位为0，则为1，反之则为0

```java
~4 = -5
4 = 00000100
~4
反码：11111011
补码：11111010
原码：10000101 = -5
```

> 异或：转换成二进制后，对应的位相同，结果就为0 ，否则为1

```java
4 ^ 5 = 5
4 = 00000100
5 = 00000101
4 ^ 5 = 001 = 1

// 特点：一个数对另一个数据位异或两次，该数不变
int a = 10;
int b = 20;
a^b^b = a; a^b^a = b
```

#### 移位运算符

> 用来操作数向某个方向(向左或者向右)移动指定的二进制位数

```
>>(右移): 最高位是0，左边补0，最高位是1，左边补1
<<(左移): 左边最高位丢弃，右边补0
>>>(无符号右移):无论最高位是0还是1，左边补0
```

```java
8 >> 1:
    8 = 00001000
    8 >> 1 = 00000100 = 4 = 8/2^1
    左边的数据 / 2 的移动次幂
    
8<<1:
    8 = 00001000
    8 << 1 = 00010000 = 16 = 8 * 2^1
    左边的数据 * 2 的移动次幂
```

#### 三目运算符

> 条件表达式?表达式1:表达式2
>
> - 判断表达式为true还是false
> - True: 返回表达式1值，否则则返回表达式2的值

### 键盘录入(掌握)

> Scanner类的使用

```java
import java.util.Scanner;

public class ScannerTest2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("请你输入一个整型数据：");
		int x = sc.nextInt();
		System.out.println("你输入的数据是：" + x);

	}
}
```

### 选择结构——If语句(掌握)

> 三个格式

#### if 条件语句

> 语句可以单独判断表达式的结果
>
> if 语句只会执行一个 语句体，即使满足多个条件

```java
int a = 10;
if(a > 10) {
	returtrue;
}
returfalse;
```

#### if .. else 语句

> 如果满足某种条件,就进行某种处理,否则就进行另一种处理

```java
int a = 10;
int b = 11;
if (a >= b){
    System.out.println("a>=b");
} else {
	System.out.println("a<b");
}
```

#### if .. else if .. else if .. else 

> 多个判断条件
>
> 只会执行这个：System.out.println("X的值大于30但小于60");

```java
int x=40;
if(x>60){ 
    System.out.println("x的值大于60");
}
else if (x>30){
    System.out.println("X的值大于30但小于60");
}
else if (x>0){
    System.out.println("X的值大于0但小于30");
}
else 
    System.out.println("x的值小于等于0");
```

#### 注意事项

- 只要有一个执行, 其他的不在执行.

### 选择结构——switch语句(掌握)

> 多分支语句

> 格式

```java
switch(表达式) {
	case 值1:
		语句体1;
		break;
	case 值2:
		语句体2;
		break;
	....
	default:
		语句体n+1;
		break;
}

int week = 2;
switch (week) {   
	case 1:     
		System.out.println("Monday");     
		break;   
	case 2:     
		System.out.println("Tuesday");     
		break;   
	case 3:     
		System.out.println("Wednesday");     
		break;   
	case 4:     
		System.out.println("Thursday"); 
		break;   
	case 5:     
		System.out.println("Friday");     
		break;   
	case 6:     
		System.out.println("Saturday");     
		break;   
	case 7:     
		System.out.println("Sunday");     
		break;   
	default:     
		System.out.println("No Else");     
		break; 
}
```

#### 适用switch的表达式类型

- byte, short, int, char
- JDK5之后可以是枚举类型
- JDK7之后可以是字符串类型

#### 注意事项

- case后面只能是常量，并且后面的值不能相同。
- default可以省略但是不建议。
- break可以省略但是不建议省略，会出现**case穿透问题**。
- default语句可以出现再switch语句中的任意地方,但是建议放在最后。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082217962.png)	

### 循环结构——for循环(掌握)

> 格式

```java
for(初始化语句; 判断条件语句; 控制条件语句) {
循环体语句
}
for(int i = 1,j = i + 10;i < 5;i++, j = j * 2){}
```

> 执行流程

```java
执行初始化流程
执行判断条件语句
	如果是true, 则继续
	如果是false, 循环结束
执行循环体语句
执行控制条件语句
回到执行判断条件语句
```

#### 逗号操作符

> - Java里唯一用到逗号操作符的就是for循环控制语句。
> - 通过逗号操作符,可以在for语句内定义多个变量,但它们必须具有相同的类型

```java
for(int i = 1,j = i + 10;i < 5;i++, j = j * 2){}
```

#### for each语句

> 更加简洁的、方便对**数组和集合**进行遍历的方法

```java
int array[] = {7, 8, 9}; 
for (int arr : array) {      
    System.out.println(arr); 
}
```

### 循环结构——while循环(掌握)

> 格式

```java
while(判断条件语句) {
	循环体语句;
}

初始化语句;
while(判断条件语句) {
	循环体语句;
	控制条件语句;
}
```

```java
int a = 10;
while(a>5){
    System.out.println("a: " + a);
    a --;
}
```

### for循环和while循环的区别

- for循环的控制条件变量(初始化变量), 在循环结束后就不能使用了, 而while可以继续使用.
- for循环适合一个范围的判断, 而while适合循环测试不明确的

### 循环结构——do..while循环

> **while与do..hile循环的唯一区别是do..while语句至少执行一次**,即使第一次的表达式为 false。而在 while循环中,如果第一次条件为 false,那么其中的语句根本不会执行。在实际应用中, while要比do. while应用的更广。

```java
do {
	循环体语句;
} while(判断条件语句);

初始化语句;
do {
	循环体语句;
	控制条件语句;
} while(判断条件语句);
```

```java
int b = 10; 
// do···while
do {   
    System.out.println("b == " + b);   
    b--; 
} while(b == 1);
```

### 控制跳转语句

#### break:中断

- 用在循环和switch语句中, 离开此场景则无意义
- 作用：
  - 跳出单层循环
  - 跳出多层循环, 需要使用标签

```java
public class BreakTest {
	public static void main(String[] args) throws java.lang.Exceptio{
		wc: for (int x = 1; x <= 3; x++) {
			nc: for (int y = 1; y <= 3; y++) {
				System.out.println("y:" + y);
				break wc;
			}
			System.out.println("x:" + x);
		}
	}
}
```

#### continue:继续

- 用在循环语句中, 离开此场景则无意义
- 作用：
  - 跳出单层循环中的一次, 继续下一次循环

#### return:返回

- 主要是用于结束方法的.

### 方法(掌握)

> **完成特定功能的代码块**

```
修饰符 返回类型 方法名(参数类型 参数名1, 参数类型 参数名2...) {
	方法体语句;
	retur返回值; (返回类型为void的没有返回值)
}
```

#### 方法重载

> 在同一类中, 方法名称相同, 但是**参数列表不同**的方法(**与返回值无关**)

参数列表的不同:

- 参数的个数不同
- 参数的数据类型不同

```java
class test  
{
	public static void mai(String[] args) throws java.lang.Exception
	{   
	    System.out.println(sum(10, 20));
	    System.out.println(sum(10,20,30));
	    System.out.println(sum(10.2f, 20.3f));
	}
	
	/**
	 * 计算两个整型变量的值
	 */
	public static int sum(int a, int b) {
	    retura+b;
	}
	
	/**
	 * 方法的重写-参数的个数不同
	 * 计算三个整型变量的值
	 */
	public static int sum(int a, int b, int c) {
	    retura+b+c;
	}
	
	/**
	 * 方法的重写-参数的类型不同
	 * 计算两个float变量的值
	 */
	public static float sum(float a, float b) {
	    retura+b;
	}
}
```

### 数组

> 存储同一种数据类型额多个元素的容器.

#### 特点

> 每一个元素都有索引, 从0开始,最大编号是长度-1

#### 格式

```java
数据类型[] 数组名称;(推荐使用这种)
数据类型 数组名称[];

int a[];
int[] a;// 这种是最常用的格式

int a[5]; // 这样声明一维数组是非法的。
```

#### 数组的初始化

> 为数组开辟内存空间，并为每一个元素给出初始化值

##### 动态初始化

> 初始化的时候只指定数组长度，由系统为数组分配初始值
>
> **数组定义与为数组元素分配空间和赋值的操作分开进行**

```java
class test  
{
	public static void mai(String[] args) throws java.lang.Exception
	{   
	    // 数组的定义
	    // int[] a;
	    // 为数组分配内存空间(new)
	    // a = new int[3];
	    // 上面两个也可以写在一起
	    int[] a = new int[3];
	    // 赋值
	    a[0] = 2;
	    System.out.println(a[0]);
        // 直接输出数组会得到地址值(内存)  // 地址值：[I@659e0bfd
	}
}
```

##### 静态初始化

> 在定义数组的同时给数组分配空间并赋值

```java
class test  
{
	public static void mai(String[] args) throws java.lang.Exception
	{   
	    // 在定义数组的同时给数组分配空间并赋值。
        int[] b = new int[]{1,2,3};
	    int[] a = { 3, 9, 8};   
	    System.out.println(a[1]);
	}
}
```

#### Java的内存分配

- 栈: 存储局部变量
- 堆: 存储所有new出来的变量
- 方法区(面向对象)
- 本地方法区(系统相关)
- 寄存器(CPU使用)
- 注意：
  - 局部变量:所有定义在方法中或者方法声明中的变量
  - 栈内存和堆内存的区别
    - 栈内存中, 数据使用完毕后就消失
    - 堆内存中, 每一个new出来的数据都有地址
    - 堆内存中, 每一个变量都有默认值([基本数据类型](#_基本数据类型), **引用数据类型: null**)
    - 堆内存中, 数据使用完毕后, 等待垃圾回收器空闲的时候回收

#### 数组的内存分配图

> 内存图解1：一个数组的初始化和赋值

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082240041.jpg)	

> 内存图解2：两个数组的初始化和赋值

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082240223.jpg)	

> 内存图解3：三个数组, 其中两个栈变量指向同一个数组对象

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082250002.jpg)		

#### 数组的常见操作

```java
/**  
 * 数组的常见使用：遍历, 最值, 逆序, 查表, 基本查找法
 * @author hyatt  
 * @versio1.0  
 */
public class ArrayTest2 {

	public static void main(String[] args) {
		int[] arr = { 1, 2, 3 };
		reverseArray2(arr);
		printArray2(arr);
		System.out.println(getMaxValue(arr));
	}

	/**
	 * 遍历数组  
	 * @param arr
	 */
	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

	/**
	 * 遍历数组2
	 * @param arr
	 */
	public static void printArray2(int[] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
				System.out.print("]");
			} else {
				System.out.print(arr[i]);
				System.out.print(",");
			}

		}
		System.out.println();
	}

	/**
	 * 获取数组中的最值-这里取最大值  
	 * @param arr
	 * @return
	 */
	public static int getMaxValue(int[] arr) {
		int max = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		returmax;
	}

	/**
	 * 
	 * 逆序, 将数组倒过来 
	 * @param arr
	 */
	public static void reverseArray(int[] arr) {
		for (int i = 0; i < arr.length / 2; i++) {
			int temp = arr[i];
			arr[i] = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = temp;
		}
	}

	/**
	 * 
	 * 逆序, 将数组倒过来 
	 * @param arr
	 */
	public static void reverseArray2(int[] arr) {
		for (int start = 0, end = arr.length - 1; start < end; start++, end--) {
			int temp = arr[start];
			arr[start] = arr[end];
			arr[end] = temp;
		}
	}

	/**
	 * 获取某一个索引的值
	 * @param arr
	 * @param index
	 * @return
	 */
	public static int getValue(int[] arr, int index) {
		returarr[index];
	}

	/**
	 * 获取值对应的索引
	 * @param arr
	 * @param value
	 * @return
	 */
	public static int getIndex(int[] arr, int value) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == value) {
				returi;
			}
		}
		// 没有则返回-1
		retur-1;
	}

	/**
	 * 获取值对应的索引
	 * @param arr
	 * @param value
	 * @return
	 */
	public static int getIndex2(int[] arr, int value) {
		int index = -1;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == value) {
				index = i;
				break;
			}
		}
		// 没有则返回-1
		returindex;
	}
}
```

### 二维数组(掌握)

> 元素是一维数组的数组.

```java
数据类型[][] 数组名称 = new 数据类型[m][n];
数据类型[][] 数组名称 = new 数据类型[m][];
数据类型[][] 数组名称 = new 数据类型[][]{{..},{..}};
数据类型[][] 数组名称 = {{..},{..}};
```

#### 初始化

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082244488.png)	

#### 内存图解

> 内存图解一

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082246503.jpg)	

> 内存图解二:注意和1的区别

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082247127.jpg)	

> 内存图解三:和2差不多，但是是由系统自动分配空间

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082247089.jpg)	

> 各个维度的数组模型

​	![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082248511.png)

#### 练习

> 二维数组的遍历, 求和, 打印杨辉三角

```java
import java.util.Scanner;

/**  
 * 二维数组的遍历, 求和, 杨辉三角
 * 
 * 1
 * 1 1
 * 1 2 1
 * 1 3 3 1
 * 1 4 6 4 1
 * 1 5 10 10 5 1
 * @author hyatt  
 * @versio1.0  
 */
public class ArrayTest3 {

	public static void main(String[] args) {
		int[][] arr = { { 1, 2, 3 }, { 4, 5, 6 } };
		printArray(arr);
		System.out.println(getSum(arr));
		Scanner sc = new Scanner(System.in);
		System.out.println("输入杨辉三角的行数:");
		int rows = sc.nextInt();
		printYhTriangle(rows);
	}

	/**
	 * 打印二维数组
	 * @param arr
	 */
	public static void printArray(int[][] arr) {
		System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print("[");
			for (int j = 0; j < arr[i].length; j++) {
				if (j == arr[i].length - 1) {
					System.out.print(arr[i][j] + "]");
					if (i != arr.length - 1) {
						System.out.print(",");
					}
				} else {
					System.out.print(arr[i][j] + ",");
				}
			}
		}
		System.out.print("]");
	}

	/**
	 * 对二维数组求和
	 * @param arr
	 * @return
	 */
	public static int getSum(int[][] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				sum += arr[i][j];
			}
		}
		retursum;
	}

	/**
	 * 打印杨辉三角
	 * @param rows 几行
	 */
	public static void printYhTriangle(int rows) {
		int[][] arr = new int[rows][rows];

		// 任意一行的第一列和最后一列都是1
		for (int i = 0; i < arr.length; i++) {
			arr[i][0] = 1;
			arr[i][i] = 1;
		}
		// 从第二行还是计算, 从第二列还是到倒数第二列结束, 每一列的值等于上一行的自己列和前一列的值相加
		for (int i = 2; i < arr.length; i++) {
			for (int j = 1; j < arr[i].length - 1; j++) {
				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
			}
		}
		// 打印
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}

	}
}

```

### 数组高级

#### 排序

```java
import java.util.Arrays;

/**
 * 数组排序工具类.<br>
 * 主要包含快速排序, 冒泡排序, 选择排序
 * @author hyatt
 * @versio1.0
 */
public class ArraySort {

	/**
	 * 选择排序: 从小到大<br>
	 * 从0索引开始，依次和后面的元素去比较，小的往前放，第一次完毕之后最小值出现在了最小索引处
	 * @param arr  
	 * @returvoid    返回类型  
	 * @throws
	 */
	public static void selectSort(int[] arr) {
		int temp;
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[i]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
				// 每次比较完输出一次数组
				// System.out.println(Arrays.toString(arr));
			}
		}

	}

	/**
	 * 冒泡排序: 相邻元素两两比较, 大的往后放, 第一次完毕之后, 最大值就出现在最大索引处, 同理继续, 即可得到一个排好序的数组.<br>
	 * 排序结果: 从小到大
	 * 
	 * @param arr 整型数组
	 */
	public static void bubbleSort(int[] arr) {
		int temp;
		// 次数 最多只需要排序数组的长度减一次
		/* 举例: 从小到大排序最多的数组肯定是它本身是从大到小, 例如原数组是: [5,4,3,2,1]
		 * 第一次排序后: 4,3,2,1,5
		 * 第二次排序后: 3,2,1,4,5
		 * 第三次排序后: 2,1,3,4,5
		 * 第四次排序后: 1,2,3,4,5
		 * 结束
		 */
		for (int i = 0; i < arr.length - 1; i++) {
			// 排序
			for (int j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j + 1] < arr[j]) {
					temp = arr[j + 1];
					arr[j + 1] = arr[j];
					arr[j] = temp;
				}
				// 每次比较完输出一次数组
				// System.out.println(Arrays.toString(arr)); 
			}
		}
	}

	/**
	 * 该方法实现了快速排序 从小到大
	 * 
	 * @param arr  需要排序的数组
	 * @param low  最小序列
	 * @param high 最大序列
	 */
	public static void quickSort(int[] arr, int low, int high) {

		if (low < high) {
			// 找寻基准数据的正确索引
			System.out.println(Arrays.toString(arr));
			int index = getIndex(arr, low, high);
			System.out.println(Arrays.toString(arr));
			// 进行迭代对index之前和之后的数组进行相同的操作使整个数组变成有序
			quickSort(arr, 0, index - 1);
			quickSort(arr, index + 1, high);
		}

	}

	/**
	 * 获取索引
	 * 
	 * @param arr  需要排序的数组
	 * @param low  开始索引
	 * @param high 结束索引
	 * @returint 返回整型
	 */
	private static int getIndex(int[] arr, int low, int high) {
		// 基准数据
		int tmp = arr[low];
		while (low < high) {
			// 当队尾的元素大于等于基准数据时,向前挪动high指针
			while (low < high && arr[high] >= tmp) {
				high--;
			}
			// 如果队尾元素小于tmp了,需要将其赋值给low
			arr[low] = arr[high];
			// 当队首元素小于等于tmp时,向前挪动low指针
			while (low < high && arr[low] <= tmp) {
				low++;
			}
			// 当队首元素大于tmp时,需要将其赋值给high
			arr[high] = arr[low];

		}
		// 跳出循环时low和high相等,此时的low或high就是tmp的正确索引位置
		// 由原理部分可以很清楚的知道low位置的值并不是tmp,所以需要将tmp赋值给arr[low]
		arr[low] = tmp;
		returlow; // 返回tmp的正确位置
	}

}
```

#### 查找

```java
/**
 * 二分查找
 * @param arr
 * @return
 */
public static int binarySearch(int[] arr, int value) {
	int mi= 0;
	int max = arr.length - 1;
	int mid = (mi+ max) / 2;
	while (arr[mid] != value) {
		if (arr[mid] > value) {
			max = mid - 1;
		} else {
			mi= mid + 1;
		}
		if (mi> max) {
			retur-1;
		}
		mid = (max + min) / 2;
	}
	returmid;
}
```

## Java面向对象(掌握)

> - 面向对象是一种常见的思想,比较符合人们的思考习惯
> - 面向对象可以将复杂的业务逻辑简单化,增强代码复用性; 
>
> - 面向对象具有抽象、封装、继承、多态等特性。
> - 面向对象的编程语言主要有:C++、Java、C#等。

### 面向对象

#### 面向对象的思想特点

- 是一种更加符合我们思考习惯的思想.
- 把复杂的问题简单化.
- 让我们从执行者变成了指挥者.

#### 面向对象思维

- 首先应该分析这个问题里面有哪些类和对象。
- 然后再分析这些类和对象应该具有哪些属性和方法。
- 最后分析类和类之间具体有什么关系

### 类与对象

> Java语言中最基本的单位是类, 所以我们需要使用类来体现事物.
>
> **类可以看成一类对象的模板，对象可以看成该类的一个具体实例.**

- 类
  - 是一组相关的属性(成员变量)和行为(成员方法)的集合, 是一个抽象的概念
- 对象
  - 是该类事物的具体存在, 是一个具体的实例(对象).

### 类的定义和使用

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091555931.png)	

### Java程序的开发, 设计和特征

- 开发：不断的创建对象, 通过对象调用功能
- 设计：就是管理和维护对象的关系
- 特征：封装, 继承和多态

### 成员变量和局部变量的区别

> - 在类中的位置不同
>
> - - 成员变量在类中的方法外
>   - 局部变量在类中的方法内
>
> - 在内存中的位置不同
>
> - - 成员变量在堆内存
>   - 局部变量在栈内存
>
> - 生命周期不同
>
> - - 成员变量随对象的创建而存在，随对象的消失而消失
>   - 局部变量随方法的调用而存在，随方法调用结束而消失
>
> - 初始化值不同
>
> - - 成员变量有初始化值, 初始化的值见下面的图
>   - 局部变量初始化使用,必须赋值, 否则编译报错
>
> - **注意事项：局部变量和成员变量在方法中使用的使用，采用的是就近原则**

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091558839.png)	

### 匿名对象

> 没有名字的对象.

#### 应用场景

- 调用方法, 仅仅只调用一次的时候
- 可以作为实际的参数传递

### 一个对象的初始化过程

```java
/**  
 * 以手机类为例,来理解一个内存的分配空间
 * 
 * Phone p=new Phone(); 做了什么？
 *  1.加载Phone.class文件进内存
 *  2.在栈内存为p开辟空间
 *  3.在堆内存为手机对象开辟空间
 *  4.对手机对象的成员变量进行默认初始化
 *  5.对手机对象的成员变量进行显示初始化
 *  6.通过构造方法对手机对象的成员变量赋值
 *  7.手机对象初始化完毕，把对象地址赋值给s变量
 * 
 * @author hyatt  
 * @versio1.0  
 */
public class PhoneDemo {

	public static void main() {
		Phone p = new Phone();
		p.brand = "一加7";
		p.price = 2999;
		p.color = "红色";

		p.call("虎哥");
		p.playGame();
	}

}

class Phone {
	String brand;
	int price;
	String color;

	public void call(String name) {
		System.out.println(name);
	}

	public void playGame() {
		System.out.println("打游戏");
	}

}
```

#### 默认初始化

> 默认初始化是系统在堆内存创建一个新的对象时，进行的默认初始化，如null 和0

#### 显示初始化

> 显示初始化是在类定义时，直接在各个成员变量的定义时，优先进行赋值，这叫显示初始化

#### 初始化顺序

> 默认初始化 -> 显示初始化 -> 构造方法初始化

### 构造方法

> 作用：用于对对象数据的初始化.
>
> **注意：当我们写了构造方法后, Java就不会为我们提供构造方法了,包括空构造方法**

#### 格式

- 方法名和类名一致
- 没有返回值类型，void也没有

- 没有具体返回值

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091604534.png)	

#### 内存分析

```java
public class Perso{
    int id;  //在person这类里面定义两个成员变量id和age,
    int age=20;  //给成员变量age赋了初值为20
    /**这里就是person这个类的一个构造方法
     * 构造方法的规则很简单，和类名要完全一样，一点都不能错，包括大小写。
     * 并且没有返回值，不能写void在它前面修饰
     * @param _id
     * @param _age
     */
    public Person(int _id,int _age ) {
        id = _id;
        age = _age;
    }
    
    public static void main(String[] args) {
     Persotom = new Person(1, 25); // 调用person这个构造方法创建一个新的对象，并给这个对象的成员变量赋初始值
	}
}
```

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091605726.png)	

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091605717.png)	

### this关键字

> - 方法被那个对象调用，this就代表这个对象
> - this是一个引用，它指向自身的这个对象。

#### 内存分析

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091616183.png)	

### supper关键字

> super表示父类存储空间的标识，可以理解为父类的引用，可以操作父类的成员

#### 测试

```java
package cn.hyatt.test;
/**
 * 父类
 * @author xuteng
 *
 */
class FatherClass {
    public int value;
    public void f() {
        value=100;
        System.out.println("父类的value属性值="+value); // 100
    }
}
/**
 * 子类ChildClass从父类FatherClass继承
 * @author xuteng
 *
 */
class ChildClass extends FatherClass {
    /**
     * 子类除了继承父类所具有的valu属性外，自己又另外声明了一个value属性，
     * 也就是说，此时的子类拥有两个value属性。
     */
    public int value;
    /**
     * 在子类ChildClass里面重写了从父类继承下来的f()方法里面的实现，即重写了f()方法的方法体。
     */
    public void f() {
        super.f();//使用super作为父类对象的引用对象来调用父类对象里面的f()方法
        value=200;//这个value是子类自己定义的那个valu，不是从父类继承下来的那个value
        System.out.println("子类的value属性值="+value); // 200
        System.out.println(value);//打印出来的是子类自定义的那个value的值，这个值是200
        /**
         * 打印出来的是父类里面的value值，由于子类在重写从父类继承下来的f()方法时，
         * 第一句话“super.f();”是让父类对象的引用对象调用父类对象的f()方法，
         * 即相当于是这个父类对象自己调用f()方法去改变自己的value属性的值，由0变了100。
         * 所以这里打印出来的value值是100。
         */
        System.out.println(super.value); // 100
    }
}
/**
 * 测试类
 * @author xuteng
 *
 */
public class TestInherit {
    public static void main(String[] args) {
        ChildClass cc = new ChildClass();
        cc.f();
    }
}
```

#### 内存分析图

> 分析任何程序都是从main方法的第一句开始分析的，所以首先分析main方法里面的第一句话:
>
> ChlidClass cc = new ChlidClass();
>
> 程序执行到这里时，首先在**栈空间里面会产生一个变量cc**，cc里面的值是什么这不好说，总而言之，通过这个值我们可以找到new出来的ChlidClass对象。由于子类ChlidClass是从父类FatherClass继承下来的，所以当我们new一个子类对象的时候，这个子类对象里面会包含有一个父类对象，而这个父类对象拥有他自身的属性value。这个value成员变量在FatherClass类里面声明的时候并没有对他进行初始化，所以系统默认给它初始化为0，成员变量（在类里面声明）在声明时可以不给它初始化，编译器会自动给这个成员变量初始化，但局部变量（在方法里面声明）在声明时一定要给它初始化，因为编译器不会自动给局部变量初始化，任何变量在使用之前必须对它进行初始化。
>
> 　　子类在继承父类value属性的同时，自己也单独定义了一个value属性，所以当我们**new出一个子类对象的时候，这个对象会有两个value属性**，一个是从父类继承下来的value，另一个是自己的value。在子类里定义的成员变量value在声明时也没有给它初始化，所以编译器默认给它初始化为0。因此，执行完第一句话以后，系统内存的布局如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091623606.png)	

> **接下来执行第二句话：cc.f();**
>
> 当new一个对象出来的时候，这个对象会产生一个this的引用，这个this引用指向对象自身。如果new出来的对象是一个子类对象的话，那么这个子类对象里面还会有一个super引用，这个super指向当前对象里面的父对象。所以相当于程序里面有一个this，**this指向对象自己，还有一个super，super指向当前对象里面的父对象**。
>
> 　　这里调用重写之后的f()方法，方法体内的第一句话：“super.f();”是让这个子类对象里面的父对象自己调用自己的f()方法去改变自己value属性的值，父对象通过指向他的引用super来调用自己的f()方法，所以执行完这一句以后，父对象里面的value的值变成了100。接着执行“value=200；”这里的vaule是子类对象自己声明的value，不是从父类继承下来的那个value。所以这句话执行完毕后，子类对象自己本身的value值变成了200。此时的内存布局如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091623109.png)	

> 方法体内的最后三句话都是执行打印value值的命令，前两句打印出来的是子类对象自己的那个value值，因此打印出来的结果为200，最后一句话打印的是这个子类对象里面的父类对象自己的value值，打印出来的结果为100。
>
> 　　到此，整个内存分析就结束了，最终内存显示的结果如上面所示。

### this和super的区别和使用

- 区别
  - this表示当前对象的引用
  - [super](#_supper关键字)可以理解为父类对象的引用
- 使用
  - 调用成员变量
    - this.成员变量
    - super.成员变量
  - 调用构造方法
    - this(…)
    - super(…)
  - 调用成员方法
    - this.成员方法
    - super.成员方法

### static关键字

> 原来一个类里面的成员变量，每new一个对象，这个对象就有一份自己的成员变量，因为这些成员变量都不是静态成员变量。对于static成员变量来说，这个成员变量只有一份，而且这一份是这个类所有的对象共享

![img](Java%E5%9F%BA%E7%A1%80%E6%80%BB%E7%BB%93.assets/202201091627709.png)	

 #### 特点

> 主要是上面描述的概括

- 随着类的加载而加载
- 优先于对象而存在
- 被类的所有对象共享
- 可以通过类名调用(也可以用对象调用)

#### 静态变量和成员变量的区别

- 所属不同

- - 静态变量属于类，所以也称为类变量
  - 非静态成员变量属于对象，所以也称为实例变量(实例对象)

- 内存中位置不同

- - 静态成员变量存在于方法区的静态区
  - 非静态成员变量存在于堆内存中

- 内存中出现的时间不同

- - 静态变量随这个类的加载而存在，随着类的消失而消失
  - 非静态随着对象的创建而存在，随着对象的消失而消失

- 调用不同

- - 静态变量可以通过类名调用,也可以通过对象调用
  - 非静态变量只可以通过对象名调用

#### 内存分析

```java
// 以下面的例子为例说明
package cn.hyatt.test;
public class Cat {
    /**
     * 静态成员变量
     */
    private static int sid = 0;
    private String name;
    int id;
    Cat(String name) {
        this.name = name;
        id = sid++;
    }
    public void info() {
        System.out.println("My Name is " + name + ",NO." + id);
    }
    public static void main(String[] args) {
        Cat.sid = 100;
        Cat mimi = new Cat("mimi");
        Cat pipi = new Cat("pipi");
        mimi.info(); // 100
        pipi.info(); // 101
    }
}
```

> 执行程序的第一句话：Cat.sid = 100;时，这里的sid是一个静态成员变量，静态变量存放在方法区中的静态区(data seg)，所以首先在静态区里面分配一小块空间sid，第一句话执行完后，sid里面装着一个值就是100。
>
> 　　此时的内存布局示意图如下所示:

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091631464.png)	

> 接下来程序执行到：

```java
Cat  mimi = new Cat(“mimi”);
// 这里，调用Cat类的构造方法Cat(String name)，构造方法的定义如下：
Cat (String name){
　　　this.name = name;
　　　id=sid++;
}
```

> 调用时首先在栈内存里面分配一小块内存mimi，里面装着可以找到在堆内存里面的Cat类的实例对象的地址，mimi就是堆内存里面Cat类对象的引用对象。这个构造方法声明有字符串类型的形参变量,所以这里把“mimi”作为实参传递到构造方法里面，由于**字符串常量是分配在方法区中的字符串常量池中存储的**，所以**字符串常量池中**里面多了一小块内存用来存储字符串“mimi”。此时的内存分布如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091634277.png)	

> 当调用构造方法时，首先在栈内存里面给形参name分配一小块空间，名字叫name,接下来把”mimi”这个字符串作为实参传递给name，字符串也是一种引用类型，除了那四类8种基础数据类型之外，其他所有的都是引用类型，所以可以认为**字符串也是一个对象**。所以这里相当于把”mimi”这个对象的引用传递给了name，所以现在name指向的是”mimi”。所以此时内存的布局如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091633734.png)	

> 接下来执行构造方法体里面的代码:this.name=name;
>
> **这里的this指的是当前的对象**，指的是堆内存里面的那只猫。这里把栈里面的name里面装着的值传递给堆内存里面的cat对象的name属性，所以此时这个name里面装着的值也是可以找到位于**字符串常量池**里面的字符串对象“mimi”的，此时这个name也是字符串对象“mimi”的一个引用对象，通过它的属性值就可以找到位于**字符串常量池**里面的字符串对象“mimi”。此时的内存分布如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091635619.png)	

> 接下来执行方法体内的另一句代码：id=sid++;
>
> 这里是把sid的值传递给id，所以id的值是100，sid传递完以后，自己再加1，此时sid变成了101。此时的内存布局如下图所示。

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091634813.png)	

> 到此，构造方法调用完毕，给这个构造方法分配的局部变量所占的内存空间全部都要消失，所以位于栈空间里面的name这块内存消失了。栈内存里面指向**字符串常量池**里面的字符串对象“mimi”的引用也消失了，此时只剩下堆内存里面的指向字符串对象“mimi”的引用没有消失。此时的内存布局如下图所示：　

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091634046.png)	

> 接下来执行：Cat pipi = new Cat(“pipi”);
>
> 这里是第二次调用构造方法Cat()，整个调用过程与第一次一样，调用结束后，此时的内存布局如下图所示：

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091636712.png)	

#### 总结

- 非静态成员变量id和name是每一个对象都有单独的一份。但对于静态成员变量来说，只有一份，**不管new了多少个对象，哪怕不new对象，静态成员变量在静态区也会保留一份**。
- 任何一个对象都可以访问这个静态的值，访问的时候访问的都是**同一块内存(特点中的共享)**

- 即便是没有对象也可以访问这个静态的值，通过“**类名.静态成员变量名**”来访问这个静态的值
- 静态变量和非静态变量的区别就在于静态变量可以用来计数，而非静态变量则不行

- 局部变量分配内存永远在栈里面，new出来的东西分配内存永远是在堆里，其它的都是在方法区.

- - **静态的东西分配内存永远是在方法区中的静态区**
  - 字符串常量在方法区中字符串常量池中

- 成员变量只有在new出一个对象来的时候才在堆内存里面分配存储空间。局部变量在栈内存里面分配存储空间。
- 静态方法不再是针对某一个对象来调用，所以不能访问非静态的成员。

- 非静态成员专属于某一个对象，想访问非静态成员必须new一个对象出来才能访问。
- 静态的变量可以通过对象名去访问，也可以通过类名去访问，两者访问的都是同一块内存。

#### 注意事项

- 静态方法中是没有this关键字的。

- - **原因:** 静态是随着类的加载而存在，而this是随着对象的创建而存在

- 静态方法**只能访问**静态成员变量和静态成员方法(**非静态方法既可以**访问静态的成员变量和成员方法，也可以访问非静态的成员变量和成员方法)

### final关键字

>final 可以修饰类，方法，变量
>
>`public static final String .....`

#### 特点

- final修饰类

- - final修饰的类无法被继承。

- final修饰方法

- - final修饰的方法不可以被重写。

- final修饰变量

- - final修饰的变量无法被重新赋值。
  - final修饰的变量因为它无法被修改，所以它修饰的变量就是 **自定义的常量**

#### final关键字修饰局部变量

> 基本类型:final修饰的局部变量为基本类型的时候，值不可以被改变
>
> 引用类型:final修饰的局部变量为引用类型的时候，地址值不可以被改变，但是堆内存中的值可以被修改

```java
/**
 * @ClassName: FInalTest
 * @Description: 测试Final关键字 - Final修饰局部变量
 * @author hyatt
 * @date 2021年5月5日
 */
public class FinalTest {
	
	public static void main(String[] args) {
		// final 修饰基本类型
		int x = 10;
		x = 100;
		System.out.println("x: " + x);
		final int y = 100;
		// y = 10; 这里赋值会发生错误, 所final修饰的基本类型的值不可以修改
		System.out.println("================================");
		// final 修饰引用类型
		Student s = new Student();
		System.out.println(s.age);
		s.age = 100;
		System.out.println(s.age);
		// 使用final 修饰
		final Student ss = new Student();
		System.out.println(ss.age); // 10
		ss.age = 100; 
		System.out.println(ss.age); // 100 说明 final修饰的引用类型的数据的值是可以修改的
		
		// 更改 ss 的地址值, 重新 new 一个对象
		// ss = new Student();// 这里会发生错误, 说明final 修饰的引用类型的数据的地址值不可以修改
		
	}

}

class Student {
	int age = 10;
}
```

#### 初始化

- 只能初始化一次
- 常见的给值
  - 定义变量的时候(推荐)
  - 构造方法中

### 制作帮助文档

> 命令行生成javadoc
>
> `javadoc -d doc -author -versioDBUtil.java`
>
> **-d:表示文件路径，如果不存在则新建**
>
> 使用eclipse生成
>
> [Eclipse中生成javadoc](https://blog.csdn.net/qq_32786873/article/details/53068194)

### 代码块

> 使用{}括起来的代码称为代码块。
>
> - 局部代码块：在方法中出现，用于限定变量的声明周期，及早释放提高内存利用率。
> - 构造代码块：在类的方法外中出现，**每次调用构造方法都会执行**(即声明对象),并且**在构造方法之前执行(与代码的位置无关)**，用于将多个构造方法中相同的代码存放到一起。
>
> - 静态代码块：在类的方法外中出现，使用**static修饰**，在类的加载的时候执行，**并且只执行一次。**
> - **执行顺序：静态代码块 -> 构造代码块 -> 构造方法.**

```java
class TestDemo {
    // 静态代码块
    static {
        System.out.println("开始执行静态代码块1");
    }
    
    // 构造代码块
    {
        System.out.println("开始执行构造代码块1");
    }
    // 构造方法
    public TestDemo() {
        System.out.println("开始执行构造方法");
    }
    
    // 构造代码块
    {
        System.out.println("开始执行构造代码块2");
    }
    
    static {
        System.out.println("开始执行静态代码块2");
    }
}

class test  
{
	public static void mai(String[] args) throws java.lang.Exception
	{   
	    // 局部代码块
	    {
	        int x = 10;
	        System.out.println("x: " + x);
	    }
	    // error: cannot find symbol
	    // System.out.println("x: " + x);
	    // 声明两个对象，发现每次声明对象的时候都会先执行构造代码,在执行构造方法(与位置无关)
	    // 声明两个对象, 发现静态代码块只执行了一次，并且是在构造函数和构造代码块执行执行(静态是随着类的加载而加载)
	    TestDemo t = new TestDemo();
	    TestDemo t1 = new TestDemo();
	}
}
/*
输出结果：
x: 10
开始执行静态代码块1
开始执行静态代码块2
开始执行构造代码块1
开始执行构造代码块2
开始执行构造方法
开始执行构造代码块1
开始执行构造代码块2
开始执行构造方法
*/
```

### 分层初始化

```java
class X {
	Y b = new Y();
    X() {
    	System.out.println("X");
    }
}

class Y {
	Y() {
    	System.out.println("Y");
    }
}

public class Z extends X {
	Y y = new Y();
    Z() {
    	System.out.println("Z");
    }
    public static void main(String[] args) {
    	new Z();
    }
}
/*
    结果: YXYZ
    执行顺序：
    1. 执行main方法, 然后执行 new Z()语句
    2. 由于类Z继承了X, 所以先执行 new X()
    3. new X() 执行, 按照默认初始化 -> 显示初始化 -> 构造初始化的顺序, 所以先输出 Y , 然后输出X
    4. 父类初始化执行完毕后, 还是按照上面初始化的顺序, 然后在类Z中执行 Y y = new Y(); 所以在输出 Y, 最后输出Z
    5. 所以最后结果是 YXYZ
    疑问:
    1. 按照 默认初始化 -> 显示初始化 -> 构造初始化 的顺序, 应该先执行 类Z 中的 Y y = new Y();再执行构造方法中的super();
    2. 即时先执行了父类的初始化,那为什么不继续执行构造方法中的System.out.println("Z");这个呢
    总结：
        子父类的初始化遵循分层初始化的原则, 即先进行父类初始化, 在执行子类初始化.
        这两个的初始化是分开的, 和super()在子类中的位置和顺序无关
*/
```

### 方法的重写

> 子类中的方法和父类中的方法的声明一样(包括返回类型, 方法名称以及方法参数)

#### 注意事项

- 父类中的私有方法不可以被重写

- - 子类不能继承父类的私有方法

- 子类重写父类方法的时候，权限不能更低

- - 最好和父类中的方法权限一致

- 父亲的静态方法, 子类也必须通过静态方法去重写

### 抽象类

> - 抽象类和抽象方法必须使用abstract关键字修饰
> - 抽象类中不一定要有抽象方法，但是有抽象方法的类必须是抽象类。
>
> - 抽象类不可以实例化
>
> - - 抽象类是有构造方法的，但是它不能实例化，所以它是子类用来调用的
>
> - 抽象的子类
>
> - - 如果不想重写抽象方法，该子类是一个抽象类
>   - 重写所有的抽象方法，该子类是一个具体类

![image.png](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091648544.gif)	

#### 通过程序理解抽象类

```java
/**
 * 父类Animal
 * 在class的前面加上abstract，即声明成这样：abstract class Animal
 * 这样Animal类就成了一个抽象类了
 */
abstract class Animal {
    public String name;
    public Animal(String name) {
        this.name = name;
    }
   
    /**
     * 抽象方法
     * 这里只有方法的定义，没有方法的实现。
     */
    public abstract void enjoy();
   
}
/**
 * 这里的子类Cat从抽象类Animal继承下来，自然也继承了Animal类里面声明的抽象方法enjoy()，
 * 但子类Cat觉得自己去实现这个enjoy()方法也不合适，因此它把它自己也声明成一个抽象的类，
 * 那么，谁去实现这个抽象的enjoy方法，谁继承了子类，那谁就去实现这个抽象方法enjoy()。
 * @author xuteng
 *
 */
abstract class Cat extends Animal {
    /**
     * Cat添加自己独有的属性
     */
    public String eyeColor;
    public Cat(String n, String c) {
        super(n);//调用父类Animal的构造方法
        this.eyeColor = c;
    }
}
/**
 * 子类BlueCat继承抽象类Cat，并且实现了从父类Cat继承下来的抽象方法enjoy
 * @author xuteng
 *
 */
class BlueCat extends Cat {
    public BlueCat(String n, String c) {
        super(n, c);
    }
    /**
     * 实现了抽象方法enjoy
     */
    @Override
    public void enjoy() {
        System.out.println("蓝猫叫...");
    }
   
}
/**
 * 子类Dog继承抽象类Animal，并且实现了抽象方法enjoy
 * @author xuteng
 *
 */
class Dog extends Animal {
    /**
     * Dog类添加自己特有的属性
     */
    public String furColor;
    public Dog(String n, String c) {
        super(n);//调用父类Animal的构造方法
        this.furColor = c;
    }
    @Override
    public void enjoy() {
        System.out.println("狗叫....");
    }
}
public class TestAbstract {
    /**
     * @param args
     */
    public static void main(String[] args) {
        /**
         * 把Cat类声明成一个抽象类以后，就不能再对Cat类进行实例化了，
         * 因为抽象类是残缺不全的，缺胳膊少腿的，因此抽象类不能被实例化。
         */
        //Cat c = new Cat("Catname","blue");
        Dog d = new Dog("dogname","black");
        d.enjoy();//调用自己实现了的enjoy方法
       
        BlueCat c = new BlueCat("BlueCatname","blue");
        c.enjoy();//调用自己实现了的enjoy方法
    }
}
```

#### 问题

>  一个类如果没有抽象方法，可不可以定义为抽象类，如果可以，有什么意义

可以。

不让创建该类对象

> 抽象类不能实例化, 那构造方法有什么用

让子类调用父类的构造方法，初始化父类的数据

> abstract不可以和哪些关键字共存

- private

- - 冲突(private修饰的属性和方法无法被继承,但是抽象方法必须被继承然后重写)

- final

- - 冲突(final修饰的方法可以被继承，但是不可以被重写,而抽象方法必须被重写)

- static

- - 无意义(static修饰的方法可以直接由类直接调用，但是抽象方法没有方法体，所以无意义)

### 接口

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091652937.gif)	

#### 多重继承

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091705519.jpg)	

#### 接口和抽象类

> 抽象类所具有的一些东西接口可以具有，假如**一个抽象类里面所有的方法全都是抽象的**，没有任何一个方法需要这个抽象类去实现，**并且这个抽象类里面所有的变量都是静态(static)**变量，都是不能改变(final)的变量(public static final 类型 属性名)，这时**可以把这样的抽象类定义为一个接口(interface)**。把一个类定义成一个接口的**格式是把声明类的关键字class**用声明接口的关键字interface**替换掉即可**。

#### 接口特性

- 接口中只能定义抽象方法, 所以他没有构造方法(**JDK8****之后可以有静态和默认方法**)
- 接口可以多重实现
- 接口中声明的属性默认为public static final的, 也只能是这样修饰
- 接口中只能定义抽象方法，而且这些方法默认是public也只能是public
- 接口可以继承其它的接口, 并添加新的属性和抽象方法

#### 关系

- 类与类：继承关系. 只能单继承不能多继承
- 类与接口：实现关系. 只能类实现接口, 一个类可以实现多个接口, 一个接口也可以被多个类实现
- 接口与接口：继承关系. 可以单继承也可以多继承

#### 接口新特性代码展示(JDK8)

```java
package com.hyatt.duotai;

/**  
 * @author hyatt  
 * @versio1.0  
 */
public interface UserDao {

	public static void show() {
		System.out.println("接口中的静态方法");
	}

	public default void play() {
		System.out.println("接口中的默认方法");
	}
}
```

```java
package com.hyatt.duotai;

/**  
 * @author hyatt  
 * @versio1.0  
 */
public class UserDaoImpl implements UserDao {
	
}
```

```java
package com.hyatt.duotai;

/**  
 * @author hyatt  
 * @versio1.0  
 */
public class UserDaoTest {
	public static void main(String[] args) {
		UserDao.show();
		UserDao userDao = new UserDaoImpl();

		userDao.play();
	}
}
// 接口中的静态方法
// 接口中的默认方法
```

### 面向对象的特征

#### 封装

> 隐藏实现细节, 提供公共的访问方式.

##### 好处

- 隐藏了实现细节，提供公共的访问方式
- 提高了代码的复用性

- 提高了代码的安全性

##### 设计原则

>  将不想要外界知道的实现细节给隐藏起来, 提供公共的访问方式(**private就是封装的一种体现**)

##### 访问权限

> public  protected   default    private

|            | private | default | protected | public |
| ---------- | ------- | ------- | --------- | ------ |
| 同一类     | ✔️       | ✔️       | ✔️         | ✔️      |
| 同一包的类 |         | ✔️       | ✔️         | ✔️      |
| 子类       |         |         | ✔️         | ✔️      |
| 其他包的类 |         |         |           | ✔️      |

#### 继承

> 多个类中存在相同属性和行为时，将这些内容抽取到单独一个类中，那么多个类无需再定义这些属性和行为，只要继承那个类就可以，这个单独的类称为**父类，基类或者超类**，其他类称为**子类或者派生类。**

##### 继承的好处

- 提高代码的复用性

- - 可以少很多重复的代码

- 提高的代码维护性

- - 如果B类继承A类的方法, 这样修改的时候，我只需要修改A类即可，不用A类和B类同时修改

- 让类与类产生了关系也是多态的前提(也是继承的一个弊端---类的**耦合性**增强了)

##### 继承的特点

- Java中只支持单继承，不支持多继承(只能继承一个父类)
- Java中支持多层继承(B继承A, C继承B)

##### 注意事项

- 子类只能继承父类中非私有的成员变量和成员方法(这**其实也打破了类的封装性**)。
- 子类**不能继承父类的构造方法**，但是可以通过super去访问父类的构造方法。

- 不要为了部分功能去继承。
- 只要他满足**继承关系**，就可以考虑使用继承。

##### Java继承中成员的访问(就近原则)

- 成员变量
  - 父类和子类的成员变量名称不一致, 直接访问即可
  - 父类和子类的成员变量一直, 遵循就近原则, 先在子类的局部范围找, 再在成员范围找, 再在父类的成员范围找, 有就使用, 没有就报错
- 构造方法
  - 子类的构造方法会默认取访问父类的无参构造方法
  - 如果父类没有无参构造方法, 需要使用super去明确调用构造方法
- 成员方法(访问参照成员变量)

##### 继承和构造方法的关系

> - 子类中所有的构造方法都会默认访问父类的空参构造方法.
>
> - - **子类初始化之前都会先完成父类的初始化**
>
> - 如果父类中没有无参构造方法,需要使用super关键字去调用其他构造方法
>
> - - super语句和this语句去调用构造方法必须放在第一行
>
> - **总结: 必须要调用父类的构造方法。(子类的构造方法默认调用父类的无参构造方法, super())**

#### 多态

> 一个对象变量可以指向多种实际类型的现象称为多态

##### 多态的前提

- 要有继承或者实现关系
- 要有方法重写

- 要有父类(接口)引用指向子类对象

##### 多态的分类

- 具体类

  ```java
  class F{}
  class Zi extends F{}
  ```

- 抽象类

  ```java
  abstract class Fu{}
  class Zi extends F{}
  ```

- 接口

  ```java
  interface F{}
  class Zi implements F{}
  ```

##### 多态中的成员访问特点(动态绑定)

- 成员变量
  - 编译看左边，运行看左边
- 构造方法
  - 创建子类的对象的时候，访问父类的构造方法，对父类的数据进行初始化
- 成员方法
  - 编译看左边, 运行看右边
- 静态方法
  - 编译看左边，运行看左边

```java
/**
 * @ClassName: DuoTaiTest
 * @Description: 多态的测试 - 编译和执行
 * @author hyatt
 * @date 2021年5月5日
 */
public class DuoTaiTest {

	public static void main(String[] args) {
		// 父类引用指向子类对象
		Ff= new Zi();
		// 成员变量
		System.out.println(fu.num); // 100, 取的值是父类中的值
		// System.out.println(fu.num2);// 编译报错. 由于父类中没有num2变量. 所以这里针对成员变量是 编译看左边， 运行看左边

		// 成员方法
		fu.show(); // show Zi. 调用的是子类的方法
		// fu.method(); // 编译报错. 由于父类中没有method方法. 所以这里针对成员方法是 编译看左边, 运行看右边

		// 静态方法
		fu.function(); // functioFu. 调用的是父类中的静态方法, 所以这里是 编译看左边, 运行看左边
	}
}

class F{
	public int num = 100;

	public void show() {
		System.out.println("show Fu");
	}

	public static void function() {
		System.out.println("functioFu");
	}
}

class Zi extends F{
	int num = 1000;
	int num2 = 2000;

	public void show() {
		System.out.println("show Zi");
	}

	public void method() {
		System.out.println("method Zi");
	}

	public static void function() {
		System.out.println("functioZi");
	}

}
```

##### 多态的好处

- 提高代码的维护性(继承体现)
- 提高代码的扩展性(多态体现)

##### 多态的弊端

> 父不能使用子的特有功能

##### 多态中的转型

- 向上转型(子到父)
  - 子转父: F f = new Zi();
- 向下转型(父到子)
  - 父转子: Zi   z = (Zi)f;

### 类与类之间的关系

#### 关联关系(最弱的一种关系)

> 关系最弱的一种关系叫关联关系。关联关系反应到代码上往往是一个类的方法里面的参数是另一个类的具体的某一个对象，比如教授教研究生，教哪个研究生，教是教授这个类里面的一个方法，某个研究生是研究生这个类里面的一个具体的对象。关联关系是最弱的一种关系，**咱们两个类之间有关系，或者两个对象之间有关系，但关系不是很紧密**

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091735458.png)	

#### 继承关系（比较强的一种关系）

> **继承关系封装了这样一种逻辑：“XX是一种XX”**，只要这东西能说通了，就可以**考虑用继承关系来封装它**。如：游泳运动员从运动员继承，游泳运动员是一种运动员，这能说得通，所以游泳运动员就是从运动员继承过来的，游泳运动员和运动员就是一种继承关系。学生是一个人，学生从人继承，老师是一个人，老师也从人继承，学生是一种老师，这说不通，所以学生和老师就不存在继承关系。所以将来做设计的时候要分清继承关系很简单，你只要说通这么一句话：“XX是一种XX”。OK，那他们之间就是继承关系。篮球运动员是一种球类运动员，这说得通，所以篮球运动员从球类运动员继承，这样继承很有可能会产生一棵继承树，运动员派生出来，派生出来的意思是游泳运动员这个类、球类运动员这个类、射击运动员类从它继承，相当于运动员派生出来了这些个不同的运动员，包括游泳的，球类的，射击的。球类的再派生足球的，篮球的，排球的。这就是一棵继承树，不过这棵树是比较理想化的情况，只有一个根节点。但实际当中，我们真实世界当中的继承关系不一定只从一个类继承，一个类可能从多个类继承过来，比如说：金丝猴从动物这个类继承，这很正常，但我还有另外一个专门的类：“应该受到保护的东西”，这也是一个类，金丝猴应该受到保护，所以金丝猴是一种应该受到保护的东西。所以金丝猴从“应该受到保护的东西”这个类继承过来。所以在现实情况当中，一个类完完全全有可能从多个不同的类继承，C正是因为想封装这种继承关系，所以C存在多重继承。java没有多重继承(但是由接口实现)

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091735681.png)	

#### 聚合关系（整体和部分）（比较强的一种关系）

> 什么是聚合？聚合就是一个整体与部分的关系。我们说“XX是XX的一部分”，只要说得通，那么他们之间的关系就是聚合关系，队长是球队的一部分，队员是球队的一部分。所以队长和球队是聚合关系，队员和球队也是聚合关系。脑袋是人的以部分，身体和胳膊也是人的一部分，因此脑袋，身体和胳膊与人都是聚合关系。聚合关系分得再细一点的话就可以分成**聚集关系和组合关系**，比如球队、队长，队员，这三者是聚集关系，假如这个队长既是足球的队长，同时也是篮球的队长，一个人分属两个不同的球队，这是可以的，球队与队长之间没有我离不了你，你离不了我这种情况，所以如果分得更细的话，这种就叫做聚集关系。还有一种情况叫组合，组合说的是咱们俩密不可分，我是你必不可少的一部分。一个人的脑袋不可能既属于你又属于别人，身体也一样，不可能既属于你又属于别人。所以你的身体，脑袋和你是密不可分的，这是一种更加严格的聚合关系，专门给它取了个名字叫组合。

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091736057.png)	

#### 实现关系

> 作为父类来说，我觉得我应该具有这样一个方法，但我不知道怎么去实现，谁去实现，我的子类去实现，这就是实现关系。和实现关系息息相关的还有一种关系叫**多态**。

### 内部类

> 把类定义在其它类的内部，这个类称之为内部类。

```java
class Outer{
    // 内部类
	class Inner {
    
    }
}
```

#### 内部类的访问特点

- 内部类可以直接访问外部类的成员(包括私有)
- 外部类访问内部类成员必须创建内部类对象然后访问

#### 内部类的位置

> 成员内部类和局部内部类

```java
class Outer {
	private int num = 10;
    
    // 成员位置 - 成员内部类
    class Inner{}
    
    public void method() {
        // 局部位置 - 局部内部类
    	class Inner2{
        }
    }
}
```

#### 内部类的访问

> 外部类.内部类 **对象名 =** **外部类对象.内部类对象
>
> 成员内部类被static修饰后的访问方式是:
>
> 外部类.内部类 对象名 = new 外部类名.内部类类名()

```java
class Outer {
	private int num = 10;
    
    // 成员位置 - 成员内部类
    class Inner{
    	public void show() {
        	System.out.println(num);
        }
    }
    
    public static class Inner2 {
    	public void show() {
        	System.out.println("Inner2");
        }
    }
}

public class InnerClassDemo {
	public static void main(String[] args) {
    	Outer.Inner oi = new Outer().new Inner();
        oi.show();
        Outer.Inner2 oi2 = new Outer.Inner2();
        oi2.show();
    }
}
```

#### 匿名内部类

> 本质: 是一个继承了该类或者实现了改接口的子类匿名对象

```java
interface Inner3 {
	void show();
}

class Outer3 {
	public void method() {
		new Inner3() {
			public void show() {
				System.out.println("show");  
			}
		}.show();
	}
	
}

public class InnerClassTest3 {
	public static void main(String[] args) {
		Outer3 o = new Outer3();
		o.method();
	}
}
```

#### 面试题

##### 调用外部类的对象

```java
class Outer {
	private int num = 10;

	class Inner {
		private int num = 20;

		public void show() {
			int num = 30;
			System.out.println(num); // 30
			System.out.println(this.num); // 20
			System.out.println(Outer.this.num); // 10
		}
	}
}

public class InnerClassTest {
	public static void main(String[] args) {
		Outer.Inner oi = new Outer().new Inner();
		oi.show();
	}
}
```

##### 局部内部类访问局部变量必须要用final修饰

> 因为局部变量会随着方法的调用完毕而消失, 这个时候局部对象并没有立刻从堆内存中消失, 还要使用那个变量，为了让对象还可以继续使用，所以用final修饰，这样，它其实是一个常量值。

```java
class Outer2 {
	private int num = 10;
	
	public void method() {
		int num2 = 20; // 需要使用final修饰: final int num2 = 20;
		class Inner2 {
			public void show() {
				System.out.println(num); 
				System.out.println(num2);   // 这里没有使用final修饰是因为1.8之后隐式添加了final
			}
		}
		Inner2 i = new Inner2();
		i.show();
	}
	
}

public class InnerClassTest2 {
	public static void main(String[] args) {
		Outer2 o = new Outer2();
		o.method();
	}
}
```

## 常用类

### Object类

> Object类是类层次结构的根类, 所有的类都直接或者间接的继承自Object类

#### == 和 equals 方法的区别

- ==
  - 基本类型: 比较值是否相等
  - 引用类型: 比较地址值是否相等
- equals
  - 只能比较引用类型, 默认情况下是比较地址值, 但是我们可以根据需要重写这个方法

### String类

#### 字符串的特点

> 字符串一旦被赋值, 就不可以修改(这里指的是字符串的内容而不是引用)

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091752963.jpg)	

#### 面试题

String str = new String("Hello")和String str = "Hello";不同之处

> 前者创建两个对象，后者只会创建一个对象

![image.png](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091753864.gif)	

### StringBuffer类

> 用字符串做拼接, 比较耗时并且也耗内存, 而这种拼接操作又是比较常见的, 为了解决这个问题, Java就提供了一个字符串缓冲区的类 ——StringBuffer

#### StringBuffer，String和StringBuilder的区别

- StringBuffer和StringBuilder对应的值的长度和内容是可变的，而String是不可变的。
- StringBuffer类可以解决String类既耗时又浪费空间的缺点
- StringBuffer是同步的，在多线程下是安全的，效率低，但是StringBuilder是不同步的，在多线程下不安全，但是单线程无影响，并且效率较高。

### Arrays工具类

```java
import java.util.Arrays;

/**  
 * Arrays中的二分查找的返回值
 * @author hyatt  
 * @versio1.0  
 */
public class ArrayTest3 {

	public static void main(String[] args) {
		int[] arr = { 1, 3, 4, 10, 5, 6 };
		System.out.println(Arrays.toString(arr));
		// 排序
		Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		// 查找
		System.out.println(Arrays.binarySearch(arr, 10)); // 5
		System.out.println(Arrays.binarySearch(arr, 100)); // 最大长度加一 -7
	}
}
```

### Interger

> 为了让基本类型的数据进行更多的操作, Java就为每种基本类型提供了对应的包装类的类型.

| byte   | Byte      |
| ------ | --------- |
| short  | Short     |
| int    | Interger  |
| long   | Long      |
| float  | Float     |
| double | Double    |
| char   | Character |
| boolea | Boolea    |

#### String和int的互相转换

```java
/**  
 * 常见对象 - Interger
 * 字符串和int类型的变量值的相互转换
 * @author hyatt  
 * @versio1.0  
 */
public class IntegerTest {

	public static void main(String[] args) {
		// int -> String
		int num = 100;
		// 方式1
		String s = "" + num;
		System.out.println("s: " + s);
		// 方式2 推荐使用方式2
		String s1 = String.valueOf(num);
		System.out.println("s1: " + s1);
		// 方式3： int -> Interger -> String
		Integer i_num = new Integer(num);
		String s3 = i_num.toString();
		System.out.println("s3: " + s3);
		// 方式4
		String s4 = Integer.toString(num);
		System.out.println("s4: " + s4);
		
		// String -> int
		String s5 = "100";
		// 方式1 推荐使用这种
		int number = Integer.parseInt(s5);
		System.out.println("number: " + number);
		// 方式2
		Integer s_i = new Integer(s5);
		int number1 = s_i.intValue();
		System.out.println("number1: " + number1);
	}
}
```

#### 自动拆箱和装箱

```java
/**  
 * 常见对象 - Interger
 * JDK5的新特性: 自动拆箱和装箱
 * 自动拆箱: 引用类型装换成基本类型
 * 自动装箱: 基本类型转换成引用类型
 * @author hyatt  
 * @versio1.0  
 */
public class IntegerTest2 {

	public static void main(String[] args) {
		// Integer i = new Integer(100);
		// 自动装箱
		Integer i2 = 100;
		// Integer -> int 加10 之后 -> Integer
		i2 += 10;
		System.out.println("i2: " + i2);
		
		// 实际执行的代码: 通过反编译拿到的代码
		// Integer i2 = Integer.valueOf(100);
		// i2 = Integer.valueOf(i2.intValue() + 10);
		// System.out.println((new StringBuilder("i2: ")).append(i2).toString());
	}
}
```

#### 面试题

> -128到127之前的数据缓冲池的问题. 

```java
/**  
 * 常见对象 - Interger
 * JDK5的新特性: 缓冲池
 * @author hyatt  
 * @versio1.0  
 */
public class IntegerTest3 {

	public static void main(String[] args) {
		Integer i1 = new Integer(127);
		Integer i2 = new Integer(127);
		System.out.println(i1 == i2); // false
		System.out.println(i1.equals(i2)); // true

		Integer i3 = new Integer(128);
		Integer i4 = new Integer(128);
		System.out.println(i3 == i4); // falsee
		System.out.println(i3.equals(i4));// true

		Integer i5 = 128;
		Integer i6 = 128;
		System.out.println(i5 == i6); // false
		System.out.println(i5.equals(i6)); // true
		// 通过查看源码, 针对-128 到 127之间数据做了一个数据缓冲池, 如果数据是该范围内的，每次并不创建新的空间
		Integer i7 = 127;
		Integer i8 = 127;
		System.out.println(i7 == i8); // true
		System.out.println(i7.equals(i8)); // true
	}
}
```

> 源码查看：
>
> ![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091758315.png)	

### Random

> 产生随机数的类

```java
package com.hyatt.commonobjects;

import java.util.Random;

/**  
 * 常见对象 - Random：用于产生随机数的类
 * 
 * 构造方法:
 * 		1. Random(): 没有给种子, 用的是默认种子, 是当前时间的毫秒值
 * 		2. Random(long seed): 给出指定的种子
 * 
 * 给出种子后每次出现的随机数都是相同的.
 * 
 * 成员方法：
 * 		1. public int nextInt()：返回的是int类型范围内的随机数
 * 		2. public int nextInt(int bound)：返回的是[0,n)范围内的随机数
 * @author hyatt  
 * @versio1.0  
 */
public class RandomTest {

	public static void main(String[] args) {
		// Random r = new Random();
		Random r = new Random(100);

		// 多输出几次随机数
		for (int i = 0; i < 10; i++) {
			// int num = r.nextInt();
			int num = r.nextInt(100);
			System.out.println(num);
		}
	}

}
```

### BigDecimal

> 解决计算float和double运行是的精度问题

```java
package com.hyatt.commonobjects;

import java.math.BigDecimal;

/**  
 * BigDecimal: 解决计算精度的类
 * @author hyatt  
 * @versio1.0  
 */
public class BigDecimalTest {

	public static void main(String[] args) {
		/*
		System.out.println(0.01 + 0.09); // 0.09999999999999999
		System.out.println(1.0 - 0.32); // 0.6799999999999999
		System.out.println(1.015 * 100); // 101.49999999999999
		System.out.println(1.301 / 100); // 0.013009999999999999
		*/
		BigDecimal b1 = new BigDecimal("0.01");
		BigDecimal b2 = new BigDecimal("0.09");
		System.out.println(b1.add(b2)); // 0.10
		BigDecimal b3 = new BigDecimal("1.0");
		BigDecimal b4 = new BigDecimal("0.32");
		System.out.println(b3.subtract(b4)); // 0.68
		BigDecimal b5 = new BigDecimal("1.015");
		BigDecimal b6 = new BigDecimal("100");
		System.out.println(b5.multiply(b6)); // 101.500
		BigDecimal b7 = new BigDecimal("1.015");
		BigDecimal b8 = new BigDecimal("100");
		System.out.println(b7.divide(b8)); // 0.01015
	} 

}
```

### Date

> 表示精确的时间可以精确到毫秒

```java
package com.hyatt.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Date类的测试
 * 
 * Date的构造函数:
 * 		public Date():根据当前默认的毫秒值创建日期对象
 * 		public Date(long date): 根据指定的毫秒值创建对象
 * 
 * 成员方法:
 * 		public long getTime(): 获取 Date的毫秒值
 * 
 * 日期和字符串的格式化和解析
 */
import java.util.Date;

public class DateTest {
	public static void main(String[] args) throws ParseExceptio{
		// 创建对象
		Date d = new Date();
		System.out.println("d:" + d);

		// long time = System.currentTimeMillis();
		long time = 1000 * 60 * 60; // 1h
		Date d2 = new Date(time);
		System.out.println("d2:" + d2); // ThJa01 09:00:00 GMT+08:00 1970 时差 东八区
		
		// 成员方法
		// 获取时间的毫秒值
		// long time2 = d.getTime();
		long time2 = d2.getTime();
		System.out.println("time2：" + time2);
		// Date -> String
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(format.format(new Date()));
		System.out.println(new SimpleDateFormat().format(new Date()));
		// String -> Date
		String dateString = "2018-01-01 23:59:59";
		// 在把一个字符串解析为日期格式的时候, 必须要和给定的字符串的格式一致
		// SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(format2.parseObject(dateString));
	}

}
```

### Calendar

> 日历类

```java
package com.hyatt.calendar;

import java.util.Calendar;

/**  
 * Calendar: 日历类
 * 
 * public void add(int field,int amount):根据给定的日历字段和对应的时间, 对当前日日历进行操作
 * public final void set(int year,int month,int date): 设置年月日
 * 
 * 
 * @author hyatt  
 * @versio1.0  
 */
public class CalendarTest {
	public static void main(String[] args) {
		// GregorianCalendar calendar = new GregorianCalendar();
		Calendar calendar = Calendar.getInstance();
		// 当前年份 - 1
		calendar.add(Calendar.YEAR, -1);
		calendar.add(Calendar.MONTH, -1);
		calendar.add(Calendar.DATE, -1);
		// 获取1年前1月前一天前的年月日
		// 获取年份
		int year = calendar.get(Calendar.YEAR);
		// 获取月份
		int month = calendar.get(Calendar.MONTH);
		// 获取日
		int day = calendar.get(Calendar.DATE);
		System.out.println(year + "年" + (month + 1) + "月" + day + "日");

		// set 方法
		calendar.set(2019, 12, 1); // 月份是从0开始, 所以12表示1月, 其它都是 +1 月
		// 获取年份
		int year2 = calendar.get(Calendar.YEAR);
		// 获取月份
		int month2 = calendar.get(Calendar.MONTH);
		// 获取日
		int day2 = calendar.get(Calendar.DATE);
		System.out.println(year2 + "年" + (month2 + 1) + "月" + day2 + "日");
	}
}
```

##### 练习

> 获取任何年2月份的天数

```java
package com.hyatt.calendar;

import java.util.Calendar;
import java.util.Scanner;

/**  
 * 获取任意一年内的2月有多少天
 * @author hyatt  
 * @versio1.0  
 */
public class CalendarTest2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入年(yyyy)：");
		int year = sc.nextInt();
		/* 拿3月1号 - 2月1号
		// 3月1号
		Calendar c = Calendar.getInstance();
		c.set(year, 2, 1);
		Calendar c2 = Calendar.getInstance();
		// 2月1号
		c2.set(year, 1, 1);
		int twoMonthDays = c.get(Calendar.DAY_OF_YEAR) - c2.get(Calendar.DAY_OF_YEAR);
		System.out.println(year + "年的2月有" + twoMonthDays + "天");
		*/
		// 设置日立到3月1号, 然后向前推1天, 获取日期
		Calendar c = Calendar.getInstance();
		c.set(year, 2, 1);
		c.add(Calendar.DATE, -1);
		int twoMonthDays = c.get(Calendar.DATE);
		System.out.println(year + "年的2月有" + twoMonthDays + "天");
	}

}
```

### 正则表达式

> [则表达式-菜鸟教程](https://www.runoob.com/regexp/regexp-tutorial.html)
>
> [正则表达式在线测试](http://tool.oschina.net/regex/)

#### 正则表达式语法

| 元字符       | 描述                                                         |
| ------------ | ------------------------------------------------------------ |
| \            | 将下一个字符标记符、或一个向后引用、或一个八进制转义符。例如，“\\n”匹配\n。“\n”匹配换行符。序列“\\”匹配“\”而“\(”则匹配“(”。即相当于多种编程语言中都有的“转义字符”的概念。 |
| ^            | 匹配输入字符串的开始位置。如果设置了RegExp对象的Multiline属性，^也匹配“\n”或“\r”之后的位置。 |
| $            | 匹配输入字符串的结束位置。如果设置了RegExp对象的Multiline属性，$也匹配“\n”或“\r”之前的位置。 |
| *            | 匹配前面的子表达式任意次。例如，zo*能匹配“z”，“zo”以及“zoo”。*等价于{0,}。 |
| +            | 匹配前面的子表达式一次或多次(大于等于1次）。例如，“zo+”能匹配“zo”以及“zoo”，但不能匹配“z”。+等价于{1,}。 |
| ?            | 匹配前面的子表达式零次或一次。例如，“do(es)?”可以匹配“do”或“does”中的“do”。?等价于{0,1}。 |
| {n}          | n是一个非负整数。匹配确定的n次。例如，“o{2}”不能匹配“Bob”中的“o”，但是能匹配“food”中的两个o。 |
| {n,}         | n是一个非负整数。至少匹配n次。例如，“o{2,}”不能匹配“Bob”中的“o”，但能匹配“foooood”中的所有o。“o{1,}”等价于“o+”。“o{0,}”则等价于“o*”。 |
| {n,m}        | m和n均为非负整数，其中n<=m。最少匹配n次且最多匹配m次。例如，“o{1,3}”将匹配“fooooood”中的前三个o。“o{0,1}”等价于“o?”。请注意在逗号和两个数之间不能有空格。 |
| ?            | 当该字符紧跟在任何一个其他限制符（*,+,?，{n}，{n,}，{n,m}）后面时，匹配模式是非贪婪的。非贪婪模式尽可能少的匹配所搜索的字符串，而默认的贪婪模式则尽可能多的匹配所搜索的字符串。例如，对于字符串“oooo”，“o+?”将匹配单个“o”，而“o+”将匹配所有“o”。 |
| .            | 匹配除“\r\n”之外的任何单个字符。要匹配包括“\r\n”在内的任何字符，请使用像“[\s\S]”的模式。 |
| (pattern)    | 匹配pattern并获取这一匹配。所获取的匹配可以从产生的Matches集合得到，在VBScript中使用SubMatches集合，在JScript中则使用$0…$9属性。要匹配圆括号字符，请使用“\(”或“\)”。 |
| (?:pattern)  | 匹配pattern但不获取匹配结果，也就是说这是一个非获取匹配，不进行存储供以后使用。这在使用或字符“(\|)”来组合一个模式的各个部分是很有用。例如“industr(?:y\|ies)”就是一个比“industry\|industries”更简略的表达式。 |
| (?=pattern)  | 正向肯定预查，在任何匹配pattern的字符串开始处匹配查找字符串。这是一个非获取匹配，也就是说，该匹配不需要获取供以后使用。例如，“Windows(?=95\|98\|NT\|2000)”能匹配“Windows2000”中的“Windows”，但不能匹配“Windows3.1”中的“Windows”。预查不消耗字符，也就是说，在一个匹配发生后，在最后一次匹配之后立即开始下一次匹配的搜索，而不是从包含预查的字符之后开始。 |
| (?!pattern)  | 正向否定预查，在任何不匹配pattern的字符串开始处匹配查找字符串。这是一个非获取匹配，也就是说，该匹配不需要获取供以后使用。例如“Windows(?!95\|98\|NT\|2000)”能匹配“Windows3.1”中的“Windows”，但不能匹配“Windows2000”中的“Windows”。 |
| (?<=pattern) | 反向肯定预查，与正向肯定预查类似，只是方向相反。例如，“(?<=95\|98\|NT\|2000)Windows”能匹配“2000Windows”中的“Windows”，但不能匹配“3.1Windows”中的“Windows”。 |
| (?<!pattern) | 反向否定预查，与正向否定预查类似，只是方向相反。例如“(?<!95\|98\|NT\|2000)Windows”能匹配“3.1Windows”中的“Windows”，但不能匹配“2000Windows”中的“Windows”。 |
| x\|y         | 匹配x或y。例如，“z\|food”能匹配“z”或“food”或"zood"(此处请谨慎)。“(z\|f)ood”则匹配“zood”或“food”。 |
| [xyz]        | 字符集合。匹配所包含的任意一个字符。例如，“[abc]”可以匹配“plain”中的“a”。 |
| [^xyz]       | 负值字符集合。匹配未包含的任意字符。例如，“[^abc]”可以匹配“plain”中的“plin”。 |
| [a-z]        | 字符范围。匹配指定范围内的任意字符。例如，“[a-z]”可以匹配“a”到“z”范围内的任意小写字母字符。注意:只有连字符在字符组内部时,并且出现在两个字符之间时,才能表示字符的范围; 如果出字符组的开头,则只能表示连字符本身. |
| [^a-z]       | 负值字符范围。匹配任何不在指定范围内的任意字符。例如，“[^a-z]”可以匹配任何不在“a”到“z”范围内的任意字符。 |
| \b           | 匹配一个单词边界，也就是指单词和空格间的位置（即正则表达式的“匹配”有两种概念，一种是匹配字符，一种是匹配位置，这里的\b就是匹配位置的）。例如，“er\b”可以匹配“never”中的“er”，但不能匹配“verb”中的“er”。**单词边界: 就是不是单词字符的地方(位置)****单词字符: a-zA-z0-9;** |
| \B           | 匹配非单词边界。“er\B”能匹配“verb”中的“er”，但不能匹配“never”中的“er”。 |
| \cx          | 匹配由x指明的控制字符。例如，\cM匹配一个Control-M或回车符。x的值必须为A-Z或a-z之一。否则，将c视为一个原义的“c”字符。 |
| \d           | 匹配一个数字字符。等价于[0-9]。                              |
| \D           | 匹配一个非数字字符。等价于[^0-9]。                           |
| \f           | 匹配一个换页符。等价于\x0c和\cL。                            |
| \n           | 匹配一个换行符。等价于\x0a和\cJ。                            |
| \r           | 匹配一个回车符。等价于\x0d和\cM。                            |
| \s           | 匹配任何不可见字符，包括空格、制表符、换页符等等。等价于[ \f\n\r\t\v]。 |
| \S           | 匹配任何可见字符。等价于[^ \f\n\r\t\v]。                     |
| \t           | 匹配一个制表符。等价于\x09和\cI。                            |
| \v           | 匹配一个垂直制表符。等价于\x0b和\cK。                        |
| \w           | 匹配包括下划线的任何单词字符。类似但不等价于“[A-Za-z0-9_]”，这里的"单词"字符使用Unicode字符集。 |
| \W           | 匹配任何非单词字符。等价于“[^A-Za-z0-9_]”。                  |
| \xn          | 匹配n，其中n为十六进制转义值。十六进制转义值必须为确定的两个数字长。例如，“\x41”匹配“A”。“\x041”则等价于“\x04&1”。正则表达式中可以使用ASCII编码。 |
| \num         | 匹配num，其中num是一个正整数。对所获取的匹配的引用。例如，“(.)\1”匹配两个连续的相同字符。 |
| \n           | 标识一个八进制转义值或一个向后引用。如果\n之前至少n个获取的子表达式，则n为向后引用。否则，如果n为八进制数字（0-7），则n为一个八进制转义值。 |
| \nm          | 标识一个八进制转义值或一个向后引用。如果\nm之前至少有nm个获得子表达式，则nm为向后引用。如果\nm之前至少有n个获取，则n为一个后跟文字m的向后引用。如果前面的条件都不满足，若n和m均为八进制数字（0-7），则\nm将匹配八进制转义值nm。 |
| \nml         | 如果n为八进制数字（0-7），且m和l均为八进制数字（0-7），则匹配八进制转义值nml。 |
| \un          | 匹配n，其中n是一个用四个十六进制数字表示的Unicode字符。例如，\u00A9匹配版权符号（&copy;）。 |
| \< \>        | 匹配词（word）的开始（\<）和结束（\>）。例如正则表达式\<the\>能够匹配字符串"for the wise"中的"the"，但是不能匹配字符串"otherwise"中的"the"。注意：这个元字符不是所有的软件都支持的。 |
| \( \)        | 将 \( 和 \) 之间的表达式定义为“组”（group），并且将匹配这个表达式的字符保存到一个临时区域（一个正则表达式中最多可以保存9个），它们可以用 \1 到\9 的符号来引用。 |
| \|           | 将两个匹配条件进行逻辑“或”（Or）运算。例如正则表达式(him\|her) 匹配"it belongs to him"和"it belongs to her"，但是不能匹配"it belongs to them."。注意：这个元字符不是所有的软件都支持的。 |
| +            | 匹配1或多个正好在它之前的那个字符。例如正则表达式9+匹配9、99、999等。注意：这个元字符不是所有的软件都支持的。 |
| ?            | 匹配0或1个正好在它之前的那个字符。注意：这个元字符不是所有的软件都支持的。 |
| {i} {i,j}    | 匹配指定数目的字符，这些字符是在它之前的表达式定义的。例如正则表达式A[0-9]{3} 能够匹配字符"A"后面跟着正好3个数字字符的串，例如A123、A348等，但是不匹配A1234。而正则表达式[0-9]{4,6} 匹配连续的任意4个、5个或者6个数字 |

#### 校验数字

```java
1. 数字：^[0-9]*$
2. n位的数字：^\d{n}$
3. 至少n位的数字：^\d{n,}$7. 
4. m-n位的数字：^\d{m,n}$
5. 零和非零开头的数字：^(0|[1-9][0-9]*)$
6. 非零开头的最多带两位小数的数字：^([1-9][0-9]*)+(.[0-9]{1,2})?$
8. 带1-2位小数的正数或负数：^(\-)?\d+(\.\d{1,2})?$
9. 正数、负数、和小数：^(\-|\+)?\d+(\.\d+)?$
10. 有两位小数的正实数：^[0-9]+(.[0-9]{2})?$
11. 有1~3位小数的正实数：^[0-9]+(.[0-9]{1,3})?$
12. 非零的正整数：^[1-9]\d*$ 或 ^([1-9][0-9]*){1,3}$ 或 ^\+?[1-9][0-9]*$
13. 非零的负整数：^\-[1-9][]0-9"*$ 或 ^-[1-9]\d*$
14. 非负整数：^\d+$ 或 ^[1-9]\d*|0$
15. 非正整数：^-[1-9]\d*|0$ 或 ^((-\d+)|(0+))$
16. 非负浮点数：^\d+(\.\d+)?$ 或 ^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$
17. 非正浮点数：^((-\d+(\.\d+)?)|(0+(\.0+)?))$ 或 ^(-([1-9]\d*\.\d*|0\.\d*[1-9]\d*))|0?\.0+|0$
18. 正浮点数：^[1-9]\d*\.\d*|0\.\d*[1-9]\d*$ 或 ^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$
19. 负浮点数：^-([1-9]\d*\.\d*|0\.\d*[1-9]\d*)$ 或 ^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$
20. 浮点数：^(-?\d+)(\.\d+)?$ 或 ^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$
```

#### 校验字符的表达式

```java
1. 汉字：^[\u4e00-\u9fa5]{0,}$
2. 英文和数字：^[A-Za-z0-9]+$ 或 ^[A-Za-z0-9]{4,40}$
3. 长度为3-20的所有字符：^.{3,20}$
4. 由26个英文字母组成的字符串：^[A-Za-z]+$
5. 由26个大写英文字母组成的字符串：^[A-Z]+$
6. 由26个小写英文字母组成的字符串：^[a-z]+$
7. 由数字和26个英文字母组成的字符串：^[A-Za-z0-9]+$
8. 由数字、26个英文字母或者下划线组成的字符串：^\w+$ 或 ^\w{3,20}
9. 中文、英文、数字包括下划线：^[\u4E00-\u9FA5A-Za-z0-9_]+$
10. 中文、英文、数字但不包括下划线等符号：^[\u4E00-\u9FA5A-Za-z0-9]+$ 或 ^[\u4E00-\u9FA5A-Za-z0-9]{2,20}$
11. 可以输入含有^%&',;=?$\"等字符：[^%&',;=?$\x22]+
12. 禁止输入含有~的字符[^~\x22]+
13. 匹配除 \以外的任何字符:.*
14. 汉字:/[\u4E00-\u9FA5]/
15. 全角符号:/[\uFF00-\uFFFF]/
16. 半角符号:/[\u0000-\u00FF]/ 
```

#### 特殊需求表达式

```java
1. Email地址：^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$
2. 域名：[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(/.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+/.?
3. InternetURL：[a-zA-z]+://[^\s]* 或 ^http://([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?$
4. 手机号码：^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$
5. 电话号码("XXX-XXXXXXX"、"XXXX-XXXXXXXX"、"XXX-XXXXXXX"、"XXX-XXXXXXXX"、"XXXXXXX"和"XXXXXXXX)：^(\(\d{3,4}-)|\d{3.4}-)?\d{7,8}$
6. 国内电话号码(0511-4405222、021-87888822)：\d{3}-\d{8}|\d{4}-\d{7}
7. 身份证号(15位、18位数字)：^\d{15}|\d{18}$
8. 短身份证号码(数字、字母x结尾)：^([0-9]){7,18}(x|X)?$ 或 ^\d{8,18}|[0-9x]{8,18}|[0-9X]{8,18}?$
9. 帐号是否合法(字母开头，允许5-16字节，允许字母数字下划线)：^[a-zA-Z][a-zA-Z0-9_]{4,15}$
10. 密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)：^[a-zA-Z]\w{5,17}$
11. 强密码(必须包含大小写字母和数字的组合，不能使用特殊字符，长度在8-10之间)：^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,10}$
12. 日期格式：^\d{4}-\d{1,2}-\d{1,2}
13. 一年的12个月(01～09和1～12)：^(0?[1-9]|1[0-2])$
14. 一个月的31天(01～09和1～31)：^((0?[1-9])|((1|2)[0-9])|30|31)$
15. xml文件：^([a-zA-Z]+-?)+[a-zA-Z0-9]+\\.[x|X][m|M][l|L]$
16. 中文字符的正则表达式：[\u4e00-\u9fa5]
17. 双字节字符：[^\x00-\xff] (包括汉字在内，可以用来计算字符串的长度(一个双字节字符长度计2，ASCII字符计1))
18. 空白行的正则表达式：\n\s*\r (可以用来删除空白行)
19. HTML标记的正则表达式：<(\S*?)[^>]*>.*?</\1>|<.*? /> (网上流传的版本太糟糕，上面这个也仅仅能部分，对于复杂的嵌套标记依旧无能为力)
20. 首尾空白字符的正则表达式：^\s*|\s*$或(^\s*)|(\s*$) (可以用来删除行首行尾的空白字符(包括空格、制表符、换页符等等)，非常有用的表达式)
21. 腾讯QQ号：[1-9][0-9]{4,} (腾讯QQ号从10000开始)
22. 中国邮政编码：[1-9]\d{5}(?!\d) (中国邮政编码为6位数字)
23. IP地址：\d+\.\d+\.\d+\.\d+ (提取IP地址时有用)
24. IP地址：((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))
25. IP-v4地址：\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b (提取IP地址时有用)
26. 校验IP-v6地址:(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))
27. 子网掩码：((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))
28. 校验日期:^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)$(“yyyy-mm-dd“ 格式的日期校验，已考虑平闰年。)
29. 抽取注释：<!--(.*?)-->
30. 查找CSS属性:^\\s*[a-zA-Z\\-]+\\s*[:]{1}\\s[a-zA-Z0-9\\s.#]+[;]{1}
31. 提取页面超链接:(<a\\s*(?!.*\\brel=)[^>]*)(href="https?:\\/\\/)((?!(?:(?:www\\.)?'.implode('|(?:www\\.)?', $follow_list).'))[^" rel="external nofollow" ]+)"((?!.*\\brel=)[^>]*)(?:[^>]*)>
32. 提取网页图片:\\< *[img][^\\\\>]*[src] *= *[\\"\\']{0,1}([^\\"\\'\\ >]*)
33. 提取网页颜色代码:^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$
34. 文件扩展名效验:^([a-zA-Z]\\:|\\\\)\\\\([^\\\\]+\\\\)*[^\\/:*?"<>|]+\\.txt(l)?$
35. 判断IE版本：^.*MSIE [5-8](?:\\.[0-9]+)?(?!.*Trident\\/[5-9]\\.0).*$
```

#### 钱的输入格式

```
1. 有四种钱的表示形式我们可以接受:"10000.00" 和 "10,000.00", 和没有 "分" 的 "10000" 和 "10,000"：^[1-9][0-9]*$
2. 这表示任意一个不以0开头的数字,但是,这也意味着一个字符"0"不通过,所以我们采用下面的形式：^(0|[1-9][0-9]*)$
3. 一个0或者一个不以0开头的数字.我们还可以允许开头有一个负号：^(0|-?[1-9][0-9]*)$
4. 这表示一个0或者一个可能为负的开头不为0的数字.让用户以0开头好了.把负号的也去掉,因为钱总不能是负的吧.下面我们要加的是说明可能的小数部分：^[0-9]+(.[0-9]+)?$
5. 必须说明的是,小数点后面至少应该有1位数,所以"10."是不通过的,但是 "10" 和 "10.2" 是通过的：^[0-9]+(.[0-9]{2})?$
6. 这样我们规定小数点后面必须有两位,如果你认为太苛刻了,可以这样：^[0-9]+(.[0-9]{1,2})?$
7. 这样就允许用户只写一位小数.下面我们该考虑数字中的逗号了,我们可以这样：^[0-9]{1,3}(,[0-9]{3})*(.[0-9]{1,2})?$
8. 1到3个数字,后面跟着任意个 逗号+3个数字,逗号成为可选,而不是必须：^([0-9]+|[0-9]{1,3}(,[0-9]{3})*)(.[0-9]{1,2})?$
```

#### 练习

> 匹配验证-验证Email是否正确

```
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: RegexTest
 * @Description: 正则表达式的练习
 				 Pattern类的API文档中有一些规则
 * @author hyatt
 * @date 2021年4月27日
 */
public class RegexTest {
	public static void main(String[] args) {
		// 要验证的字符串
		String str = "service@xsoftlab.net";
		// 邮箱验证规则
		String regEx = "[a-zA-Z_]{1,}[0-9]{0,}@(([a-zA-z0-9]-*){1,}\\.){1,3}[a-zA-z\\-]{1,}";
		// 编译正则表达式
		Patterpatter= Pattern.compile(regEx);
		// 忽略大小写的写法
		// Patterpat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		// 字符串是否与正则表达式相匹配
		boolears = matcher.matches();
		System.out.println(rs);
	}
}
```

> 在字符串中查询字符或者字符串

```java
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: RegexTest
 * @Description: 正则表达式的练习
 * @author hyatt
 * @date 2021年4月27日
 */
public class RegexTest {
	public static void main(String[] args) {
		// 要验证的字符串
		String str = "baike.xsoftlab.net";
		// 正则表达式规则
		String regEx = "baike.*";
		// 编译正则表达式
		Patterpatter= Pattern.compile(regEx);
		// 忽略大小写的写法
		// Patterpat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		// 查找字符串中是否有匹配正则表达式的字符/字符串
		boolears = matcher.find();
		System.out.println(rs);
	}

}
```

### Math

> 针对数学运算操作的类(具体功能查询Java Api文档)

## 集合

### 集合和数组的区别

- 长度区别

- - 数组长度是固定的，但是集合长度是可以变化的。

- 内容不同

- - 数组存储的是同一种类型的元素，但是集合可以存储不同类型的元素。

- 元素的数据类型的问题

- - 数组可以存储基本类型的元素也可以存储引用类型的元素
  - 集合只可以存储引用类型的元素

### 集合的继承体系

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091808812.jpeg)	

### 迭代器

> - 集合获取元素的方式
> - 依赖于集合而存在的

#### 迭代器遍历

```java
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**  
 * Collection的遍历
 * 1. 创建集合对象
 * 2. 创建元素对象
 * 3. 添加元素
 * 4. 遍历集合
 * @author hyatt  
 * @versio1.0  
 */
public class CollectionTest3 {

	public static void main(String[] args) {
		// 创建集合对象
		Collectioc = new ArrayList();

		// 创建并添加元素对象
		c.add("hello");
		c.add("world");
		c.add("java");

		// 遍历集合
		Iterator it = c.iterator();
		while (it.hasNext()) {
			String s = (String) it.next();
			System.out.println("s: " + s);
		}
        // for 循环改写
		for (Iterator iterator = c.iterator(); iterator.hasNext();) {
			String s = (String) iterator.next();
			System.out.println(s);
		}
	}
}
```

### List

> List是Collection的子接口
>
> List集合是有序的(存储和取出的顺序一致，而不是排序)，并且是可以重复的。

#### 子类的特点

- ArrayList:

- - 底层数据结构是数组, 查询快，增删慢
  - 线程不安全，效率高

- Vector:(不经常使用)

- - 底层数据结构是数组, 查询快，增删慢
  - 线程安全，效率低

- LinkedList:

- - 底层数据结构是链表, 查询慢, 增删快。
  - 线程不安全, 效率高。

#### List遍历

```java
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**  
 * List的两种遍历 
 * @author hyatt  
 * @versio1.0  
 */
public class LIstTest2 {

	public static void main(String[] args) {
		// 创建集合对象
		List list = new ArrayList();

		// 创建并添加元素对象
		list.add("hello");
		list.add("world");
		list.add("java");

		// 1. 使用迭代器遍历
		Iterator it = list.iterator();
		while (it.hasNext()) {
			String s = (String) it.next();
			System.out.println("s: " + s);
		}
		System.out.println("----------------------------");
		// 2. 使用List独有的方法去遍历
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
}
```

#### 并发修改异常

> java.util.ConcurrentModificationException

```java
package com.hyatt.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**  
 * 集合中的并发修改异常
 * java.util.ConcurrentModificationException: 当对象的并发修改不允许时，检测到该修改的方法可能会引发此异常。
 * 
 * 产生的原因：
 * 		迭代器是依赖于集合而存在的, 在判断成功后, 集合中添加了一个元素, 但是迭代器并不知道, 所以就出现了 异常
 * 		迭代器在迭代的时候不允许修改集合
 * 
 * 解决方法：
 * 		A:迭代器迭代元素, 迭代器修改元素
 * 			元素不是一定在最后面, 它是跟在刚才迭代的元素后面
 * 		B:集合遍历元素, 集合修改元素(普通for)
 * 			元素是在最后面
 * @author hyatt  
 * @versio1.0  
 */
public class ListIteratorTest {

	public static void main(String[] args) {
		List list = new ArrayList();
		list.add("Hello");
		list.add("World");
		list.add("Java");

		// 如果存在 World 则添加 JavaEE
		// 遍历集合 - 使用迭代器 java.util.ConcurrentModificationException
		// Iterator iterator = list.iterator();
		// while (iterator.hasNext()) {
		// String s = (String) iterator.next();
		// if ("World".equals(s)) {
		// list.add("JavaEE");
		// }
		// }
		// 方式1: 使用迭代器迭代元素, 迭代器修改元素, 由于迭代器Iterator没有添加功能, 所以这里使用它的子接口 ListIterator
		// ListIterator listIterator = list.listIterator();
		// while (listIterator.hasNext()) {
		// String s = (String) listIterator.next();
		// if ("World".equals(s)) {
		// listIterator.add("JavaEE");
		// }
		// }
		// System.out.println(list); // [Hello, World, JavaEE, Java]
		// 方式2: 使用集合迭代元素, 使用集合修改元素
		for (int i = 0; i < list.size(); i++) {
			String s = (String) list.get(i);
			if ("World".equals(s)) {
				list.add("JavaEE");
			}
		}
		System.out.println(list); // [Hello, World, Java, JavaEE]
	}
}
```

#### 面试题

> 用LinkedList模拟栈数据结构的集合类(先进后出)

```java
import java.util.LinkedList;

/**  
 * 使用LinkedList类模拟栈结构
 * 自定义的集合类
 * @author hyatt  
 * @versio1.0  
 */
public class MyStack {

	LinkedList<String> linkedList = null;

	public MyStack() {
		this.linkedList = new LinkedList<String>();
	}

	public void add(String s) {
		this.linkedList.addFirst(s);
	}

	public String get() {
		returthis.linkedList.removeFirst();
	}

	public booleaisEmpty() {
		returthis.linkedList.isEmpty();
	}
}
/**  
 * 测试模拟的栈数据结构集合类
 * @author hyatt  
 * @versio1.0  
 */
public class MyStackTest {

	public static void main(String[] args) {
		MyStack myStack = new MyStack();

		myStack.add("hello");
		myStack.add("world");
		myStack.add("java");

		while(!myStack.isEmpty()) {
			System.out.println(myStack.get());
		}
	}
}
```

### 常见的数据结构

- 栈: 先进后出
- 队列：先进先出
- 数组：查询快, 增删慢
- 列表：查询慢，增删快

### 泛型

> 是一种把明确数据类型的工作推迟到创建对象或者调用方法的时候才去明确的特殊的类型(**该数据类型只可以是引用数据类型**)

#### 好处

- 把运行时期的问题提前到编译期间(让程序更加安全)
- 避免了强制数据类型转换、

#### 泛型类

```java
/**  
 * 泛型类的使用
 * @author hyatt  
 * @versio1.0  
 */
public class GenericTest<T> {

	private T obj;

	public T getObj() {
		returobj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
}
```

#### 泛型方法

```java
/**  
 * 泛型方法: 可以接收任意类型
 * @author hyatt  
 * @versio1.0  
 */
public class GenericTest2 {

	public static <T> void show(T t) {
		System.out.println(t);
	}

	public static void main(String[] args) {
		show("hello");
		show(true);
		show(100);
	}

}
```

#### 泛型接口

> 泛型接口的定义

```java
/**  
 * 泛型接口
 * @author hyatt  
 * @versio1.0  
 */
public interface Inter<T> {
	public abstract void show(T t);
}
```

> 泛型接口实现类的定义

```java
/**  
 * 实现泛型接口类 -- 实现的时候就指定数据类型  
 * @author hyatt  
 * @versio1.0  
 */
public class InterImpl implements Inter<String> {
	@Override
	public void show(String t) {
		System.out.println(t);
	}
}

/**  
 * 实现泛型接口类 -- 实现的时候不指定数据类型  
 * @author hyatt  
 * @versio1.0  
 */
public class InterImpl2<T> implements Inter<T> {
	@Override
	public void show(T t) {
		System.out.println(t);
	}
}
```

> 泛型实现类的使用

```java
/**  
 * 泛型接口实现类的测试
 * @author hyatt  
 * @versio1.0  
 */
public class InterTest {

	public static void main(String[] args) {
		InterImpl impl = new InterImpl();
		impl.show("hello");
		InterImpl2<String> impl2 = new InterImpl2<String>();
		impl2.show("world");
		InterImpl2<Integer> impl3 = new InterImpl2<Integer>();
		impl3.show(1000);
	}

}
```

#### 泛型高级通配符

> - ?: 任意类型
> - ? extends E: 向下限定, E或者E的子类
> - ? super E：向上限定, E或者E的父类

```java
import java.util.ArrayList;
import java.util.Collection;

/**  
 * 高级通配符: ?, ? entends E, ? super E
 * @author hyatt  
 * @versio1.0  
 */
public class GenericTest3 {

	public static void main(String[] args) {
		// 泛型明确的时候, 前面和后面必须一致
		Collection<Object> c1 = new ArrayList<Object>();
		// Collection<Object> c2 = new ArrayList<Animal>();
		// Collection<Object> c3 = new ArrayList<Dog>();
		// Collection<Object> c4 = new ArrayList<Cat>();

		// ? 任意类型
		Collection<?> c5 = new ArrayList<Object>();
		Collection<?> c6 = new ArrayList<Animal>();
		Collection<?> c7 = new ArrayList<Dog>();
		Collection<?> c8 = new ArrayList<Cat>();

		// ? entends E: E及其子类
		// Collection<? extends Animal> c9 = new ArrayList<Object>();
		Collection<? extends Animal> c10 = new ArrayList<Animal>();
		Collection<? extends Animal> c11 = new ArrayList<Dog>();
		Collection<? extends Animal> c12 = new ArrayList<Cat>();

		// ? super E: E及其父类
		Collection<? super Animal> c13 = new ArrayList<Object>();
		Collection<? super Animal> c14 = new ArrayList<Animal>();
		// Collection<? super Animal> c15 = new ArrayList<Dog>();
		// Collection<? super Animal> c16 = new ArrayList<Cat>();
	}
}

class Animal {
}

class Dog extends Animal {
}

class Cat extends Animal {
}
```

### 增强for循环

#### 格式

```java
for(元素的数据类型 变量名称:数组或者Collection集合额对象){}
```

#### 好处

- 简化了数组和集合的遍历

#### 弊端

- 增强for循环的目标不能为null, 使用之前需要判断是否为null

### 静态导入

> 可以导入到方法级别的导入

```java
/**  
 * 静态导入: 方法必须是静态的
 * @author hyatt  
 * @versio1.0  
 */
import static java.lang.Math.abs;

public class StaticImportTest {

	public static void main(String[] args) {
		System.out.println(Math.abs(-100));
		System.out.println(abs(-76));
	}
}
```

### 可变参数

> 在写方法的时候, 当参数个数不明确的时候, 可以定义可变参数

#### 格式

```java
修饰符 返回值类型 方法名(数据类型… 变量名) {}
```

#### 注意事项

- 该变量其实就是数组名称
- 如果一个方法有多个参数，并且有可变参数，可变参数必须在最后

#### Arrays的asList()方法

把数组转成集合，但是这个集合的长度不能改变。

#### 练习

```java
/**  
 * 可变参数
 * @author hyatt  
 * @versio1.0  
 */
public class VarargsTest {

	public static void main(String[] args) {
		System.out.println(sum(10, 20, 30));
	}

	/**
	 * 可变参数
	 * @param a
	 * @return
	 */
	public static int sum(int... a) {
		int s = 0;
		for (int i : a) {
			s += i;
		}
		returs;
	}
}
```

### Set集合

> 无序的，唯一的.(**这里的有序或者无序指的是存储顺序**)

#### HashSet集合

- 底层数据结构是哈希表(是一个元素为链表的数组)
- 哈希表底层依赖两个方法：hashcode()方法和equals()方法
  - 执行顺序：
    - 首先判断hashCode()的值是否相同
      - 相同:继续执行equals()方法，看其返回值。
        - True：表示集合中存在了，元素重复不添加
        - False：添加至集合
      - 不同：直接添加到集合

#### LinkedHashSet

- 底层数据结构是哈希表和链表组成
- 哈希表保证了元素唯一
- 链表保证了集合有序

#### TreeSet集合

> 特点：排序和唯一

- 底层数据结构是红黑树(是一个自平衡的二叉树)
- 保证数据的唯一性
  - 根据比较的返回是否是0决定的
- 保证元素的排序方式
  - 自然排序(元素具备比较性)
    - 让元素所属类实现Comparable接口
  - 比较器排序(集合具备比较性)
    - 让集合构造方法接收Comparator接口的实现类对象

> TreeSet保证元素唯一性和自然排序的原理和图解

​	![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091919403.jpg)	

#### 自然排序和比较器排序

```java
/**  
 * 学生类
 * @author hyatt  
 * @versio1.0  
 */
public class Student implements Comparable<Student> {

	private String name;
	private int age;

	public Student() {
	}

	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		returname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		returage;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Student o) {
		// 按照姓名的长度排序
		int num = this.name.length() - o.name.length();
		// 长度一直不代表姓名一样
		int num2 = num == 0 ? this.name.compareTo(o.name) : num;
		// 姓名一样不代表年龄一样
		int num3 = num2 == 0 ? this.age - o.age : num2;
		returnum3;
	}
}
```

```java
import java.util.Comparator;
import java.util.TreeSet;

/**  
 * TreeSet的排序
 * 1. 自然排序
 * 2. 比较器排序
 * 
 * 按照名字的长度进行排序
 * @author hyatt  
 * @versio1.0  
 */
public class TreeSetTest2 {

	public static void main(String[] args) {
		// 按照自然排序(空的构造方法)
		// TreeSet<Student> treeSet = new TreeSet<Student>();
		// 比较器排序 - 这里使用匿名对象(也可以创建一个实现了Comparator接口的类)
		TreeSet<Student> treeSet = new TreeSet<Student>(new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				// 按照姓名的长度排序
				int num = o1.getName().length() - o2.getName().length();
				// 长度一直不代表姓名一样
				int num2 = num == 0 ? o1.getName().compareTo(o2.getName()) : num;
				// 姓名一样不代表年龄一样
				int num3 = num2 == 0 ? o1.getAge() - o2.getAge() : num2;
				returnum3;
			}

		});

		// 添加学生类对象
		treeSet.add(new Student("嬴政", 50));
		treeSet.add(new Student("刘邦", 70));
		treeSet.add(new Student("刘彻", 65));
		treeSet.add(new Student("李渊", 55));
		treeSet.add(new Student("朱元璋", 75));
		treeSet.add(new Student("朱元璋", 70));

		// 遍历集合
		for (Student s : treeSet) {
			System.out.println(s);
		}
	}
}
```

### Map集合

> 特点：将键映射到值的对象。一个映射不能包含重复的键(Set集合); 每一个键最多只能映射一个值(值是可以重复的，可以使用List)。
>
> 添加功能(put方法)同时具有添加和修改功能(具体其他功能和功能实现查询Java Api文档)

#### Map和Collection的区别

- Map存储的是键值对元素, 键唯一, 值可以重复.
- Collection存储的是单独的元素, 子接口Set元素唯一, 子接口List元素可以重复

#### LinkedHashMap

> 是Map接口的哈希表和链表实现, 具有可预知的迭代顺序即**有序的****(****存储顺序)****，也是唯一的**

- 由哈希表保证键的唯一性
- 由链表保证键的有序

#### HashMap和Hashtable的区别

> HashMap类大致相当于Hashtable，只是它不同步并且允许空值。

- HashMap线程不安全，效率高，允许null键和null值
- Hashtable线程安全，效率低，不允许null键和null值

#### 练习

```java
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**  
 * Map集合的学习 
 * 获取功能
 * @author hyatt  
 * @versio1.0  
 */
public class MapTest {

	public static void main(String[] args) {
		// 创建map对象
		Map<String, String> map = new HashMap<String, String>();

		// 添加元素至集合
		map.put("郭靖", "黄蓉");
		map.put("杨康", "穆念慈");
		map.put("杨过", "小龙女");
		map.put("郭芙", "耶律齐");

		// 遍历map集合
		// 首先获取map的键集合
		Set<String> set = map.keySet();
		// 通过get方法获取键对应的值
		for (String key : set) {
			System.out.println(key + "---" + map.get(key));
		}
		System.out.println("-------------------------------");
		// 使用 entrySet 方法
		Set<Map.Entry<String, String>> set2 = map.entrySet();
		for (Map.Entry<String, String> me : set2) {
			// 获取键
			String key = me.getKey();
			// 获取键对应的值
			String value = me.getValue();
			System.out.println(key + "---" + value);
		}
	}
}
```

### 集合的选用

是否需要使用键值对

- 是：Map
  - 键是否需要排序
    - 是：TreeMap
    - 否：HashMap
    - 不确定：HashMap
- 否：Collection
  - 是否要求值唯一
    - 是：Set
      - 是否需要排序
        - 是：TreeSet
        - 否：HashSet
        - 不确定：HashSet
    - 否：List
      - 是否需要确保安全
        - 是：Vector(之后其实也不会使用它)
        - 否：ArrayList, LinkedList
          - 增删多：LinkedLIst
          - 查询多：ArrayList
        - 不确定：ArrayList

## 异常

> **异常指的是运行期出现的错误，也就是当程序开始执行以后执行期出现的错误**。出现错误时观察错误的名字和行号最为重要.

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091926497.gif)	

### 异常的分类

> Exception类下不是RuntimeException下子类的异常都是编译时期的异常, 是必须需要处理的错误

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091927286.gif)	

### 异常的捕获和处理

> Java异常处理的五个关键字：try、catch、finally、throw、throws

### try语句

> try中的语句越少越好

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091928930.gif)	

### catch语句

- catch后面可以跟多个兄弟异常, 使用 | 分割
- 分开写catch语句注意父类异常一定要放在最后面

```java
/**  
 * 异常的学习
 * @author hyatt  
 * @versio1.0  
 */
public class ExceptionTest {

	public static void main(String[] args) {
		int[] arr = { 1, 2, 3 };
		try {
			// System.out.println(2 / 0);
			System.out.println(arr[3]);
		} catch (ArithmeticExceptio| ArrayIndexOutOfBoundsExceptioe) {
			System.out.println("除数不能为0");
		}
		System.out.println("over");
	}
}
```

### finally语句

> 主要用于释放资源

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091929630.gif)	

> finally控制的语句体一定会执行,但是如果中途jvm退出了就不会执行了(例如在捕获异常直接退出jvm：System.exit(0))

### 自定义异常

```java
import java.util.Scanner;

/**  
 * 自定义异常
 * @author hyatt  
 * @versio1.0  
 */
public class ExceptionTest3 {

	public static void main(String[] args) {
		// 分数必须在 0 - 100 之间
		Scanner sc = new Scanner(System.in);
		System.out.println("输入分数：");
		int score = sc.nextInt();
		try {
			check(score);
		} catch (MyExceptioe) {
			e.printStackTrace();
		}
	}

	/**
	 * 检查分数
	 * @param score
	 * @throws MyException
	 */
	public static void check(int score) throws MyExceptio{

		if (score < 0 || score > 100) {
			throw new MyException("分数必须在 0-100 之间");
		}
	}
}
/**
 * 自定义异常类：继承Exception或者RuntimeException
 * @author hyatt  
 * @versio1.0
 */
class MyExceptioextends Exceptio{

	public MyException() {
		super();
	}

	public MyException(String message) {
		super(message);
	}
}
```

### 编译期异常和运行期异常的区别

- 编译期的异常是必须要处理的, 否则编译就报错
- 运行期异常可以处理也可以不处理

### 异常和重写的关系

- 子类重写父类方法时，子类必须抛出和父类相同的异常或者是父类异常的子类, 或者不抛出异常(这里可以抛出RuntimeException及其子类异常)
- 如果父类抛出多个异常, 子类重写父类时, 只能抛出相同的异常或者是他的子集(**包括子类异常的子集**), 子类不能抛出父类没有的异常(**子类抛出的异常可以是父类异常的子类)**
- 如果被重写的方法没有异常抛出, 那么子类的方法绝不可以抛出异常，如果子类方法内有异常发生，那么子类只能try不能throws

```java
package com.hyatt.exception;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.concurrent.TimeoutException;

import javax.annotation.processing.FilerException;

import org.omg.CORBA.portable.ApplicationException;

/**  
 * 异常的学习 - 继承和异常的重写
 * @author hyatt  
 * @versio1.0  
 */
public class ExceptionTest4 {
	public static void main(String[] args) throws FileNotFoundException, FilerException, FileSystemExceptio{
		B b = new B();
		b.show3();
	}
}

class A {
	// 子类在覆盖父类方法时，父类方法抛出异常，那么只能抛出的异常只能抛出父类异常或父类异常的子类或不抛出异常。
	public void show() throws IOExceptio{
		System.out.println("show A");
	}

	public void show2() throws IOExceptio{
		System.out.println("show A");
	}

	// 如果父类抛出多个异常, 子类重写父类时, 只能抛出相同的异常或者是他的子集, 子类可以抛出父类没有的异常, 但是子类的异常必须是父类异常的子类
	public void show3() throws IOException, TimeoutExceptio{
		System.out.println("show A");
	}
	
	// 如果被重写的方法没有异常抛出, 那么子类的方法绝不可以抛出异常，如果子类方法内有异常发生，那么子类只能try不能throws
	public void show4() {
		System.out.println("show A");
	}
}

class B extends A {
	// RuntimeExceptio可以是因为它是运行期间异常可以抛出也可以不抛出
	public void show() throws RuntimeExceptio{
		System.out.println("show b");
	}

	// 这里报错，就是因为TimeoutException继承自Exception，又不是IOException的子类异常
	// public void show2() throws TimeoutExceptio{
	// System.out.println("show A");
	// }

	// 成功 FileNotFoundException是IOException的子类异常
	public void show2() throws FileNotFoundExceptio{
		System.out.println("show A");
	}

	// 报错：ApplicationException既不是在父类方法的异常中也不再父类异常的子类中
	// public void show3() throws ApplicationExceptio{
	// System.out.println("show A");
	// }
	public void show3() throws FileNotFoundException, FilerException, FileSystemExceptio{
		System.out.println("show A");
	}
	
	public void show4() {
		System.out.println("show A");
	}
}
```

### final,finally和finalize的区别

- final
  - 最终的意思, 它可以修饰类，成员变量，成员方法
  - final修饰的类不能被继承
  - final修饰的成员变量是常量
  - final修饰的成员方法不能被重写
- finally
  - 用在异常处理语句中
  - finally修饰的语句一定会被执行，除非jvm中途退出
- finalize
  - 用于垃圾回收器自动回收垃圾对象

### catch中存在return语句，finally语句会继续执行吗，在return之前还是之后

> finally语句会执行(前面有除非jvm中途退出否则会继续执行)
>
> 在retur前(准确的说应该是中间, 因为通过debug发现return语句会执行两次)

```java
/**  
 * 异常的学习 - finally和return
 * @author hyatt  
 * @versio1.0  
 */
public class ExceptionTest {

	public static void main(String[] args) {
		System.out.println(getInt()); // 30
	}

	public static int getInt() {
		int a = 0;
		try {
			System.out.println(2 / 0);
		} catch (ArithmeticExceptioe) {
			a = 30;
			retura;
			/**
			 * 根据debug结果来看, 首先执行 a = 30; 然后执行 retura; 接着执行 a=40;再次执行retura;
			 * 
			 * 为什么最后结果输出是 30?;
			 * 1. a = 30
			 * 2. retura 其实是 retur30;
			 * 3. a = 40;
			 * 4. retura 其实还是 retur30;
			 */
		} finally {
			a = 40;
		}
		System.out.println("over");
		retura; // 这个其实并不会执行
	}
}
```

### throw和throws的区别throw和throws的区别

- throws
  - 用在方法的声明后面, 跟的是异常类名
  - 可以跟多个异常类名，使用逗号隔开
  - 表示抛出异常, 由该方法的调用者来处理
  - throws表示出现异常的一种可能性, 并不一定发生这种异常
- throw
  - 用在方法体内, 跟的是异常对象名
  - 只能抛出一个异常对象名
  - 表示抛出异常, 由方法体内的语句处理
  - 执行throw一定是抛出了某种异常

### 总结

> **养成良好的编程习惯，不要把错误给吞噬掉（即捕获到异常以后又不做出相应处理的做法，这种做法相当于是把错误隐藏起来了，可实际上错误依然还是存在的）， 也不要轻易地往外抛错误，能处理的一定要处理，不能处理的一定要往外抛。**往外抛的方法有两种，一种是在知道异常的类型以后，方法声明时使用throws把 异常往外抛，另一种是手动往外抛，使用“throw+异常对象”你相当于是把这个异常对象抛出去了，然后在方法的声明写上要抛的那种异常。

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091948571.png)	

## IO

### File类

> 常见功能查询Java的API文档

#### 注意事项

- mkdirs()方法创建的是多级文件夹不会创建文件

```java
File file = new File("D:\\temp\\a.txt");
System.out.println("file.mkdirs(): " + file.mkdirs()); // true 创建了temp文件夹以及temp下的a.txt文件夹
```

- mkdir方法只能创建单级文件夹
- 不写盘符, 默认在当前项目下创建文件或者文件夹(即相对路径)
- Java删除不走回收站
- delete方法要删除一个文件夹，该文件夹中不能包含文件或者文件夹
- 重命名功能(renameTo), 如果路径名称相同即改名, 否则则剪切加改名

### IO流分类

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091937169.gif)	

### 字节流

#### 输入流(读)

```java
import java.io.FileInputStream;
import java.io.IOException;

/**  
 * 字节输入流：FileInputStream的使用, 读数据到程序
 * 1. 创建字节输入流对象
 * 2. 读数据, 输出到控制台
 * 3. 释放资源
 * @author hyatt  
 * @versio1.0  
 */
public class FileInputStreamTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建字节输入流FileInputStream的对象
		FileInputStream fis = new FileInputStream("fos.txt");
		// 读数据, 一个一个字节读取
		// 开发中常用
		int by = 0;
		while ((by = fis.read()) != -1) {
			System.out.print((char) by);
		}
		fis.close();
	}

}
```

```java
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**  
 * 字节缓冲区输入流 - BufferedInputStream  提高读取速度
 * @author hyatt  
 * @versio1.0  
 */
public class BufferedInputStreamTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream("bis.txt"));

		// 读数据 一个字节一个字节的读
		int by = 0;
		while((by = bis.read())!=-1) {
			System.out.print((char)by);
		}
		// 释放资源
		bis.close();

	}
}
```

#### 输出流

> 追加数据可以使用构造方法：FileOutputStream(String name, booleaappend)

```java
import java.io.FileOutputStream;
import java.io.IOException;

/**  
 * 字节输出流：FileOutputStream的使用, 写数据到文件
 * 1. 创建字节输出流对象
 * 2. 写数据
 * 3. 释放资源
 * @author hyatt  
 * @versio1.0  
 */
public class FileOutputStreamTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建字节输出流FileOutputStream的对象
		/**
		 * 创建对象做的事情:
		 * 	1. 调用系统功能创建文件
		 * 	2. 创建fos对象
		 * 	3. 将fos对象指向fos.txt文件
		 */
		FileOutputStream fos = new FileOutputStream("fos.txt");
		
		// 写数据到文件中
		fos.write("hello, IO".getBytes());
		
		// 释放资源：关闭此文件输出流并释放与此流关联的所有系统资源。
		/**
		 * 1. 让流对象变成垃圾, 这样就可以被垃圾回收器回收
		 * 2. 通知系统去释放和改文件相关的资源
		 */
		fos.close();
	}

}
```

```java
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**  
 * 字节缓冲区输出流 - BufferedOutputStream 增加写入速度
 * @author hyatt  
 * @versio1.0  
 */
public class BufferedOutputStreamTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("bos.txt"));

		// 写数据
		bos.write("BufferedOutputStream".getBytes());

		// 释放资源
		bos.close();

	}
}
```

#### 字节缓冲区流和基本字节流的速度

```java
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**  
 * 测试基本字节流和缓冲字节流的速度
 * 
 * 字节流的四种方式复制文件：test.mp4(7,401,783字节)
 * 
 * 基本字节流一次读写一个字节:共耗时:42907毫秒
 * 基本字节流一次读写一个字节数组：共耗时:56毫秒
 * 缓冲(高效)字节流一次读写一个字节：共耗时:222毫秒
 * 缓冲(高效)字节流一次读写一个字节数组：共耗时:20毫秒
 * @author hyatt  
 * @versio1.0  
 */
public class CopyFileTest2 {

	public static void main(String[] args) throws IOExceptio{
		long start = System.currentTimeMillis();
		// 基本字节流一次读写一个字节
		// method1("test.mp4", "test1.mp4");
		// 基本字节流一次读写一个字节数组
		// method2("test.mp4", "test2.mp4");
		// 缓冲(高效)字节流一次读写一个字节
		// method3("test.mp4", "test3.mp4");
		// 缓冲(高效)字节流一次读写一个字节数组
		method4("test.mp4", "test4.mp4");
		long end = System.currentTimeMillis();
		System.out.println("共耗时:" + (end - start) + "毫秒");
	}

	/**  
	 * 将文件srcString复制到destString
	 * 缓冲(高效)字节流一次读写一个字节数组
	 * @param srcString
	 * @param destString   
	 * @throws IOExceptio
	 */
	private static void method4(String srcString, String destString) throws IOExceptio{
		// 创建对象
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(srcString));
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destString));
		// 读写数据
		byte[] bytes = new byte[1024];
		int length = 0;
		while ((length = bis.read(bytes)) != -1) {
			bos.write(bytes, 0, length);
		}
		// 释放资源
		bos.close();
		bis.close();
	}

	/**  
	 * 将文件srcString复制到destString
	 * 缓冲(高效)字节流一次读写一个字节
	 * @param srcString
	 * @param destString   
	 * @throws IOExceptio
	 */
	private static void method3(String srcString, String destString) throws IOExceptio{
		// 创建对象
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(srcString));
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destString));
		// 读写数据
		int by = 0;
		while ((by = bis.read()) != -1) {
			bos.write(by);
		}
		// 释放资源
		bos.close();
		bis.close();
	}

	/**  
	 * 将文件srcString复制到destString
	 * 基本字节流一次读写一个字节数组
	 * @param srcString
	 * @param destString   
	 * @throws IOExceptio
	 */
	private static void method2(String srcString, String destString) throws IOExceptio{
		// 创建对象
		FileInputStream fis = new FileInputStream(srcString);
		FileOutputStream fos = new FileOutputStream(destString);
		// 读写数据
		byte[] bytes = new byte[1024];
		int length = 0;
		while ((length = fis.read(bytes)) != -1) {
			fos.write(bytes, 0, length);
		}
		// 释放资源
		fos.close();
		fis.close();
	}

	/**  
	 * 将文件srcString复制到destString
	 * 基本字节流一次读写一个字节
	 * @param srcString
	 * @param destString   
	 * @throws IOExceptio
	 */
	private static void method1(String srcString, String destString) throws IOExceptio{
		// 创建对象
		FileInputStream fis = new FileInputStream(srcString);
		FileOutputStream fos = new FileOutputStream(destString);
		// 读写数据
		int by = 0;
		while ((by = fis.read()) != -1) {
			fos.write(by);
		}
		// 释放资源
		fos.close();
		fis.close();
	}
}
```

### 字符流

#### 输入流

> 一次读一个字符

```java
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**  
 * 字符流：输入流 InputStreamReader
 * @author hyatt  
 * @versio1.0  
 */
public class InputStreamReaderTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		// InputStreamReader isr = new InputStreamReader(new
		// FileInputStream("osw.txt")); // 使用默认编码读取
		InputStreamReader isr = new InputStreamReader(new FileInputStream("osw.txt"), "UTF-8"); // 使用指定编码
		// 读取数据
		int chs = 0;
		while ((chs = isr.read()) != -1) {
			System.out.print((char) chs);
		}
		// 释放资源
		isr.close();
	}
}
```

> 一次读一个字符数组

```java
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**  
 * 字符流：输入流 InputStreamReader 一次读一个字符数组
 * @author hyatt  
 * @versio1.0  
 */
public class InputStreamReaderTest2 {
	public static void main(String[] args) throws IOExceptio{
		InputStreamReader isr = new InputStreamReader(new FileInputStream("fos.txt"));
		// 读取数据
		char[] chs = new char[1024];
		int le= 0;
		while ((le= isr.read(chs)) != -1) {
			System.out.print(new String(chs, 0, len));
		}
		// 释放资源
		isr.close();
	}

}
```

#### 输出流

> 一次写入一个字符

```java
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**  
 * 字符流: OutputStreamWriter 输出流
 * @author hyatt  
 * @versio1.0  
 */
public class OutputStreamWriterTest {
	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		// OutputStreamWriter osw = new OutputStreamWriter(new
		// FileOutputStream("osw.txt")); // 使用默认编码
		OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("osw.txt"), "UTF-8"); // 使用指定编码
		// 写入数据
		osw.write("北京欢迎你");
		// 释放资源
		osw.close();
	}

}
```

#### FileReader和FileWriter

> 为了简化字符流(转换流)的使用, Java提供了FileReader和FileWriter(使用本地默认编码)

```java
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**  
 * Copy文件
 * @author hyatt  
 * @versio1.0  
 */
public class CopyFileTest3 {

	public static void main(String[] args) throws IOExceptio{
		// 创建数据源对象
		FileReader fr = new FileReader("fos.txt");
		// 创建目标对象
		FileWriter fw = new FileWriter("fos2.txt");
		// 复制文件 一次一个字符
		// int ch = 0;
		// while ((ch = fr.read()) != -1) {
		// fw.write(ch);
		// }
		// 复制文件 一次一个字符数组
		char[] chs = new char[1024];
		int length = 0;
		while ((length = fr.read(chs)) != -1) {
			fw.write(chs, 0, length);
			fw.flush(); // 数据比较多的时候可以先刷新一下, 从缓冲区进入文件
		}
		// 关闭资源
		fw.close();
		fr.close();
	}
}
```

#### 字符缓冲区流

> 用来提高字符的读写速度

```java
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**  
 * 字符缓冲区流
 * @author hyatt  
 * @versio1.0  
 */
public class BufferWriterTest {

	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		BufferedWriter bw = new BufferedWriter(new FileWriter("bw.txt"));
		// 写入数据
		bw.write("hello");
		bw.write("world");
		bw.write("java");
		// 释放资源
		bw.close();
	}
}
```

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**  
 * 字符缓冲区流
 * @author hyatt  
 * @versio1.0  
 */
public class BufferReaderTest {

	public static void main(String[] args) throws IOExceptio{
		// 创建对象
		BufferedReader br = new BufferedReader(new FileReader("bw.txt"));
		// 读取数据 读取一个字符
		// int ch;
		// while ((ch = br.read()) != -1) {
		// System.out.print((char) ch);
		// }
		// 读取数据 读取一个字符数组
		char[] chs = new char[1024];
		int length;
		while ((length = br.read(chs)) != -1) {
			System.out.print(new String(chs, 0, length));
		}
		// 释放资源
		br.close();
	}
}
```

### 字节流和字符流的总结

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091949490.jpeg)

### 基本数据的输入流和输出流

```java
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**  
 * 基本数据流的操作
 * @author hyatt  
 * @versio1.0  
 */
public class DataStreamTest {
	public static void main(String[] args) throws IOExceptio{
		// 写
		write();
		// 读
		read();
	}

	/**  
	 * 读文件
	 */
	private static void read() throws IOExceptio{
		DataInputStream dis = new DataInputStream(new FileInputStream("dos.txt"));
		// 读数据
		System.out.println(dis.readByte());
		System.out.println(dis.readShort());
		System.out.println(dis.readInt());
		System.out.println(dis.readLong());
		System.out.println(dis.readChar());
		System.out.println(dis.readFloat());
		System.out.println(dis.readDouble());
		System.out.println(dis.readBoolean());
		System.out.println(dis.readInt());
		// 释放资源
		dis.close();
	}

	/**  
	 * 写文件
	 */
	private static void write() throws IOExceptio{
		DataOutputStream dos = new DataOutputStream(new FileOutputStream("dos.txt"));

		// 写基本数据类型的数据
		dos.writeByte(10);
		dos.writeShort(100);
		dos.writeInt(1000);
		dos.writeLong(100000L);
		dos.writeChar('a');
		dos.writeFloat(10.00F);
		dos.writeDouble(100.00D);
		dos.writeBoolean(true);
		dos.writeInt(1);
		// 释放资源
		dos.close();
	}
}
```

### 其他数据流

> 具体功能查看API

- 内存流
  - 操作字节数组
    - ByteArrayInputStream
    - ByteArrayOutputStream
  - 操作字符数组
    - CharArrayReader
    - CharArrayWriter
  - 操作字符串
    - StringReader
    - StringWriter
- 打印流(没有读操作)
  - PrintWriter：字符流
  - PrintStream：字节流
  - 特点：
    - 只有写数据, 没有读取数据
    - 可以操作任意类型的数据(print, println等方法)
    - 如果启动了自动刷新, 就可以自动刷新(对固定的方法有效)
    - 该流可以直接操作文本文件
- 可以操作文本文件的流
  - FileInputStream
  - FileOutputStream
  - FileWriter
  - FileReader
  - PrintStream
  - PrintWriter
- 基本流和高级流
  - 基本流: 可以直接操作文本文件
  - 高级流：在基本流的基础上提供了更多的功能
- 标准的输入和输出流
  - System.out
  - System.in
- 随机访问流
  - RandomAccessFile: 继承自Object类，既可以读也可以写
- 合并流
  - 可以读取多个文件：SequenceInputStream
- 对象流
  - ObjectOutputStream
  - ObjectInputStream
  - 对象需要实现Serializable接口, 可以使用transient关键字声明不需要序列化的变量

### 键盘录入的方式

- 在运行java的时候输入值
  - java helloword.java hello world java
- 使用Scanner类
- 使用标准的输入流

```java
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**  
 * 标准的输入流:System.in
 * @author hyatt  
 * @version 1.0  
 */
public class SystemInTest {
	public static void main(String[] args) throws IOException {
		// 创建对象
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("请输入一个字符串：");
		String line = br.readLine();
		System.out.println(line);
		System.out.println("请输入一个整数：");
		line = br.readLine();
		int i = Integer.parseInt(line);
		System.out.println(i);
		br.close();
	}
}
```

### Properties

> 是一个集合类，Hashtable的子类

#### Properties结合IO使用

```java
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Properties;

/**  
 * Properties类的使用, 和IO在一起使用
 * @author hyatt  
 * @version 1.0  
 */
public class PropertiesTest2 {

	public static void main(String[] args) throws IOException {
		myLoad();
		// 存储
		myStore();
	}

	/**  
	 * 将集合的数据存储的文件中   
	 */
	private static void myStore() throws IOException {
		// 创建Properties对象
		Properties props = new Properties();
		// 添加数据
		props.setProperty("嬴政", "秦始皇");
		props.setProperty("杨坚", "隋文帝");
		// 创建Writer对象
		Writer writer = new FileWriter("name.txt");
		// 调用方法
		props.store(writer, "Properties");
		writer.close();
	}

	/**  
	 * 导入文件数据
	 * 由于Properties存储的是键值对数据, 所以文件也要求必须是键值对
	 */
	private static void myLoad() throws IOException {
		Properties props = new Properties();
		// 声明 Reader对象
		Reader reader = new FileReader("prop.txt");
		// 调用方法
		props.load(reader);
		reader.close();
		// 输出props
		System.out.println(props);
	}

}
```

### NIO

- JDK4出现的NIO, 对以前的IO流进行了优化, 提高了效率
- JDK7之后对NIO的使用：
  - Path
  - Paths
  - Files

## 递归

> - 递归一定要有出口
> - 递归的次数不能太多, 否则会内存溢出
> - 构造方法不能递归使用

```java
import java.io.File;

/**  
 * 递归的练习
 * 递归输出指定目录下所有的java文件的绝对路径：这里以D盘为例
 * 
 * 分析步骤
 * 	1. 封装根目录
 *  2. 获取目录数组
 *  3. 遍历目录数组, 获取file对象
 *  4. 判断file对象是否是文件夹
 *  	是:回到2
 *  	否：判断是否以java结尾
 *  		是：输出文件的绝对值路径
 *  		否：不用管
 * @author hyatt  
 * @version 1.0  
 */
public class DiGuiTest {

	public static void main(String[] args) {
		// 封装根目录
		File srcFolder = new File("D:\\eclipse-workspace");

		// 通过递归输出文件的绝对路劲
		getAllJavaFilePaths(srcFolder);
	}

	/**  
	 * 输出文件的绝对值路径  
	 * @param srcFolder   
	 */
	private static void getAllJavaFilePaths(File srcFolder) {
		// 封装根目录
		File[] fileArray = srcFolder.listFiles();

		// 遍历目录数组, 获取file对象
		for (File file : fileArray) {
			if (file.isDirectory()) {
				// 继续向下寻找
				getAllJavaFilePaths(file);
			} else {
				// 以java结尾
				if (file.getName().endsWith(".java")) {
					// 输出绝对路劲
					System.out.println(file.getAbsolutePath());
				}
			}
		}

	}
}
```

## 多线程

### 线程的基本概念

> 线程理解：线程是一个程序里面不同的执行路径
>
> 学习线程首先要理清楚三个概念：
>
> 1. 进程：进程是一个静态的概念
>
> 2. 线程：一个进程里面有一个主线程叫main()方法，是一个程序里面的，一个进程里面不同的执行路径。
>
> 3. 在同一个时间点上，一个CPU只能支持一个线程在执行。因为CPU运行的速度很快，因此我们看起来的感觉就像是多线程一样。
>
> 　　什么才是真正的多线程？如果你的机器是双CPU，或者是双核，这确确实实是多线程。

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091958996.png)	

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201091959696.png)	

> **每一个分支都叫做一个线程，main()**叫做主分支，也叫主线程。
>
> 　　程只是一个静态的概念，机器上的一个.class文件，机器上的一个.exe文件，这个叫做一个进程。**程序的执行过程都是这样的**：首先把程序的代码放到内存的代码区里面，代码放到代码区后并没有马上开始执行，但这时候说明了一个进程准备开始，进程已经产生了，但还没有开始执行，这就是进程，所以进程其实是一个静态的概念，它本身就不能动。平常所说的进程的执行指的是进程里面主线程开始执行了，也就是main()方法开始执行了。**进程是一个静态的概念，在我们机器里面实际上运行的都是线程。**
>
> 　　Windows操作系统是支持多线程的，它可以同时执行很多个线程，也支持多进程，因此Windows操作系统是支持多线程多进程的操作系统。Linux和Uinux也是支持多线程和多进程的操作系统。DOS就不是支持多线程和多进程了，它只支持单进程，**在同一个时间点只能有一个进程在执行，这就叫单线程**。
>
> 　　CPU难道真的很神通广大，能够同时执行那么多程序吗？不是的，CPU的执行是这样的：CPU的速度很快，一秒钟可以算好几亿次，因此CPU把自己的时间分成一个个小时间片，我这个时间片执行你一会，下一个时间片执行他一会，再下一个时间片又执行其他人一会，虽然有几十个线程，但一样可以在很短的时间内把他们通通都执行一遍，但对我们人来说，CPU的执行速度太快了，因此看起来就像是在同时执行一样，但实际上在一个时间点上，CPU只有一个线程在运行。

### 线程的创建和启动

- 方式一：继承Thread类
  - 自定义MyThread类继承Thread类
  - Mythread类里面重写run方法
  - 创建对象
  - 启动线程
- 方式二：
  - 自定义MyRunnable类实现Runnable接口
  - MyRunnable类重写run方法
  - 创建MyRunnable类对象
  - 创建Thread对象,并将MyRunnable类对象作为构造参数传递

#### 使用实现Runnable接口创建和启动新线程

```java
// 范例1：使用实现Runnable接口创建和启动新线程
// 开辟一个新的线程来调用run方法
package cn.hyatt.test;
public class TestThread1{
    public static void main(String args[]){
        Runner1 r1 = new Runner1();//这里new了一个线程类的对象出来
        //r1.run();//这个称为方法调用，方法调用的执行是等run()方法执行完之后才会继续执行main()方法
        Thread t = new Thread(r1);//要启动一个新的线程就必须new一个Thread对象出来
        //这里使用的是Thread(Runnable target) 这构造方法
        t.start();//启动新开辟的线程，新线程执行的是run()方法，新线程与主线程会一起并行执行
        for(int i=0;i<10;i++){
            System.out.println("maintheod："+i);
        }
    }
}
/*定义一个类用来实现Runnable接口，实现Runnable接口就表示这个类是一个线程类*/
class Runner1 implements Runnable{
    public void run(){
        for(int i=0;i<10;i++){
            System.out.println("Runner1："+i);
        }
    }
}
```

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092003538.png)	

> 多线程程序执行的过程如下所示：(多线程，你执行一会,我执行一会)

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092004047.png)		

> 不开辟新线程直接调用run方法

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092004170.png)	

#### 继承Thread类，创建和启动新的线程

```java
// 范例2：继承Thread类，并重写其run()方法创建和启动新的线程
package cn.hyatt.test;
/*线程创建与启动的第二种方法：定义Thread的子类并实现run()方法*/
public class TestThread2{
    public static void main(String args[]){
        Runner2 r2 = new Runner2();
        r2.start();//调用start()方法启动新开辟的线程
        for(int i=0;i<=10;i++){
            System.out.println("mainMethod："+i);
        }
    }
}
/*Runner2类从Thread类继承
通过实例化Runner2类的一个对象就可以开辟一个新的线程
调用从Thread类继承来的start()方法就可以启动新开辟的线程*/
class Runner2 extends Thread{
    public void run(){//重写run()方法的实现
        for(int i=0;i<=10;i++){
            System.out.println("Runner2："+i);
        }
    }
}
```

#### 两种方式的比较和区别

> 应该用接口去实现

接口实现的优点

- 解决了单继承的局限性
- 适合多个相同程序的代码去处理同一个资源的情况，把线程同程序的代码和数据有效的分离，较好的体现了面向对象的设计思想

### 线程的调度

- 分时调度
- 抢占式调度(Java采用的是该调度方式)

### 线程的优先级

> - 线程的默认优先级是5
> - 线程的优先级的范围是1-10
> - 线程优先级表示线程获取CPU 时间片的几率高, 但是要在次数比较多, 或者多次运行的时候才能看到效果

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092007130.png)	

```java
package cn.hyatt.test;
public class TestThread6 {
    public static void main(String args[]) {
        MyThread4 t4 = new MyThread4();
        MyThread5 t5 = new MyThread5();
        Thread t1 = new Thread(t4);
        Thread t2 = new Thread(t5);
        t1.setPriority(Thread.NORM_PRIORITY + 3);// 使用setPriority()方法设置线程的优先级别，这里把t1线程的优先级别进行设置
        /*
         * 把线程t1的优先级(priority)在正常优先级(NORM_PRIORITY)的基础上再提高3级
         * 这样t1的执行一次的时间就会比t2的多很多 　　　　
         * 默认情况下NORM_PRIORITY的值为5
         */
        t1.start();
        t2.start();
        System.out.println("t1线程的优先级是：" + t1.getPriority());
        // 使用getPriority()方法取得线程的优先级别，打印出t1的优先级别为8
    }
}
class MyThread4 implements Runnable {
    public void run() {
        for (int i = 0; i <= 1000; i++) {
            System.out.println("T1：" + i);
        }
    }
}
class MyThread5 implements Runnable {
    public void run() {
        for (int i = 0; i <= 1000; i++) {
            System.out.println("===============T2：" + i);
        }
    }
}
// run()方法一结束，线程也就结束了。
```

### 线程状态转换

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092009594.png)	

### 阻塞状态

- 其他阻塞：sleep，join等
- 同步阻塞：synchronized

- 等待阻塞：wait方法

### 线程控制的基本方法

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092010155.png)	

### sleep/join/yield方法介绍

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092011865.png)	

#### sleep方法的使用

```java
// sleep方法的应用范例：
import java.util.*;
public class TestThread3 {
    public static void main(String args[]){
        MyThread thread = new MyThread();
        thread.start();//调用start()方法启动新开辟的线程
        try {
            /*Thread.sleep(10000);
            sleep()方法是在Thread类里面声明的一个静态方法，因此可以使用Thread.sleep()的格式进行调用
            */
            /*MyThread.sleep(10000);
            MyThread类继承了Thread类，自然也继承了sleep()方法，所以也可以使用MyThread.sleep()的格式进行调用
            */
            /*静态方法的调用可以直接使用“类名.静态方法名”
              或者“对象的引用.静态方法名”的方式来调用*/
            MyThread.sleep(10000);
            System.out.println("主线程睡眠了10秒种后再次启动了");
            //在main()方法里面调用另外一个类的静态方法时，需要使用“静态方法所在的类.静态方法名”这种方式来调用
            /*
            所以这里是让主线程睡眠10秒种
            在哪个线程里面调用了sleep()方法就让哪个线程睡眠，所以现在是主线程睡眠了。
            */
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //thread.interrupt();//使用interrupt()方法去结束掉一个线程的执行并不是一个很好的做法
        thread.flag=false;//改变循环条件，结束死循环
        /**
         * 当发生InterruptedException时，直接把循环的条件设置为false即可退出死循环，
         * 继而结束掉子线程的执行，这是一种比较好的结束子线程的做法
         */
        /**
         * 调用interrupt()方法把正在运行的线程打断
        相当于是主线程一盆凉水泼上去把正在执行分线程打断了
        分线程被打断之后就会抛InterruptedException异常，这样就会执行return语句返回，结束掉线程的执行
        所以这里的分线程在执行完10秒钟之后就结束掉了线程的执行
         */
    }
}
class MyThread extends Thread {
    boolean flag = true;// 定义一个标记，用来控制循环的条件
    public void run() {
        /*
         * 注意：这里不能在run()方法的后面直接写throw Exception来抛异常，
         * 因为现在是要重写从Thread类继承而来的run()方法,重写方法不能抛出比被重写的方法的不同的异常。
         *  所以这里只能写try……catch()来捕获异常
         */
        while (flag) {
            System.out.println("==========" + new Date().toLocaleString() + "===========");
            try {
                /*
                 * 静态方法的调用格式一般为“类名.方法名”的格式去调用 在本类中声明的静态方法时调用时直接写静态方法名即可。 当然使用“类名.方法名”的格式去调用也是没有错的
                 */
                // MyThread.sleep(1000);//使用“类名.方法名”的格式去调用属于本类的静态方法
                sleep(1000);//睡眠的时如果被打断就会抛出InterruptedException异常
                // 这里是让这个新开辟的线程每隔一秒睡眠一次，然后睡眠一秒钟后再次启动该线程
                // 这里在一个死循环里面每隔一秒启动一次线程，每个一秒打印出当前的系统时间
            } catch (InterruptedException e) {
                /*
                 * 睡眠的时一盘冷水泼过来就有可能会打断睡眠
                 * 因此让正在运行线程被一些意外的原因中断的时候有可能会抛被打扰中断(InterruptedException)的异常
                 */
                return;
                // 线程被中断后就返回，相当于是结束线程
            }
        }
    }
}
```

> 运行结果

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092011962.png)	

#### join方法的使用

```java
// join方法的使用范例：
public class TestThread4 {
    public static void main(String args[]) {
        MyThread2 thread2 = new MyThread2("mythread");
        // 在创建一个新的线程对象的同时给这个线程对象命名为mythread
        thread2.start();// 启动线程
        try {
            thread2.join();// 调用join()方法合并线程，将子线程mythread合并到主线程里面
            // 合并线程后，程序的执行的过程就相当于是方法的调用的执行过程(就是等到run()方法跑完后再往下执行)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i <= 5; i++) {
            System.out.println("I am main Thread");
        }
    }
}
class MyThread2 extends Thread {
    MyThread2(String s) {
        super(s);
        /*
         * 使用super关键字调用父类的构造方法
         * 父类Thread的其中一个构造方法：“public Thread(String name)”
         * 通过这样的构造方法可以给新开辟的线程命名，便于管理线程
         */
    }
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("I am a\t" + getName());
            // 使用父类Thread里面定义的
            //public final String getName()，Returns this thread's name.
            try {
                sleep(1000);// 让子线程每执行一次就睡眠1秒钟
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
```

> 运行结果

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092013339.png)	

#### yield方法的使用

```java
// yield方法的使用范例：
public class TestThread5 {
    public static void main(String args[]) {
        MyThread3 t1 = new MyThread3("t1");
        /* 同时开辟了两条子线程t1和t2，t1和t2执行的都是run()方法 */
        /* 这个程序的执行过程中总共有3个线程在并行执行，分别为子线程t1和t2以及主线程 */
        MyThread3 t2 = new MyThread3("t2");
        t1.start();// 启动子线程t1
        t2.start();// 启动子线程t2
        for (int i = 0; i <= 5; i++) {
            System.out.println("I am main Thread");
        }
    }
}
class MyThread3 extends Thread {
    MyThread3(String s) {
        super(s);
    }
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(getName() + "：" + i);
            if (i % 2 == 0) {
                yield();// 当执行到i能被2整除时当前执行的线程就让出来让另一个在执行run()方法的线程来优先执行
                /*
                 * 在程序的运行的过程中可以看到，
                 * 线程t1执行到(i%2==0)次时就会让出线程让t2线程来优先执行
                 * 而线程t2执行到(i%2==0)次时也会让出线程给t1线程优先执行
                 */
            }
        }
    }
}
```

### 线程安全问题

#### 导致线程安全的原因

- 是否是多线程环境
- 是否有共享数据

- 是否有多条语句操作共享数据

#### 解决线程安全问题

> 同步可以解决安全问题的关键就在对象上.该对象如同一个锁的功能, **对象需要唯一即锁唯一**
>
> **同步代码块的对象可以是任意对象.**

- 一般前两个是已经确定的，无法解决。
- 使用同步代码块解决第三个问题

```java
同步代码块：
synchronize(对象) {
    操作语句
}
```

### 线程同步

> - 同步的前提：
>   - 多个线程
>   - 多个线程使用的是同一个锁对象
> - 同步的好处
>   - 同步的出现解决了多线程的 安全问题
> - 同步的弊端
>   - 当线程相当多事，因为每一个线程都会去判断同步上的锁，这是很耗费资源的，无形中会降低程序的运行效率

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092015258.png)	

```java
public class TestSync implements Runnable {
    Timer timer = new Timer();
    public static void main(String args[]) {
        TestSync test = new TestSync();
        Thread t1 = new Thread(test);
        Thread t2 = new Thread(test);
        t1.setName("t1");// 设置t1线程的名字
        t2.setName("t2");// 设置t2线程的名字
        t1.start();
        t2.start();
    }
    public void run() {
        timer.add(Thread.currentThread().getName());
    }
}
class Timer {
    private static int num = 0;
    public/* synchronized */void add(String name) {// 在声明方法时加入synchronized时表示在执行这个方法的过程之中当前对象被锁定
        synchronized (this) {
            /*
             * 使用synchronized(this)来锁定当前对象，这样就不会再出现两个不同的线程同时访问同一个对象资源的问题了 只有当一个线程访问结束后才会轮到下一个线程来访问
             */
            num++;
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + "：你是第" + num + "个使用timer的线程");
        }
    }
}
```

### 线程死锁的问题

> **解决线程死锁的问题最好只锁定一个对象，不要同时锁定两个对象**
>
> 同步的弊端
>
> - 效率低
> - 如果出现了同步嵌套, 就容易产生死锁问题(是指两个或者两个以上的线程在执行的过程中因争夺资源而产生的一种互相等待的现象)

```java
public class TestDeadLock implements Runnable {
    public int flag = 1;
    static Object o1 = new Object(), o2 = new Object();
    public void run() {
        System.out.println(Thread.currentThread().getName() + "的flag=" + flag);
        /*
         * 运行程序后发现程序执行到这里打印出flag以后就再也不往下执行后面的if语句了
         * 程序也就死在了这里，既不往下执行也不退出
         */
        /* 这是flag=1这个线程 */
        if (flag == 1) {
            synchronized (o1) {
                /* 使用synchronized关键字把对象01锁定了 */
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (o2) {
                    /*
                     * 前面已经锁住了对象o1，只要再能锁住o2，那么就能执行打印出1的操作了
                     * 可是这里无法锁定对象o2，因为在另外一个flag=0这个线程里面已经把对象o1给锁住了
                     * 尽管锁住o2这个对象的线程会每隔500毫秒睡眠一次，可是在睡眠的时候仍然是锁住o2不放的
                     */
                    System.out.println("1");
                }
            }
        }
        /*
         * 这里的两个if语句都将无法执行，因为已经造成了线程死锁的问题
         * flag=1这个线程在等待flag=0这个线程把对象o2的锁解开，
         * 而flag=0这个线程也在等待flag=1这个线程把对象o1的锁解开
         * 然而这两个线程都不愿意解开锁住的对象，所以就造成了线程死锁的问题
         */
        /* 这是flag=0这个线程 */
        if (flag == 0) {
            synchronized (o2) {
                /* 这里先使用synchronized锁住对象o2 */
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (o1) {
                    /*
                     * 前面已经锁住了对象o2，只要再能锁住o1，那么就能执行打印出0的操作了 可是这里无法锁定对象o1，因为在另外一个flag=1这个线程里面已经把对象o1给锁住了 尽管锁住o1这个对象的线程会每隔500毫秒睡眠一次，可是在睡眠的时候仍然是锁住o1不放的
                     */
                    System.out.println("0");
                }
            }
        }
    }
    public static void main(String args[]) {
        TestDeadLock td1 = new TestDeadLock();
        TestDeadLock td2 = new TestDeadLock();
        td1.flag = 1;
        td2.flag = 0;
        Thread t1 = new Thread(td1);
        Thread t2 = new Thread(td2);
        t1.setName("线程td1");
        t2.setName("线程td2");
        t1.start();
        t2.start();
    }
}
```

### 生产者和消费者

> 这里最好使用while而不是if

```java
/*    
 *    要  点：
 *        1. 共享数据的不一致性/临界资源的保护
 *        2. Java对象锁的概念
 *        3. synchronized关键字/wait()及notify()方法
 */
public class ProducerConsumerTest {
	public static void main(String[] args) {
		SyncStack stack = new SyncStack();
		
		// 创建生产者和消费者对象
		Runnable producer = new Producer(stack);
		Runnable consumer = new Consumer(stack);
		
		// 创建线程对象
		Thread p1 = new Thread(producer);
		Thread c1 = new Thread(consumer);
		
		p1.start();
		c1.start();
	}

}
/**  
 * 支持多线程同步操作的堆栈的实现
 * @author hyatt  
 * @version 1.0  
 */
public class SyncStack {

	private int index = 0;
	private char[] data = new char[6];

	// 入栈
	public synchronized void push(char c) {

		if (data.length == index) { // 栈满了, 需要等待栈有空余空间
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.notify(); // 唤醒当前对象等待的线程. 这里的 notify方法为了唤醒pop方法
		data[index] = c;
		index++;
	}

	public synchronized char pop() {
		if (index == 0) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.notify();// 唤醒当前对象等待的线程. 这里的 notify方法为了唤醒push方法
		index --;
		return data[index];

	}

}
/**  
 * 生产者
 * @author hyatt  
 * @version 1.0  
 */
public class Producer implements Runnable {

	private SyncStack stack = null;

	/**  
	* 创建一个新的实例 Producer.    
	*/
	public Producer(SyncStack stack) {
		this.stack = stack;
	}

	@Override
	public void run() {
		while (true) {
			char c = (char) (Math.random() * 26 + 'A');
			stack.push(c);
			System.out.println("生产：" + c);
			try {
				Thread.sleep((int) (Math.random() * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
/**  
 * 消费者
 * @author hyatt  
 * @version 1.0  
 */
public class Consumer implements Runnable {

	private SyncStack stack = null;

	/**  
	 * 创建一个新的实例 Consumer.    
	 */
	public Consumer(SyncStack stack) {
		this.stack = stack;
	}

	@Override
	public void run() {
		while (true) {
			char c = this.stack.pop();
			System.out.println("消费：" + c);
			try {
				Thread.sleep((int) (Math.random() * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
```

### 线程池

> 程序启动一个新线程的成本是比较高的, 因为它涉及到要与操作系统进行交互。而使用线程池可以很好的提高性能，尤其是当程序中要创建大量生存期很短的线程的时候, 更应该使用线程池
>
> 线程池里面的每一个线程代码结束后, 并不会死亡，而是再次回到线程池中称为空闲状态，等待下一个对象来使用

```java
package com.hyatt.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**  
 * 线程池的使用：Runnable 和 Callable 对象都可以
 * @author hyatt  
 * @version 1.0  
 */
public class ExecutorsTest {
	
	public static void main(String[] args) {
		// 创建一个线程池, 包含两个线程
		ExecutorService es = Executors.newFixedThreadPool(2);
		// 创建Runnable对象
		MyRunnable mr = new MyRunnable();
		// 运行线程
		es.submit(mr);
		es.submit(mr);
		
		// 结束线程池
		es.shutdown();
	}
}
```

### 定时器

> 定时器是一个应用十分广泛的线程工具, 可以用于调度多个定时任务以后台线程的方式执行。在java中可以通过Timer类和TimerTask类来实现定义调度的功能

### 面试题

> JVM虚拟机的启动是单线程还是多线程

多线程的.

最少有两个线程, 一个是主线程, 一个是垃圾回收器线程

> start()方法和run方法的不同之处

run方法仅仅只是封装被线程执行的方法, 直接调用相当于调用普通方法

start方法首先启动了线程, 然后再由JVM调用该线程的run方法

> 线程的生命周期

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092020694.jpg)	

> 线程状态

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092021762.jpg)	

> 多线程有几种实现方案, 分别是哪几种

常用: 继承Thread类和实现Runnable接口

扩展: 实现Callable接口(依赖于线程池)

> 同步有几种方式, 分别是什么

同步代码块和同步方法

> sleep()和wait()方法的区别

sleep(): 必须指定时间; 不释放锁

wait()：可以不指定时间, 也可以指定时间; 释放锁

> 为什么wait(), notify(), notifyAll()等方法都定义在Object类的对象中

因为这些方法的调用是依赖于锁的，而锁对象是可以是任意对象的，而Object类代表任意的对象, 所以定义在Object类中

## GUI

> GUI: Graphical User Interface(图形用户接口)
>
> CLI:Command Line User Interface(命令行用户接口)

### 两个包

java.awt:和系统关联较强

java.swing:纯java编写

### GUI继承体系图

![img](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092023122.jpg)	

### 图标设置和皮肤设置

图标设置使用 Toolkit 类

皮肤设置使用UIManager.setLookAndFeel

#### 皮肤设置无效

需要在main方法中加入：

```java
JFrame.setDefaultLookAndFeelDecorated(true);
```

## 网络编程

> 首先理清一个概念：网络编程 ！= 网站编程，网络编程现在一般称为TCP/IP编程。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092025670.jpg)	

### 网络通信协议和接口

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092025955.jpg)	

### 通信协议分层思想

​	![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092025597.jpg)

### 参考模型

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092025610.jpg)	

### IP协议

> 每个人的电脑都有一个独一无二的IP地址，这样互相通信时就不会传错信息了。
>
> IP地址是用一个点来分成四段的，在计算机内部IP地址是用四个字节来表示的，一个字节代表一段，每一个字节代表的数最大只能到达255。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092026372.jpg)	

### TCP协议和UDP协议

> **TCP和UDP位于同一层，都是建立在IP**层的基础之上**。由于两台电脑之间有不同的IP地址，因此两台电脑就可以区分开来，也就可以互相通话了。通话一般有两种通话方式：**第一种是**TCP**，第二种是**UDP**。**TCP是可靠的连接，TCP就像打电话，需要先打通对方电话，等待对方有回应后才会跟对方继续说话，也就是一定要确认可以发信息以后才会把信息发出去。TCP上传任何东西都是可靠的，只要两台机器上建立起了连接，在本机上发送的数据就一定能传到对方的机器上，UDP就好比发电报，发出去就完事了，对方有没有接收到它都不管，所以UDP是不可靠的**。TCP传送数据虽然可靠，但传送得比较慢，UDP传送数据不可靠，但是传送得快。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092027659.jpg)	

### Socket编程

> 一般的网络编程都称为**Socket编程**，Socket的英文意思是“插座”。
>
> 两台电脑都安装上一个插座，然后使用一根线的两端插到两台电脑的插座上，这样两台电脑就建立好了连接。这个插座就是Socket。
>
> 因为互相之间都能互相通信，我说你是我的Server只是从逻辑意义上来讲，我应该把东西先发到你那里去，然后由你来处理，转发。所以你叫Server。但从技术意义上来讲，只有TCP才会分Server和Client。对于UDP来说，从严格意义上来讲，并没有所谓的Server和Client。TCP的Server的插座就叫ServerSocket，Client的插座就叫Socket。
>
> 两台计算机互相连接，那么首先必须得知道它们的IP地址，但是只提供IP地址是不够的，还必须要有连接的端口号，也就是要连接到哪个应用程序上。
>
> 端口号是用来区分一台机器上不同的应用程序的。**端口号在计算机内部是占2个字节。一台机器上最多有65536个端口号**。一个应用程序可以占用多个端口号。端口号如果被一个应用程序占用了，那么其他的应用程序就无法再使用这个端口号了。记住一点，**我们编写的程序要占用端口号的话占用1024以上的端口号，1024以下的端口号不要去占用，因为系统有可能会随时征用**。端口号本身又分为TCP端口和UDP端口，TCP的8888端口和UDP的8888端口是完全不同的两个端口。TCP端口和UDP端口都有65536个。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092029814.jpg)	

### TCP Socket通信模型

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092029355.jpg)	

### Socket使用范例

> 客户端通过端口6666向服务器端请求连接，服务器端接受客户端的连接请求以后，就在服务器端上安装一个Socket，然后让这个Socket与客户端的Socket连接，这样服务器端就可以与客户端互相通信了，当有另外一个客户端申请连接时，服务器端接受了以后，又会安装另外一个Socket与这个客户端的Socket进行连接。

![image](https://gitee.com/image_bed/image-bed1/raw/master/img/202201092030165.gif)	

#### UDP协议测试

> 接收端

```java
package com.hyatt.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**  
 * UDP协议接收
 * @author hyatt  
 * @version 1.0  
 */
public class UDPReceiveTest {
	public static void main(String[] args) throws IOException {
		// 创建UDPSocket对象
		DatagramSocket ds = new DatagramSocket(10080);
		// 创建数据包对象 DatagramPacket(byte[] buf, int length)
		byte[] buf = new byte[1024];
		int length = buf.length;
		DatagramPacket dp = new DatagramPacket(buf, length);
		// 接收数据包
		ds.receive(dp);
		// 解析并打印接收数据
		String name = dp.getAddress().getHostName();
		String ip = dp.getAddress().getHostAddress();
		String data = new String(buf, 0, dp.getLength());
		System.out.println("name:" + name);
		System.out.println("ip:" + ip);
		System.out.println("data:" + data);
		// 释放资源
		ds.close();
	}

}
```

> 发送端

```java
package com.hyatt.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**  
 * UDP协议发送
 * @author hyatt  
 * @version 1.0  
 */
public class UDPSendTest {
	public static void main(String[] args) throws IOException {
		// 创建UDPSocket对象
		DatagramSocket ds = new DatagramSocket();
		// 创建数据包 DatagramPacket(byte[] buf, int length, InetAddress address, int port)
		byte[] buf = "你好, 我这里是2080年, 你那边是什么时间?".getBytes();
		int length = buf.length;
		InetAddress address = InetAddress.getByName("xuteng");
		int port = 10080;
		DatagramPacket dp = new DatagramPacket(buf, length, address, port);
		// 发送数据包
		ds.send(dp);
		// 释放资源
		ds.close();
	}
}
```

#### TCP协议测试

> 客户端

```java
package com.hyatt.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**  
 * TCP客户端
 * Connection refused: connect： 发送之前, 服务器对应的端口必须是开着的, 否则报错
 * @author hyatt  
 * @version 1.0  
 */
public class ClientTest {
	public static void main(String[] args) throws UnknownHostException, IOException {
		// 创建对象
		Socket socket = new Socket("127.0.0.1", 8888);
		
		// 写入数据
		OutputStream os = socket.getOutputStream();
		os.write("你好, TCP".getBytes());
		// 释放资源
		socket.close();
	}

}
```

> 服务端

```java
package com.hyatt.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**  
 * TCP服务端
 * @author hyatt  
 * @version 1.0  
 */
public class ServerTest {
	public static void main(String[] args) throws UnknownHostException, IOException {
		// 创建对象
		ServerSocket ss = new ServerSocket(8888);
		// 接收返回一个Socket对象
		Socket s = ss.accept();
		// 读数据
		InputStream is = s.getInputStream();
		byte[] bys = new byte[1024];
		int length = is.read(bys);
		String str = new String(bys, 0, length);
		System.out.println(str);
		// 释放资源
		s.close();
		ss.close();
	}
}
```

## 反射

> 简单的来说，反射机制指的是程序在运行时能够获取自身的信息。**在java中，只要给定类的名字，那么就可以通过反射机制来获得类的所有信息** 

### 常用的方法

- Constructor getConstructor(Class[] params)//根据指定参数获得public构造器
- Constructor[] getConstructors()//获得public的所有构造器
- Constructor getDeclaredConstructor(Class[] params)//根据指定参数获得public和非public的构造器
- Constructor[] getDeclaredConstructors()//获得public的所有构造器 

### 获得类方法的方法

- Method getMethod(String name, Class[] params),根据方法名，参数类型获得方法
- Method[] getMethods()//获得所有的public方法
- Method getDeclaredMethod(String name, Class[] params)//根据方法名和参数类型，获得public和非public的方法(**私有方法不在同一个类中调用依然报错**)
- Method[] getDeclaredMethods()//获得所以的public和非public方法 

### 获得类中属性的方法 

- Field getField(String name)//根据变量名得到相应的public变量
- Field[] getFields()//获得类中所以public的方法
- Field getDeclaredField(String name)//根据方法名获得public和非public变量
- Field[] getDeclaredFields()//获得类中所有的public和非public方法

### 练习

```java
package com.hyatt.reflect;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class Teacher {

	public Teacher() {
	}

	public void love() {
		System.out.println("爱生活,爱教书");
	}
}
```

```java
package com.hyatt.reflect;

/**  
* @author hyatt  
 * @version 1.0  
 */
public class Student {

	public String name;
	public int age;

	public Student() {
	}

	public void love() {
		System.out.println("爱生活,爱学习");
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
}
```

```java
package com.hyatt.reflect;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class Worker {

	public Worker() {
	}

	public void love() {
		System.out.println("爱生活,爱老婆");
	}
}
```

```java
package com.hyatt.reflect;

import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;

/**  
 * 使用反射
 * @author hyatt  
 * @version 1.0  
 */
public class Test {

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		FileReader fr = new FileReader("prop.txt");
		props.load(fr);
		// 获取文件中的值
		String className = props.getProperty("className");
		String classMethod = props.getProperty("classMethod");
		// 获取类对象
		Class c = Class.forName(className);
		// 创建对象
		Constructor con = c.getConstructor();
		Object obj = con.newInstance();
		// 调用方法
		Method method = c.getMethod(classMethod);
		method.invoke(obj);

		// Student s = new Student();
		// s.name = "嬴政";
		// s.age = 48;
		// System.out.println(s);

		Field field = c.getDeclaredField("name");
		Field field2 = c.getDeclaredField("age");
		field.set(obj, "嬴政");
		field2.set(obj, 48);
		System.out.println(obj);

	}
}
```

### 练习2

```java
package com.hyatt.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**  
 * 提供一个ArratList<Integer>对象, 我想在这个集合中添加字符串
 * @author hyatt  
 * @version 1.0  
 */
public class ArrayListTest {
	public static void main(String[] args) throws Exception {
		ArrayList<Integer> arrays = new ArrayList<Integer>();
		// 正常添加是只能添加Integer对象的数据
		arrays.add(10);
		// arrays.add("hello"); // 报错

		// 使用反射调用add方法
		// 创建ArrayList对象
		Class c = Class.forName("java.util.ArrayList");
		Constructor con = c.getConstructor();
		Object obj = con.newInstance();
		// 获取add方法
		Method method = c.getMethod("add", Object.class);
		// 调用
		method.invoke(obj, "hello");
		method.invoke(obj, "world");
		method.invoke(obj, "java");
		System.out.println(obj);
	}
}
```

### 练习3

```java
package com.hyatt.reflect;

import java.lang.reflect.Field;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class ReflectUtil {
	
	/**
	 * 
	 * 将 obj对象中名为propertyName属性的值设置为value
	 * @param obj
	 * @param propertyName
	 * @param value
	 * @throws Exception 
	 */
	public static void setProperty(Object obj, String propertyName, Object value) throws Exception {
		Class c = obj.getClass();
		// 获取属性
		Field field = c.getField(propertyName);
		// 设置值
		field.set(obj, value);
	}
}
```

```java
package com.hyatt.reflect;

/**  
 * 测试类
 * @author hyatt  
 * @version 1.0  
 */
public class ReflectUtilTest {
	public static void main(String[] args) throws Exception {
		// 给 Person类的name复制
		Class c = Class.forName("com.hyatt.reflect.Person");
		Object obj = c.getConstructor().newInstance();
		ReflectUtil.setProperty(obj, "name", "嬴政");
		System.out.println(obj);
		// 给Animal类的name赋值
		Class c2 = Class.forName("com.hyatt.reflect.Animal");
		Object obj2 = c2.getConstructor().newInstance();
		ReflectUtil.setProperty(obj2, "name", "大熊猫");
		System.out.println(obj2);
	}

}

// 创建几个类以供使用
class Person {

	public String name;
	public int age;

	public Person() {
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
}

class Animal {

	public String name;
	public int level;

	public Animal() {
	}

	@Override
	public String toString() {
		return "Animal [name=" + name + ", level=" + level + "]";
	}

}
```

## 动态代理

### 代理

> 本来应该自己做的事情, 却请了别人来做, 被请的人就是代理对象

### 动态代理

> 在程序运行过程中产生了这个对象

### 练习

```java
package com.hyatt.proxy;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public interface UserDao {
	
	public abstract void add();
	public abstract void delete();
	public abstract void modify();
	public abstract void find();
}
```

```java
package com.hyatt.proxy;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class UserDaoImpl implements UserDao {

	@Override
	public void add() {
		System.out.println("添加功能");
	}

	@Override
	public void delete() {
		System.out.println("删除功能");
	}

	@Override
	public void modify() {
		System.out.println("修改功能");
	}

	@Override
	public void find() {
		System.out.println("查询功能");
	}

}
```

```java
package com.hyatt.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class MyInvocationHandler implements InvocationHandler {
	private Object target;

	public MyInvocationHandler(Object target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("权限校验");
		Class c = this.target.getClass();
		Object obj = method.invoke(target, args);
		System.out.println("日志记录");
		return obj;
	}
}
```

```java
package com.hyatt.proxy;

import java.lang.reflect.Proxy;

/**  
 * 动态代理
 * @author hyatt  
 * @version 1.0  
 */
public class ProxyTest {
	public static void main(String[] args) {
		// 普通使用
		UserDao userDao = new UserDaoImpl();
		userDao.add();
		userDao.delete();
		userDao.modify();
		userDao.find();
		System.out.println("-----------------------");
		// 使用动态代理
		MyInvocationHandler handler = new MyInvocationHandler(userDao);
		// newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler
		// h)
		UserDao proxy = (UserDao) Proxy.newProxyInstance(userDao.getClass().getClassLoader(),
				userDao.getClass().getInterfaces(), handler);
		proxy.add();
		proxy.delete();
		proxy.modify();
		proxy.find();
	}
}
```

## 枚举

> 枚举是指将变量的值一一列举出来, 变量的值只限于列举出来的值的范围

### 注意事项

- 定义枚举类需要使用关键字enum
- 所有枚举类都是Enum的子类
- 枚举类的第一行必须是枚举项, 最后一个枚举项后的分号是可以省略的，但是**如果枚举类有其他的东西，这个分号就不能省略**. 建议不要省略
- 枚举类可以有构造器, 但是必须是private的，它默认的也是private的。枚举项的用法比较特殊：枚举(“”);
- 枚举类可以有抽象方法, 但是枚举项必须重写该方法
- 枚举可以在switch中使用

### 练习

```java
package com.hyatt.jdk5_new;

/**  
 * JDK5新特性:枚举
 * @author hyatt  
 * @version 1.0  
 */
public enum Direction {
	// 第一种
	// FONT, BEHIND, LEFT, RIGHT;
	// 第二种带参构造
	FONT("前") {
		@Override
		public void show() {
			System.out.println("前");
		}
	},
	BEHIND("后") {
		@Override
		public void show() {
			System.out.println("后");
		}
	},
	LEFT("左") {
		@Override
		public void show() {
			System.out.println("左");
		}
	},
	RIGHT("右") {
		@Override
		public void show() {
			System.out.println("右");
		}
	};

	String name;

	private Direction(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void show();

}
```

```java
package com.hyatt.jdk5_new;

/**  
 * @author hyatt  
 * @version 1.0  
 */
public class DirectionTest {
	public static void main(String[] args) {
		// 第一种
		Direction direction = Direction.FONT;
		// 执行的是Enum类的toString方法
		System.out.println(direction);
		System.out.println("==========================");
		// 第二种带参数
		Direction direction2 = Direction.FONT;
		System.out.println(direction2);
		System.out.println(direction2.getName());
		System.out.println("==========================");
		// 第三种有一个抽象方法
		Direction direction3 = Direction.FONT;
		System.out.println(direction3);
		System.out.println(direction3.getName());
		direction3.show();
	}
}
```

