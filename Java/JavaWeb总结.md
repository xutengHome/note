# JavaWeb总结

## Junit单元测试

### 测试分类

- 黑盒测试：不需要编写代码(不关心具体代码逻辑),只是输入值，看程序是否可以给出期望的结果
- 白盒测试：需要写代码。关注程序的具体执行流程

### Junit的使用(白盒测试)

#### 步骤

- 定义一个测试类
  - 建议
    - 测试类名：被测试的类名Test, 例如：CalculatorTest
    - 包名：xxx.xxx.xx.test, 例如：cn.itcast.test
- 定义一个测试方法：可以独立运行
  - 建议
    - 方法名：test测试的方法名，例如testAdd()
    - 返回值：void
    - 参数列表：空参
- 给方法加上@Test
- 导入Junit依赖环境

#### 判定结果

- 红色：失败
- 绿色：成功
- 一般我们会使用断言操作来处理结果, 而不是输出结果

#### 注解

- @Before
  - 修饰的方法会在测试方法之前被自动运行，一般用来做资源的申请
- @After
  - 修饰的方法会在测试方法之后被自动运行(即使测试方法报错),一般用来做资源的释放

#### 添加junit4依赖

![image-20211118103136542](https://gitee.com/image_bed/image-bed1/raw/master/img/202112101601781.png)	

#### 方法测试

> 点击运行

![image-20211118103229085](https://gitee.com/image_bed/image-bed1/raw/master/img/202112101601318.png)	

#### 测试案例

```java
/**
 * <p>进行数学计算的类</p>
 *
 * @author hyatt 2021/11/18 10:23
 * @version 1.0
 */
public class Calculator {

    /**
     * 计算两个整数之和
     *
     * @param a 整数
     * @param b 整数
     * @return 整数之和
     */
    public int add(int a, int b) {
        return a + b;
    }

    /**
     * 计算两个整数的差
     *
     * @param a 被减数
     * @param b 减数
     * @return 差
     */
    public int sub(int a, int b) {
        return a - b;
    }
}
```

```java
// 测试类
import cn.itcast.junit.Calculator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * <p>Calculator类的测试类</p>
 *
 * @author hyatt 2021/11/18 10:26
 * @version 1.0
 */
public class CalculatorTest {

    /**
     * 初始化方法：
     * 用于资源的申请，所有的测试方法在执行之前都会执行该方法
     */
    @Before
    public void init() {
        System.out.println("init.....");
    }

    /**
     * 释放资源方法：
     * 在所有测试方法执行完之后，都会自动执行该方法
     */
    @After
    public void close() {
        System.out.println("close.....");
    }

    /**
     * 测试add方法
     */
    @Test
    public void testAdd() {
        System.out.println("testAdd...");
        // 1. 创建Calculator对象
        Calculator c = new Calculator();
        // 2. 调用add方法
        int result = c.add(1, 2);
        // 3. 断言
        Assert.assertEquals(result, 33);
    }

    /**
     * 测试 sub 方法
     */
    @Test
    public void testSub() {
        System.out.println("testSub.....");
        // 1. 创建Calculator对象
        Calculator c = new Calculator();
        // 2. 调用add方法
        int result = c.sub(1, 2);
        // 3. 断言
        Assert.assertEquals(result, -1);
    }
}
```

## 反射: 框架设计的灵魂

- 框架：半成品软件。可以在框架的基础上进行软件开发，简化编码
- 反射：将类的各个组成部分封装为其他对象，这就是反射机制
  - 好处
    1. 可以在程序运行过程中操作这些对象
    2. 可以解耦，提高程序可扩展性
- 获取Class对象的方式
  - Class.forName("全类名")： 将字节码文件加载进内存, 返回Class对象
    - 多用于配置文件，将类名定义在配置文件中。读取文件，加载类
  - 类名.class: 通过类名的属性class获取
    - 多用于参数的传递
  - 对象.getClass(): getClass()方法在Object类中定义
    - 多用于对象的获取字节码的方式
  - 结论
    - 同一个字节码文件(*.class)在一次程序运行过程中，只会被加载一次，不论哪一种方式获取的Class对象都是同一个

![image-20211118110926682](https://gitee.com/image_bed/image-bed1/raw/master/img/202112101601976.png)

```java
import cn.itcast.domain.Person;

/**
 * <p>反射：类文件的获取</p>
 *
 * @author hyatt 2021/11/18 11:25
 * @version 1.0
 */
public class ReflectTest {
    public static void main(String[] args) throws Exception {
        /**
         * 获取Class对象的方式
         * 1. Class.forName("全类名")： 将字节码文件加载进内存, 返回Class对象
         * 2. 类名.class: 通过类名的属性class获取
         * 3. 对象.getClass(): getClass()方法在Object类中定义
         */
        Class cls1 = Class.forName("cn.itcast.domain.Person");
        System.out.println(cls1);
        Class cls2 = Person.class;
        System.out.println(cls2);
        Class cls3 = new Person().getClass();
        System.out.println(cls3);
        // 判断是否相等
        // true
        System.out.println(cls1 == cls2);
        // true
        System.out.println(cls1 == cls3);
    }
}
```

### Class对象功能

- 获取功能：
  1. 获取成员变量
     - Field[]	getFields()：获取到所有public修饰的field
     - Field	getField(String name)：获取指定名称的public修饰的field，否则报错:Exception in thread "main" java.lang.NoSuchFieldException:
     - Field[]	getDeclaredFields()：可以获取所有的成员变量, 不考虑修饰符
     - Field	getDeclaredField(String name)：获取指定名称的成员变量，不考虑修饰符
  2. 获取构造方法
     - Constructor<?>[]	getConstructors()：获取所有public修饰的构造方法
     - Constructor<T>	getConstructor(Class<?>... parameterTypes)：获取public修饰的指定参数列表的构造方法
     - Constructor<?>[]	getDeclaredConstructors()：获取所有的构造方法
     - Constructor<T>	getDeclaredConstructor(Class<?>... parameterTypes)：获取public修饰的指定参数列表的构造方法
  3. 获取成员方法
     - Method[]	getMethods()：获取所有public修饰的方法
     - Method	getMethod(String name, Class<?>... parameterTypes)：获取public修饰的指定参数列表的方法
     - Method[]	getDeclaredMethods()：获取所有的方法
     - Method	getDeclaredMethod(String name, Class<?>... parameterTypes)：获取指定参数列表的方法
  4. 获取类名
     - String	getName()：全类名(包括包)

### Filed: 成员变量

#### 操作

1. 设置值
   - void	set(Object obj, Object value)
2. 获取值
   - Object	get(Object obj)
3. 忽略访问权限修饰符的安全检查
   - setAccessible(true)：暴力反射

#### 实例

```java
import cn.itcast.domain.Person;

import java.lang.reflect.Field;

/**
 * <p>
 * - 获取功能：
 *   1. 获取成员变量
 *      - Field[]	getFields()
 *      - Field	getField(String name)
 *      - Field[]	getDeclaredFields()
 *      - Field	getDeclaredField(String name)
 *   2. 获取构造方法
 *      - Constructor<?>[]	getConstructors()
 *      - Constructor<T>	getConstructor(Class<?>... parameterTypes)
 *      - Constructor<?>[]	getDeclaredConstructors()
 *      - Constructor<T>	getDeclaredConstructor(Class<?>... parameterTypes)
 *   3. 获取成员方法
 *      - Method[]	getMethods()
 *      - Method	getMethod(String name, Class<?>... parameterTypes)
 *      - Method[]	getDeclaredMethods()
 *      - Method	getDeclaredMethod(String name, Class<?>... parameterTypes)
 *   4. 获取类名
 *      - String	getName()
 * </p>
 *
 * @author hyatt 2021/11/18 11:25
 * @version 1.0
 */
public class ReflectTest2 {
    public static void main(String[] args) throws Exception {
        /**
         * 1. 获取成员变量
         *    - Field[]	getFields(): 获取到所有public修饰的field
         *    - Field	getField(String name): 获取指定名称的public修饰的field，否则报错:Exception in thread "main" java.lang.NoSuchFieldException: b
         *    - Field[]	getDeclaredFields(): 可以获取所有的成员变量, 不考虑修饰符
         *    - Field	getDeclaredField(String name)：获取指定名称的成员变量，不考虑修饰符
         */
        // Field[]	getFields()
        Class<Person> personClass = Person.class;
        Field[] fields = personClass.getFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        // Field	getField(String name)
        // 获取public修饰的属性
        Field a = personClass.getField("a");
        System.out.println(a);
        System.out.println("------------------------------");
        // Field[]	getDeclaredFields()
        Field[] declaredFields = personClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println(declaredField);
        }
        // Field	getDeclaredField(String name)
        // 获取public修饰的属性
        Field a1 = personClass.getDeclaredField("a");
        System.out.println(a1);
        // 获取非public修饰的属性，这里以private修饰符为例
        Field age = personClass.getDeclaredField("age");
        System.out.println(age);
        System.out.println("==================================");
        // 测试私有属性的值的设置
        Field name = personClass.getDeclaredField("name");
        Person person = new Person();
        // 由于私有属性不允许本类以外的类访问所以这里需要设置暴力反射
        name.setAccessible(true);
        name.set(person, "嬴政");
        System.out.println(person);
        // get方法
        System.out.println(name.get(person));
    }
}
```

### Constructor：构造方法

#### 创建对象

- T	newInstance(Object... initargs)
- 如果使用空参构造方法创建对象，操作可以简化：使用Class对象的newInstance()方法

#### 实例

```java
import cn.itcast.domain.Person;

import java.lang.reflect.Constructor;

/**
 * <p>
 * - 获取功能：
 *   1. 获取成员变量
 *      - Field[]	getFields()
 *      - Field	getField(String name)
 *      - Field[]	getDeclaredFields()
 *      - Field	getDeclaredField(String name)
 *   2. 获取构造方法
 *      - Constructor<?>[]	getConstructors()
 *      - Constructor<T>	getConstructor(Class<?>... parameterTypes)
 *      - Constructor<?>[]	getDeclaredConstructors()
 *      - Constructor<T>	getDeclaredConstructor(Class<?>... parameterTypes)
 *   3. 获取成员方法
 *      - Method[]	getMethods()
 *      - Method	getMethod(String name, Class<?>... parameterTypes)
 *      - Method[]	getDeclaredMethods()
 *      - Method	getDeclaredMethod(String name, Class<?>... parameterTypes)
 *   4. 获取类名
 *      - String	getName()
 * </p>
 *
 * @author hyatt 2021/11/18 11:25
 * @version 1.0
 */
public class ReflectTest3 {
    public static void main(String[] args) throws Exception {
        /**
         *   2. 获取构造方法
         *      - Constructor<?>[]	getConstructors(): 获取所有public修饰的构造方法
         *      - Constructor<T>	getConstructor(Class<?>... parameterTypes): 获取public修饰的指定参数列表的构造方法
         *      - Constructor<?>[]	getDeclaredConstructors()：获取所有的构造方法
         *      - Constructor<T>	getDeclaredConstructor(Class<?>... parameterTypes)：获取public修饰的指定参数列表的构造方法
         */
        Class<Person> personClass = Person.class;
        // Constructor<?>[]	getConstructors()
        Constructor<?>[] constructors = personClass.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
        }
        // Constructor<T>	getConstructor(Class<?>... parameterTypes)
        // 获取空构造方法
        Constructor<Person> constructor = personClass.getConstructor();
        System.out.println(constructor);
        // 获取带参构造函数
        Constructor<Person> constructor1 = personClass.getConstructor(int.class, String.class);
        System.out.println(constructor1);
        // 设置对象的值
        Person person = constructor1.newInstance(40, "嬴政");
        System.out.println(person);
        // 使用空参构造函数的简便方式
        Person person1 = personClass.newInstance();
        System.out.println(person1);
        System.out.println("=============================");
        System.out.println("Declare修饰的就不一一测试了");
    }
}
```

### Method方法对象

#### 执行方法

- Object	invoke(Object obj, Object... args)

#### 获取方法名称

- String	getName()：获取方法名称

#### 实例

```java
import cn.itcast.domain.Person;
import java.lang.reflect.Method;

/**
 * <p>
 * - 获取功能：
 *   1. 获取成员变量
 *      - Field[]	getFields()
 *      - Field	getField(String name)
 *      - Field[]	getDeclaredFields()
 *      - Field	getDeclaredField(String name)
 *   2. 获取构造方法
 *      - Constructor<?>[]	getConstructors()
 *      - Constructor<T>	getConstructor(Class<?>... parameterTypes)
 *      - Constructor<?>[]	getDeclaredConstructors()
 *      - Constructor<T>	getDeclaredConstructor(Class<?>... parameterTypes)
 *   3. 获取成员方法
 *      - Method[]	getMethods()
 *      - Method	getMethod(String name, Class<?>... parameterTypes)
 *      - Method[]	getDeclaredMethods()
 *      - Method	getDeclaredMethod(String name, Class<?>... parameterTypes)
 *   4. 获取类名
 *      - String	getName()
 * </p>
 *
 * @author hyatt 2021/11/18 11:25
 * @version 1.0
 */
public class ReflectTest4 {
    public static void main(String[] args) throws Exception {
        /**
         *   2. 获取构造方法
         *      - Method[]	getMethods(): 获取所有public修饰的方法
         *      - Method	getMethod(String name, Class<?>... parameterTypes)：获取public修饰的指定参数列表的方法
         *      - Method[]	getDeclaredMethods()：获取所有的方法
         *      - Method	getDeclaredMethod(String name, Class<?>... parameterTypes)：获取修饰的指定参数列表的方法
         */
        Class<Person> personClass = Person.class;
        // Method[]	getMethods()
        Method[] methods = personClass.getMethods();
        for (Method method : methods) {
            System.out.println(method);
            System.out.println(method.getName());
        }
        // Method	getMethod(String name, Class<?>... parameterTypes)
        Method eat = personClass.getMethod("eat");
        // 执行方法
        Person person = new Person();
        eat.invoke(person);
        // Method	getDeclaredMethod(String name, Class<?>... parameterTypes)
        // 获取私有方法
        Method eat1 = personClass.getDeclaredMethod("eat", String.class);
        eat1.setAccessible(true);
        eat1.invoke(person, "饭");

        // 获取类名
        // cn.itcast.domain.Person
        System.out.println(personClass.getName());
    }
}
```

### 案例

> 可以创建任意类的对象并执行任意方法

```java
import java.io.FileReader;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * <p>
 *     反射案例：可以创建任意类的对象并执行任意方法
 *          前提：类代码不能改变
 * </p>
 *
 * @author hyatt 2021/11/19 11:07
 * @version 1.0
 */
public class ReflectCase {
    public static void main(String[] args) throws Exception {
        // 1. 加载配置文件
        // 1.1 创建Properties对象
        Properties properties = new Properties();
        // 1.2 加载配置文件，转换为一个集合
        // FileReader fileReader = new FileReader("src/prop.properties");
        // properties.load(fileReader);
        ClassLoader classLoader = ReflectCase.class.getClassLoader();
        InputStream is = classLoader.getResourceAsStream("prop.properties");
        properties.load(is);
        // 2. 获取类名和方法名
        String className = properties.getProperty("className");
        String methodName = properties.getProperty("methodName");
        // 3. 创建类对象
        Class<?> aClass = Class.forName(className);
        // 4. 创建对象
        Object o = aClass.newInstance();
        // 5. 获取方法对象
        Method method = aClass.getMethod(methodName);
        // 6. 执行方法
        method.invoke(o);
    }
}
```

> src/prop.properties: 维护对应的类和方法

```java
className=cn.itcast.domain.Student
methodName=study
```

## 注解

### 注解定义

> 从JDK5开始,Java增加对元数据的支持，也就是注解，注解与注释是有一定区别的，可以把注解理解为代码里的特殊标记，这些标记可以在编译，类加载，运行时被读取，并执行相应的处理。通过注解开发人员可以在不改变原有代码和逻辑的情况下在源代码中嵌入补充信息。

### 内置注解

- @Override：当子类重写父类方法时，子类可以加上这个注解，那这有什么什么用？这可以确保子类确实重写了父类的方法，避免出现低级错误
- @Deprecated：这个注解用于表示某个程序元素类，方法等已过时，当其他程序使用已过时的类，方法时编译器会给出警告（删除线，这个见了不少了吧）
- @SuppressWarnings：被该注解修饰的元素以及该元素的所有子元素取消显示编译器警告，例如修饰一个类，那他的字段，方法都是显示警告(一般传递参数all @SuppressWarnings("all"))

### 自定义注解

#### 格式

```java
public @interface MyAnno {
}
```

#### 本质

> 注解的本质就是一个接口，该接口默认继承java.lang.annotation.Annotation接口
>
> 使用javap命令反编译class文件

```java
public interface MyAnno extends java.lang.annotation.Annotation {
}
```

#### 属性

> 接口中的抽象方法

##### 要求

* 属性的返回值类型有以下取值
  * 基本数据类型
  * String类型
  * 枚举类型
  * 注解
  * 以上类型的数组
* 定义了属性，在使用属性的时候需要赋值
  * 如果定义属性时，使用default关键字给属性默认初始化值，则使用注解的时候可以不进行属性的赋值
  * 如果只有一个属性需要赋值，并且属性的名称是value，则value可以省略，直接定义值即可
  * 数组赋值的时候，值使用{}包裹。如果数组中只有一个值，则可以省略{}

> 注解定义

```java
package cn.itcast.annotation;

/**
 * <p>自定义注解</p>
 *
 * @author hyatt 2021/12/9 14:52
 * @version 1.0
 */
public @interface MyAnno {
    int age();
    String name() default "嬴政";
    Direction dir();
    MyAnno2 anno2();
    String[] strs();
}
```

> 注解使用

```java
/**
 * <p>注解使用测试</p>
 *
 * @author hyatt 2021/12/9 15:15
 * @version 1.0
 */
@MyAnno(age = 12, dir = Direction.D, anno2 = @MyAnno2, strs = {"嬴政", "刘邦"})
public class AnnoTest {
}
```

### 元注解

> 用于描述注解的注解

- @Target：描述注解能够作用的位置
  - ElementType取值：
    - TYPE：可以作用于类上
    - METHOD：可以作用于方法上
    - FIELD：可以作用于属性上
- @Retention：描述注解被保留的阶段
  - @Retention(RetentionPolicy.RUNTIME)：当前被描述的注解，会被保留到class字节码文件中，并被JVM读取到
- @Documented：描述注解是否被抽取到api文档中
- @inherited：描述注解是否被子类继承

### 在程序中使用(解析)注解

> 获取属性中定义的属性值

1. 获取注解定义位置的对象(Class, Method, Field)
2. 获取指定的注解
3. 调用注解中定义的抽象方法获取属性值

> 代码实例

```java
import java.lang.reflect.Method;

/**
 * <p>反射：通过注解</p>
 *
 * @author hyatt 2021/11/18 11:25
 * @version 1.0
 */
@SuppressWarnings("all")
@Pro(className = "cn.itcast.domain.Student", methodName = "study")
public class ReflectTest {
    public static void main(String[] args) throws Exception {
        // 1. 解析注解
        // 1.1 获取该类的字节码文件对象
        Class<ReflectTest> reflectTestClass = ReflectTest.class;
        // 2. 获取注解对象
        /**
         * 其实就是在内存中生成了一个该注解接口的子类实现对象
         * public class ProImpl implements Pro {
         *     public String className() {
         *         return "cn.itcast.domain.Student"
         *     }
         *
         *     public String methodName() {
         *         return "study"
         *     }
         * }
         */
        Pro annotation = reflectTestClass.getAnnotation(Pro.class);
        // 3. 调用注解中定义的抽象方法，获取返回值
        String className = annotation.className();
        String methodName = annotation.methodName();
        System.out.println(className);
        System.out.println(methodName);
        // 通过反射调用对应的对象和方法
        // 4. 加载该类进入内存
        Class<?> aClass = Class.forName(className);
        // 5. 创建对象
        Object o = aClass.newInstance();
        // 6. 获取方法对象
        Method method = aClass.getMethod(methodName);
        // 7. 方法执行
        method.invoke(o);
    }
}
```

> 简单的测试框架

> Calculator.java

```java
/**
 * <p>
 * 计算器类
 * 通过注解来测试这个类中的方法是否有异常
 * 加上自定义注解然后编写解析程序检查加上注解的程序是否有异常
 * </p>
 *
 * @author hyatt 2021/12/10 15:25
 * @version 1.0
 */
public class Calculator {

    @Check
    public void add() {
        System.out.println("1 + 0 = " + (1 + 0));
    }

    @Check
    public void sub() {
        System.out.println("1 - 0 = " + (1 - 0));
    }

    @Check
    public void mul() {
        System.out.println("1 * 0 = " + (1 * 0));
    }

    @Check
    public void div() {
        System.out.println("1 / 0 = " + (1 / 0));
    }

    public void show() {
        System.out.println("永远没有bug的方法");
    }
}	
```

> Check.java

```java
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Check {
}
```

> CheckTest.java

```java
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * <p>简单的测试框架</p>
 *
 * @author hyatt 2021/12/10 15:29
 * @version 1.0
 */
public class CheckTest {
    public static void main(String[] args) throws IOException {
        // 1. 获取计算器对象
        Calculator calculator = new Calculator();
        Class calculatorClass = calculator.getClass();
        // 2. 获取方法
        Method[] methods = calculatorClass.getMethods();

        // io
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("bug.txt"));
        // 记录出现异常的次数
        int number = 0;

        // 3. 遍历方法并执行
        for (Method method : methods) {
            // 4. 判断是否有被Check标签标记
            if (method.isAnnotationPresent(Check.class)) {
                // 5. 执行方法
                try {
                    method.invoke(calculator);
                } catch (Exception e) {
                    number++;
                    // 将异常信息放在bug.txt文件中
                    bufferedWriter.write(method.getName() + " 方法出现异常了");
                    bufferedWriter.newLine();
                    bufferedWriter.write("异常的名称：" + e.getCause().getClass().getSimpleName());
                    bufferedWriter.newLine();
                    bufferedWriter.write("异常的原因：" + e.getCause().getMessage());
                    bufferedWriter.newLine();
                    bufferedWriter.write("----------------------------");
                    bufferedWriter.newLine();
                }
            }
        }
        bufferedWriter.write("本次测试一共出现了" + number + "次异常");
        bufferedWriter.flush();
        bufferedWriter.close();
    }
}	
```

![image-20211210160118002](https://gitee.com/image_bed/image-bed1/raw/master/img/202112101601102.png)	

## SQL

### SQL的定义

> Structured Query Language：结构化查询语言
>
> 其实就是定义了操作所有的关系型数据库的规则，但是每一个数据库的操作会有不同的地方，称为"方言"

### 通用语法

- SQL语句可以单行或者多行书写，以分好结尾。
- 可使用空格和缩进来增强语句的可读性
- MySQL 数据库和 SQL 语句不区分大小写，关键字建议使用大写
- 注释方式
  - 单行注释：-- 注释内容 或者 # 注释内容(mysql独有)
  - 多行注释：/* 注释内容 */

### SQL语句的分类

> 具体见MySQL的使用

- Data Definition Language (DDL 数据定义语言) 如：建库，建表 
- Data Manipulation Language(DML 数据操纵语言)，如：对表中的记录操作增删改 
- Data Query Language(DQL 数据查询语言)，如：对表中的查询操作 
- Data Control Language(DCL 数据控制语言)，如：对用户权限的设置

![image-20211210165324258](https://gitee.com/image_bed/image-bed1/raw/master/img/202112101653208.png)

## MySQL

### 安装

> [MySQL安装教程](https://blog.csdn.net/WHEeeee/article/details/115286833)

### 配置

#### MySQL服务启动

> 通过服务启动，打开服务界面

```
cmd --> services.msc 打开服务的窗口
```

> 使用管理员打开cmd

```mysql
net start mysql -- 启动mysql服务
net stop mysql -- 关闭mysql服务
```

### MySQL登陆

```mysql
1. mysql -uroot -p密码
2. mysql -hip地址 -uroot -p目标的密码
3. mysql --host=ip底子 --user=root --password=目标密码
```

### MySQL退出

```mysql
1. quit;
2. exit;
```

### MySQL目录结构

> 几个概念

- 数据库：文件夹
- 表：文件
- 数据：数据

### DDL：操作数据库、表

#### 操作数据库：CRUD

##### C(Create)：创建

```mysql
create database 数据库名称; -- 创建数据库 默认编码为utf8
create database if not exists 数据库名称; -- 创建数据库，判断不存在再创建
create database 数据库名称 character set 字符集名称; -- 创建数据库并制定字符集
-- 练习：创建数据库，判断不存在再创建并制定字符集
create database if not exists 数据库名称 character set 字符集名称;
```

##### R(Retrieve)：查询

```mysql
show databases; -- 查询所有数据库
show create databases 数据库名称; -- 查询某个数据库的字符集，查询数据库的创建语句
```

##### U(Update)：修改

```mysql
alter database 数据库名称 character set 字符集名称; -- 修改数据库的字符集
```

##### D(Delete)：删除

```mysql
drop database 数据库名称; -- 删除数据库
drop database if exists 数据库名称; -- 删除数据库，判断存在则删除
```

##### 使用数据库

```mysql
select database(); -- 查询当前正在使用的数据库名称
use 数据库名称; -- 使用数据库
```

#### 操作表

##### C(Create)：创建

```mysql
1. 创建语法
create table 表名(
	列名1 数据类型1，
    列名2 数据类型2，
    ......
    列名n 数据类型n
);
2. 数据库类型(常用)
	int: 整数类型
		* age int,
	double: 小数类型
		* score double(5,2) 表示长度5, 小数有两位, 整数3位
	date: 日期，只包含年月日，yyyy-MM-dd
	datetime: 日期，包含年月日时分秒 yyyy-MM-dd HH:mm:ss
	timestamp: 时间戳类型 包含年月日时分秒 yyyy-MM-dd HH:mm:ss 默认当前系统时间
	varchar: 字符串
		* name varchar(20): 最多20个字符
		* zhangsan 8个字符 张三 2和字符
3. 创建表
create table student (
	id int,
    name varchar(32),
    age int,
    score double(4,1),
    birthday date,
    creation_date timestamp
);
create table 表名 like 被复制表名; -- 复制表
```

##### R(Retrieve)：查询

```mysql
show tables; -- 查询某个数据库中所有的表名称
desc 表名; -- 查询表结构
```

##### U(Update)：修改

```mysql
alter table 表名 rename to 新的表名; -- 修改表名
alter table 表名 character set 字符集名称; -- 修改字符集
alter table 表名 add 列名 数据类型; -- 新增一列
alter table 表名 change 列名 新列名 新数据类型; -- 修改列名和数据类型
alter table 表名 modify 列名 新数据类型; -- 修改数据类型
```

##### D(Delete)：删除

```mysql
drop table 表名; -- 删除表
drop table if exists 表名; -- 如果存在则删除
```

### DML：增删改表中数据

#### 添加数据

> 语法

```mysql
insert into table 表名(列名1,列名2,..列名n) values(值1,值2,...值n)
```

> 注意

- 列名和值要一一对应
- 如果表名后，不定义列名，则默认给所有列添加值

```mysql
insert into table 表名 values(值1,值2,...值n)
```

- 除了数字类型，其他类型需要使用引号(单双都可以)引起来

#### 删除数据

> 语法

```mysql
delete from 表名 [where 条件]
```

> 注意

- 如果不加条件则删除表中的所有记录
- 如果要删除所有记录

```mysql
delete from 表名 -- 不推荐使用。有多少条记录就会执行多少次删除操作
truncate table 表名; -- 推荐使用，效率更高，先删除表，然后在创建一张一样的表包括索引
```

#### 修改数据

> 语法

```mysql
update 表名 set 列名1 = 值1, 列名2 = 值2, ... 列名n = 值n [where 条件]
```

### DQL: 查询表中的数据

> 语法

```mysql
select 字段列表 from 表名列表 where 条件列表 group 分组字段 having 分组之后的条件 order by 排序字段 limit 分页限定
```

#### 基础查询

```mysql
1. 多个字段的查询
select 字段名1, 字段名2 ... from 表名;
	* 注意，如果查询所有字段可以使用 * 来替换字段列表
		select * from 表名; -- 查询表中的所有数据
2. 去除重复
	* distinct： select distinct name from student;
3. 计算列
	* 一般可以使用四则表达式计算一些列的值(一般只进行数值运算)
	* ifnull(表达式1, 表达式2)：null参与的运算，计算结果都为null。如果表达式1的值为null则取表达式2的值
		select ifnull(score, -1) from student; -- 分数为空则为-1
4. 起别名
	* as(可以省略)
		select score 分数 from student;
```

#### 条件查询

| 比较运算符          | 说明                                                         |
| ------------------- | ------------------------------------------------------------ |
| >、<、<=、>=、=、<> | <>在 SQL 中表示不等于，在 mysql 中也可以使用!= , 没有==      |
| BETWEEN...AND       | 在一个范围之内，如：between 100 and 200 相当于条件在 100 到 200 之间，包头又包尾 |
| IN(集合)            | 集合表示多个值，使用逗号分隔                                 |
| LIKE '张%'          | 模糊查询(下划线(_)：表示单个任意字符。%: 表示任意字符)       |
| IS NULL             | 查询某一列为 NULL 的值，注：不能写=NULL                      |

##### 具体使用

```mysql
1. 准备表和数据
CREATE TABLE student ( 
  id INT,  -- 编号 
  NAME VARCHAR(20), -- 姓名 
  age INT, -- 年龄 
  sex VARCHAR(5),  -- 性别 
  address VARCHAR(100),  -- 地址 
  math INT, -- 数学 
  english INT -- 英语 
); 
 
INSERT INTO student(id,NAME,age,sex,address,math,english) VALUES (1,'马云',55,'男','杭州',66,78),(2,'马化腾',45,'女','深圳',98,87),(3,'马景涛',55,'男','香港',56,77),(4,'柳岩',20,'女','湖南',76,65),(5,'柳青',20,'男','湖南',86,NULL),(6,'刘德华',57,'男','香港',99,99),(7,'马德',22,'女','香港',99,99),(8,'德玛西亚',18,'男','南京',56,65);

2. 运算符的使用
-- 查询年龄大于20岁
SELECT * FROM student WHERE age > 20;

-- 查询年龄等于20岁
SELECT * FROM student WHERE age = 20;

-- 查询年龄不等于20岁
SELECT * FROM student WHERE age <> 20;
SELECT * FROM student WHERE age != 20;

-- 查询年龄大于等于20 小于等于30
SELECT * FROM student WHERE age >=20 AND age <=30;
SELECT * FROM student WHERE age >=20 && age <=30;
SELECT * FROM student WHERE age BETWEEN 20 AND 30;

-- 查询年龄是22岁, 18岁, 25岁的信息
SELECT * FROM student WHERE age IN (22,18,25);

-- 查询英语成绩为null
SELECT * FROM student WHERE english IS NULL;

-- 查询英语成绩不为null
SELECT * FROM student WHERE english IS NOT NULL;

-- 模糊查询
-- 查询姓马的
SELECT * FROM student WHERE NAME LIKE '马%';

-- 查询姓名第二个字为化的人
SELECT * FROM student WHERE NAME LIKE '_化%';

-- 查询名字是3个字的人 三个下划线(_)
SELECT * FROM student WHERE NAME LIKE '___';

-- 查询姓名中包含德的人
SELECT * FROM student WHERE NAME LIKE '%德%';
```

#### 排序查询

> 语法

```mysql
order by 排序字段1,排序字段2...
```

> 排序方式
>
> - ASC：升序(默认)
> - DESC：降序
>
> 如果有多个排序条件，则当前边的条件值一样的时候，才会判断第二个条件

```mysql
SELECT * FROM student ORDER BY math ASC, english DESC; -- 先按照math成绩排序，然后在math成绩相同时按照english成绩降序排序
```

#### 聚合函数

> 将一列数据作为一个整体，进行纵向的计算 

> count：计算个数

```mysql
-- 1. 一般选择非空的列：主键
-- 2. count(*): 不建议使用
-- 3. 可以使用count(1)
select count(id) from student;
```

> max: 计算最大值

```mysql
SELECT MAX(english) FROM student;
```

> min: 计算最小值

```mysql
SELECT MIN(english) FROM student;
```

> avg: 计算平均值

```mysql
SELECT AVG(english) FROM student;
```

> sum: 汇总

```mysql
SELECT SUM(english) FROM student;
```

##### 注意

> 聚合函数的计算，排除null值

```mysql
select count(id) from student; -- 8个
select count(english) from student; -- 由于english字段有一个值为null，所以这里的值为7
```

#### 分组查询

> 语法：group by 分组字段

```mysql
-- 按照性别分组，分别查询男、女同学的平均分
-- 为了避免null值加上判断
SELECT AVG(IFNULL(math,0)+IFNULL(english,0)), sex FROM student GROUP BY sex;

-- 数学小于70分的人不参与分组
SELECT AVG(IFNULL(math,0)), sex FROM student WHERE math > 70 GROUP BY sex;

-- 数学小于70分的人不参与分组并且人数要大于2人
SELECT AVG(IFNULL(math,0)), sex, COUNT(id) FROM student WHERE math > 70 GROUP BY sex HAVING COUNT(id)>2;
```

##### 注意

- 分组之后的查询字段：分组字段、集合函数
- where和having的区别
  - where在分组之前进行限定，如果不满足条件，则不参与分组。having在分组之后进行限定，如果不满足结果，则不会被查询出来
  - where 后不可以跟聚合函数，having可以进行聚合函数的判断

#### 分页查询

> 语法：limit 开始的索引，每页查询的条数;

> 公式：开始的索引 = (当前的页码-1) * 每页显示的条数

```mysql
SELECT * FROM student LIMIT 0, 3; -- 第一页
SELECT * FROM student LIMIT 3, 3; -- 第二页
SELECT * FROM student LIMIT 6, 3; -- 第三页
```

> limit 是MySQL的一个 "方言"

### 约束

> 概念：约束是对表中的数据进行限定，保证数据的正确性、有效性和完整性

#### 主键约束

> primary key
>
> 注意：
>
> - 非空且唯一
> - 一张表只能有一个字段作为表的主键
> - 主键就是表中记录的唯一标识

```mysql
-- 创建表的时候添加主键
CREATE TABLE stu(
	id INT PRIMARY KEY, -- 给id添加主键约束
	NAME VARCHAR(20)
);
-- 删除主键约束
ALTER TABLE stu DROP PRIMARY KEY;
-- 添加主键约束
ALTER TABLE stu MODIFY id INT PRIMARY KEY;
```

```mysql
-- 自动增长
-- 创建表的时候添加主键并设置自动增加
CREATE TABLE stu(
	id INT PRIMARY KEY AUTO_INCREMENT, -- 给id添加主键约束
	NAME VARCHAR(20)
);
-- 删除自动增长
ALTER TABLE stu MODIFY id INT;
-- 添加自动增长
ALTER TABLE stu MODIFY id INT AUTO_INCREMENT;
```

#### 非空约束

> not null

```mysql
-- 创建表的时候添加约束
CREATE TABLE stu (
	id INT,
	NAME VARCHAR(20) NOT NULL -- name非空
)
-- 删除非空约束
ALTER TABLE stu MODIFY NAME VARCHAR(10);

-- 创建表后，添加非空约束
ALTER TABLE stu MODIFY NAME VARCHAR(20) NOT NULL;
```

#### 唯一约束

> unique, 某一列的值不能重复，唯一约束可以有null值，但是只能有一条记录为null

```mysql
-- 在创建表的时候添加唯一约束
CREATE TABLE stu (
	id INT,
	phone_number VARCHAR(20) UNIQUE
);
-- 删除唯一约束
ALTER TABLE stu DROP INDEX phone_number;
-- 添加唯一约束
ALTER TABLE stu MODIFY phone_number VARCHAR(20) UNIQUE;
```

#### 外键约束

> foreign key, 让表和表之间产生关系，从而保证数据的正确性(不可以随便更新外键的数据)

> 语法

```mysql
-- 在创建表的时候添加外键
creata table 表名 (
	.....
	CONSTRAINT 外键名称 FOREIGN KEY (外键列名称) REFERENCES 主表名称(主表列名称)
);
-- 删除外键
alter table 表名 drop FOREIGN KEY 外键名称;
-- 添加外键
alter table 表名 add CONSTRAINT 外键名称 FOREIGN KEY (外键列名称) REFERENCES 主表名称(主表列名称);
```

```mysql
-- 创建部门表(id,dep_name,dep_location) 
-- 一方，主表 
CREATE TABLE department( 
 id INT PRIMARY KEY AUTO_INCREMENT, 
 dep_name VARCHAR(20), 
 dep_location VARCHAR(20) 
);

DROP TABLE IF EXISTS employee;
-- 创建员工表(id,name,age,dep_id) 
-- 多方，从表 
CREATE TABLE employee( 
 id INT PRIMARY KEY AUTO_INCREMENT, 
 NAME VARCHAR(20), 
 age INT, 
 dep_id INT, -- 外键对应主表的主键
 CONSTRAINT emp_dept_fk FOREIGN KEY (dep_id) REFERENCES department(id)
);
 
-- 添加 2 个部门 
INSERT INTO department VALUES(NULL, '研发部','广州'),(NULL, '销售部', '深圳'); 
SELECT * FROM department; 
 
-- 添加员工,dep_id 表示员工所在的部门 
INSERT INTO employee (NAME, age, dep_id) VALUES ('张三', 20, 1); 
INSERT INTO employee (NAME, age, dep_id) VALUES ('李四', 21, 1); 
INSERT INTO employee (NAME, age, dep_id) VALUES ('王五', 20, 1); 
INSERT INTO employee (NAME, age, dep_id) VALUES ('老王', 20, 2); 
INSERT INTO employee (NAME, age, dep_id) VALUES ('大王', 22, 2); 
INSERT INTO employee (NAME, age, dep_id) VALUES ('小王', 18, 2); 
 
SELECT * FROM employee;
```

```mysql
-- 级联操作
alter table 表名 add CONSTRAINT 外键名称 FOREIGN KEY (外键列名称) REFERENCES 主表名称(主表列名称) ON UPDATE CASCADE ON DELETE CASCADE;

-- 级联更新
ON UPDATE CASCADE -- 更新department表后自动更新employee表
-- 级联删除
ON DELETE CASCADE -- 

-- 先删除在添加
ALTER TABLE employee DROP FOREIGN KEY emp_dept_fk;
ALTER TABLE employee ADD CONSTRAINT emp_dept_fk FOREIGN KEY (dep_id) REFERENCES department(id) ON UPDATE CASCADE;
```

### 数据库的设计

#### 多表之间的关系

##### 一对一

> 一对一(了解)，在实际的开发中应用不多.因为一对一可以创建成一张表

| 一对一的建表原则 | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| 外键唯一         | 主表的主键和从表的外键（唯一），形成主外键关系，外键唯一 UNIQUE |
| 外键是主键       | 主表的主键和从表的主键，形成主外键关系                       |

![image-20211215094043134](https://gitee.com/image_bed/image-bed1/raw/master/img/202112150940728.png)

##### 一对多

> 在多的一方创建外键，指向一的一方的主键

![image-20211215094225401](https://gitee.com/image_bed/image-bed1/raw/master/img/202112150942858.png)

> 案例：一个旅游线路分类中有多个旅游线路

```mysql
-- 创建旅游线路分类表 tab_category 
-- cid 旅游线路分类主键，自动增长 
-- cname 旅游线路分类名称非空，唯一，字符串 100 
CREATE TABLE tab_category ( 
  cid INT PRIMARY KEY AUTO_INCREMENT, 
  cname VARCHAR(100) NOT NULL UNIQUE 
);
 
-- 添加旅游线路分类数据： 
INSERT INTO tab_category (cname) VALUES ('周边游'), ('出境游'), ('国内游'), ('港澳游'); 
 
SELECT * FROM tab_category; 
 
-- 创建旅游线路表 tab_route 
/* 
rid 旅游线路主键，自动增长 
rname 旅游线路名称非空，唯一，字符串 100 
price 价格 
rdate 上架时间，日期类型 
cid 外键，所属分类 
*/ 
CREATE TABLE tab_route( 
  rid INT PRIMARY KEY AUTO_INCREMENT, 
  rname VARCHAR(100) NOT NULL UNIQUE, 
  price DOUBLE, 
  rdate DATE, 
  cid INT, 
  FOREIGN KEY (cid) REFERENCES tab_category(cid) 
);
 
-- 添加旅游线路数据 
INSERT INTO tab_route VALUES 
(NULL, '【厦门+鼓浪屿+南普陀寺+曾厝垵 高铁 3 天 惠贵团】尝味友鸭面线 住 1 晚鼓浪屿', 1499, '2018-01-27', 1), 
(NULL, '【浪漫桂林 阳朔西街高铁 3 天纯玩 高级团】城徽象鼻山 兴坪漓江 西山公园', 699, '2018-02-22', 3), 
(NULL, '【爆款￥1699 秒杀】泰国 曼谷 芭堤雅 金沙岛 杜拉拉水上市场 双飞六天【含送签费 泰风情 广州往返 特价团】', 1699, '2018-01-27', 2), 
(NULL, '【经典•狮航 ￥2399 秒杀】巴厘岛双飞五天 抵玩【广州往返 特价团】', 2399, '2017-12-23', 2), 
(NULL, '香港迪士尼乐园自由行 2 天【永东跨境巴士广东至迪士尼去程交通+迪士尼一日门票+香港如心海景酒店暨会议中心标准房 1 晚住宿】', 799, '2018-04-10', 4); 
 
SELECT * FROM tab_route;
```

![image-20211215095315112](https://gitee.com/image_bed/image-bed1/raw/master/img/202112150953282.png)	

##### 多对多

> 多对多关系实现需要借助第三张中间表。
>
> 中间表至少包含两个字段，这两个字段作为第三张表的外键，分别指向两张表的主键

> 案例：一个用户收藏多个线路，一个线路被多个用户收藏

![image-20211215095406470](https://gitee.com/image_bed/image-bed1/raw/master/img/202112150954151.png)

```mysql
/*  
创建用户表 tab_user
uid 用户主键，自增长
username 用户名长度 100，唯一，非空
password 密码长度 30，非空
name 真实姓名长度 100
birthday 生日
sex 性别，定长字符串 1
telephone 手机号，字符串 11 
email 邮箱，字符串长度 100
*/
CREATE TABLE tab_user ( 
  uid INT PRIMARY KEY AUTO_INCREMENT, 
  username VARCHAR(100) UNIQUE NOT NULL, 
  PASSWORD VARCHAR(30) NOT NULL, 
  NAME VARCHAR(100), 
  birthday DATE, 
  sex CHAR(1) DEFAULT '男', 
  telephone VARCHAR(11), 
  email VARCHAR(100) 
);
-- 添加用户数据 
INSERT INTO tab_user VALUES 
(NULL, 'cz110', 123456, '老王', '1977-07-07', '男', '13888888888', '66666@qq.com'), 
(NULL, 'cz119', 654321, '小王', '1999-09-09', '男', '13999999999', '99999@qq.com'); 
 
SELECT * FROM tab_user; 

/*  
创建收藏表 tab_favorite 
rid 旅游线路 id，外键 
date 收藏时间 
uid 用户 id，外键 
rid 和 uid 不能重复，设置复合主键，同一个用户不能收藏同一个线路两次 
*/ 
CREATE TABLE tab_favorite ( 
  rid INT, 
  DATE DATETIME, 
  uid INT, 
  -- 创建复合主键 
  PRIMARY KEY(rid,uid), 
  FOREIGN KEY (rid) REFERENCES tab_route(rid), 
  FOREIGN KEY(uid) REFERENCES tab_user(uid) 
);
-- 增加收藏表数据 
INSERT INTO tab_favorite VALUES 
(1, '2018-01-01', 1), -- 老王选择厦门 
(2, '2018-02-11', 1), -- 老王选择桂林 
(3, '2018-03-21', 1), -- 老王选择泰国 
(2, '2018-04-21', 2), -- 小王选择桂林 
(3, '2018-05-08', 2), -- 小王选择泰国 
(5, '2018-06-02', 2); -- 小王选择迪士尼 

SELECT * FROM tab_favorite;
```

![image-20211215100036811](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241037089.png)	

#### 数据库设计的范式

> 设计数据库时，需要遵循的一些规范
>
> 目前关系数据库有六种范式：第一范式（1NF）、第二范式（2NF）、第三范式（3NF）、巴斯-科德范式（BCNF）、第四范式(4NF）和第五范式（5NF，又称完美范式）。 满足最低要求的范式是第一范式（1NF）。在第一范式的基础上进一步满足更多规范要求的称为第二范式（2NF），其余范式以次类推。一般说来，数据库只需满足第三范式(3NF）就行了

##### [数据库设计三大范式 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/63146817)

[(6条消息) 函数依赖_平湖的博客-CSDN博客_函数依赖](https://blog.csdn.net/eahau/article/details/91127992)

- 第一范式（1NF）：每一列都是不可分割的原子数据项

- 第二范式（2NF）：在1NF的基础上，非主属性必须完全依赖于码（在1NF基础上消除非主属性对主码的部分函数依赖）

  - 函数依赖：A-->B,如果通过A属性(属性组)的值，可以确定唯一B属性的值。则称B依赖于A
    - 例如：学号-->姓名。  （学号，课程名称） --> 分数
  - 完全函数依赖：A-->B， 如果A是一个属性组，则B属性值的确定需要依赖于A属性组中所有的属性值。
    - 例如：（学号，课程名称） --> 分数
  - 部分函数依赖：A-->B， 如果A是一个属性组，则B属性值的确定只需要依赖于A属性组中某一些值即可。
    - 例如：（学号，课程名称） -- > 姓名
  - 传递函数依赖：A-->B, B -- >C . 如果通过A属性(属性组)的值，可以确定唯一B属性的值，在通过B属性（属性组）的值可以确定唯一C属性的值，则称 C 传递函数依赖于A
    - 例如：学号-->系名，系名-->系主任
  - 码：如果在一张表中，一个属性或属性组，被其他所有属性所完全依赖，则称这个属性(属性组)为该表的码
    - 例如：该表中码为：（学号，课程名称）
  - 主属性：码属性组中的所有属性

  - 非主属性：除了码属性组的属性

- 第三范式（3NF）：在2NF基础上，任何非主属性不依赖于其它非主属性（在2NF基础上消除传递依赖）

![image-20211215104655251](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151047278.png)	

![image-20211215104725881](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151047302.png)	

![image-20211215104929774](https://gitee.com/image_bed/image-bed1/raw/master/img/202112151049847.png)

### 数据库的备份和还原

#### 备份

```mysql
mysqldump -u用户名 -p密码 数据库名称 > 保存的路径
```

#### 还原

- 登陆数据库
- 创建数据库
- 使用数据库
- 执行文件：source 文件路径

### 多表查询

#### 查询语法

```mysql
select
	列名列表
from
	表名列表
where....
```

#### 准备数据

```mysql
# 创建部门表 
CREATE TABLE dept( 
  id INT PRIMARY KEY AUTO_INCREMENT, 
  NAME VARCHAR(20) 
);
 
INSERT INTO dept (NAME) VALUES ('开发部'),('市场部'),('财务部');   
# 创建员工表 
CREATE TABLE emp ( 
  id INT PRIMARY KEY AUTO_INCREMENT, 
  NAME VARCHAR(10), 
  gender CHAR(1),   -- 性别 
  salary DOUBLE,   -- 工资 
  join_date DATE,  -- 入职日期 
  dept_id INT, 
  FOREIGN KEY (dept_id) REFERENCES dept(id) -- 外键，关联部门表(部门表的主键) 
);
INSERT INTO emp(NAME,gender,salary,join_date,dept_id) VALUES('孙悟空','男',7200,'2013-02-24',1);
INSERT INTO emp(NAME,gender,salary,join_date,dept_id) VALUES('猪八戒','男',3600,'2010-12-02',2);
INSERT INTO emp(NAME,gender,salary,join_date,dept_id) VALUES('唐僧','男',9000,'2008-08-08',2); 
INSERT INTO emp(NAME,gender,salary,join_date,dept_id) VALUES('白骨精','女',5000,'2015-10-07',3); 
INSERT INTO emp(NAME,gender,salary,join_date,dept_id) VALUES('蜘蛛精','女',4500,'2011-03-14',1);
```

#### 笛卡尔积

> - 有两个集合A,B .取这两个集合的所有组成情况
> - 要完成多表查询，需要消除无用的数据

#### 多表查询的分类

- 内连接查询

  - 隐式内连接

  ```mysql
  -- 查询所有员工信息和对应的部门信息
  SELECT *
  FROM emp,
    dept
  WHERE emp.`dept_id` = dept.`id`;
  -- 查询员工表的名称，性别。部门表的名称
  SELECT
    emp.name,
    emp.gender,
    dept.name
  FROM emp,
    dept
  WHERE emp.`dept_id` = dept.`id`;
  -- 
  SELECT
    t1.name,
   -- 员工表的姓名
   t1.gender,
   -- 员工表的性别
   t2.name-- 部门表的名称
  FROM emp t1,
    dept t2
  WHERE t1.`dept_id` = t2.`id`;
  ```

  - 显式内连接

  ```mysql
  -- 语法
  select 字段列表 from 表名1 [inner] join 表名2 on 条件
  
  -- 查询
  SELECT * FROM emp INNER JOIN dept ON emp.`dept_id` = dept.`id`;	
  SELECT * FROM emp JOIN dept ON emp.`dept_id` = dept.`id`;	
  ```

- 外链接查询

  - 左外连接

  ```mysql
  -- 语法
  select 字段列表 from 表1 left [outer] join 表2 on 条件 -- 查询的是左表所有数据以及其交集部分
  -- 查询所有员工信息，如果员工有部门，则查询部门名称，没有部门，则不显示部门名称
  SELECT 	t1.*,t2.`name` FROM emp t1 LEFT JOIN dept t2 ON t1.`dept_id` = t2.`id`;
  ```

  - 右外连接

  ```mysql
  -- 语法
  select 字段列表 from 表1 right [outer] join 表2 on 条件； -- 查询的是右表所有数据以及其交集部分
  --
  SELECT 	* FROM dept t2 RIGHT JOIN emp t1 ON t1.`dept_id` = t2.`id`;
  ```

- 子查询

  - 查询中嵌套查询，称嵌套查询为子查询

  ```mysql
  -- 查询工资最高的员工信息
  -- 1 查询最高的工资是多少 9000
  SELECT MAX(salary) FROM emp;
  
  -- 2 查询员工信息，并且工资等于9000的
  SELECT * FROM emp WHERE emp.`salary` = 9000;
  
  -- 一条sql就完成这个操作。子查询
  SELECT * FROM emp WHERE emp.`salary` = (SELECT MAX(salary) FROM emp);
  ```

  - 子查询的结果是单行单列的

  ```mysql
  -- 查询员工工资小于平均工资的人
  SELECT * FROM emp WHERE emp.salary < (SELECT AVG(salary) FROM emp);
  ```

  - 子查询的结果是多行单列的

  ```mysql
  -- 查询'财务部'和'市场部'所有的员工信息
  SELECT id FROM dept WHERE NAME = '财务部' OR NAME = '市场部';
  SELECT * FROM emp WHERE dept_id = 3 OR dept_id = 2;
  -- 子查询
  SELECT * FROM emp WHERE dept_id IN (SELECT id FROM dept WHERE NAME = '财务部' OR NAME = '市场部');
  ```

  - 子查询的结果是多行多列的

  ```mysql
  -- 查询员工入职日期是2011-11-11日之后的员工信息和部门信息
  -- 子查询
  SELECT *
  FROM dept t1,
    (SELECT *
     FROM emp
     WHERE emp.`join_date` > '2011-11-11') t2
  WHERE t1.id = t2.dept_id;
  ```

### 事务

#### 事务的基本介绍

> 如果一个包含多个步骤的业务操作，被事务管理，那么这些操作要么同时成功，要么同时失败

##### 操作

- 开启事务：start transaction;
- 执行DML语句
- 回滚：rollback;
- 提交：commit;

#### MySQL数据库中事务默认自动提交

##### 事务提交的两种方式

- 自动提交：
  - mysql就是自动提交的
  - 一条DML(增删改)语句会自动提交一次事务。
- 手动提交：
  - Oracle 数据库默认是手动提交事务
  - 需要先开启事务，再提交

##### 修改事务的默认提交方式

- 查看事务的默认提交方式：`SELECT @@autocommit;` -- 1 代表自动提交  0 代表手动提交
- 修改默认提交方式： `set @@autocommit = 0;`(@@可以省略：`SET autocommit = 1;`)

#### 事务的四大特征

> 事务的四大特性 ACID

| 事务特性              | 含义                                                         |
| --------------------- | ------------------------------------------------------------ |
| 原子性（Atomicity）   | 每个事务都是一个整体，不可再拆分，事务中所有的 SQL 语句要么都执行成功，要么都失败 |
| 一致性（Consistency） | 事务在执行前数据库的状态与执行后数据库的状态保持一致。如：转账前2个人的个人的总金额是 2000，转账后 2 个人总金额也是 2000 |
| 隔离性（Isolation）   | 事务与事务之间不应该相互影响，执行时保持隔离的状态           |
| 持久性（Durability）  | 一旦事务执行成功，对数据库的修改是持久的。就算关机，也是保存下来的 |

#### 事务的隔离级别（了解）

> 多个事务之间隔离的，相互独立的。但是如果多个事务操作同一批数据，则会引发一些问题，设置不同的隔离级别就可以解决这些问题

##### 问题

| 并发访问的问题   | 含义                                                         |
| ---------------- | ------------------------------------------------------------ |
| 脏读             | 一个事务读取到了另外一个事务没有提交的数据                   |
| 不可重复读(虚读) | 一个事务中两次读取的内容不一致。要求的是一个事务中多次读取是一致的，这个事务是update时引发的问题 |
| 幻读             | 一个事务操作(DML)数据表中所有记录，另一个事务添加了一条数据，则第一个事务查询不到自己的修改 |

##### 隔离级别

> 隔离级别从小到大安全性越来越高，但是效率越来越低

| 级别 | 名字     | 隔离级别         | 脏读 | 不可重复读 | 幻读 | 数据库默认隔离几倍   |
| ---- | -------- | ---------------- | ---- | ---------- | ---- | -------------------- |
| 1    | 读未提交 | read uncommitted | 是✔  | 是✔        | 是✔  |                      |
| 2    | 读已提交 | read committed   | 否✖  | 是✔        | 是✔  | Oracle 和 SQL Server |
| 3    | 可重复读 | repeatable read  | 否✖  | 否✖        | 是✔  | Mysql                |
| 4    | 串行化   | serializable     | 否✖  | 否✖        | 否✖  |                      |

##### MySQL 事务隔离级别相关的命令

> 查询全局事务隔离级别

```mysql
select @@tx_isolation; -- REPEATABLE-READ
```

> 设置事务隔离级别，需要退出 MySQL 再重新登录才能看到隔离级别的变化

```mysql
set global transaction isolation level 级别字符串;
```

##### 脏读的演示

> 创建银行账户表作为演示需要

```mysql
-- 创建数据表 
CREATE TABLE account ( 
 id INT PRIMARY KEY AUTO_INCREMENT, 
 NAME VARCHAR(10), 
 balance DOUBLE 
); 
 
-- 添加数据 
INSERT INTO account (NAME, balance) VALUES ('张三', 1000), ('李四', 1000);
```

```mysql
-- 设置事务隔离级别为 read uncommitted
set global transaction isolation level read uncommitted;
-- 分别打开两个cmd窗口然后进行演示
-- 在A窗口执行更新语句然后并不提交
update account set balance=balance-500 where id=1; 
update account set balance=balance+500 where id=2;
-- 在B窗口执行查询，发现可以获取更新后的数据
select * from account;
```

![image-20211216141627874](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161416995.png)

##### read committed

```mysql
-- 设置事务隔离级别为 read committed
set global transaction isolation level read committed;
-- 分别打开两个cmd窗口然后进行演示
-- 在A窗口执行更新语句然后并不提交
update account set balance=balance-500 where id=1; 
update account set balance=balance+500 where id=2;
-- 在B窗口执行查询，发现数据还是原来的，但是等A窗口提交之后，B窗口在执行查询数据就变了(同一个事务中出现的了不同的查询结果)
select * from account;
```

![image-20211216142415388](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161424039.png)	

##### repeatable read

```mysql
-- 设置事务隔离级别为 repeatable read
set global transaction isolation level repeatable read;
-- 分别打开两个cmd窗口然后进行演示
-- 在A窗口执行更新语句然后并不提交
update account set balance=balance-500 where id=1; 
update account set balance=balance+500 where id=2;
-- 在B窗口执行查询，发现数据还是原来的，但是等A窗口提交之后，B窗口在执行查询数据依然和原来一样，除非提交原来的事务重新开启一个事务在查询数据才会变化
select * from account;
```

![image-20211216142950502](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161429365.png)

##### serializable

```mysql
-- 设置事务隔离级别为 serializable
set global transaction isolation level serializable
-- 分别打开两个cmd窗口然后进行演示
-- 在A窗口执行更新语句然后并不提交
update account set balance=balance-500 where id=1; 
update account set balance=balance+500 where id=2;
-- 在B窗口执行查询，发现A窗口不执行提交，查询则一直等待，提交后自动出现查询结果(有时间限制)：ERROR 1205 (HY000): Lock wait timeout exceeded; try restarting transaction
select * from account;
```

![image-20211216143257964](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161433947.png)

### DCL

> Data Control Language

#### 创建用户

> 语法

```mysql
CREATE USER '用户名'@'主机名' IDENTIFIED BY '密码密码';
```

> 关键字说明

| 关键字   | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| '用户名' | 将创建的用户名                                               |
| ‘主机名’ | 指定该用户在哪个主机上可以登陆，如果是本地用户可用 localhost，如果想让该用户可以从任意远程主机登陆，可以使用通配符% |
| ‘密码’   | 该用户的登陆密码，密码可以为空，如果为空则该用户可以不需要密码登陆服务器 |

> 具体操作

```mysql
-- 创建 user1 用户，只能在 localhost 这个服务器登录 mysql 服务器，密码为 123
CREATE USER 'user1'@'localhost' IDENTIFIED BY '123';
-- 创建 user2 用户可以在任何电脑上登录 mysql 服务器，密码为 123
CREATE USER 'user2'@'%' IDENTIFIED BY '123';
```

> 可以查询mysql数据库中的user表，密码进行了加密

> 创建完之后需要对用户进行授权否则该用户什么都不能做

![image-20211216150835869](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161508843.png)	

#### 用户授权

> 语法

```mysql
GRANT 权限 1,权限 2... ON 数据库名.表名 TO '用户名'@'主机名';
```

> 关键字说明

| 关键字            | 说明                                                         |
| ----------------- | ------------------------------------------------------------ |
| GRANT…ON…TO       | 授权关键字                                                   |
| 权限              | 授予用户的权限，如 CREATE、ALTER、SELECT、INSERT、UPDATE 等。如果要授予所有的权限则使用 ALL |
| 数据库名.表名     | 该用户可以操作哪个数据库的哪些表。如果要授予该用户对所有数据库和表的相应操作权限则可用*表示，如*.* |
| '用户名'@'主机名' | 给哪个用户授权，注：有 2 对单引号                            |

> 具体操作

```mysql
-- 给 user1 用户分配对 db1 这个数据库操作的权限：创建表，修改表，插入记录，更新记录，查询
GRANT CREATE, ALTER, INSERT, UPDATE, SELECT ON db1.* TO 'user1'@'localhost';
-- 给 user2 用户分配所有权限，对所有数据库的所有表
GRANT ALL ON *.* TO 'user2'@'%';
```

> 权限分配完成后就可以看到对应的数据库(以user1为例)

![image-20211216151400451](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161514048.png)	

#### 撤销授权

> 语法

```mysql
revoke 权限列表 on 数据库名.表名 from '用户名'@'主机名';
```

> 具体操作

```mysql
--  撤销 user1 用户对 db1 数据库所有表的操作的权限（重新查询数据库发现查询不到db1了）
REVOKE ALL ON db1.* FROM 'user1'@'localhost';
```

#### 查看权限

> 语法

```mysql
SHOW GRANTS FOR '用户名'@'主机名';
```

> 具体操作

```mysql
--  查看 user1 用户的权限
SHOW GRANTS FOR 'user1'@'localhost';
```

#### 删除用户

> 语法

```mysql
DROP USER '用户名'@'主机名';
```

> 具体操作

```mysql
--  删除 user2
DROP USER 'user2'@'%';
```

#### 修改管理员密码

> 语法

```mysql
mysqladmin -uroot -p password 新密码
```

> 具体操作

```mysql
1. 将 root 管理员的新密码改成 rootroot
2. 要求输入旧密码
3. 使用新密码登录
```

#### 修改普通用户密码

> 语法

```mysql
set password for '用户名'@'主机名' = '新密码';
```

> 具体操作

```mysql
-- 将'user1'@'localhost'的密码改成'666666'：
SET PASSWORD FOR 'user1'@'localhost' = '666666';
-- 使用新密码登录，老密码登录不了
```

#### 忘记管理员密码

```mysql
1. cmd(管理员打开) -- > net stop mysql 停止mysql服务
2. 使用无验证方式启动mysql服务： mysqld --skip-grant-tables
3. 打开新的cmd窗口,直接输入mysql命令，敲回车。就可以登录成功
4. use mysql;
5. update user set password = password('你的新密码') where user = 'root';
6. 关闭两个窗口
7. 打开任务管理器，手动结束mysqld.exe 的进程
8. 启动mysql服务：net start mysql
9. 使用新密码登录
```

## JDBC

> Java数据库连接，（Java Database Connectivity，简称JDBC）
>
> JDBC 规范定义接口，具体的实现由各大数据库厂商来实现。 
>
> JDBC 是 Java 访问数据库的标准规范，真正怎么操作数据库还需要具体的实现类，也就是数据库驱动。每个数据库厂商根据自家数据库的通信格式编写好自己数据库的驱动。所以我们只需要会调用 JDBC 接口中的方法即可，数据库驱动由数据库厂商提供

### 快速入门

> 导入驱动jar包 mysql-connector-java-5.1.27.jar
>
> - 复制mysql-connector-java-5.1.37-bin.jar到项目的libs目录下
>
> - 右键-->Add As Library

![image-20211216175954051](https://gitee.com/image_bed/image-bed1/raw/master/img/202112161800170.png)	

```java
package cn.itcast.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>
 * JDBC使用代码的简单实现
 * 异常都暂时抛出
 * </p>
 *
 * @author hyatt 2021/12/16 17:48
 * @version 1.0
 */
public class JdbcTest {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // 1. 导入驱动包
        // 2. 注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        // 3. 获取数据库连接对象
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "123456");
        // 4. 定义sql语句
        String sql = "update account set balance = 2000 where id = 1";
        // 5. 获取执行sql的对象
        Statement stmt = conn.createStatement();
        // 6. 执行sql
        int count = stmt.executeUpdate(sql);
        // 7. 处理结果
        System.out.println("更新：" + count + " 条");
        // 8. 释放资源
        stmt.close();
        conn.close();
    }
}
```

### 对象的详解

####  DriverManager：驱动管理对象

> 功能

- 注册驱动：告诉程序该使用哪一个数据库驱动jar

```java
static void registerDriver(Driver driver) :注册与给定的驱动程序 DriverManager 。 
写代码使用：  Class.forName("com.mysql.jdbc.Driver");
通过查看源码发现：在com.mysql.jdbc.Driver类中存在静态代码块
 static {
		try {
			java.sql.DriverManager.registerDriver(new Driver());
		} catch (SQLException E) {
			throw new RuntimeException("Can't register driver!");
		}
	}

注意：mysql5之后的驱动jar包可以省略注册驱动的步骤。
```

- 获取数据库连接

```java
* 方法：static Connection getConnection(String url, String user, String password) 
* 参数：
	* url：指定连接的路径
		* 语法：jdbc:mysql://ip地址(域名):端口号/数据库名称
		* 例子：jdbc:mysql://localhost:3306/db1
		* 细节：如果连接的是本机mysql服务器，并且mysql服务默认端口是3306，则url可以简写为：jdbc:mysql:///数据库名称 jdbc:mysql:///db1
	* user：用户名
	* password：密码 
```

#### Connection：数据库连接对象

> 功能

- 获取执行sql 的对象
  - Statement createStatement()
  - PreparedStatement prepareStatement(String sql) 
- 管理事务
  - 开启事务：setAutoCommit(boolean autoCommit) ：调用该方法设置参数为false，即开启事务
  - 提交事务：commit() 
  - 回滚事务：rollback() 

#### Statement：执行sql的对象

- boolean execute(String sql) ：可以执行任意的sql (了解 )
- int executeUpdate(String sql) ：执行DML（insert、update、delete）语句、DDL(create，alter、drop)语句
  - 返回值：影响的行数，可以通过这个影响的行数判断DML语句是否执行成功 返回值>0的则执行成功，反之，则失败。
- ResultSet executeQuery(String sql)  ：执行DQL（select)语句

```java
-- account表 添加一条记录
package cn.itcast.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>account 表插入一条记录</p>
 *
 * @author hyatt 2021/12/17 9:52
 * @version 1.0
 */
public class JdbcTest2 {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            // 1. 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 2. 获取连接
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "123456");
            // 3. sql语句
            String sql = "insert into account values(null, '" + "王五" + "', 1000)";
            // 4. 获取sql执行对象
            stmt = conn.createStatement();
            // 5. 执行sql
            int count = stmt.executeUpdate(sql);
            // 6. 输出结果
            if (count > 0) {
                System.out.println("数据插入成功");
            } else {
                System.out.println("数据插入失败");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源 避免空指针异常
            if(stmt !=null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn!=null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```

#### ResultSet：结果集对象,封装查询结果

- boolean next(): 游标向下移动一行，判断当前行是否是最后一行末尾(是否有数据)，如果是，则返回false，如果不是则返回true
- getXxx(参数):获取数据
  - Xxx：代表数据类型   如： int getInt() ,	String getString()
  - 参数
    - int：代表列的编号,从1开始   如： getString(1)
    - String：代表列名称。 如： getDouble("balance")

```java
package cn.itcast.jdbc;

import java.sql.*;

/**
 * <p>结果集的使用</p>
 *
 * @author hyatt 2021/12/17 10:16
 * @version 1.0
 */
public class JdbcTest3 {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            // 1. 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 2. 获取连接
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1","root","123456");
            // 3. sql查询
            String sql = "select * from account";
            // 4. 获取sql执行对象
            stmt  = conn.createStatement();
            // 5. 执行sql
            resultSet = stmt.executeQuery(sql);
            // 6. 输出结果
            while(resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString("name");
                int balance = resultSet.getInt("balance");
                System.out.println(id + "---" + name + "---" + balance);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源 避免空指针异常
            if(resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(stmt!=null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(conn !=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

```

#### PreparedStatement：执行sql的对象

> SQL注入问题：在拼接sql时，有一些sql的特殊关键字参与字符串的拼接。会造成安全性问题

```java
package cn.itcast.jdbc;

import java.sql.*;
import java.util.Scanner;

/**
 * <p>SQL的注入问题：在拼接sql时，有一些sql的特殊关键字参与字符串的拼接。会造成安全性问题</p>
 * <p>
 * 用户名：随便输入
 * 密码： 'a' or 1=1
 * 结果查询出来了，就出现了问题
 *
 * @author hyatt 2021/12/17 10:24
 * @version 1.0
 */
public class JdbcTest4 {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            // 1. 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 2. 获取连接
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "123456");
            // 3. sql
            Scanner scanner = new Scanner(System.in);
            System.out.println("输入用户名");
            String username = scanner.nextLine();
            System.out.println("输入用户密码");
            String password = scanner.nextLine();
            String sql = "select * from tab_user where 1=1 and username=" + username + " and password=" + password;
            System.out.println(sql);
            // 4. 获取sql执行对象
            stmt = conn.createStatement();
            // 5. 执行sql
            resultSet = stmt.executeQuery(sql);
            // 6. 输出结果
            while (resultSet.next()) {
                String db_username = resultSet.getString("username");
                String db_password = resultSet.getString("password");
                System.out.println(db_username + "---" + db_password);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // 7. 释放资源 避免空指针异常
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```

#####  解决sql注入问题：使用PreparedStatement对象来解决

> 预编译的SQL：参数使用?作为占位符
>
> 注意：后期都会使用PreparedStatement来完成增删改查的所有操作
>
> - 可以防止SQL注入
> - 效率更高

```java
package cn.itcast.jdbc;

import java.sql.*;

/**
 * <p>使用PreparedStatement对象: 解决SQL注入</p>
 *
 * @author hyatt 2021/12/17 14:16
 * @version 1.0
 */
public class JdbcTest5 {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        try {
            // 1. 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            // 2. 获取连接
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db1", "root", "123456");
            // 3. sql语句
            String sql = "select * from tab_user where 1=1 and username=? and password= ?";
            // 4. 获取sql执行对象
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, "abcd");
            pstmt.setString(2, "'a' or 1=1");
            // 5. 执行sql
            resultSet = pstmt.executeQuery();
            // 6. 输出结果
            while (resultSet.next()) {
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                System.out.println(username + "---" + password);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // 释放资源 防止空指针异常
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```

### 抽取JDBC工具类 ： JDBCUtils

```java
package cn.itcast.util;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.Properties;

/**
 * <p>JDBC工具类</p>
 *
 * @author hyatt 2021/12/17 14:26
 * @version 1.0
 */
public class JdbcUtils {

    private static String url;
    private static String username;
    private static String password;
    private static String driver;

    public static void main(String[] args) {
        System.out.println("---");
    }

    // 通过配置文件设置mysql的连接属性
    // 静态块：随着类的加载而加载只会执行一次
    static {
        Properties props = new Properties();
        //获取src路径下的文件的方式--->ClassLoader 类加载器
        ClassLoader classLoader = JdbcUtils.class.getClassLoader();
        URL resource = classLoader.getResource("profile/jdbc.properties");
        String path = resource.getPath();
        try {
            props.load(new FileReader(path));
            username = props.getProperty("username");
            password = props.getProperty("password");
            url = props.getProperty("url");
            driver = props.getProperty("driver");
            // 注册驱动
            Class.forName(driver);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取连接
     *
     * @return 数据库连接
     * @throws SQLException 获取数据库连接异常
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * 释放资源
     *
     * @param resultSet         结果集对象
     * @param preparedStatement PreparedStatement对象
     * @param connection        连接
     */
    public static void close(ResultSet resultSet, PreparedStatement preparedStatement, Connection connection) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

> 重新JdbcTest5

```java
package cn.itcast.jdbc;

import cn.itcast.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>使用JdbcUtils类 修改JdbcTest5</p>
 *
 * @author hyatt 2021/12/17 14:44
 * @version 1.0
 */
public class JdbcTest6 {
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        try {
            // 获取连接
            conn = JdbcUtils.getConnection();
            // 准备sql
            String sql = "select * from account";
            // 创建sql执行对象
            pstmt = conn.prepareStatement(sql);
            // 执行查询
            resultSet = pstmt.executeQuery();
            // 输出结果集
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int balance = resultSet.getInt("balance");
                System.out.println(id + "---" + name + "---" + balance);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            JdbcUtils.close(resultSet, pstmt, conn);
        }
    }
}
```

### JDBC控制事务

> 事务：一个包含多个步骤的业务操作。如果这个业务操作被事务管理，则这多个步骤要么同时成功，要么同时失败。

```java
package cn.itcast.jdbc;

import cn.itcast.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * <p>事务</p>
 *
 * @author hyatt 2021/12/17 14:57
 * @version 1.0
 */
public class JdbcTest7 {
    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement1 = null;
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();
            // 开启事务
            connection.setAutoCommit(false);
            //2.定义sql
            //2.1 张三 - 500
            String sql1 = "update account set balance = balance - ? where id = ?";
            //2.2 李四 + 500
            String sql2 = "update account set balance = balance + ? where id = ?";
            // 3. 创建sql执行对象
            preparedStatement = connection.prepareStatement(sql1);
            preparedStatement1 = connection.prepareStatement(sql2);
            // 4. 设置变量
            preparedStatement.setInt(1, 500);
            preparedStatement.setInt(2, 1);
            preparedStatement1.setInt(1, 500);
            preparedStatement1.setInt(2, 2);
            // 5. 执行sql
            preparedStatement.executeUpdate();
            // 6. 出现异常
            int i = 2 / 0;
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
    }
}
```

## JDBCTemplate

> 数据库连接池

### 概念

> 其实就是一个容器(集合), 存放数据库连接的容器

当系统初始化好之后容器被创建，容器会申请一些数据库连接对象，当用户来访问数据库时，从容器中获取连接对象，用户访问完后，再将连接对象归还给容器

### 好处

- 节约资源
- 用户访问更加搞笑

### 实现

> 标准接口：DataSource   javax.sql包下的

- 获取连接：getConnection()
- 归还连接：Connection.close()。如果连接对象Connection是从连接池中获取的，那么调用Connection.close()方法，则不会再关闭连接了。而是归还连接

> 一般我们不去实现它，有数据库厂商来实现

- C3P0：数据库连接池技术
- Druid：数据库连接池实现技术，由阿里巴巴提供的(最好的技术之一)

### C3P0

> 导包：c3p0-0.9.5.2.jar、mchange-commons-java-0.2.12.jar和

> 定义配置文件
>
> - 名称： c3p0.properties 或者 c3p0-config.xml
> - 路径：直接将文件放在src目录下即可。

> 使用：
>
> - 创建数据库连接池对象
> - 获取连接

```java
package cn.itcast.datasource.c3p0;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>C3P0配置属性的测试</p>
 *
 * @author hyatt 2021/12/20 14:01
 * @version 1.0
 */
public class C3P0Test2 {
    public static void main(String[] args) throws SQLException {
        // 1. 创建数据库连接池对象 默认获取默认配置
        DataSource dataSource = new ComboPooledDataSource();
        // 获取另外一个数据库的配置信息
        DataSource dataSource2 = new ComboPooledDataSource("otherc3p0");
        // 2. 获取连接
        // 验证是否只能获取10个连接,这里获取第11个时等待3秒报错
        for (int i = 0; i < 11; i++) {
            Connection connection = dataSource.getConnection();
            System.out.println(connection);
        }
    }
}
```

### Druid

> 数据库连接池实现技术，由阿里巴巴提供的

> 使用
>
> - 导入jar包 druid-1.0.9.jar
> - 定义配置文件：
>   - 是properties形式的
>   - 可以叫任意名称，可以放在任意目录下
> - 加载配置文件。Properties
> - 获取数据库连接池对象：通过工厂来来获取  DruidDataSourceFactory
> - 获取连接：getConnection

```java
package cn.itcast.datasource.utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * <p>使用DRUID数据库连接池的JDBC工具类</p>
 *
 * @author hyatt 2021/12/20 14:23
 * @version 1.0
 */
public class JDBCUtils {
    // 数据库连接池对象
    private static DataSource dataSource;

    // 静态代码块，加载配置文件
    static {
        Properties properties = new Properties();
        InputStream is = JDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");
        try {
            properties.load(is);
            properties.load(is);
            dataSource = DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据连接
     *
     * @return 数据连接
     * @throws SQLException 异常
     */
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * 关闭资源
     *
     * @param statement  操作sql对象
     * @param connection 数据库连接
     */
    public static void close(Statement statement, Connection connection) {
        close(null, statement, connection);
    }

    /**
     * 关闭资源
     *
     * @param resultSet  结果集
     * @param statement  操作sql对象
     * @param connection 数据库连接
     */
    public static void close(ResultSet resultSet, Statement statement, Connection connection) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                // 将连接释放发挥数据库连接池
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取数据库连接池
     *
     * @return 数据库连接池对象
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

}
```

> 通过上面的工具类操作数据库

```java
package cn.itcast.datasource.druid;

import cn.itcast.datasource.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * <p>使用新的JDBC工具类操作数据库</p>
 *
 * @author hyatt 2021/12/20 14:23
 * @version 1.0
 */
public class DruidTest2 {
    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // 1. 创建数据库连接对象
            connection = JDBCUtils.getConnection();
            // 2. sql
            String sql = "insert into account values(null, ?, ?)";
            // 3. 创建sql操作对象
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, "王五");
            preparedStatement.setDouble(2, 3000);
            // 4. 执行sql
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(preparedStatement, connection);
        }
    }
}
```

### Spring JDBC

> Spring框架对JDBC的简单封装。提供了一个JDBCTemplate对象简化JDBC的开发

#### 实现

- 导入jar包

![image-20211220145821038](https://gitee.com/image_bed/image-bed1/raw/master/img/202112201458241.png)	

- 创建JdbcTemplate对象。依赖于数据源DataSource
- 调用JdbcTemplate的方法来完成CRUD的操作
  - update():执行DML语句。增、删、改语句
  - queryForMap():查询结果将结果集封装为map集合，将列名作为key，将值作为value 将这条记录封装为一个map集合
    - 注意：这个方法查询的结果集长度只能是1
  - queryForList():查询结果将结果集封装为list集合
    - 注意：将每一条记录封装为一个Map集合，再将Map集合装载到List集合中
  - query():查询结果，将结果封装为JavaBean对象
    - query的参数：RowMapper
      - 一般我们使用BeanPropertyRowMapper实现类。可以完成数据到JavaBean的自动封装
      - new BeanPropertyRowMapper<类型>(类型.class)
  - queryForObject：查询结果，将结果封装为对象
    - 一般用于聚合函数的查询

```java
package cn.itcast.datasource.jdbctemplate;

import cn.itcast.datasource.domain.Emp;
import cn.itcast.datasource.utils.JDBCUtils;
import org.junit.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * JDBCTemplate使用:
 * 表为 emp表
 * 1. 修改1号数据的 salary 为 10000
 * 2. 添加一条记录
 * 3. 删除刚才添加的记录
 * 4. 查询id为1的记录，将其封装为Map集合
 * 5. 查询所有记录，将其封装为List
 * 6. 查询所有记录，将其封装为Emp对象的List集合
 * 7. 查询总记录数
 * </p>
 *
 * @author hyatt 2021/12/20 15:00
 * @version 1.0
 */
public class JDBCTemplateTest {

    private JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());

    /**
     * 1. 修改1号数据的 salary 为 10000
     */
    @Test
    public void test1() {
        String sql = "update emp set salary = 10000 where id = ?";
        int count = jdbcTemplate.update(sql, 1);
        System.out.println("更新成功：" + count);
    }

    /**
     * 2. 添加一条记录
     */
    @Test
    public void test2() {
        String sql = "insert into emp(id, name, gender) values(?,?,?)";
        int count = jdbcTemplate.update(sql, null, "郭靖", "男");
        System.out.println("插入成功：" + count);
    }

    /**
     * 3. 删除刚才添加的记录
     */
    @Test
    public void test3() {
        String sql = "delete from emp where id = ?";
        int count = jdbcTemplate.update(sql, 7);
        System.out.println("删除成功：" + count);
    }

    /**
     * 4. 查询id为1的记录，将其封装为Map集合
     * 这个方法查询的结果集长度只能是1
     */
    @Test
    public void test4() {
        String sql = "select * from emp where id = ?";
        Map<String, Object> stringObjectMap = jdbcTemplate.queryForMap(sql, 1);
        System.out.println(stringObjectMap);
    }

    /**
     * 5. 查询所有记录，将其封装为List
     */
    @Test
    public void test5() {
        String sql = "select * from emp";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        for (Map map : maps) {
            System.out.println(map);
        }
    }

    /**
     * 6. 查询所有记录，将其封装为Emp对象的List集合
     * 自定义
     */
    @Test
    public void test6() {
        String sql = "select * from emp";
        List<Emp> query = jdbcTemplate.query(sql, new RowMapper<Emp>() {
            @Override
            public Emp mapRow(ResultSet resultSet, int i) throws SQLException {
                Emp emp = new Emp();
                emp.setId(resultSet.getInt("id"));
                emp.setName(resultSet.getString("name"));
                emp.setGender(resultSet.getString("gender"));
                emp.setSalary(resultSet.getDouble("salary"));
                emp.setDept_id(resultSet.getInt("dept_id"));
                emp.setJoin_date(resultSet.getDate("join_date"));
                return emp;
            }
        });
        // 遍历
        for (Emp emp : query) {
            System.out.println(emp);
        }
    }

    /**
     * 6. 查询所有记录，将其封装为Emp对象的List集合
     *  使用自带的类
     */
    @Test
    public void test6_2() {
        String sql = "select * from emp";
        List<Emp> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Emp>(Emp.class));
        // 遍历
        for (Emp emp : query) {
            System.out.println(emp);
        }
    }

    /**
     * 7. 查询总记录数
     */
    @Test
    public void test7() {
        String sql = "select count(id) from emp";
        Long count = jdbcTemplate.queryForObject(sql, Long.class);
        System.out.println("总数：" + count);
    }
}
```

## HTML

> [HTML 参考手册_w3cschool](https://www.w3cschool.cn/htmltags/)

### JavaWeb

> 使用Java语言开发基于互联网的项目

### 软件架构

- C/S: Client/Server 客户端/服务器端
  - 在用户本地有一个客户端程序，在远程有一个服务器端程序
  - 如：QQ，迅雷...
  - 优点
    - 用户体验好
  - 缺点
    - 开发、安装，部署，维护 麻烦
- B/S: Browser/Server 浏览器/服务器端
  - 只需要一个浏览器，用户通过不同的网址(URL)，客户访问不同的服务器端程序
  - 优点
    - 开发、安装，部署，维护 简单
  - 缺点
    - 如果应用过大，用户的体验可能会受到影响
    - 对硬件要求过高

### B/S架构详解

#### 资源分类

##### 静态资源

> 使用静态网页开发技术发布的资源
>
> - HTML：用于搭建基础网页，展示页面的内容
> - CSS：用于美化页面，布局页面
> - JavaScript：控制页面的元素，让页面有一些动态的效果

特点：

- 所有用户访问，得到的结果是一样的
- 如：文本，图片，音频、视频, HTML,CSS,JavaScript
- 如果用户请求的是静态资源，那么服务器会直接将静态资源发送给浏览器。浏览器中内置了静态资源的解析引擎，可以展示静态资源

##### 动态资源

> 使用动态网页及时发布的资源

特点：

- 所有用户访问，得到的结果可能不一样。
- 如：jsp/servlet,php,asp...
- 如果用户请求的是动态资源，那么服务器会执行动态资源，转换为静态资源，再发送给浏览器

### HTML

> 是最基础的网页开发语言
>
> Hyper Text Markup Language 超文本标记语言
>
> - 超文本
>   - 超文本是用超链接的方法，将各种不同空间的文字信息组织在一起的网状文本.
> - 标记语言
>   - 由标签构成的语言。<标签名称> 如 html，xml
>   - 标记语言不是编程语言

#### 快速入门

##### 语法

- html文档后缀名 .html 或者 .htm
- 标签分类
  - 围堵标签：有开始标签和结束标签。如 <html> </html>
  - 自闭合标签：开始标签和结束标签在一起。如 <br/>
- 标签可以嵌套
- 在开始标签中可以定义属性。属性是由键值对构成，值需要用引号(单双都可)引起来
- html的标签不区分大小写，但是建议使用小写。

```html
<html>

<head>
    <title>title</title>
</head>

<body>
<FONT color='red'>Hello World</font><br/>

<font color='green'>Hello World</font>

</body>

</html>
```

#### 标签学习

##### 文件标签

> 构成html最基本的标签

- html:html文档的根标签
- head：头标签。用于指定html文档的一些属性。引入外部的资源
- title：标题标签。
- body：体标签
- `<!DOCTYPE html>`：html5中定义该文档是html文档

##### 文本标签：和文本有关的标签

```html
* 注释：<!-- 注释内容 -->
* <h1> to <h6>：标题标签
	* h1~h6:字体大小逐渐递减
* <p>：段落标签
* <br>：换行标签
* <hr>：展示一条水平线
	* 属性：
		* color：颜色
		* width：宽度
		* size：高度
		* align：对其方式
			* center：居中
			* left：左对齐
			* right：右对齐
* <b>：字体加粗
* <i>：字体斜体
* <font>:字体标签
* <center>:文本居中
	* 属性：
		* color：颜色
		* size：大小
		* face：字体
* 属性定义：
	* color：
		1. 英文单词：red,green,blue
		2. rgb(值1，值2，值3)：值的范围：0~255  如  rgb(0,0,255)
		3. #值1值2值3：值的范围：00~FF之间。如： #FF00FF
	* width：
		1. 数值：width='20' ,数值的单位，默认是 px(像素)
		2. 数值%：占比相对于父元素的比例
```

> 示例

```html
<!DOCTYPE html>
<html lang="ch">
<head>
    <meta charset="UTF-8">
    <title>黑马程序员简介</title>
</head>
<body>

<h1>
    公司简介
</h1>
<hr color="#ffd700">

<p>
    <font color="#FF0000">"中关村黑马程序员训练营"</font>是由<b><i>传智播客</i></b>联合中关村软件园、CSDN，
    并委托传智播客进行教学实施的软件开发高端培训机构，致力于服务各大软件企业，解决当前软件开发技术飞速发展， 而企业招不到优秀人才的困扰。
</p>

<p>
    目前，“中关村黑马程序员训练营”已成长为行业“学员质量好、课程内容深、企业满意”的移动开发高端训练基地， 并被评为中关村软件园重点扶持人才企业。
</p>

<p>

    黑马程序员的学员多为大学毕业后，有理想、有梦想，想从事IT行业，而没有环境和机遇改变自己命运的年轻人。 黑马程序员的学员筛选制度，远比现在90%以上的企业招聘流程更为严格。任何一名学员想成功入学“黑马程序员”，
    必须经历长达2个月的面试流程，这些流程中不仅包括严格的技术测试、自学能力测试，还包括性格测试、压力测试、 品德测试等等测试。毫不夸张地说，黑马程序员训练营所有学员都是精挑细选出来的。百里挑一的残酷筛选制度确
    保学员质量，并降低企业的用人风险。
    中关村黑马程序员训练营不仅着重培养学员的基础理论知识，更注重培养项目实施管理能力，并密切关注技术革新， 不断引入先进的技术，研发更新技术课程，确保学员进入企业后不仅能独立从事开发工作，更能给企业带来新的技术体系和理念。
</p>

<p>

    一直以来，黑马程序员以技术视角关注IT产业发展，以深度分享推进产业技术成长，致力于弘扬技术创新，倡导分享、 开放和协作，努力打造高质量的IT人才服务平台。
</p>

<hr color="#ffd700">

<font color="gray" size="2">
    <center>
        江苏传智播客教育科技股份有限公司<br>
        版权所有Copyright 2006-2018&copy;, All Rights Reserved 苏ICP备16007882
    </center>
</font>
</body>
</html>
```

##### 图片标签

- img：展示图片
  - src：指定图片的位置

```html
<!--展示一张图片 img-->

<img src="image/jingxuan_2.jpg" align="right" alt="古镇" width="500" height="500"/>

<!--
	相对路径
		* 以.开头的路径
			* ./：代表当前目录  ./image/1.jpg
			* ../:代表上一级目录
 -->

<img src="./image/jiangwai_1.jpg">

<img src="../image/jiangwai_1.jpg">
```

##### 列表标签

- 有序列表
  - ol
  - li
- 无序列表
  - ul
  - li

##### 链接标签

- a:定义一个超链接
  - href：指定访问资源的URL(统一资源定位符)
  - target：指定打开资源的方式
    - _self:默认值，在当前页面打开
    - _blank：在空白页面打开

```html
<!--超链接  a-->

<a href="http://www.itcast.cn">点我</a>
<br>

<a href="http://www.itcast.cn" target="_self">点我</a>
<br>
<a href="http://www.itcast.cn" target="_blank">点我</a>

<br>

<a href="./5_列表标签.html">列表标签</a><br>
<a href="mailto:itcast@itcast.cn">联系我们</a>

<br>
<a href="http://www.itcast.cn"><img src="image/jiangwai_1.jpg"></a>
```

##### div和span

- div:每一个div占满一整行。块级标签
- span：文本信息在一行展示，行内标签 内联标签

##### 语义化标签：html5中为了提高程序的可读性，提供了一些标签。

```html
<header>：页眉
<footer>：页脚
```

##### 表格标签

- table：定义表格
  - width：宽度
  - border：边框
  - cellpadding：定义内容和单元格的距离
  - cellspacing：定义单元格之间的距离。如果指定为0，则单元格的线会合为一条、
  - bgcolor：背景色
  - align：对齐方式
- tr：定义行
  - bgcolor：背景色
  - align：对齐方式
- td：定义单元格
  - colspan：合并列
  - rowspan：合并行
- th：定义表头单元格
- <caption>：表格标题
- <thead>：表示表格的头部分
- <tbody>：表示表格的体部分
- <tfoot>：表示表格的脚部分

##### 表单标签

> 用于采集用户输入的数据的。用于和服务器进行交互。

form：用于定义表单的。可以定义一个范围，范围代表采集用户数据的范围

- action：指定提交数据的URL

- method:指定提交方式(一共7种，2种比较常用)
  - get
    - 请求参数会在地址栏中显示。会封装到请求行中。
    - 请求参数大小是有限制的。
    - 不太安全。
  - post
    - 请求参数不会再地址栏中显示。会封装在请求体中
    - 请求参数的大小没有限制
    - 较为安全
  
- 表单项中的数据要想被提交：必须指定其name属性

- 表单项标签

  - input：可以通过type属性值，改变元素展示的样式

    - type属性
      - text：文本输入框，默认值
        - placeholder：指定输入框的提示信息，当输入框的内容发生变化，会自动清空提示信息	

      - password：密码输入框
      - radio:单选框
        - 要想让多个单选框实现单选的效果，则多个单选框的name属性值必须一样
        - 一般会给每一个单选框提供value属性，指定其被选中后提交的值
        - checked属性，可以指定默认值

      - checkbox：复选框
        - 一般会给每一个单选框提供value属性，指定其被选中后提交的值
        - checked属性，可以指定默认值

      - file：文件选择框
      - hidden：隐藏域，用于提交一些信息。
      - 按钮
        - submit：提交按钮。可以提交表单
        - button：普通按钮
        - image：图片提交按钮
          - src属性指定图片的路径

  - label：指定输入项的文字描述信息

    - label的for属性一般会和 input 的 id属性值 对应。如果对应了，则点击label区域，会让input输入框获取焦点。

    ```html
    <!DOCTYPE html>
    <html>
    <head> 
    <meta charset="utf-8"> 
    <title>菜鸟教程(runoob.com)</title> 
    </head>
    <body>
    
    <p>点击其中一个文本标签选中选项：</p>
    
    <form action="demo_form.php">
      <label for="male1">Male</label>
      <input type="radio" name="sex" id="male" value="male"><br>
      <label for="female">Female</label>
      <input type="radio" name="sex" id="female" value="female"><br><br>
      <input type="submit" value="提交">
    </form>
    
    </body>
    </html>
    ```

  - select: 下拉列表

    - 子元素：option，指定列表项

  - textarea：文本域

    - cols：指定列数，每一行有多少个字符
    - rows：默认多少行。

## CSS

> [CSS 教程_w3cschool](https://www.w3cschool.cn/css/)

> Cascading Style Sheets 层叠样式表
>
> - 层叠：多个样式可以作用在同一个html的元素上，同时生效

### 好处

- 功能强大
- 将内容展示和样式控制分离
  - 降低耦合度。解耦
  - 让分工协作更容易
  - 提高开发效率

### CSS的使用

#### 内联样式

> 在标签内使用style属性指定css代码

```html
<div style="color:red;">hello css</div>
```

#### 内部样式

> 在head标签内，定义style标签，style标签的标签体内容就是css代码

```html
<style>
	div{
		color:blue;
	}

</style>
<div>hello css</div>
```

#### 外部样式

> 1. 定义css资源文件
> 2. 在head标签内，定义link标签，引入外部的资源文件

```html
div{
		color:green;
	}
<link rel="stylesheet" href="css/a.css">
<div>hello css</div>
<div>hello css</div>
```

> - 1,2,3种方式 css作用范围越来越大
> - 1方式不常用，后期常用2,3
> - 第3种格式可以写为
>
> ```html
> <style>
> 	@import "css/a.css";
> </style>
> ```

#### CSS语法

##### 格式

```html
选择器 {
	属性名1:属性值1;
	属性名2:属性值2;
	...
}
```

#### 选择器:筛选具有相似特征的元素

##### 基础选择器

id选择器：选择具体的id属性值的元素.建议在一个html页面中id值唯一

- 语法：#id属性值 {}

元素选择器：选择具有相同标签名称的元素

- 语法： 标签名称 {}
- 注意：id选择器优先级高于元素选择器

类选择器：选择具有相同的class属性值的元素。

- 语法：.class属性值 {}
- 注意：id选择器优先级高于类选择器高于元素选择器

##### 扩展选择器

- 选择所有元素

  - 语法： *{}

- 并集选择器

  - 选择器1,选择器2{}
  - div, p {}

- 子选择器: 筛选选择器1元素下的选择器2元素

  - 语法：  选择器1 选择器2{}

- 父选择器：筛选选择器2的父元素选择器1

  - 语法：  选择器1 > 选择器2{}

- 属性选择器：选择元素名称，属性名=属性值的元素

  - 语法：  元素名称[属性名="属性值"]{}

- 伪类选择器：选择一些元素具有的状态

  - 语法： 元素:状态{}

  ```html
  <style>
  	a:link {
  		color: red;
  	}
  
  	a:hover {
  		color: green;
  	}
  	
  	a:active {
  		color: aqua;
  	}
  
  	a:visited {
  		color: green;
  	}
  </style>
  ```

#### 属性

- 字体、文本
  - font-size：字体大小
  - color：文本颜色
  - text-align：对其方式
  - line-height：行高 
- 背景
  - background：
- 边框
  - border：设置边框，符合属性
- 尺寸
  - width：宽度
  - height：高度
- 盒子模型：控制布局
  - margin：外边距
  - padding：内边距
    - 默认情况下内边距会影响整个盒子的大小
    -  box-sizing: border-box;  设置盒子的属性，让width和height就是最终盒子的大小
  - float：浮动
    - left
    - right

#### 实例

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        * {
            margin: 0px;
            padding: 0px;
            box-sizing: border-box;
        }

        body {
            background: url("img/register_bg.png") no-repeat center;
            padding-top: 25px;
        }

        .rg_layout {
            width: 900px;
            height: 500px;
            border: 8px solid #EEEEEE;
            /* 水平居中 */
            margin: auto;
            background-color: white;
        }

        .rg_left {
            float: left;
            margin: 15px;
        }

        .rg_left>p:first-child {
            font-size: 20px;
            color: #FFD026;
        }

        .rg_left>p:last-child {
            font-size: 20px;
            color: #A6A6A6;
        }

        .rg_center {
            float: left;
            padding-top: 10px;
        }

        .rg_right {
            float: right;
            margin: 15px;
        }

        .rg_right>p:first-child {
            font-size: 15px;
        }

        .rg_right p a {
            color: pink;
        }

        .td_left {
            width: 100px;
            height: 40px;
            text-align: right;
        }

        .td_right {
            padding-left: 50px;
        }

        #username,
        #password,
        #email,
        #name,
        #tel,
        #birthday,
        #checkcode {
            width: 251px;
            height: 32px;
            border: 1px solid #A6A6A6;
            border-radius: 5px;
            padding-left: 10px;
        }

        #checkcode{
            width: 110px;
        }

        #img_check {
            height: 32px;
            vertical-align: middle;
            margin-left: 5px;
        }

        #btn_sub {
            width: 150px;
            height: 40px;
            background-color: #FFD026;
            border: 1px solid #FFD026;
            margin-top: 20px;
        }
    </style>
</head>

<body>
    <div class="rg_layout">
        <div class="rg_left">
            <p>新用户注册</p>
            <p>USER REGISTER</p>
        </div>
        <div class="rg_center">
            <form action="#" method="get">
                <table>
                    <tr>
                        <td class="td_left"><label for="username">用户名</label></td>
                        <td class="td_right"><input type="text" name="username" id="username" placeholder="请输入用户名"></td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="password">密码</label></td>
                        <td class="td_right"><input type="password" name="password" id="password" placeholder="请输入密码">
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="email">Email</label></td>
                        <td class="td_right"><input type="email" name="email" id="email" placeholder="请输入Email"></td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="name">姓名</label></td>
                        <td class="td_right"><input type="text" name="name" id="name" placeholder="请输入姓名"></td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="tel">手机号</label></td>
                        <td class="td_right"><input type="text" name="tel" id="tel" placeholder="请输入手机号"></td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="gender">性别</label></td>
                        <td class="td_right">
                            <input type="radio" name="gender" id="male" value="男"> 男
                            <input type="radio" name="gender" id="female" value="女"> 女
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="birthday">出生日期</label></td>
                        <td class="td_right"><input type="date" name="birthday" id="birthday"></td>
                    </tr>
                    <tr>
                        <td class="td_left"><label for="checkcode">验证码</label></td>
                        <td class="td_right"><input type="password" name="checkcode" id="checkcode"
                                placeholder="请输入验证码"><img src="img/verify_code.jpg" id="img_check"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="注册" id="btn_sub"></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="rg_right">
            <p>已有账号?<a href="#">立即登陆</a></p>
        </div>
    </div>
</body>

</html>
```

## JavaScript

> [JavaScript 教程_w3cschool](https://www.w3cschool.cn/javascript/)

### 概念

> 一门客户端脚本语言,可以来增强用户和html页面的交互过程，可以来控制html元素，让页面有一些动态的效果，增强用户的体验。
>
> - 运行在客户端浏览器中的。每一个浏览器都有JavaScript的解析引擎
> - 脚本语言：不需要编译，直接就可以被浏览器解析执行了

### JavaScript发展史

1. 1992年，Nombase公司，开发出第一门客户端脚本语言，专门用于表单的校验。命名为 ： C--	，后来更名为：ScriptEase
2. 1995年，Netscape(网景)公司，开发了一门客户端脚本语言：LiveScript。后来，请来SUN公司的专家，修改LiveScript，命名为JavaScript
3. 1996年，微软抄袭JavaScript开发出JScript语言
4. 1997年，ECMA(欧洲计算机制造商协会)，制定出客户端脚本语言的标准：ECMAScript，就是统一了所有客户端脚本语言的编码方式。

> JavaScript = ECMAScript + JavaScript自己特有的东西(BOM+DOM)

### ECMAScript：客户端脚本语言的标准

#### 基本语法

##### 与html结合方式

```html
<!--
    与html结合方式：
        内部js
            定义<script>，标签体内容就是js代码
        外部js
            定义<script>，通过src属性引入外部的js文件
    
    注意：
        <script>可以定义在html页面的任何地方。但是定义的位置会影响执行顺序。
        <script>可以定义多个
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- 导入外部js -->
    <script src="js/source.js"></script>
</head>
<body>

    <script>
        // 属于内部js
        // js 代码
        alert("Hello World");
    </script>
</body>
</html>
```

> source.js文件内容

```javascript
alert("外部js");
```

##### 注释

```
单行注释：//注释内容
多行注释：/*注释内容*/
```

##### 数据类型

###### 原始数据类型(基本数据类型)

- number：数字。 整数/小数/NaN(not a number 一个不是数字的数字类型)
- string：字符串。 字符串  "abc" "a" 'abc'
- boolean: true和false
- null：一个对象为空的占位符
- undefined：未定义。如果一个变量没有给初始化值，则会被默认赋值为undefined

###### 引用数据类型：对象

##### 变量

> 一小块存储数据的内存空间

```
var 变量名 = 初始化值;
```

Java语言是强类型语言，而JavaScript是弱类型语言。

- 强类型：在开辟变量存储空间时，定义了空间将来存储的数据的数据类型。只能存储固定类型的数据
- 弱类型：在开辟变量存储空间时，不定义空间将来的存储数据类型，可以存放任意类型的数据。

 typeof运算符：获取变量的类型。

- null运算后得到的是object

##### 运算符

###### 一元运算符：只有一个运算数的运算符

> ++，-- ， +, - 

++ --: 自增(自减)

- ++(--) 在前，先自增(自减)，再运算
- ++(--) 在后，先运算，再自增(自减)

###### 算术运算符

```
+ - * / % ...
```

###### 赋值运算符

> = += -=

###### 比较运算符

```
> < >= <= == ===(全等于)	
```

- 类型相同：直接比较
  - 字符串：按照字典顺序比较。按位逐一比较，直到得出大小为止
- 类型不同：先进行类型转换，再比较
  - ===：全等于。在比较之前，先判断类型，如果类型不一样，则直接返回false

###### 逻辑运算符

> && || !

###### 三元运算符

> 表达式? 值1:值2;
>
> 判断表达式的值，如果是true则取值1，如果是false则取值2；

```javascript
var a = 3;
var b = 4;

var c = a > b ? 1:0;
```

> 在JS中，如果运算数不是运算符所要求的类型，那么js引擎会自动的将运算数进行类型转换

- 其他类型转number
  - string转number：按照字面值转换。如果字面值不是数字，则转为NaN（不是数字的数字）
  -  boolean转number：true转为1，false转为0
- 其他类型转boolean
  - number：0或NaN为假，其他为真
  - string：除了空字符串("")，其他都是true
  - null&undefined:都是false
  - 对象：所有对象都为true

##### 流程控制语句

```html
<!--
流程控制语句
    if .. else
    switch:
        和java不同的是switch语句可以接受任意的原始数据类型
        java：
            byte, short, int, char
            JDK5之后可以是枚举类型
            JDK7之后可以是字符串类型
    while
    do..while
    for
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JS流程控制语句</title>
    <script>
        var score = 90;
        if(score>=60){
            alert("及格");
        }

        switch(score) {
            case 90:
                alert("number");
                break;
            case "abc":
                alert("string");
            case true:
                alert("boolean");
            case null:
                alert("null");
            case undefined:
                alert("undefined");
        }

        var sum = 0;
        var num = 1;
        while(num <= 100) {
            sum += num;
            num ++;
        }
        alert(sum);

        sum = 0;
        for(var i = 1; i<= 100; i++) {
            sum +=i;
        }
        alert(sum);

        sum=0;
        num = 1;
        do{
            sum += num;
            num ++;
        } while(num<=100);
        alert(sum);
    </script>
</head>
<body>
    
</body>
</html>
```

##### JS特殊语法

-  语句以;结尾，如果一行只有一条语句则 ;可以省略 (不建议)
- 变量的定义使用var关键字，也可以不使用
  - 用： 定义的变量是局部变量
  - 不用：定义的变量是全局变量(不建议)

##### 练习

```html
<!--
九九乘法表
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        td {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <script>
        document.write("<table align='center'>");
        for (var i = 1; i <= 9; i++) {
            document.write('<tr>')
            for (var j = 1; j <= i; j++) {
                document.write('<td>');
                document.write(i + "*" + j + "=" + (i * j) + "&nbsp;&nbsp;&nbsp;");
                document.write('</td>');
            }
            document.write('</tr>');
            document.write("<br/>");
        }
        document.write("</table>");
    </script>
</body>

</html>
```

#### 基本对象

##### Function：函数(方法)对象

> [JavaScript 函数定义_w3cschool](https://www.w3cschool.cn/javascript/js-function-definition.html)

```html
<!--
function 对象：
    1. 创建
        1. var fun1 = new Function(形式参数列表,方法体);
        2. var fun2 = function (形式参数列表) {
                方法体
           }
        3. function fun3(形式参数列表) {
                方法体
           }
    2. 方法：
    3. 属性：
        1. length:代表形参的个数
    4. 特点：
        1. 方法定义是，形参的类型不用写,返回值类型也不写。
        2. 方法是一个对象，如果定义名称相同的方法，会覆盖
        3. 在JS中，方法的调用只与方法的名称有关，和参数列表无关
        4. 在方法声明中有一个隐藏的内置对象（数组），arguments,封装所有的实际参数
    5. 调用
        1. 方法名称(实际参数列表);
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>函数对象</title>
</head>

<body>
    <script>
        // 创建方式1
        var fun1 = new Function('a', 'b', 'return a+b');
        alert(fun1(1, 2)); // 3

        // 创建方式2
        var fun2 = function (a, b) {
            return a + b;
        }
        alert(fun2(2, 2)); // 4

        // 创建方式3
        function fun3(a, b) {
            return a + b;
        }
        alert(fun3(3, 2)); //5

        function fun4() {
            for(var i=0; i< arguments.length; i++) {
                console.log(arguments[i]);
            }
        }
        // 可以传递任意多的参数
        fun4(1,2,3,4,5);
    </script>
</body>

</html>
```

##### Array:数组对象

> [JavaScript Array对象_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-array.html)

```HTML
<!--
Array 对象：
    1. 创建:
        1. var arr1 = new Array(元素列表);
        2. var arr = new Array(默认长度);
        3. var arr = [元素列表];
    2. 方法：
        1. join(参数):将数组中的元素按照指定的分隔符拼接为字符串
        2. push()	向数组的末尾添加一个或更多元素，并返回新的长度。
    3. 属性：
        length:数组的长度
    4. 特点：
        1. JS中，数组元素的类型可变的。
        2. JS中，数组长度可变的。
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array对象</title>
</head>

<body>
    <script>
        // 创建方式1
        var arr1 = new Array(1,2,3);
        console.log(arr1);

        // 创建方式2
        var arr2 = new Array(5);
        console.log(arr2);
        // 创建方式3
        var arr3 = [1,2,3,'SUV',true];
        console.log(arr3);

        // 方法
        arr3.push("hello");
        console.log(arr3);

        console.log(arr3.join('--'));
    </script>
</body>

</html>
```

##### Boolean

> Boolean 对象用于转换一个不是 Boolean 类型的值转换为 Boolean 类型值 (true 或者false).
>
> [JavaScript Boolean 对象_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-boolean.html)

##### Date：日期对象

> [JavaScript Date：日期对象_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-date.html)

```html
<!--
Date 对象：
    1. 创建:
       1. var date = new Date();
    2. 方法：
        1. toLocaleString()：返回当前date对象对应的时间本地字符串格式
        2. getTime():获取毫秒值。返回当前如期对象描述的时间到1970年1月1日零点的毫秒值差
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date对象</title>
</head>

<body>
    <script>
      var date = new Date();

      console.log(date.toLocaleString()); // 2021/12/22 下午4:06:23
      console.log(date.getTime());
    </script>
</body>

</html>
```

##### Math：数学对象

> [JavaScript Math：数学对象_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-math.html)

```html
<!--
Math 对象：
    1. 创建:
       1. Math对象不用创建，直接使用。  Math.方法名();
    2. 方法：
        1. random():返回 0 ~ 1 之间的随机数。 含0不含1
        2. ceil(x)：对数进行上舍入
        3. floor(x)：对数进行下舍入
        4. round(x)：把数四舍五入为最接近的整数。 
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Math对象</title>
</head>

<body>
    <script>
      console.log(Math.PI);
      console.log(Math.random());
      console.log(Math.ceil(0.4)); // 1
      console.log(Math.floor(0.6)); // 0
      console.log(Math.round(0.4)); // 0
    </script>
</body>

</html>
```

##### Number

> Number 对象是原始数值的包装对象。
>
> [JavaScript Number_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-number.html)

```javascript
var num = new Number(value); // 如果一个参数值不能转换为一个数字将返回 NaN (非数字值)。
```

##### String

> String 对象用于处理文本（字符串）
>
> [JavaScript String 对象 方法_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-string.html)

```javascript
var txt = new String("string");
var txt = "string";
```

#####  RegExp：正则表达式对象

> 正则表达式：定义字符串的组成规则。
>
> [JavaScript RegExp_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-regexp.html)

```html
<!--
RegExp 对象：
    1. 创建:
       1. var reg = new RegExp("正则表达式");
       2. var reg = /正则表达式/
    2. 方法：
        1. test(参数):验证指定的字符串是否符合正则定义的规范	
    3. 注意：
        1. 开始和结束符号(不加会有问题)
            ^:开始
            $:结束
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RegExp对象</title>
</head>

<body>
    <script>
        var reg = new RegExp("^\\w{6,7}$");
        var reg2 = /^\w{6,12}$/;
        // 不加开始和结束符号
        var reg3 = /\w{6,7}/; // 6到7个字符
        var name = "zhangsan"; // 8个字符
        var flag = reg.test(name);
        var flag2 = reg2.test(name);
        var flag3 = reg3.test(name); // 应该为false，但是结果为true
        console.log(flag);
        console.log(flag2);
        console.log(flag3)
    </script>
</body>

</html>
```

##### Global

> [JavaScript 全局属性/函数_w3cschool](https://www.w3cschool.cn/jsref/jsref-obj-global.html)
>
> 全局对象，这个Global中封装的方法不需要对象就可以直接调用。  方法名();

```html
<!--
全局对象：
    1. 方法：
        1. encodeURI():url编码
        2. decodeURI():url解码

        3. encodeURIComponent():url编码,编码的字符更多
        4. decodeURIComponent():url解码

        5. parseInt():将字符串转为数字
            * 逐一判断每一个字符是否是数字，直到不是数字为止，将前边数字部分转为number
        6. isNaN():判断一个值是否是NaN
        7. eval():将 JavaScript 字符串，并把它作为脚本代码来执行。
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>全局对象</title>
</head>
<body>
    <script>
        var str = "https://www.baidu.com/s?wd=微信";
        var encodeStr = encodeURI(str);
        console.log("url编码：" + encodeStr);
        var decodeStr = decodeURI(encodeStr);
        console.log("url解码：" + decodeStr);

        // 
        var str = "https://www.baidu.com/s?wd=微信";
        var encodeStr = encodeURIComponent(str);
        console.log("url编码：" + encodeStr);
        var decodeStr = decodeURIComponent(encodeStr);
        console.log("url解码：" + decodeStr);

        var str = "123abc";
        console.log(parseInt(str)); // 123
        var str = "abc123abc";
        console.log(parseInt(str)); // NaN

        console.log(eval("2+2")); // 4
    </script>
</body>
</html>
```

## JavaScript高级

### Dom

> [JavaScript HTML DOM (w3school.com.cn)](https://www.w3school.com.cn/js/js_htmldom.asp)
>
> Document Object Model 文档对象模型
>
> 将标记语言文档的各个组成部分，封装为对象。可以使用这些对象，对标记语言文档进行CRUD的动态操作

#### W3C DOM 标准被分为 3 个不同的部分

* 核心 DOM - 针对任何结构化文档的标准模型
	* Document：文档对象
	* Element：元素对象
	* Attribute：属性对象
	* Text：文本对象
	* Comment:注释对象

	* Node：节点对象，其他5个的父对象
* XML DOM - 针对 XML 文档的标准模型
* HTML DOM - 针对 HTML 文档的标准模型

#### 核心DOM模型

##### Document：文档对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document对象</title>
</head>
<body>

<div id="div1">div1</div>
<div id="div2">div2</div>

<div id="div3">div3</div>

<div class="cls1">div4</div>
<div class="cls1">div5</div>

<input type="text" name="username">
</body>

<script>
    /**
     *  Document：文档对象
            1. 创建(获取)：在html dom模型中可以使用window对象来获取
                 1. window.document
                 2. document
            2. 方法：
                1. 获取Element对象：
                     1. getElementById()	： 根据id属性值获取元素对象。id属性值一般唯一
                     2. getElementsByTagName()：根据元素名称获取元素对象们。返回值是一个数组
                     3. getElementsByClassName():根据Class属性值获取元素对象们。返回值是一个数组
                     4. getElementsByName(): 根据name属性值获取元素对象们。返回值是一个数组
                2. 创建其他DOM对象：
                     createAttribute(name)
                     createComment()
                     createElement()
                     createTextNode()
            3. 属性
     */
    var div1 = document.getElementById("div1");
    alert(div1.innerHTML);

    var cls1 = document.getElementsByClassName("cls1");
    alert(cls1.length)

    var uname = document.getElementsByName("username");
    alert(uname.length)


</script>
</html>
```

##### Element：元素对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Element对象</title>
</head>
<body>
<a>点我试试</a>
<input type="button" id="set" value="设置属性">
<input type="button" id="remove" value="删除属性">
</body>
<script>
  /**
   * 1. 获取/创建：通过document来获取和创建
     2. 方法：
           1. removeAttribute()：删除属性
           2. setAttribute()：设置属性
   */
  var set = document.getElementById("set");
  var a_href = document.getElementsByTagName("a")[0];
  set.onclick = function() {
      a_href.setAttribute("href", "https://www.baidu.com");
  }

  var set = document.getElementById("remove");
  set.onclick = function() {
      a_href.removeAttribute("href");
  }

</script>

</html>
```

##### Node：节点对象，其他5个的父对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>12_Node对象</title>
    <style>
        div{

            border: 1px solid red;

        }
        #div1{
            width: 200px;
            height: 200px;
        }

        #div2{
            width: 100px;
            height: 100px;
        }


        #div3{
            width: 100px;
            height: 100px;
        }
    </style>
</head>
<body>
<div id="div1">
    <div id="div2">div2</div>
    div1
</div>
<a href="javascript:void(0);" id="del">删除子节点</a>
<a href="javascript:void(0);" id="add">添加子节点</a>
</body>
<script>
    /**
     * 1. 特点：所有dom对象都可以被认为是一个节点
       2. 方法：
            * CRUD dom树：
                 * appendChild()：向节点的子节点列表的结尾添加新的子节点。
                 * removeChild()	：删除（并返回）当前节点的指定子节点。
                 * replaceChild()：用新节点替换一个子节点。
       3. 属性：
             * parentNode 返回节点的父节点。
     */
        //1.获取超链接
    var element_a = document.getElementById("del");
    //2.绑定单击事件
    element_a.onclick = function(){
        var div1 = document.getElementById("div1");
        var div2 = document.getElementById("div2");
        div1.removeChild(div2);
    }

    //1.获取超链接
    var element_add = document.getElementById("add");
    //2.绑定单击事件
    element_add.onclick = function(){
        var div1 = document.getElementById("div1");
        //给div1添加子节点
        //创建div节点
        var div3 = document.createElement("div");
        div3.setAttribute("id","div3");

        div1.appendChild(div3);
    }


    /*
        超链接功能：
            1.可以被点击：样式
            2.点击后跳转到href指定的url

        需求：保留1功能，去掉2功能
        实现：href="javascript:void(0);"
     */

    var div2 = document.getElementById("div2");
    var div1 = div2.parentNode;
    alert(div1);
</script>
</html>
```

#### 练习：动态表格

> 注意表格应添加tbody元素和浏览器一致(有的浏览器会自动添加这个tbody)，防止添加节点或者删除节点报错

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>动态表格</title><style>
    table{
        border: 1px solid;
        margin: auto;
        width: 500px;
    }

    td,th{
        text-align: center;
        border: 1px solid;
    }
    div{
        text-align: center;
        margin: 50px;
    }
</style>

</head>
<body>
<div>
    <input type="text" id="id" placeholder="请输入编号">
    <input type="text" id="name" placeholder="请输入姓名">
    <input type="text" id="gender" placeholder="请输入性别">
    <input type="button" value="添加" id="btn_add">

</div>


<table>
    <caption>学生信息表</caption>
    <tbody>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>

    <tr>
        <td>1</td>
        <td>令狐冲</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td>任我行</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>

    <tr>
        <td>3</td>
        <td>岳不群</td>
        <td>?</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    </tbody>

</table>
</body>

<script>

    /**
     * 1. 获取tbody元素对象，以便删除子节点
     * 2. 获取改行的tr元素对象
     * 3. 删除
     * @param e
     */
    function delTr(e) {
        // 获取table元素对象，以便删除
        var tableNode = document.getElementsByTagName("tbody")[0];
        // 获取该行对象
        var del_tr = e.parentNode.parentNode;
        tableNode.removeChild(del_tr);
    }

    /**
     * 1. 获取输入框的值以便填充
     * 2. 获取tbody元素对象，以便填充
     * 3. 创建tr对象，以便填充
     *      1. 创建td对象
     *      2. 创建text对象
     *      3. 将text的值填充到td中
     *      4. 将td对象填充到新建的tr对象中
     *      5. 对所有新建的td重复上面的操作
     * 4. 删除列一样,所以放在最后添加
     * 4. 第四步完成后，一行创建成功，最后在将tr对象添加到tbody对象中
     */
    function add() {
        // 获取输入框的值
        var id_value = document.getElementById("id").value;
        var name_value = document.getElementById("name").value;
        var gender_value = document.getElementById("gender").value;

        // 获取table
        var tableNode = document.getElementsByTagName("tbody")[0];
        // 创建tr
        var add_tr = document.createElement("tr");
        // 创建id列
        // 创建td元素
        var td_id = document.createElement("td");
        // 创建text元素
        var text_id = document.createTextNode(id_value);
        // 将值填充到td元素中
        td_id.appendChild(text_id);
        // 创建name列
        // 创建td元素
        var td_name = document.createElement("td");
        // 创建text元素
        var text_name = document.createTextNode(name_value);
        // 将值填充到td元素中
        td_name.appendChild(text_name);
        // 创建gender列
        // 创建td元素
        var td_gender = document.createElement("td");
        // 创建text元素
        var text_gender = document.createTextNode(gender_value);
        // 将值填充到td元素中
        td_gender.appendChild(text_gender);

        // 将td填充到tr中
        add_tr.appendChild(td_id);
        add_tr.appendChild(td_name);
        add_tr.appendChild(td_gender);
        // 删除列一样,所以放在最后添加
        var td_a = document.createElement("td");
        var del_a = document.createElement("a");
        del_a.setAttribute("href", "javascript:void(0);");
        del_a.setAttribute("onclick", "delTr(this);");
        del_a.appendChild(document.createTextNode("删除"));
        td_a.appendChild(del_a);
        add_tr.appendChild(td_a);

        // 最后将tr填充到table中
        tableNode.appendChild(add_tr);
    }

    var addButton = document.getElementById("btn_add");
    addButton.onclick=add;


</script>

</html>
```

#### HTML Dom

> [JavaScript HTML DOM - 改变 HTML (w3school.com.cn)](https://www.w3school.com.cn/js/js_htmldom_html.asp)

- 标签体的设置和获取：innerHTML

> 使用innerHTML对上面的动态表格进行修改

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>动态表格</title>
    <style>
        table {
            border: 1px solid;
            margin: auto;
            width: 500px;
        }

        td, th {
            text-align: center;
            border: 1px solid;
        }

        div {
            text-align: center;
            margin: 50px;
        }
    </style>
</head>
<body>
<div>
    <input type="text" id="id" placeholder="请输入编号">
    <input type="text" id="name" placeholder="请输入姓名">
    <input type="text" id="gender" placeholder="请输入性别">
    <input type="button" value="添加" id="btn_add">

</div>
<table>
    <caption>学生信息表</caption>
    <tbody>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>
    <tr>
        <td>1</td>
        <td>令狐冲</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td>任我行</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td>岳不群</td>
        <td>?</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    </tbody>
</table>
</body>

<script>
    /**
     * 1. 获取tbody元素对象，以便删除子节点
     * 2. 获取改行的tr元素对象
     * 3. 删除
     * @param e
     */
    function delTr(e) {
        // 获取table元素对象，以便删除
        var tableNode = document.getElementsByTagName("tbody")[0];
        // 获取该行对象
        var del_tr = e.parentNode.parentNode;
        tableNode.removeChild(del_tr);
    }

    /**
     * 1. 获取输入框的值以便填充
     * 2. 获取tbody元素对象，以便填充
     * 3. 使用innerHTML添加
     */
    function add() {
        // 获取输入框的值
        var id_value = document.getElementById("id").value;
        var name_value = document.getElementById("name").value;
        var gender_value = document.getElementById("gender").value;

        // 获取table
        var tbodyNode = document.getElementsByTagName("tbody")[0];
        // 使用innerHTML添加
        tbodyNode.innerHTML += '<tr>\n' +
            '        <td>' + id_value + '</td>\n' +
            '        <td>' + name_value + '</td>\n' +
            '        <td>' + gender_value + '</td>\n' +
            '        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>\n' +
            '    </tr>';
    }
    // 添加点击事件
    var addButton = document.getElementById("btn_add");
    addButton.onclick = add;
</script>
</html>
```

- 使用html元素对象的属性

- 控制元素样式

  - 使用元素的style属性来设置

  ```javascript
  // 获取元素对象省略
  div1.style.border = "1px solid red";
  div1.style.width = "200px";
  //font-size--> fontSize
  div1.style.fontSize = "20px";
  ```

  - 提前定义好类选择器的样式，通过元素的className属性来设置其class属性值。

  ```html
  <style>
  .d1 {
  width: 200px
  height: 200px;
  }
  </style>
  
  div1.className = "d1";
  ```

### 事件

> [JavaScript HTML DOM 事件 (w3school.com.cn)](https://www.w3school.com.cn/js/js_htmldom_events.asp)
>
> 某些组件被执行了某些操作后，触发某些代码的执行。	

* 事件：某些操作。如： 单击，双击，键盘按下了，鼠标移动了
* 事件源：组件。如： 按钮 文本输入框...
* 监听器：代码。
* 注册监听：将事件，事件源，监听器结合在一起。 当事件源上发生了某个事件，则触发执行某个监听器代码。

#### 常见的事件

	1. 点击事件：
		1. onclick：单击事件
		2. ondblclick：双击事件
	2. 焦点事件
		1. onblur：失去焦点
		2. onfocus:元素获得焦点。
	
	3. 加载事件：
		1. onload：一张页面或一幅图像完成加载。
	
	4. 鼠标事件：
		1. onmousedown	鼠标按钮被按下。
		2. onmouseup	鼠标按键被松开。
		3. onmousemove	鼠标被移动。
		4. onmouseover	鼠标移到某元素之上。
		5. onmouseout	鼠标从某元素移开。
	5. 键盘事件：
		1. onkeydown	某个键盘按键被按下。	
		2. onkeyup		某个键盘按键被松开。
		3. onkeypress	某个键盘按键被按下并松开。
	
	6. 选择和改变
		1. onchange	域的内容被改变。
		2. onselect	文本被选中。
	
	7. 表单事件：
		1. onsubmit	确认按钮被点击。
		2. onreset	重置按钮被点击。

#### 绑定事件

- 直接在html标签上，指定事件的属性(操作)，属性值就是js代码
- 通过js获取元素对象，指定事件属性，设置一个函数

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>事件绑定</title>
</head>
<body>
    <img src="img/off.gif" alt="" onclick="this.src='img/on.gif'">
    <img src="img/off.gif" alt="" id="light" onclick="func();">
    <img src="img/off.gif" alt="" id="light2">
</body>

<script>
    var light = document.getElementById("light");
    function func() {
        light.src = "img/on.gif"
    }
    var light2 = document.getElementById("light2");
    light2.onclick = function() {
        light2.src = "img/on.gif"
    };
</script>

</html>
```

> 案例1：电灯开关

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>点灯开关</title>
</head>
<body>
    <img src="img/off.gif" alt="" id="light">
</body>
<script>
    // flag 为false，则为关闭，否则为打开
    var flag = false;
    var light = document.getElementById("light");
    light.onclick = function() {
        if(flag) {
            flag = false;
            light.src = "img/off.gif"
        } else {
            flag = true;
            light.src = "img/on.gif"
        }
    };
</script>
</html>
```

#### 练习：表格操作

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>表格操作</title>
    <style>
        table {
            border: 1px solid;
            width: 500px;
            margin-left: 30%;
        }

        td, th {
            text-align: center;
            border: 1px solid;
        }

        div {
            margin-top: 10px;
            margin-left: 30%;
        }

        .out {
            background-color: white;
        }

        .over {
            background-color: pink;
        }
    </style>

    <script>
        window.onload = function () {
            /**
             * 全选
             *      1. 为全选按钮添加点击事件
             *      2. 获取checkbox所有对象
             *      3. 循环所有checkbox对象将值改为已选中
             */
            document.getElementById("selectAll").onclick = function () {
                let cbs = document.getElementsByName("cb");
                for (let i = 0; i < cbs.length; i++) {
                    cbs[i].checked = true;
                }
            }

            /**
             * 全不选
             *      1. 为全不选按钮添加点击事件
             *      2. 获取checkbox所有对象
             *      3. 循环所有checkbox对象将值改为未选中
             */
            document.getElementById("unSelectAll").onclick = function () {
                let cbs = document.getElementsByName("cb");
                for (let i = 0; i < cbs.length; i++) {
                    cbs[i].checked = false;
                }
            }

            /**
             * 反选
             *      1. 为反选按钮添加点击事件
             *      2. 获取checkbox所有对象
             *      3. 循环所有checkbox对象将值取反
             */
            document.getElementById("selectRev").onclick = function () {
                let cbs = document.getElementsByName("cb");
                for (let i = 0; i < cbs.length; i++) {
                    cbs[i].checked = !cbs[i].checked;
                }
            }

            /**
             * 和第一个checkbox的值一致
             *      1. 为第一个checkbox添加点击事件
             *      2. 获取checkbox所有对象
             *      3. 循环所有checkbox对象和第一个checkbox的值一致
             */
            document.getElementById("firstCheckBox").onclick = function () {
                let cbs = document.getElementsByName("cb");
                for (let i = 0; i < cbs.length; i++) {
                    cbs[i].checked = this.checked;
                }
            }

            /**
             * 鼠标移动到行上和移走
             *      1. 获取所有的行对象
             *      2. 为所有的行对象添加事件
             */
            let trs = document.getElementsByTagName("tr");
            for(let i=0;i<trs.length;i++){
                trs[i].onmouseover = function (event) {
                    this.className = "over";
                };

                trs[i].onmouseout = function (event) {
                    this.className = "out";
                };
            }
        }
    </script>

</head>
<body>
<table>
    <caption>学生信息表</caption>
    <tbody>
    <tr>
        <th><input type="checkbox" name="cb" id="firstCheckBox"></th>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>

    <tr>
        <td><input type="checkbox" name="cb"></td>
        <td>1</td>
        <td>令狐冲</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    <tr>
        <td><input type="checkbox" name="cb"></td>
        <td>2</td>
        <td>任我行</td>
        <td>男</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>

    <tr>
        <td><input type="checkbox" name="cb"></td>
        <td>3</td>
        <td>岳不群</td>
        <td>?</td>
        <td><a href="javascript:void(0);" onclick="delTr(this);">删除</a></td>
    </tr>
    </tbody>
</table>
<div>
    <input type="button" id="selectAll" value="全选">
    <input type="button" id="unSelectAll" value="全不选">
    <input type="button" id="selectRev" value="反选">
</div>
</body>
</html>
```

#### 练习：表单验证

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>注册页面</title>
    <style>
        * {
            margin: 0px;
            padding: 0px;
            box-sizing: border-box;
        }

        body {
            background: url("img/register_bg.png") no-repeat center;
            padding-top: 25px;
        }

        .rg_layout {
            width: 900px;
            height: 500px;
            border: 8px solid #EEEEEE;
            background-color: white;
            /*让div水平居中*/
            margin: auto;
        }

        .rg_left {
            /*border: 1px solid red;*/
            float: left;
            margin: 15px;
        }

        .rg_left > p:first-child {
            color: #FFD026;
            font-size: 20px;
        }

        .rg_left > p:last-child {
            color: #A6A6A6;
            font-size: 20px;

        }


        .rg_center {
            float: left;
            /* border: 1px solid red;*/

        }

        .rg_right {
            /*border: 1px solid red;*/
            float: right;
            margin: 15px;
        }

        .rg_right > p:first-child {
            font-size: 15px;

        }

        .rg_right p a {
            color: pink;
        }

        .td_left {
            width: 100px;
            text-align: right;
            height: 45px;
        }

        .td_right {
            padding-left: 50px;
        }

        #username, #password, #email, #name, #tel, #birthday, #checkcode {
            width: 251px;
            height: 32px;
            border: 1px solid #A6A6A6;
            /*设置边框圆角*/
            border-radius: 5px;
            padding-left: 10px;
        }

        #checkcode {
            width: 110px;
        }

        #img_check {
            height: 32px;
            vertical-align: middle;
        }

        #btn_sub {
            width: 150px;
            height: 40px;
            background-color: #FFD026;
            border: 1px solid #FFD026;
        }

        .error {
            color: red;
        }

        #td_sub {
            padding-left: 150px;
        }

    </style>
    <script>
        window.onload = function () {
            // 为用户名输入框添加事件
            document.getElementById("username").onblur = checkUserName;
            document.getElementById("password").onblur = checkPassword;
        }

        /**
         * 校验用户名
         */
        function checkUserName() {
            // 正则表达式：6-12个字符
            let reg = /^\w{6,12}$/;
            // 获取用户名
            let username = document.getElementById("username").value;
            let flag = reg.test(username);
            // 获取提示信息对象
            let s_username = document.getElementById("s_username");
            if (flag) {
                // 校验通过
                s_username.innerHTML = "<img width='35' height='25' src='img/gou.png'/>";
            } else {
                s_username.innerHTML = "用户名格式有误";
            }
        }

        /**
         * 校验密码
         */
        function checkPassword() {
           // 获取密码
            let password = document.getElementById("password").value;
            let s_password = document.getElementById("s_password");
            if (password==="admin") {
                // 校验通过
                s_password.innerHTML = "<img width='35' height='25' src='img/gou.png'/>";
            } else {
                s_password.innerHTML = "密码有误";
            }
        }
    </script>
</head>
<body>

<div class="rg_layout">
    <div class="rg_left">
        <p>新用户注册</p>
        <p>USER REGISTER</p>

    </div>

    <div class="rg_center">
        <div class="rg_form">
            <!--定义表单 form-->
            <form action="#" id="form" method="get">
                <table>
                    <tr>
                        <td class="td_left"><label for="username">用户名</label></td>
                        <td class="td_right">
                            <input type="text" name="username" id="username" placeholder="请输入用户名">
                            <span id="s_username" class="error"></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="password">密码</label></td>
                        <td class="td_right">
                            <input type="password" name="password" id="password" placeholder="请输入密码">
                            <span id="s_password" class="error"></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="email">Email</label></td>
                        <td class="td_right"><input type="email" name="email" id="email" placeholder="请输入邮箱"></td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="name">姓名</label></td>
                        <td class="td_right"><input type="text" name="name" id="name" placeholder="请输入姓名"></td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="tel">手机号</label></td>
                        <td class="td_right"><input type="text" name="tel" id="tel" placeholder="请输入手机号"></td>
                    </tr>

                    <tr>
                        <td class="td_left"><label>性别</label></td>
                        <td class="td_right">
                            <input type="radio" name="gender" value="male" checked> 男
                            <input type="radio" name="gender" value="female"> 女
                        </td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="birthday">出生日期</label></td>
                        <td class="td_right"><input type="date" name="birthday" id="birthday" placeholder="请输入出生日期">
                        </td>
                    </tr>

                    <tr>
                        <td class="td_left"><label for="checkcode">验证码</label></td>
                        <td class="td_right"><input type="text" name="checkcode" id="checkcode" placeholder="请输入验证码">
                            <img id="img_check" src="img/verify_code.jpg">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="td_sub"><input type="submit" id="btn_sub" value="注册"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="rg_right">
        <p>已有账号?<a href="#">立即登录</a></p>
    </div>

</div>
</body>
</html>
```

### Bom

> Browser Object Model 浏览器对象模型
>
> [JavaScript Window - 浏览器对象模型 (w3school.com.cn)](https://www.w3school.com.cn/js/js_window.asp)

#### 组成

* Window：窗口对象
* Navigator：浏览器对象
* Screen：显示器屏幕对象
* History：历史记录对象
* Location：地址栏对象

#### Window对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>window对象</title>
</head>
<body>
<input type="button" value="打开一个新窗口" id="open">
<input type="button" value="关闭新窗口" id="close">
</body>

<script>
    /**
     *  1. 创建
        2. 方法
            1. 与弹出框有关的方法：
                 alert()    显示带有一段消息和一个确认按钮的警告框。
                 confirm()    显示带有一段消息以及确认按钮和取消按钮的对话框。
                     * 如果用户点击确定按钮，则方法返回true
                     * 如果用户点击取消按钮，则方法返回false
                 prompt()    显示可提示用户输入的对话框。
                    * 返回值：获取用户输入的值
            2. 与打开关闭有关的方法：
                close()    关闭浏览器窗口。
                    * 谁调用我 ，我关谁
                open()    打开一个新的浏览器窗口
                    * 返回新的Window对象
            3. 与定时器有关的方法
                setTimeout()    在指定的毫秒数后调用函数或计算表达式。
                     * 参数：
                         1. js代码或者方法对象
                         2. 毫秒值
                     * 返回值：唯一标识，用于取消定时器
                clearTimeout()    取消由 setTimeout() 方法设置的 timeout。

                setInterval()    按照指定的周期（以毫秒计）来调用函数或计算表达式。
                clearInterval()    取消由 setInterval() 设置的 timeout。

        3. 属性：
            1. 获取其他BOM对象：
                 history
                 location
                 Navigator
                 Screen:
            2. 获取DOM对象
                 document
        4. 特点
             * Window对象不需要创建可以直接使用 window使用。 window.方法名();
             * window引用可以省略。  方法名();
     */
    /**
     * 与弹出框有关的方法
     */
    // alert("Hello Window");
    // window.alert("window对象");
    // var flag = confirm("确认取消吗");
    // alert(flag);
    // var username = prompt("请输入用户名", "admin");
    // alert(username)

    /**
     * 与打开关闭有关的方法
     */
    // var openButton = document.getElementById("open");
    // var newWindow;
    // openButton.onclick = function() {
    //     newWindow = open("https://www.baidu.com");
    // }
    // var closeButton = document.getElementById("close");
    // closeButton.onclick = function () {
    //     newWindow.close();
    // }

    /**
     * 与定时器有关的方法
     */
    function boom() {
        alert("bom~~~");
    }
    // setTimeout("boom();", 3000);
    setInterval("boom();", 3000);
</script>

</html>
```

#### Location对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Location对象</title>
</head>
<body>

</body>
<script>
    /**
     * 1. 创建(获取)：
             1. window.location
             2. location
     2. 方法：
            * reload()	重新加载当前文档。刷新
     3. 属性
            * href	设置或返回完整的 URL。
     */

    /**
     * 创建(获取)
     */
    // var h1 = window.location;
    // alert(h1 === location);

    /**
     * reload()
     */
    // alert("reload");
    // setInterval("location.reload();", 3000);
    setTimeout("location.href='https://www.baidu.com'", 3000);
</script>
</html>
```

#### History对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>History对象</title>
</head>
<body>
<a href="03_案例1_电灯开关.html">电灯开关</a>
<input type="button" value="前进" id="button">
</body>
<script>
    /**
     *    1. 创建(获取)：
             1. window.history
             2. history

          2. 方法：
             * back()	加载 history 列表中的前一个 URL。
             * forward()	加载 history 列表中的下一个 URL。
             * go(参数)	加载 history 列表中的某个具体页面。
                 * 参数：
                 * 正数：前进几个历史记录
                 * 负数：后退几个历史记录
          3. 属性：
             * length	返回当前窗口历史列表中的 URL 数量。
     */
    var button = document.getElementById("button");
    button.onclick = function () {
        // history.forward();
        history.go(1);
    }
</script>
</html>
```

## BootStrap

> 一个前端开发的框架，Bootstrap，来自 Twitter，是目前很受欢迎的前端框架。Bootstrap 是基于 HTML、CSS、JavaScript 的，它简洁灵活，使得 Web 开发更加快捷。
>
> [Bootstrap中文网 (bootcss.com)](https://www.bootcss.com/)

### 好处

- 定义了很多的css样式和js插件。我们开发人员直接可以使用这些样式和插件得到丰富的页面效果。
- 响应式布局：
  - 同一套页面可以兼容不同分辨率的设备

### 快速入门

> 1. 下载Bootstrap
> 2.  在项目中将这三个文件夹复制
> 3. 创建html页面，引入必要的资源文件

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hello World</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="js/jquery.min.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="js/bootstrap.min.js"></script>

</head>

<body>
    <h1>Hello World</h1>
</body>

</html>
```

### 响应式布局

* 同一套页面可以兼容不同分辨率的设备。
* 实现：依赖于栅格系统：将一行平均分成12个格子，可以指定元素占几个格子
* 步骤：
	1. 定义容器。相当于之前的table、
		* 容器分类：
			1. container：两边留白
			2. container-fluid：每一种设备都是100%宽度
	2. 定义行。相当于之前的tr   样式：row
	3. 定义元素。指定该元素在不同的设备上，所占的格子数目。样式：col-设备代号-格子数目
		* 设备代号：
			1. xs：超小屏幕 手机 (<768px)：col-xs-12
			2. sm：小屏幕 平板 (≥768px)
			3. md：中等屏幕 桌面显示器 (≥992px)
			4. lg：大屏幕 大桌面显示器 (≥1200px)

	* 注意：
		1. 一行中如果格子数目超过12，则超出部分自动换行。
		2. 栅格类属性可以向上兼容。栅格类适用于与屏幕宽度大于或等于分界点大小的设备。
		3. 如果真实设备宽度小于了设置栅格类属性的设备代码的最小值，会一个元素沾满一整行。

### CSS样式和JS插件

> 这边只是练习了几个，具体的请看bootstrap官方文档

1. 全局CSS样式：
	* 按钮：class="btn btn-default"
	* 图片：
		*  class="img-responsive"：图片在任意尺寸都占100%
		*  图片形状
			*  <img src="..." alt="..." class="img-rounded">：方形
			*  <img src="..." alt="..." class="img-circle"> ： 圆形
			*  <img src="..." alt="..." class="img-thumbnail"> ：相框
	* 表格
		* table
		* table-bordered
		* table-hover
	* 表单
		* 给表单项添加：class="form-control" 
2. 组件：
	* 导航条
	* 分页条
3. 插件：
	* 轮播图

### 练习

```html
<!--
 * @Author: Hyatt
 * @Date: 2022-01-07 16:55:50
 * @LastEditors: Hyatt
 * @LastEditTime: 2022-01-11 11:15:06
 * @Description: 
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>练习</title>

    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- 引入自定义的css文件 -->
    <link rel="stylesheet" href="css/my-bootstrap.css">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="js/jquery-3.2.1.min.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="js/bootstrap.min.js"></script>

    <style>
        .company {
            background-color: orange;
            font-size: 15px;
            height: 40px;
            text-align: center;
            line-height: 40px;
        }

        .jx {
            border-bottom: 2px solid #ffc900;
            padding: 5px;
            line-height: 30px;
            font-size: 15px;
        }
        
        .padding-top {
            padding-top: 5px;
        }

    </style>

</head>

<body>
    <!-- 页面部分 -->
    <header class="container-fluid">
        <div class="row">
            <img src="img/top_banner.jpg" alt="" class="img-responsive" style="width:100%">
        </div>
        <div class="row">
            <div class="col-sm-4">
                <img src="img/logo.jpg" alt="" class="img-responsive center-block">
            </div>
            <div class="col-sm-4">
                <div class="input-group" style="margin-top: 10px;">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="background-color: orange;">搜索</button>
                    </span>
                </div>
            </div>
            <div class="col-sm-4">
                <img src="img/hotel_tel.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <div class="row">
            <nav class="navbar navbar-default">
                <div class="container-fluid" style="font-size: 18px;">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">传智播客</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">JavaSE <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                            <li><a href="#">Android</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/banner_1.jpg" class="img-responsive center-block" style="width: 100%;">
                    </div>
                    <div class="item">
                        <img src="img/banner_2.jpg" class="img-responsive center-block" style="width: 100%;">
                    </div>
                    <div class="item">
                        <img src="img/banner_3.jpg" class="img-responsive center-block" style="width: 100%;">
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </header>
    <!-- 主体部分 -->
    <div class="container">
        <div class="row jx">
            <img src="img/icon_5.jpg" alt="" class="img-responsive" style="float: left;">
            <span>黑马精选</span>
        </div>
        <div class="row padding-top">
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="img/jiangxuan_1.jpg" class="img-responsive">
                    <div class="caption">
                        <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                        <font color="red">&yen; 699</font>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="img/jiangxuan_2.jpg" class="img-responsive">
                    <div class="caption">
                        <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                        <font color="red">&yen; 699</font>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="img/jiangxuan_3.jpg" class="img-responsive">
                    <div class="caption">
                        <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                        <font color="red">&yen; 699</font>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="img/jiangxuan_4.jpg" class="img-responsive">
                    <div class="caption">
                        <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                        <font color="red">&yen; 699</font>
                    </div>
                </div>
            </div>
        </div>
        <div class="row jx">
            <img src="img/icon_6.jpg" alt="" class="img-responsive" style="float: left;">
            <span>国内游</span>
        </div>
        <div class="row padding-top">
            <div class="col-md-4">
                <img src="img/guonei_1.jpg" style="padding-top: 10px;">                
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_2.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_1.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_5.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_1.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_3.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img src="img/jiangxuan_2.jpg" class="img-responsive">
                            <div class="caption">
                                <p>上海直飞三亚5天4晚自由行(春节预售+亲子/蜜月/休闲游首选+豪华酒店任选+接送机)</p>
                                <font color="red">&yen; 699</font>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
        </div>
    </div>
    <!-- 页脚部分 -->
    <footer class="container-fluid padding-top">
        <div class="row">
            <img src="img/footer_service.png" alt="" class="img-responsive" style="width: 100%;">
        </div>
        <div class="row company">
            江苏传智播客教育科技股份有限公司 版权所有Copyright 2006-2018, All Rights Reserved 苏ICP备16007882
        </div>
    </footer>
</body>

</html>
```

### 问题

#### 轮播图阴影

> bootstrap框架中轮播轮播图两边默认的是有些阴影

![image-20220110105240934](https://gitee.com/image_bed/image-bed1/raw/master/img/202201101052159.png)	

> 新建css文件，然后引入

```
<link rel="stylesheet" href="css/my-bootstrap.css">
```

> css/my-bootstrap.css

```css
/* 解决bootstrap轮播图阴影问题 */
.carousel-control.left {

    background-image: none;
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
}

.carousel-control.right {
    left: auto;
    right: 0;

    background-image: none;
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
}

/* 解决bootstrap轮播图阴影问题 */
```

## XML

> Extensible Markup Language 可扩展标记语言

* 可扩展：标签都是自定义的。 <user>  <student>
* 功能
	* 存储数据
		- 配置文件
		- 在网络中传输
* xml与html的区别
	- xml标签都是自定义的，html标签是预定义。
	- xml的语法严格，html语法松散
	- xml是存储数据的，html是展示数据
* w3c:万维网联盟

### 基本语法

- xml文档的后缀名 .xml
- xml第一行必须定义为文档声明
- xml文档中有且仅有一个根标签
- 属性值必须使用引号(单双都可)引起来
- 标签必须正确关闭
- xml标签名称区分大小写

### 快速入门

```xml
<?xml version='1.0' ?>
<users>
	<user id='1'>
		<name>zhangsan</name>
		<age>23</age>
		<gender>male</gender>
		<br/>
	</user>
	
	<user id='2'>
		<name>lisi</name>
		<age>24</age>
		<gender>female</gender>
	</user>
</users>
```

### 组成部分

- 文档声明
  - 格式：`<?xml 属性列表 ?>`
  - 属性列表：
    * version：版本号，必须的属性
    * encoding：编码方式。告知解析引擎当前文档使用的字符集，默认值：ISO-8859-1
    * standalone：是否独立
    	* 取值：
    		* yes：不依赖其他文件
    		* no：依赖其他文件
- 指令(了解)：结合css的
  * `<?xml-stylesheet type="text/css" href="a.css" ?>`
- 标签：标签名称自定义的
  * 规则：
  	* 名称可以包含字母、数字以及其他的字符 
  	* 名称不能以数字或者标点符号开始 
  	* 名称不能以字母 xml（或者 XML、Xml 等等）开始 
  	* 名称不能包含空格 
- 属性：
  * id属性值唯一
- 文本：
  * CDATA区：在该区域中的数据会被原样展示
  	* 格式：  `<![CDATA[ 数据 ]]>`

### 约束

> 规定xml文档的书写规则
>
> * 作为框架的使用者(程序员)：
>   * 能够在xml中引入约束文档
>   * 能够简单的读懂约束文档

#### 分类

- DTD:一种简单的约束技术
- Schema:一种复杂的约束技术

#### DTD:一种简单的约束技术

> [DTD 简介 (w3school.com.cn)](https://www.w3school.com.cn/dtd/dtd_intro.asp)

##### 引入dtd文档到xml文档中

- 内部dtd：将约束规则定义在xml文档中

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE students [
		<!ELEMENT students (student*) >
		<!ELEMENT student (name,age,sex)>
		<!ELEMENT name (#PCDATA)>
		<!ELEMENT age (#PCDATA)>
		<!ELEMENT sex (#PCDATA)>
		<!ATTLIST student number ID #REQUIRED>
		]>

<students>
	<student number="itcast_0001">
		<name>tom</name>
		<age>18</age>
		<sex>male</sex>
	</student>
	
</students>
```

- 外部dtd：将约束的规则定义在外部的dtd文件中

  * 本地：`<!DOCTYPE 根标签名 SYSTEM "dtd文件的位置">`

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <!DOCTYPE students SYSTEM "student.dtd">
  
  <students>
  	<student number="itcast_0001">
  		<name>tom</name>
  		<age>18</age>
  		<sex>male</sex>
  	</student>
  	
  </students>
  ```

  * 网络：`<!DOCTYPE 根标签名 PUBLIC "dtd文件名字" "dtd文件的位置URL">`

#### Schema:一种复杂的约束技术

> [XML Schema 简介 (w3school.com.cn)](https://www.w3school.com.cn/schema/schema_intro.asp)

##### 引入xml文档

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
	1.填写xml文档的根元素
	2.引入xsi前缀.  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	3.引入xsd文件命名空间.  xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd"
	4.为每一个xsd约束声明一个前缀,作为标识  xmlns="http://www.itcast.cn/xml" 
	
	
 -->
<students xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns="http://www.itcast.cn/xml"
          xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd">
    <student number="heima_0001">
        <name>tom</name>
        <age>18</age>
        <sex>male</sex>
    </student>
</students>
```

> application_mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:context="http://www.springframework.org/schema/context"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    
    <context:annotation-config />

   
    <context:component-scan base-package="cn.cisol.mvcdemo">
        <context:include-filter type="annotation"
            expression="org.springframework.stereotype.Controller" />
    </context:component-scan>

   
    <mvc:annotation-driven />

    
    <mvc:resources mapping="/resources/**" location="/resources/" />


    
    <bean
        class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
        <property name="order" value="1" />
        <property name="mediaTypes">
            <map>
                <entry key="json" value="application/json" />
                <entry key="xml" value="application/xml" />
                <entry key="htm" value="text/html" />
            </map>
        </property>

        <property name="defaultViews">
            <list>
                
                <bean
                    class="org.springframework.web.servlet.view.json.MappingJackson2JsonView">
                </bean>
            </list>
        </property>
        <property name="ignoreAcceptHeader" value="true" />
    </bean>

    <bean
        class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
            value="org.springframework.web.servlet.view.JstlView" />
        <property name="prefix" value="/WEB-INF/jsps/" />
        <property name="suffix" value=".jsp" />
    </bean>


  
    <bean id="multipartResolver"
        class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize" value="209715200" />
        <property name="defaultEncoding" value="UTF-8" />
        <property name="resolveLazily" value="true" />
    </bean>

</beans>
```

### 解析

> 操作xml文档，将文档中的数据读取到内存中

#### 解析xml的方式

- DOM：将标记语言文档一次性加载进内存，在内存中形成一颗dom树
  - 优点：操作方便，可以对文档进行CRUD的所有操作
  - 缺点：占内存
- SAX：逐行读取，基于事件驱动的。
  - 优点：不占内存。
  - 缺点：只能读取，不能增删改
- xml常见的解析器
  - JAXP：sun公司提供的解析器，支持dom和sax两种思想(不常用)
  - DOM4J：一款非常优秀的解析器
  - Jsoup：jsoup 是一款Java 的HTML解析器，可直接解析某个URL地址、HTML文本内容。它提供了一套非常省力的API，可通过DOM，CSS以及类似于jQuery的操作方法来取出和操作数据。
  - PULL：Android操作系统内置的解析器，sax方式的。

#### Jsoup学习

> 可以下载Jsoup帮助文档：[Gitee.com](https://gitee.com/xutengHome/note/tree/master/Java/Jsoup)

##### 快速入门

```java
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * Jsoup快速入门：
 *      1. 导入jar包 jsoup-1.11.2.jar
 *      2. 获取Document对象
 *      3. 获取对应的标签Element对象
 *      4. 获取数据
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest {
    public static void main(String[] args) throws IOException {
        /**
         * 1. 获取Document对象
         */
        String path = JsoupTest.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        /**
         * 2. 获取对应的标签
         */
        Elements students = document.getElementsByTag("student");
        for (Element student : students) {
            Elements elementNames = student.getElementsByTag("name");
            for (Element elementName : elementNames) {
                System.out.println(elementName.text());
            }
        }
    }
}
```

##### 对象的使用

> Jsoup的使用

```java
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Jsoup对象的使用
 *      1. Jsoup：工具类，可以解析html或xml文档，返回Document
 *          * parse：解析html或xml文档，返回Document
 *              * parse(File in, String charsetName)：解析xml或html文件的。
 *              * parse(String html)：解析xml或html字符串
 *              * parse(URL url, int timeoutMillis)：通过网络路径获取指定的html或xml的文档对象
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest2 {
    public static void main(String[] args) throws IOException {
        /**
         * parse(File in, String charsetName)：解析xml或html文件的。
         */
        String path = JsoupTest2.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        System.out.println(document);
        System.out.println("=============================");
        /**
         * parse(String html)：解析xml或html字符串
         */
        Document document2 = Jsoup.parse("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "\n" +
                "<students>\n" +
                "\t<student number=\"heima_0001\">\n" +
                "\t\t<name id=\"itcast\">\n" +
                "\t\t\t<xing>张</xing>\n" +
                "\t\t\t<ming>三</ming>\n" +
                "\t\t</name>\n" +
                "\t\t<age>18</age>\n" +
                "\t\t<sex>male</sex>\n" +
                "\t</student>\n" +
                "\t<student number=\"heima_0002\">\n" +
                "\t\t<name>jack</name>\n" +
                "\t\t<age>18</age>\n" +
                "\t\t<sex>female</sex>\n" +
                "\t</student>\n" +
                "\n" +
                "</students>");
        System.out.println(document2);
        System.out.println("=============================");

        /**
         * parse(URL url, int timeoutMillis)：通过网络路径获取指定的html或xml的文档对象,timeoutMillis表示超时时间
         */
        URL url = new URL("https://www.baidu.com/?tn=88093251_34_hao_pg");
        Document document3 = Jsoup.parse(url, 10000);
        System.out.println(document3);
        System.out.println("=============================");
    }
}

```

> Document：文档对象。代表内存中的dom树

```java
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * Document：文档对象。代表内存中的dom树
 *      1. 获取Element对象
 *          * getElementById(String id)：根据id属性值获取唯一的element对象
 * 			* getElementsByTag(String tagName)：根据标签名称获取元素对象集合
 * 			* getElementsByAttribute(String key)：根据属性名称获取元素对象集合
 * 			* getElementsByAttributeValue(String key, String value)：根据对应的属性名和属性值获取元素对象集合
 * 	    2. Elements：元素Element对象的集合。可以当做 ArrayList<Element>来使用
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest3 {
    public static void main(String[] args) throws IOException {
        String path = JsoupTest3.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        /**
         * getElementById(String id)：根据id属性值获取唯一的element对象
         */
        Element itcast = document.getElementById("itcast");
        System.out.println(itcast);
        System.out.println("====================================");
        /**
         * getElementsByTag(String tagName)：根据标签名称获取元素对象集合
         */
        Elements elements = document.getElementsByTag("age");
        System.out.println(elements);
        System.out.println("====================================");
        /**
         * getElementsByAttribute(String key)：根据属性名称获取元素对象集合
         */
        Elements elements1 = document.getElementsByAttribute("number");
        System.out.println(elements1);
        System.out.println("====================================");
        /**
         * getElementsByAttributeValue(String key, String value)：根据对应的属性名和属性值获取元素对象集合
         */
        Elements elements2 = document.getElementsByAttributeValue("number", "heima_0001");
        System.out.println(elements2);
        System.out.println("====================================");

    }
}
```

> Element：元素对象

```java
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * Element：元素对象
 *      1. 获取子元素对象(document对象的使用)
 *          * getElementById(String id)：根据id属性值获取唯一的element对象
 * 			* getElementsByTag(String tagName)：根据标签名称获取元素对象集合
 * 			* getElementsByAttribute(String key)：根据属性名称获取元素对象集合
 * 			* getElementsByAttributeValue(String key, String value)：根据对应的属性名和属性值获取元素对象集合
 *      2. 获取属性值
 *          * String attr(String key)：根据属性名称获取属性值
 *      3. 获取文本内容
 *          * String text():获取文本内容
 *          * String html():获取标签体的所有内容(包括字标签的字符串内容)
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest4 {
    public static void main(String[] args) throws IOException {
        String path = JsoupTest4.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        /**
         * String attr(String key)：根据属性名称获取属性值
         */
        Elements elements = document.getElementsByTag("student");
        for (Element element : elements) {
            System.out.println(element.attr("number"));
        }
        System.out.println("=========================================");
        Elements elements1 = document.getElementsByTag("name");
        for (Element element : elements1) {
            System.out.println(element.text());
            System.out.println("---------");
            System.out.println(element.html());
        }
        System.out.println("=========================================");
    }
}
```

> Node：节点对象:是Document和Element的父类

##### 快捷查询方式

> selector:选择器

```java
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * 快捷查询方式
 *      1. selector:选择器
 *          * 使用的方法：Elements select(String cssQuery)
 *              * 语法：参考Selector类中定义的语法
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest5 {
    public static void main(String[] args) throws IOException {
        String path = JsoupTest5.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        // 查询所有name标签的元素
        Elements elements = document.select("name");
        System.out.println(elements);
        System.out.println("===============================");
        // 查询id值为itcast的元素
        Elements elements1 = document.select("#itcast");
        System.out.println(elements1);
        System.out.println("===============================");
        // 获取student标签并且number属性值为heima_0001的age子标签
        Elements elements2 = document.select("student[number='heima_0001'] > age");
        System.out.println(elements2);
        System.out.println("===============================");
    }
}
```

> Path：XPath即为XML路径语言，它是一种用来确定XML（标准通用标记语言的子集）文档中某部分位置的语言
>
> * 使用Jsoup的Xpath需要额外导入jar包。 JsoupXpath-0.3.2.jar
> * 查询w3cshool参考手册，使用xpath的语法完成查询 [XPath 语法 (w3school.com.cn)](https://www.w3school.com.cn/xpath/xpath_syntax.asp)

```java
package cn.itcast.xml.jsoup;

import cn.wanghaomiao.xpath.exception.XpathSyntaxErrorException;
import cn.wanghaomiao.xpath.model.JXDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 快捷查询方式
 *      1. XPath：XPath即为XML路径语言，它是一种用来确定XML（标准通用标记语言的子集）文档中某部分位置的语言
 *          * 使用Jsoup的Xpath需要额外导入jar包。 JsoupXpath-0.3.2.jar
 * 			* 查询w3cshool参考手册，使用xpath的语法完成查询 <a href="https://www.w3school.com.cn/xpath/xpath_syntax.asp">xpath<a/>
 *
 * @author hyatt 2022/1/12 16:51
 * @version 1.0
 */
public class JsoupTest6 {
    public static void main(String[] args) throws IOException, XpathSyntaxErrorException {
        String path = JsoupTest6.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        // 根据document对象，创建JXDocument对象
        JXDocument jxDocument = new JXDocument(document);

        // 查询所有student标签
        List<Object> list = jxDocument.sel("//student");
        for (Object o : list) {
            System.out.println(o);
        }
        System.out.println("===================================");
        // 查询所有student标签下的name标签
        List<Object> list1 = jxDocument.sel("//student/name");
        for (Object o : list1) {
            System.out.println(o);
        }
        System.out.println("===================================");

        // 查询student标签下带有id属性的name标签
        List<Object> list2 = jxDocument.sel("//student/name[@id]");
        for (Object o : list2) {
            System.out.println(o);
        }
        System.out.println("===================================");

        // 查询student标签下带有id属性的name标签 并且id属性值为itcast
        List<Object> list3 = jxDocument.sel("//student/name[@id='itcast']");
        for (Object o : list3) {
            System.out.println(o);
        }
        System.out.println("===================================");
    }
}
```

## Web

### Web和JavaWeb

- Web是全球广域网，也称为万维网(www)，能够通过浏览器访问的网站(百度、京东等)
- JavaWeb就是用Java技术来解决相关web互联网领域的技术栈

### JavaWeb

> 先介绍一下一些概念

#### B/S架构

> B/S 架构：Browser/Server，浏览器/服务器 架构模式
>
> 特点：客户端只需要浏览器，应用程序的逻辑和数据都存储在服务器端。浏览器只需要请求服务器，获取Web资源，服务器把Web资源发送给浏览器即可
>
> * 打开浏览器访问百度首页，输入要搜索的内容，点击回车或百度一下，就可以获取和搜索相关的内容
> * 思考下搜索的内容并不在我们自己的点上，那么这些内容从何而来？答案很明显是从百度服务器返回给我们的
> * 日常百度的小细节，逢年过节百度的logo会更换不同的图片，服务端发生变化，客户端不需做任务事情就能获取最新内容
> * 所以说B/S架构的好处:**易于维护升级：服务器端升级后，客户端无需任何部署就可以使用到新的版本**。
>   了解了什么是B/S架构后，作为后台开发工程师的我们将来主要关注的是服务端的开发和维护工作。在服务端将来会放很多资源,都有哪些资源呢?

![image-20220113150939102](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131509900.png)	

#### 静态资源

* 静态资源主要包含HTML、CSS、JavaScript、图片等，主要负责页面的展示。
* 我们之前已经学过前端网页制作`三剑客`(HTML+CSS+JavaScript),使用这些技术我们就可以制作出效果比较丰富的网页，将来展现给用户。但是由于做出来的这些内容都是静态的，这就会导致所有的人看到的内容将是一模一样。
* 在日常上网的过程中，我们除了看到这些好看的页面以外，还会碰到很多动态内容，比如我们常见的百度登录效果:

![image-20220113151142707](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131511884.png)	

- `张三`登录以后在网页的右上角看到的是 `张三`，而`李四`登录以后看到的则是`李四`。所以不同的用户访问相同的资源看到的内容大多数是不一样的，要想实现这样的效果，光靠静态资源是无法实现的。

#### 动态资源

* 动态资源主要包含Servlet、JSP等，主要用来负责逻辑处理。

* 动态资源处理完逻辑后会把得到的结果交给静态资源来进行展示，动态资源和静态资源要结合一起使用。

* 动态资源虽然可以处理逻辑，但是当用户来登录百度的时候，就需要输入`用户名`和`密码`,这个时候我们就又需要解决的一个问题是，用户在注册的时候填入的用户名和密码、以及我们经常会访问到一些数据列表的内容展示(如下图所示)，这些数据都存储在哪里?我们需要的时候又是从哪里来取呢?

  ![image-20220113151236969](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131512028.png)	

#### 数据库

* 数据库主要负责存储数据。
* 整个Web的访问过程就如下图所示:

![image-20220113151307626](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131513328.png)	

- 浏览器发送一个请求到服务端，去请求所需要的相关资源;
- 资源分为动态资源和静态资源,动态资源可以是使用Java代码按照Servlet和JSP的规范编写的内容;
- 在Java代码可以进行业务处理也可以从数据库中读取数据;
- 拿到数据后，把数据交给HTML页面进行展示,再结合CSS和JavaScript使展示效果更好;
- 服务端将静态资源响应给浏览器;
- 浏览器将这些资源进行解析;
- 解析后将效果展示在浏览器，用户就可以看到最终的结果。

#### HTTP协议

* HTTP协议:主要定义通信规则
* 浏览器发送请求给服务器，服务器响应数据给浏览器，这整个过程都需要遵守一定的规则，之前大家学习过TCP、UDP，这些都属于规则，这里我们需要使用的是HTTP协议，这也是一种规则。

#### Web服务器

* Web服务器:负责解析 HTTP 协议，解析请求数据，并发送响应数据
* 浏览器按照HTTP协议发送请求和数据，后台就需要一个Web服务器软件来根据HTTP协议解析请求和数据，然后把处理结果再按照HTTP协议发送给浏览器

### HTTP

#### HTTP概念

> HyperText Transfer Protocol，超文本传输协议，规定了浏览器和服务器之间==数据传输的规则==。

* 数据传输的规则指的是请求数据和响应数据需要按照指定的格式进行传输。
* 如果想知道具体的格式，可以打开浏览器，右击检查，点击`Network`来查看某一次请求的请求数据和响应数据具体的格式内容，如下图所示

![image-20220113152639937](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131526739.png)	

#### HTTP协议特点

* 基于TCP协议: 面向连接，安全

  TCP是一种面向连接的(建立连接之前是需要经过三次握手)、可靠的、基于字节流的传输层通信协议，在数据传输方面更安全。

* 基于请求-响应模型的:一次请求对应一次响应

  请求和响应是一一对应关系

* HTTP协议是无状态协议:对于事物处理没有记忆能力。每次请求-响应都是独立的

  无状态指的是客户端发送HTTP请求给服务端之后，服务端根据请求响应数据，响应完后，不会记录任何信息。这种特性有优点也有缺点，

  * 缺点:多次请求间不能共享数据
  * 优点:速度快

* 请求之间无法共享数据会引发的问题

  * 京东购物，`加入购物车`和`去购物车结算`是两次请求
    * HTTP协议的无状态特性，加入购物车请求响应结束后，并未记录加入购物车是何商品
    * 发起去购物车结算的请求后，因为无法获取哪些商品加入了购物车，会导致此次请求无法正确展示数据
    * 会话技术(Cookie、Session)

#### 请求数据格式

> 请求数据总共分为三部分内容
>
> - 请求行
> - 请求头
> - 请求体

##### 格式介绍

```http
GET / HTTP/1.1
Host: www.itcast.cn
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36 Edg/97.0.1072.55
....
```

- 请求行: HTTP请求中的第一行数据，请求行包含三块内容，分别是 GET[请求方式] /[请求URL路径] HTTP/1.1[HTTP协议及版本]

  - 请求方式有七种,最常用的是GET和POST

- 请求头: 第二行开始，格式为key: value形式

  - 请求头中会包含若干个属性，常见的HTTP请求头有:

    ```http
    Host: 表示请求的主机名
    User-Agent: 浏览器版本,例如Chrome浏览器的标识类似Mozilla/5.0 ...Chrome/79，IE浏览器的标识类似Mozilla/5.0 (Windows NT ...)like Gecko；
    Accept：表示浏览器能接收的资源类型，如text/*，image/*或者*/*表示所有；
    Accept-Language：表示浏览器偏好的语言，服务器可以据此返回不同语言的网页；
    Accept-Encoding：表示浏览器可以支持的压缩类型，例如gzip, deflate等。
    ```

  - 数据的用处：服务端可以根据请求头中的内容来获取客户端的相关信息，有了这些信息服务端就可以处理不同的业务需求

    - 不同浏览器解析HTML和CSS标签的结果会有不一致，所以就会导致相同的代码在不同的浏览器会出现不同的效果
    - 服务端根据客户端请求头中的数据获取到客户端的浏览器类型，就可以根据不同的浏览器设置不同的代码来达到一致的效果
    - 这就是浏览器兼容问题

- 请求体: POST请求的最后一部分，存储请求参数

![image-20220113154911803](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131549148.png)	

> 如上图红线框的内容就是请求体的内容，请求体和请求头之间是有一个空行隔开
>
> 此时浏览器发送的是POST请求，为什么不能使用GET呢
>
> GET和POST两个请求之间的区别
>
> - GET请求请求参数在请求行中，没有请求体，POST请求请求参数在请求体中
> - GET请求请求参数大小有限制，POST没有

#### 响应数据格式

> 响应数据总共分为三部分内容
>
> - 响应行
> - 响应头
> - 响应体

##### 格式介绍

![image-20220113155312997](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131553385.png)	

- 响应行：响应数据的第一行,响应行包含三块内容，分别是 HTTP/1.1[HTTP协议及版本] 200[响应状态码] ok[状态码的描述]

- 响应头：第二行开始，格式为key：value形式

  - 响应头中会包含若干个属性，常见的HTTP响应头有:

    ```
    Content-Type：表示该响应内容的类型，例如text/html，image/jpeg；
    Content-Length：表示该响应内容的长度（字节数）；
    Content-Encoding：表示该响应压缩算法，例如gzip；
    Cache-Control：指示客户端应如何缓存，例如max-age=300表示可以最多缓存300秒
    ```

- 响应体： 最后一部分。存放响应数据

  - 上图中`<html>...</html>`这部分内容就是响应体，它和响应头之间有一个空行隔开。	

##### 响应状态码

> 这里只记录三个常见的状态码

* 200  ok 客户端请求成功
* 404  Not Found 请求资源不存在
* 500 Internal Server Error 服务端发生不可预期的错误

### Tomcat

#### Web服务器

> Web服务器是一个应用程序（==软件==），对HTTP协议的操作进行封装，使得程序员不必直接对协议进行操作，让Web开发更加便捷。主要功能是"提供网上信息浏览服务"。

![image-20220113155742323](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131557375.png)	

>  Web服务器是安装在服务器端的一款软件，将来我们把自己写的Web项目部署到Web Tomcat服务器软件中，当Web服务器软件启动后，部署在Web服务器软件中的页面就可以直接通过浏览器来访问了。

##### Web服务器软件使用步骤

* 准备静态资源
* 下载安装Web服务器软件
* 将静态资源部署到Web服务器上
* 启动Web服务器使用浏览器访问对应的资源

##### Web服务器软件

> 除了Tomcat之外的服务器软件

![image-20220113155907804](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131559244.png)	

##### Tomcat

> * Tomcat是Apache软件基金会一个核心项目，是一个开源免费的轻量级Web服务器，支持Servlet/JSP少量JavaEE规范。
>
> * 概念中提到了JavaEE规范，那什么又是JavaEE规范呢?
>
>   - JavaEE: Java Enterprise Edition,Java企业版。指Java企业级开发的技术规范总和。包含13项技术规范:JDBC、JNDI、EJB、RMI、JSP、Servlet、XML、JMS、Java IDL、JTS、JTA、JavaMail、JAF。
>
> * 因为Tomcat支持Servlet/JSP规范，所以Tomcat也被称为Web容器、Servlet容器。Servlet需要依赖Tomcat才能运行。
>
> * Tomcat的官网: https://tomcat.apache.org/ 从官网上可以下载对应的版本进行使用。

#### 基本使用

##### 下载

> 直接官网下载：http://tomcat.apache.org/

##### 安装

> 解压压缩包即可。
>
> **注意：安装目录建议不要有中文和空格**

##### 卸载

> 删除目录就行了

##### 启动

> - 双击: bin\startup.bat
> - 启动后，通过浏览器访问 `http://localhost:8080`能看到Apache Tomcat的内容就说明Tomcat已经启动成功。
> - 遇到的问题
>   - 黑窗口一闪而过
>     - 没有正确配置JAVA_HOME环境变量
>   - 启动报错
>     - 可能端口号被占用了
>       - netstat -ano  找到占用的端口号，并且找到对应的进程，杀死该进程
>     - 修改自身的端口号
>       - conf/server.xml

##### 关闭

> * 直接关掉运行窗口:强制关闭[不建议]
> * bin\shutdown.bat：正常关闭
> * ctrl+c： 正常关闭

##### 配置

###### 修改端口

> Tomcat默认的端口是8080，要想修改Tomcat启动的端口号，需要修改 conf/server.xml
>
> 注: HTTP协议默认端口号为80，如果将Tomcat端口号改为80，则将来访问Tomcat时，将不用输入端口号。

![image-20220113160804957](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131608338.png)	

###### 启动时可能出现的错误

> Tomcat的端口号取值范围是0-65535之间任意未被占用的端口，如果设置的端口号被占用，启动的时候就会包如下的错误

![image-20220113160930561](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131609046.png)	

> Tomcat启动的时候，启动窗口一闪而过: 需要检查JAVA_HOME环境变量是否正确配置



![image-20220113161000640](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131610561.png)

##### 部署

> Tomcat部署项目： 将项目放置到webapps目录下，即部署完成

- 将html文件(包括上层文件目录)拷贝到Tomcat的webapps目录下
- 通过浏览器访问`http://localhost/hello/a.html`，能看到下面的内容就说明项目已经部署成功。

![image-20220113161311037](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131613016.png)

> 一般JavaWeb项目会被打包称war包，然后将war包放到Webapps目录下，Tomcat会自动解压缩war文件(可以压缩文件的大小加快传输)

- 将war包拷贝到Tomcat的webapps目录下
- Tomcat检测到war包后会自动完成解压缩，在webapps目录下就会多一个haha目录
- 通过浏览器访问`http://localhost/haha/a.html`，能看到下面的内容就说明项目已经部署成功。

![image-20220113161451976](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131614622.png)

### Maven创建Web项目

#### 创建项目

![image-20220113161828353](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131618634.png)	

> 这边替换成自己的maven配置文件和仓库

![image-20220113162214823](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131622055.png)	

> 完成后补齐对应的目录结构，java文件夹和resources文件夹(同理test文件夹也是一样)

![image-20220113163037284](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131630743.png)	

#### Maven目录结构

> Web项目的结构分为:开发中的项目和开发完可以部署的Web项目,这两种项目的结构是不一样的

> Maven Web项目结构: 开发中的项目

![image-20220113145850596](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131458210.png)	

> 开发完成部署的Web项目
>
> - 开发项目通过执行Maven打包命令==package==,可以获取到部署的Web项目目录
> - 编译后的Java字节码文件和resources的资源文件，会被放到WEB-INF下的classes目录下
> - pom.xml中依赖坐标对应的jar包，会被放入WEB-INF下的lib目录下

![image-20220113145949866](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131459888.png)	

### 集成本地Tomcat

> xxx.war和 xxx.war exploded这两种部署项目模式的区别
>
> - war模式是将WEB工程打成war包，把war包发布到Tomcat服务器上
> - war exploded模式是将WEB工程以当前文件夹的位置关系发布到Tomcat服务器上
>
> * war模式部署成功后，Tomcat的webapps目录下会有部署的项目内容
> * war exploded模式部署成功后，Tomcat的webapps目录下没有，而使用的是项目的target目录下的内容进行部署
> * 建议大家都选war模式进行部署，更符合项目部署的实际情况

> 添加本地Tomcat的面板，配置本地Tomcat的具体路径

![image-20220113164136283](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131641417.png)	

> 将开发项目部署到Tomcat中

![image-20220113164314415](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131643791.png)	

> 之后点击运行，成功后就会自动跳转

![image-20220113164556213](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131645310.png)	

### Tomcat的插件使用

> pom.xml添加插件

```xml
<build>
  <plugins>
    <!-- tomcat插件-->
    <plugin>
      <groupId>org.apache.tomcat.maven</groupId>
      <artifactId>tomcat7-maven-plugin</artifactId>
      <version>2.2</version>
      <configuration>
        <!-- 配置默认的端口号和地址 -->
        <port>80</port>
        <path>/</path>
      </configuration>
    </plugin>
  </plugins>
</build>
```

> 使用插件运行，日志中会有地址

![image-20220113170019529](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131700910.png)	

### Servlet

#### 快速入门

##### 导入依赖

```xml
<dependencies>
  <dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.1.0</version>
    <scope>provided</scope>
  </dependency>
</dependencies>
```

##### 定义类

> - 实现Servlet接口
> - 在类上使用@WebServlet注解，配置访问路径
> - 启动tomcat，浏览器输入url，访问Servlet

```java
package com.itcast.cn;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * <p>Servlet学习</p>
 *
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet("/test1")
public class ServletTest implements Servlet {

    /**
     * 默认调用
     * @param servletRequest 请求数据
     * @param servletResponse 响应
     * @throws ServletException
     * @throws IOException
     */
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("Servlet start......");
    }

    public void init(ServletConfig servletConfig) throws ServletException {

    }

    public ServletConfig getServletConfig() {
        return null;
    }

    public String getServletInfo() {
        return null;
    }

    public void destroy() {

    }
}
```

#### 执行流程

![image-20220113173144986](https://gitee.com/image_bed/image-bed1/raw/master/img/202201131731486.png)

* 浏览器发出`http://localhost:8080/web-demo/demo1`请求，从请求中可以解析出三部分内容，分别是`localhost:8080`、`web-demo`、`demo1`
  * 根据`localhost:8080`可以找到要访问的Tomcat Web服务器
  * 根据`web-demo`可以找到部署在Tomcat服务器上的web-demo项目
  * 根据`demo1`可以找到要访问的是项目中的哪个Servlet类，根据@WebServlet后面的值进行匹配
* 找到ServletDemo1这个类后，Tomcat Web**服务器**就会为ServletDemo1这个类创建一个对象，然后调用对象中的service方法
  * ServletDemo1实现了Servlet接口，所以类中必然会重写service方法供Tomcat Web服务器进行调用
  * service方法中有ServletRequest和ServletResponse两个参数，ServletRequest封装的是请求数据，ServletResponse封装的是响应数据，后期我们可以通过这两个参数实现前后端的数据交互

#### 生命周期

> 对象的生命周期指一个对象从被创建到被销毁的整个过程
>
> 如何才能让Servlet中的destroy方法被执行：
>
> 在Terminal命令行中，先使用`mvn tomcat7:run`启动，然后再使用`ctrl+c`关闭tomcat

- 加载和实例化：默认情况下，当Servlet第一次被访问时，由容器创建Servlet对象
- 初始化：在Servlet实例化之后，容器将调用Servlet的init()方法初始化这个对象，完成一些如加载配置文件、创建连接等初始化的工作。**该方法只调用一次**
- 请求处理：**每次**请求Servlet时，Servlet容器都会调用Servlet的**service()**方法对请求进行处理
- 服务终止：当需要释放内存或者容器关闭时，容器就会调用Servlet实例的destroy()方法完成资源的释放。在destroy()方法调用之后，容器会释放这个Servlet实例，该实例随后会被Java的垃圾收集器所回收

```java
package com.itcast.cn;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * <p>Servlet学习：生命周期</p>
 *
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet(urlPatterns = "/test2", loadOnStartup = 1)
public class ServletTest2 implements Servlet {

    /**
     * 初始化方法：
     *      1. 调用时机：默认情况下，Servlet被第一次访问的时候被调用
     *          * loadOnStartup： 默认是-1，修改为0或者正整数的时候，则在服务器启动的时候就被调用
     *      2. 调用次数：1次
     * @param servletConfig
     * @throws ServletException
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init......");
    }

    /**
     * 销毁方法：
     *      1. 调用时机：内存释放或者服务器关闭的时候，Servlet对象会被销毁
     *      2. 调用次数：1次
     */
    public void destroy() {
        System.out.println("destroy....");
    }

    /**
     * 提供服务：
     *      1. 调用时机：每次Servlet被访问的时候被调用
     *      2. 调用次数：多次
     * @param servletRequest 请求数据
     * @param servletResponse 响应数据
     * @throws ServletException 异常
     * @throws IOException 异常
     */
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("Servlet start......");
    }

    public ServletConfig getServletConfig() {
        return null;
    }

    public String getServletInfo() {
        return null;
    }
}
```

#### 体系结构

![image-20220114145834455](https://gitee.com/image_bed/image-bed1/raw/master/img/202201141458780.png)	

​	

##### HttpServlet

> 在开发B/S架构的web项目，都是针对HTTP协议，所以一般都是继承这个类

```java
package com.itcast.cn;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: HttpServlet</p>
 *
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet("/test3")
public class ServletTest3 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest3 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest3 post......");
    }
}
```

#### urlPatterns配置

> 一个Servlet,可以配置多个urlPattern

- 精确匹配

```java
package com.itcast.cn;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: urlPatterns配置</p>
 *
 * 精确匹配
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet(urlPatterns = {"/test4", "/test5"})
public class ServletTest4 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest4 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest4 post......");
    }
}
```

- 目录匹配

> `/user/*`中的`/*`代表的是零或多个层级访问目录同时**精确匹配优先级要高于目录匹配**

```java
package com.itcast.cn;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: urlPatterns配置</p>
 *
 * 目录匹配
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet(urlPatterns = {"/user/*"})
public class ServletTest5 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest5 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest5 post......");
    }
}
```

- 扩展名匹配

> - 如果路径配置的不是扩展名，那么在路径的前面就必须要加`/`否则会报错
> - 如果路径配置的是`*.do`,那么在*.do的前面不能加`/`,否则会报错

```java
package com.itcast.cn;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: urlPatterns配置</p>
 *
 * 扩展名匹配:
 *      注意：*号前面不能有 /
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet(urlPatterns = {"*.do"})
public class ServletTest6 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest6 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest6 post......");
    }
}
```

- 任意匹配

> 注意：不要配置这样的url
>
> `/`和`/*`的区别：
>
> - 当我们的项目中的Servlet配置了 "/",会覆盖掉tomcat中的DefaultServlet,当其他的url-pattern都匹配不上时都会走这个Servlet
> - 当我们的项目中配置了"/*",意味着匹配任意访问路径
> - DefaultServlet是用来处理静态资源，如果配置了"/"会把默认的覆盖掉，就会引发请求静态资源的时候没有走默认的而是走了自定义的Servlet类，最终导致静态资源不能被访问

```java
package com.itcast.cn;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: urlPatterns配置</p>
 *
 * 任意匹配:
 *      @WebServlet(urlPatterns = {"/*"})
 *      @WebServlet(urlPatterns = {"/"})
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
@WebServlet(urlPatterns = {"/*"})
public class ServletTest7 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest7 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest7 post......");
    }
}
```

#### XMl配置

> Servlet从3.0版本后开始支持注解配置，3.0版本前只支持XML配置文件的配置方法

##### 编写Servlet类

```java
package com.itcast.cn;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Servlet学习: XML配置</p>
 *
 * @author hyatt 2022/1/13 17:18
 * @version 1.0
 */
public class ServletTest8 extends HttpServlet {

    /**
     * 重写HttpServlet的get方法
     *      * 调用HttpServlet的service方法，然后分流
     *      * 具体的实现可以查看HttpServlet的源码
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest8 get......");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("ServletTest7 post......");
    }
}
```

##### 配置web.xml

> 访问：[localhost:8080/test8](http://localhost:8080/test8)

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
  <display-name>Archetype Created Web Application</display-name>

  <!--Servlet 全类名-->
  <servlet>
    <servlet-name>servlet8</servlet-name>
    <servlet-class>com.itcast.cn.ServletTest8</servlet-class>
  </servlet>
  <!-- Servlet 访问路径-->
  <servlet-mapping>
    <servlet-name>servlet8</servlet-name>
    <url-pattern>/test8</url-pattern>
  </servlet-mapping>

</web-app>
```

### Request

#### Request对象

> 后台服务器(Tomcat)会将http请求中的数据解析并封装进入对象中(Request对象)

#### 继承体系

![image-20220117180039067](https://gitee.com/image_bed/image-bed1/raw/master/img/202201171800578.png)	

#### 获取数据

##### 获取请求行数据

```java
package com.itcast.cn.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>request对象的学习</p>
 *
 * @author hyatt 2022/1/17 17:44
 * @version 1.0
 */
@WebServlet("/request1")
public class RequestTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 请求访问：http://localhost:8080/request1?username=zhangsan&password=1234
        // String getMethod()：获取请求方式：
        String method = req.getMethod();
        // GET
        System.out.println(method);
        // String getContextPath()：获取虚拟目录(项目访问路径)
        String contextPath = req.getContextPath();
        // 为空 因为插件那边没有配置地址
        System.out.println(contextPath);
        // StringBuffer getRequestURL(): 获取URL(统一资源定位符)
        StringBuffer requestURL = req.getRequestURL();
        // http://localhost:8080/request1
        System.out.println(requestURL.toString());
        // String getRequestURI()：获取URI(统一资源标识符)
        String requestURI = req.getRequestURI();
        // /request1
        System.out.println(requestURI);
        // String getQueryString()：获取请求参数（GET方式）
        String queryString = req.getQueryString();
        // username=zhangsan&password=1234
        System.out.println(queryString);
    }
}
```

##### 获取请求头数据

```java
// 获取请求头的数据
String header = req.getHeader("user-agent");
System.out.println(header);
```

##### 获取请求体的数据

> post请求
>
> - 获取字节输入流，如果前端发送的是字节数据，比如传递的是文件数据
>
> ```java
> ServletInputStream getInputStream()
> ```
>
> - 获取字符输入流，如果前端发送的是纯文本数据
>
> ```java
> BufferedReader getReader()
> ```

```java
@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    BufferedReader reader = req.getReader();
    String s = reader.readLine();
    System.out.println(s);
    // 一次请求结束后自动关闭，所以这里可以不用关闭
    reader.close();
}
```

#### 通用方式获取请求参数

```java
package com.itcast.cn.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

/**
 * <p>request对象的学习</p>
 *
 * 通用方式获取参数
 *      Map<String,String[]> getParameterMap()：获取所有参数Map集合
 *      String[] getParameterValues(String name)：根据名称获取参数值（数组）
 *      String getParameter(String name)：根据名称获取参数值(单个值)
 *
 * @author hyatt 2022/1/17 17:44
 * @version 1.0
 */
@WebServlet("/request2")
public class RequestTest2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Map<String,String[]> getParameterMap()：获取所有参数Map集合
        Map<String, String[]> parameterMap = req.getParameterMap();
        for (String key : parameterMap.keySet()) {
            // 获取键对应的值并输出
            for (String value : parameterMap.get(key)) {
                System.out.println(key + ":" + value + " ");
            }
            // 每一行输出完并换行
            System.out.println("");
        }
        System.out.println("===============================");
        // String[] getParameterValues(String name)：根据名称获取参数值（数组）
        String[] hobbies = req.getParameterValues("hobby");
        for (String hobby : hobbies) {
            System.out.println(hobby);
        }
        System.out.println("===============================");
        // String getParameter(String name)：根据名称获取参数值(单个值)
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        System.out.println(username);
        System.out.println(password);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
```

#### 快速创建Servlet

![image-20220118144618998](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181446086.png)	

> 修改Servlet创建的模板

![image-20220118144733835](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181447660.png)

#### 中文乱码

```java
package com.itcast.cn.request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 中文乱码问题
 *      post请求：request.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
 *      get请求/post请求：
 *          1. 先编码(和tomcat的编码一致), 获取字节数组
 *          2. 再以UTF-8解码 字节数组
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/request3")
public class RequestTest3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        System.out.println("乱码前：" + username);
        username = new String(username.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        System.out.println("解决乱码后：" + username);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // post请求可以直接设置字符集的方式解决
//        request.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
//        String username = request.getParameter("username");
//        System.out.println("解决乱码后：" + username);
        this.doGet(request, response);
    }
}
```

#### 请求转发

> 请求转发(forward):一种在服务器内部的资源跳转方式
>
> - 浏览器发送请求给服务器，服务器中对应的资源A接收到请求
>
> - 资源A处理完请求后将请求发给资源B
>
> - 资源B处理完后将结果响应给浏览器
>
> - 请求从资源A到资源B的过程就叫请求转发
>
>   ![image-20220118152808054](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181528678.png)	
>
> **请求转发的特点：**
>
> - 浏览器地址栏路径不发生变化
> - 只能转发到当前服务器的内部资源
> - 一次请求。可以在转发资源间使用request共享数据

```java
package com.itcast.cn.request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 *  跳转
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/request4")
public class RequestTest4 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("RequestTest4 start ....");

        // 添加request的属性
        request.setAttribute("msg", "hello");
        // 跳转
        request.getRequestDispatcher("/request5").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

```java
package com.itcast.cn.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 跳转
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/request5")
public class RequestTest5 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("RequestTest5 start ....");
        // 获取属性
        Object msg = request.getAttribute("msg");
        System.out.println("msg: " + msg);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

### Response

#### Respone请求重定向

> Response重定向(redirect):一种资源跳转方式（和请求转发不一样）
>
> - 浏览器发送请求给服务器，服务器中对应的资源A接收到请求
>
> - 资源A现在无法处理该请求，就会给浏览器响应一个302的状态码+location的一个访问资源B的路径
>
> - 浏览器接收到响应状态码为302就会重新发送请求到location对应的访问地址去访问资源B
>
> - 资源B接收到请求后进行处理并最终给浏览器响应结果，这整个过程就叫重定向
>
>   ![image-20220118154424076](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181544886.png)	
>
> **重定向的特点：**
>
> - 浏览器地址栏路径发送变化
> - 可以重定向到任何位置的资源(服务内容、外部均可)
> - 两次请求，不能在多个资源使用request共享数据

> 实现方式

```java
resp.setStatus(302);
resp.setHeader("location","资源B的访问路径");
```

```java
package com.itcast.cn.response;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * 重定向
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/response1")
public class ResponseTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(302);
        response.setHeader("location", "/day13_tomcat/login.html");

        // 简化跳转代码
        // response.sendRedirect("https://www.baidu.com");
        // 优化代码 设置动态的虚拟路径
        // response.sendRedirect("/day13_tomcat/login.html");
        String contextPath = request.getContextPath();
        response.sendRedirect(contextPath + "/login.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}

```

#### 路径问题

![image-20220118155308073](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181553200.png)	

> 判断是否添加虚拟目录

* 浏览器使用:需要加虚拟目录(项目访问路径)
* 服务端使用:不需要加虚拟目录

> 例子

```java
<a href='路径'> // 超链接，从浏览器发送，需要加
<form action='路径'> // 表单，从浏览器发送，需要加
req.getRequestDispatcher("路径") // 转发，是从服务器内部跳转，不需要加
resp.sendRedirect("路径") // 重定向，是由浏览器进行跳转，需要加。
```

#### Response响应字符数据

```java
package com.itcast.cn.response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 响应字符数据
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/response2")
public class ResponseTest2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置字符集 解决中文乱码的问题
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.write("<h1>你好</h1>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

#### Response响应字节数据

```xml
<dependency>
  <groupId>commons-io</groupId>
  <artifactId>commons-io</artifactId>
  <version>2.6</version>
</dependency>
```

```java
package com.itcast.cn.response;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 响应字节数据
 *
 * @author hyatt
 * @version 1.0
 */
@WebServlet("/response3")
@SuppressWarnings("all")
public class ResponseTest3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\74220\\Pictures\\逍遥安卓照片\\172079.jpg");
        ServletOutputStream outputStream = response.getOutputStream();
        // 将文件写入输出流
//        byte[] bys = new byte[1024];
//        int lengh = 0;
//        while ((lengh = fileInputStream.read(bys)) != -1) {
//            outputStream.write(bys, 0, lengh);
//        }
        // 使用工具包 引入依赖 commons-io
        IOUtils.copy(fileInputStream, outputStream);
        fileInputStream.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

### 代码

> [午夜小学徒/Web练习 - 码云 - 开源中国 (gitee.com)](https://gitee.com/xutengHome/code/tree/master/src/main/java/com/itcast/cn/login)
>
> 登陆例子

## JSP

> JSP = html + java

### JSP快速入门

#### 添加依赖

```xml
<dependency>
  <groupId>javax.servlet.jsp</groupId>
  <artifactId>jsp-api</artifactId>
  <version>2.2</version>
  <scope>provided</scope>
</dependency>
```

#### 创建jsp页面

> 运行项目访问即可

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello Jsp</title>
</head>
<body>
<h1>Hello Jsp</h1>
<%
    System.out.println("hello Jsp");
%>
</body>
</html>
```

### JSP原理

> JSP 本质上就是一个 Servlet
>
> 可以到项目所在磁盘目录下找 `target\tomcat\work\Tomcat\localhost\jsp-demo\org\apache\jsp` 目录，而这个目录下就能看到转换后的 `servlet`

![image-20220119162043595](https://gitee.com/image_bed/image-bed1/raw/master/img/202201191620645.png)	

> - 浏览器第一次访问 `hello.jsp` 页面
> - `tomcat` 会将 `hello.jsp` 转换为名为 `hello_jsp.java` 的一个 `Servlet`
> - `tomcat` 再将转换的 `servlet` 编译成字节码文件 `hello_jsp.class`
> - `tomcat` 会执行该字节码文件，向外提供服务

### JSP脚本

#### 脚本分类

* <%...%>：内容会直接放到_jspService()方法之中
* <%=…%>：内容会放到out.print()中，作为out.print()的参数
* <%!…%>：内容会放到_jspService()方法之外，被类直接包含

### EL 表达式

> EL（全称Expression Language ）表达式语言，用于简化 JSP 页面内的 Java 代码。
>
> EL 表达式的主要作用是 **获取数据**。其实就是从域对象中获取数据，然后将数据展示在页面上。
>
> EL 表达式的语法也比较简单，${expression} 。例如：${brands} 就是获取域中存储的 key 为 brands 的数据

#### EL测试

> 创建一个Servlet用来设置属性

```java
package com.itcast.cn.jsp.servlet;

import com.itcast.cn.jsp.pojo.Brand;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/el-test")
public class ElServletTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Brand> brands = new ArrayList<Brand>();
        brands.add(new Brand(1, "三只松鼠", "三只松鼠", 100, "三只松鼠，好吃不上火", 1));
        brands.add(new Brand(2, "优衣库", "优衣库", 200, "优衣库，服适人生", 0));
        brands.add(new Brand(3, "小米", "小米科技有限公司", 1000, "为发烧而生", 1));
        // 添加到请求request中
        request.setAttribute("brands", brands);
        // 跳转
        request.getRequestDispatcher("/el-test.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

> 创建一个jsp页面用来展示
>
> **注意：el表达式不显示的问题**：[(15条消息) jsp中EL表达式不生效_kevin-CSDN博客_el表达式不生效](https://blog.csdn.net/u012965203/article/details/82462611)

```java
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 解决el表达式不生效的问题 -->
<%@page isELIgnored="false" %>
<html>
<head>
    <title>EL 表达式</title>
</head>
<body>
${brands}
</body>
</html>
```

### 域对象

> JavaWeb中有四大域对象

* page：当前页面有效
* request：当前请求有效
* session：当前会话有效
* application：当前应用有效

> el 表达式获取数据，会依次从这4个域中寻找，直到找到为止。而这四个域对象的作用范围如下图所示

![image-20220121142447595](https://gitee.com/image_bed/image-bed1/raw/master/img/202201211424013.png)	

### JSTL标签

> JSP标准标签库(Jsp Standarded Tag Library) ，使用标签取代JSP页面上的Java代码.
>
> [JSP 标准标签库（JSTL） | 菜鸟教程 (runoob.com)](https://www.runoob.com/jsp/jsp-jstl.html)

#### 导入依赖

```xml
<!-- JSTL -->
<dependency>
  <groupId>jstl</groupId>
  <artifactId>jstl</artifactId>
  <version>1.2</version>
</dependency>
```

#### 引入JSTL标签库

> 在JSP页面上引入JSTL标签库

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
```

#### if标签

```jsp
<c:if test="${status==1}">
    启用
</c:if>
<c:if test="${status!=1}">
    禁用
</c:if>
```

#### forEach 标签

> 用法一：类似于 Java 中的增强for循环
>
> * items：被遍历的容器
> * var：遍历产生的临时变量
> * varStatus：遍历状态对象
>   * status.count 可以实现序号

```jsp
<c:forEach items="${brands}" var="brand" varStatus="status">
    <tr align="center">
        <td>${brand.id}</td>
        <td>${brand.brandName}</td>
        <td>${brand.companyName}</td>
        <td>${brand.description}</td>
    </tr>
</c:forEach>
```

> 用法二：类似于 Java 中的普通for循环
>
> * begin：开始数
> * end：结束数
> * step：步长
> * i: 变量

```jsp
<c:forEach begin="0" end="${fn:length(brands)}" step="1" var="i">
    <tr align="center">
        <td>${brands[i].id}</td>
        <td>${brands[i].brandName}</td>
        <td>${brands[i].companyName}</td>
        <td>${brands[i].description}</td>
    </tr>
</c:forEach>
```

> 代码

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<!-- 引入jstl标签库 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 计算对象的长度 -->
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>Jstl使用</title>
</head>
<body>
<c:if test="${status==1}">
    启用
</c:if>
<c:if test="${status!=1}">
    禁用
</c:if>
长度：${fn:length(brands)}
<table>
    <c:forEach items="${brands}" var="brand" varStatus="status">
        <tr align="center">
            <td>${brand.id}</td>
            <td>${brand.brandName}</td>
            <td>${brand.companyName}</td>
            <td>${brand.description}</td>
        </tr>
    </c:forEach>
</table>
<br>

<table>
    <c:forEach begin="0" end="${fn:length(brands)}" step="1" var="i">
        <tr align="center">
            <td>${brands[i].id}</td>
            <td>${brands[i].brandName}</td>
            <td>${brands[i].companyName}</td>
            <td>${brands[i].description}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
```

### MVC模式和三层架构

> MVC 模式和三层架构是一些理论的知识，将来我们使用了它们进行代码开发会让我们代码维护性和扩展性更好。

#### MVC模式

> MVC 是一种分层开发的模式

* M：Model，业务模型，处理业务
* V：View，视图，界面展示
* C：Controller，控制器，处理请求，调用模型和视图

![image-20220121145526648](https://gitee.com/image_bed/image-bed1/raw/master/img/202201211455277.png)	

#### MVC好处

* 职责单一，互不影响。每个角色做它自己的事，各司其职。

* 有利于分工协作。

* 有利于组件重用

#### 三层架构

> 三层架构是将我们的项目分成了三个层面，分别是 `表现层`、`业务逻辑层`、`数据访问层`。

![image-20220121145622449](https://gitee.com/image_bed/image-bed1/raw/master/img/202201211456806.png)	

#### 案例

> [src/main/java/com/itcast/cn/jsp/controller · 午夜小学徒/Web练习 - 码云 - 开源中国 (gitee.com)](https://gitee.com/xutengHome/code/tree/master/src/main/java/com/itcast/cn/jsp/controller)

## 会话

> 用户打开浏览器，访问web服务器的资源，会话建立，直到有一方断开连接，会话结束。**在一次会话中可以包含多次请求和响应。**

### 会话跟踪

> 一种维护浏览器状态的方法，服务器需要识别多次请求是否来自于同一浏览器，以便在同一次会话的多次请求间**共享数据**

* 服务器会收到多个请求，这多个请求可能来自多个浏览器
* 服务器需要用来识别请求是否来自同一个浏览器
* 服务器用来识别浏览器的过程，这个过程就是**会话跟踪**
* 服务器识别浏览器后就可以在同一个会话中多次请求之间来共享数据

### 数据共享

#### 浏览器和服务器不支持数据共享

* 浏览器和服务器之间使用的是HTTP请求来进行数据传输
* HTTP协议是**无状态**的，每次浏览器向服务器请求时，服务器都会将该请求视为新的请求
* HTTP协议设计成无状态的目的是让每次请求之间相互独立，互不影响
* 请求与请求之间独立后，就无法实现多次请求之间的数据共享

#### 具体的实现方式

> 区别：Cookie是存储在浏览器端而Session是存储在服务器端

- 客户端会话跟踪技术：Cookie
- 服务端会话跟踪技术：Session

### Cookie

> 客户端会话技术，将数据保存到客户端，以后每次请求都携带Cookie数据进行访问

#### 工作流程

![image-20220124152216575](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241522150.png)	

* 服务端提供了两个Servlet，分别是ServletA和ServletB
* 浏览器发送HTTP请求1给服务端，服务端ServletA接收请求并进行业务处理
* 服务端ServletA在处理的过程中可以创建一个Cookie对象并将`name=zs`的数据存入Cookie
* 服务端ServletA在响应数据的时候，会把Cookie对象响应给浏览器
* 浏览器接收到响应数据，会把Cookie对象中的数据存储在浏览器内存中，此时浏览器和服务端就**建立了一次会话**
* ==在同一次会话==中浏览器再次发送HTTP请求2给服务端ServletB，浏览器会携带Cookie对象中的所有数据
* ServletB接收到请求和数据后，就可以获取到存储在Cookie对象中的数据，这样同一个会话中的多次请求之间就实现了数据共享

#### 基本使用

##### 发送Cookie

```java
package com.itcast.cn.cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/aServlet")
public class AServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie cookie = new Cookie("username", "zs");
        response.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

##### 获取cookie

```java
package com.itcast.cn.cookie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/bServlet")
public class BServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        // 查询cookie
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            if ("username".equals(name)) {
                String value = cookie.getValue();
                System.out.println(name + ":" + value);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

##### 浏览器查看

![image-20220124153121814](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241531512.png)	

#### Cookie的原理

![image-20220124153521111](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241535614.png)	

* 前面的案例中已经能够实现，AServlet给前端发送Cookie,BServlet从request中获取Cookie的功能
* 对于AServlet响应数据的时候，Tomcat服务器都是基于HTTP协议来响应数据
* 当Tomcat发现后端要返回的是一个Cookie对象之后，Tomcat就会在响应头中添加一行数据`Set-Cookie:username=zs`
* 浏览器获取到响应结果后，从响应头中就可以获取到`Set-Cookie`对应值`username=zs`,并将数据存储在浏览器的内存中
* 浏览器再次发送请求给BServlet的时候，浏览器会自动在请求头中添加==`Cookie: username=zs`==发送给服务端BServlet
* Request对象会把请求头中cookie对应的值封装成一个个Cookie对象，最终形成一个数组
* BServlet通过Request对象获取到Cookie[]后，就可以从中获取自己需要的数据

#### Cookie的存活时间

> 默认情况下，Cookie存储在浏览器内存中，当浏览器关闭，内存释放，则Cookie被销毁

> setMaxAge(int seconds): 设置Cookie的存活时间
>
> 参数seconds:
>
> - 正数：将Cookie写入浏览器所在电脑的硬盘，持久化存储。到时间自动删除
> - 负数：默认值，Cookie在当前浏览器内存中，当浏览器关闭，则Cookie被销毁
> - 零：删除对应Cookie

#### Cookie存储中文

> Cookie不可以直接存储中文

![image-20220124154105901](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241541351.png)	

> 这里使用URL编码和解码来获取值

```java
package com.itcast.cn.cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/aServlet")
public class AServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Cookie不可以直接存储中文, 这里使用 URLEncoder 编码
        String value = "张三";
        String encode = URLEncoder.encode(value, "utf-8");
        // 存储到Cookie中
        Cookie cookie = new Cookie("username", encode);
        // 参数为秒 设置cookie的存活时间
        // cookie.setMaxAge(60 * 60 * 24 * 7);
        response.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

```java
package com.itcast.cn.cookie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

@WebServlet("/bServlet")
public class BServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        // 查询cookie
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            if ("username".equals(name)) {
                String value = cookie.getValue();
                // 使用 URLDecoder 解码
                String decode = URLDecoder.decode(value, "utf-8");
                System.out.println(name + ":" + decode);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

### Session

> 服务端会话跟踪技术：将数据保存到服务端

#### 工作流程

![image-20220124154806196](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241548040.png)	

* 在服务端的AServlet获取一个Session对象，把数据存入其中
* 在服务端的BServlet获取到相同的Session对象，从中取出数据
* 就可以实现一次会话中多次请求之间的数据共享了
* 那么现在最大的问题是如何保证AServlet和BServlet使用的是同一个Session对象(原理)

#### 基本使用

> session的获取：HttpSession session = request.getSession();
>
> 常用的方法：
>
> - void setAttribute(String name, Object o)
> - Object getAttribute(String name)

```java
package com.itcast.cn.conversation.session;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/sessionTestServlet")
public class SessionTestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取Session对象
        HttpSession session = request.getSession();
        // 存储数据
        session.setAttribute("username", "zs");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

```java
package com.itcast.cn.conversation.session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/sessionTestServlet2")
public class SessionTestServlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取Session对象
        HttpSession session = request.getSession();
        // 获取数据
        String username = (String) session.getAttribute("username");
        session.setAttribute("username", "zs");
        System.out.println("username: " + username);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

#### Session的原理

> Session是基于Cookie实现的

> 前提条件

![image-20220124155617811](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241556551.png)	

> **注意:在一台电脑上演示的时候，如果是相同的浏览器必须要把浏览器全部关掉重新打开，才算新开的一个浏览器**

> 当把浏览器全部关闭后重新获取的时候，发现session其实已经和原来的不一致了，所以就不能共享数据了

##### 细节

![image-20220124155925575](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241559417.png)	

- sessionTestServlet在第一次获取session对象的时候，session对象会有一个唯一的标识，假如是`id:10`

- sessionTestServlet在session中存入其他数据并处理完成所有业务后，需要通过Tomcat服务器响应结果给浏览器

- Tomcat服务器发现业务处理中使用了session对象，就会把session的唯一标识`id:10`当做一个cookie，添加`Set-Cookie:JESSIONID=10`到响应头中，并响应给浏览器

- 浏览器接收到响应结果后，会把响应头中的coookie数据存储到浏览器的内存中

- 浏览器在同一会话中访问sessionTestServlet2的时候，会把cookie中的数据按照`cookie: JESSIONID=10`的格式添加到请求头中并发送给服务器Tomcat

- sessionTestServlet2获取到请求后，从请求头中就读取cookie中的JSESSIONID值为10，然后就会到服务器内存中寻找`id:10`的session对象，如果找到了，就直接返回该对象，如果没有则新创建一个session对象

- 关闭打开浏览器后，因为浏览器的cookie已被销毁，所以就没有JESSIONID的数据，服务端获取到的session就是一个全新的session对象

![image-20220124160514423](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241605215.png)		

![image-20220124160557868](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241606623.png)	

#### Session钝化与活化

> 只要服务器是正常关闭和启动，session中的数据是可以被保存下来的

- 钝化:在服务器正常关闭后，Tomcat会自动将Session数据写入硬盘的文件中
  - 钝化的数据路径为:`项目目录\target\tomcat\work\Tomcat\localhost\项目名称\SESSIONS.ser`
- 活化: 再次启动服务器后，从文件中加载数据到Session中
  - 数据加载到Session中后，路径中的`SESSIONS.ser`文件会被删除掉

#### Session销毁

- 默认情况下，无操作，30分钟自动销毁

  - 对于这个失效时间，是可以通过配置进行修改的

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
           version="3.1">
  
      <session-config>
          <session-timeout>100</session-timeout>
      </session-config>
  </web-app>
  ```

  - 如果没有配置，默认是30分钟，默认值是在Tomcat的web.xml配置文件中写死的

    ![image-20220124161336457](https://gitee.com/image_bed/image-bed1/raw/master/img/202201241613871.png)

- 调用Session对象的invalidate()进行销毁

  - 该销毁方法一般会在用户退出的时候，需要将session销毁掉。

```java
session.invalidate();
```

### Cookie和Session小结

#### Cookie和Session的区别

* 存储位置：Cookie 是将数据存储在客户端，Session 将数据存储在服务端
* 安全性：Cookie不安全，Session安全
* 数据大小：Cookie最大3KB，Session无大小限制
* 存储时间：Cookie可以通过setMaxAge()长期存储，Session默认30分钟
* 服务器性能：Cookie不占服务器资源，Session占用服务器资源

#### 一些应用场景

* 购物车:使用Cookie来存储
* 以登录用户的名称展示:使用Session来存储
* 记住我功能:使用Cookie来存储
* 验证码:使用session来存储

#### 使用

> [午夜小学徒/Web练习 - 码云 - 开源中国 (gitee.com)](https://gitee.com/xutengHome/code/tree/master/src/main/java/com/itcast/cn/conversation)

* Cookie是用来保证用户在未登录情况下的身份识别
* Session是用来保存用户登录后的数据

## Filter

> - Filter 表示过滤器，是 JavaWeb 三大组件(Servlet、Filter、Listener)之一
> - 过滤器一般完成一些通用的操作

### 快速入门

```java
package com.itcast.cn.filter;

import javax.servlet.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * @author hyatt
 *
 * 过滤器的学习
 */
@WebFilter("/*")
public class FilterTest1 implements Filter {
    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        // 1. 在访问资源前，先执行 过滤器
        System.out.println("1. FilterTest ....");

        // 2. 放行
        chain.doFilter(request, response);
    }
}
```

### Filter执行流程

> - 放行前：对 request数据进行处理
> - 放行后：对Response 数据进行处理

![image-20220207110641330](https://gitee.com/image_bed/image-bed1/raw/master/img/202202071106803.png)

![image-20220207110658912](https://gitee.com/image_bed/image-bed1/raw/master/img/202202071107438.png)		

### Filter拦截路径配置

* 拦截具体的资源：/index.jsp：只有访问index.jsp时才会被拦截
* 目录拦截：/user/*：访问/user下的所有资源，都会被拦截
* 后缀名拦截：*.jsp：访问后缀名为jsp的资源，都会被拦截
* 拦截所有：/*：访问所有资源，都会被拦截

### 过滤器链

> 过滤器链是指在一个Web应用，可以配置多个过滤器，这多个过滤器称为过滤器链
>
> **优先级是按照过滤器类名(字符串)的自然排序。**

![image-20220207110855857](https://gitee.com/image_bed/image-bed1/raw/master/img/202202071108205.png)

## Listener

> * Listener 表示监听器，是 JavaWeb 三大组件(Servlet、Filter、Listener)之一。
>
> * 监听器可以监听就是在 `application`，`session`，`request` 三个对象创建、销毁或者往其中添加修改删除属性时自动执行代码的功能组件。
>
>   request 和 session 我们学习过。而 `application` 是 `ServletContext` 类型的对象。
>
>   `ServletContext` 代表整个web应用，在服务器启动的时候，tomcat会自动创建该对象。在服务器关闭时会自动销毁该对象。

### 分类

![image-20220207111646979](https://gitee.com/image_bed/image-bed1/raw/master/img/202202071116315.png)	

## Ajax

> Asynchronous JavaScript And XML: 异步的 JavaScript 和 XML

### 作用

#### 与服务器进行数据交换

> 通过AJAX可以给服务器发送请求，服务器将数据直接响应回给浏览器

![image-20220214144133534](https://gitee.com/image_bed/image-bed1/raw/master/img/202202141441319.png)

> 如上图，`Servlet` 调用完业务逻辑层后将数据存储到域对象中，然后跳转到指定的 `jsp` 页面，在页面上使用 `EL表达式` 和 `JSTL` 标签库进行数据的展示。
>
> 但是在AJAX 后，就可以**使用AJAX和服务器进行通信，以达到使用 HTML+AJAX来替换JSP页面**了。如下图，浏览器发送请求servlet，servlet 调用完业务逻辑层后将数据直接响应回给浏览器页面，页面使用 HTML 来进行数据展示。	

![image-20220214144228648](https://gitee.com/image_bed/image-bed1/raw/master/img/202202141442817.png)

#### 异步交互

> 在**不重新加载整个页面**的情况下，与服务器交换数据并**更新部分网页**的技术

### 同步和异步

> 同步

![image-20220214144413551](https://gitee.com/image_bed/image-bed1/raw/master/img/202202141444022.png)	

> 异步

![image-20220214144430310](https://gitee.com/image_bed/image-bed1/raw/master/img/202202141444222.png)	



### 快速入门

#### 服务端实现

```java
package com.itcast.cn.ajax.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/2/14 14:53
 * @version 1.0
 */
@WebServlet("/ajaxServlet")
public class AjaxServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write("Hello Ajax");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

#### 前端实现

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ajax测试</title>
</head>
<body>
<script>
    // 1. 创建核心对象
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    // 2. 发送请求
    xmlhttp.open("GET", "http://localhost:8080/day13_tomcat/ajaxServlet", true);
    xmlhttp.send();

    // 3. 获取响应
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            alert(this.responseText);
        }
    }

</script>
</body>
</html>
```

## Axios

### 快速入门

#### 导入文件

```html
<script src="js/axios-0.18.0.js"></script>
```

#### 后端实现

```java
package com.itcast.cn.ajax.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * @author hyatt
 */
@WebServlet("/axiosServlet")
public class AxiosServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("方法：" + request.getMethod());
        String username = request.getParameter("username");
        response.getWriter().write("Hello " + username);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
```

#### 前端实现

> - `method` 属性：用来设置请求方式的。取值为 `get` 或者 `post`。
> - `url` 属性：用来书写请求的资源路径。如果是 `get` 请求，需要将请求参数拼接到路径的后面，格式为： `url?参数名=参数值&参数名2=参数值2`。
> - `data` 属性：作为请求体被发送的数据。也就是说如果是 `post` 请求的话，数据需要作为 `data` 属性的值。
> - `then()` 需要传递一个匿名函数。我们将 `then()` 中传递的匿名函数称为 ==回调函数==，意思是该匿名函数在发送请求时不会被调用，而是在成功响应后调用的函数。而该回调函数中的 `resp` 参数是对响应的数据进行封装的对象，通过 `resp.data` 可以获取到响应的数据。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="js/axios-0.18.0.js"></script>
</head>
<body>

<script>
    // 发送get请求
    // axios({
    //     method: "get",
    //     url: "http://localhost:8080/day13_tomcat/axiosServlet?username=zhangsan"
    // }).then(function (resp) {
    //     alert(resp.data);
    // });
    // 发送post请求
    axios({
        method: "post",
        url: "http://localhost:8080/day13_tomcat/axiosServlet",
        data: "username=lisi"
    }).then(function (resp) {
        alert(resp.data);
    });
</script>

</body>
</html>
```

### 请求方法别名

* `get` 请求 ： `axios.get(url[,config])`
* `delete` 请求 ： `axios.delete(url[,config])`
* `head` 请求 ： `axios.head(url[,config])`
* `options` 请求 ： `axios.option(url[,config])`
* `post` 请求：`axios.post(url[,data[,config])`
* `put` 请求：`axios.put(url[,data[,config])`
* `patch` 请求：`axios.patch(url[,data[,config])`

> 例如可以使用别名修改上面的html

```html
axios.get("http://localhost:8080/day13_tomcat/axiosServlet?username=zhangsan").then(function (resp) {
    alert(resp.data);
});
axios.post("http://localhost:8080/day13_tomcat/axiosServlet", "username=lisi").then(function (resp) {
    alert(resp.data);
});
```

## Json

### Json字符串和Json对象的转换

> axios可以自动将json对象转换成json字符串，然后作为参数传给后端

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Json</title>
</head>
<body>
  <script>
      //1. 定义JSON字符串
      var jsonStr = '{"name":"zhangsan","age":23,"addr":["北京","上海","西安"]}'
      alert(jsonStr);

      //2. 将 JSON 字符串转为 JS 对象
      let jsObject = JSON.parse(jsonStr);
      alert(jsObject)
      alert(jsObject.name)

      //3. 将 JS 对象转换为 JSON 字符串
      let jsonStr2 = JSON.stringify(jsObject);
      alert(jsonStr2)
  </script>
</body>
</html>
```

### JSON串和Java对象的相互转换

####  Fastjson的使用

> `Fastjson` 是阿里巴巴提供的一个Java语言编写的高性能功能完善的 `JSON` 库，是目前Java语言中最快的 `JSON` 库，可以实现 `Java` 对象和 `JSON` 字符串的相互转换。

##### 导入依赖

```
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.62</version>
</dependency>
```

##### Java对象转JSON

```java
String jsonStr = JSON.toJSONString(obj);
```

##### JSON字符串转Java对象

```java
User user = JSON.parseObject(jsonStr, User.class);
```

##### 代码

```java
import com.alibaba.fastjson.JSON;
import com.itcast.cn.json.dao.User;
import org.junit.Test;

/**
 * <p>FastJson的使用</p>
 *
 * @author hyatt 2022/2/14 15:44
 * @version 1.0
 */
public class FastJsonTest {

    /**
     * 测试Json转对象
     */
    @Test
    public void testJsonToObj() {
        User u = JSON.parseObject("{\"id\":1,\"password\":\"123\",\"username\":\"zhangsan\"}", User.class);
        System.out.println(u);
    }

    /**
     * 测试对象转json
     */
    @Test
    public void testObjToJson() {
        User user = new User();
        user.setId(1);
        user.setUsername("zhangsan");
        user.setPassword("123");

        String jsonString = JSON.toJSONString(user);
        System.out.println(jsonString);//{"id":1,"password":"123","username":"zhangsan"}
    }

}
```

## Vue

### 快速入门

#### 新建 HTML 页面，引入 Vue.js文件

```javascript
<script src="js/vue.js"></script>
```

#### 在JS代码区域，创建Vue核心对象，进行数据绑定

> * `el` ： 用来指定哪些标签受 Vue 管理。 该属性取值 `#app` 中的 `app` 需要是受管理的标签的id属性值
> * `data` ：用来定义数据模型
> * `methods` ：用来定义函数。

```vue
new Vue({
    el: "#app",
    data() {
        return {
            username: ""
        }
    }
});
```

#### 编写视图

> `{{}}` 是 Vue 中定义的 `插值表达式` ，在里面写数据模型，到时候会将该模型的数据值展示在这个位置

```html
<div id="app">
    <input name="username" v-model="username" >
    {{username}}
</div>
```

#### 全部代码

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vue快速入门</title>
    <script src="js/vue.js"></script>
</head>
<body>
<div id="app">
    <input type="text" name="username" v-model="username">
    {{username}}
</div>

<script>
    new Vue({
        el: "#app",
        data() {
            return {
                username: ""
            }
        }
    })
</script>
</body>
</html>
```

### IDEA支持Vue提示

> 安装Vue.js插件，然后重启IDEA

### Vue指令

| **指令**  | **作用**                                            |
| --------- | --------------------------------------------------- |
| v-bind    | 为HTML标签绑定属性值，如设置  href , css样式等      |
| v-model   | 在表单元素上创建双向数据绑定                        |
| v-on      | 为HTML标签绑定事件                                  |
| v-if      | 条件性的渲染某元素，判定为true时渲染,否则不渲染     |
| v-else    |                                                     |
| v-else-if |                                                     |
| v-show    | 根据条件展示某元素，区别在于切换的是display属性的值 |
| v-for     | 列表渲染，遍历容器的元素或者对象的属性              |

#### v-bind

> 为HTML标签绑定属性值，如设置  href , css样式等

```html
<a v-bind:href="url">百度一下</a>
<!--
	v-bind 可以省略
-->
<a :href="url">百度一下</a>
```

#### v-model

> 该指令可以给表单项标签绑定模型数据。这样就能实现双向绑定效果

```html
<input name="username" v-model="username">
```

#### v-on 指令

> 为HTML标签绑定事件
>
> `v-on:` 后面的事件名称是之前原生事件属性名去掉on
>
> * 单击事件 ： 事件属性名是 onclick，而在vue中使用是 `v-on:click`
> * 失去焦点事件：事件属性名是 onblur，而在vue中使用时 `v-on:blur`

```html
<input type="button" value="一个按钮" v-on:click="show()">
<!-- 可以将v-on替换成@ -->
<input type="button" value="一个按钮" @click="show()">

<script>
    new Vue({
    el: "#app",
    methods: {
        show(){
            alert("我被点了");
        }
    }
});
</scripts>
```

#### 条件判断指令

| **指令**                | **作用**                                            |
| ----------------------- | --------------------------------------------------- |
| v-if，v-else，v-else-if | 条件性的渲染某元素，判定为true时渲染,否则不渲染     |
| v-show                  | 根据条件展示某元素，区别在于切换的是display属性的值 |

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vue条件判断</title>
    <script src="js/vue.js"></script>
</head>
<body>
<div id="app">
    <div v-if="count == 3">div1</div>
    <div v-else-if="count == 4">div2</div>
    <div v-else>div3</div>
    <hr>
    <div v-show="count==3">div4</div>
    <input v-model="count">
</div>

<script>
    //1. 创建Vue核心对象
    new Vue({
        el: "#app",
        data() {
            return {
                count: 3
            }
        }
    });
</script>
</body>
</html>
```

#### v-for 指令

> 列表渲染，遍历容器的元素或者对象的属性

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vue循环</title>
    <script src="js/vue.js"></script>
</head>
<body>
<div id="app">
    <div v-for="addr in addrs">
        {{addr}} <br>
    </div>

    <hr>
    <div v-for="(addr,i) in addrs">
        {{i + 1}}--{{addr}} <br>
    </div>
</div>

<script>
    //1. 创建Vue核心对象
    new Vue({
        el: "#app",
        data() {
            return {
                addrs: ["北京", "上海", "西安"]
            }
        }
    });
</script>
</body>
</html>
```

### 声明周期

> 生命周期的八个阶段：每触发一个生命周期事件，会自动执行一个生命周期方法，这些生命周期方法也被称为钩子方法。

![image-20220215140923212](https://gitee.com/image_bed/image-bed1/raw/master/img/202202151409480.png)	

> 下图是 Vue 官网提供的从创建 Vue 到效果 Vue 对象的整个过程及各个阶段对应的钩子函数

![image-20220215140943252](https://gitee.com/image_bed/image-bed1/raw/master/img/202202151409525.png)	

## Element

### 快速入门

> 创建页面，并在页面引入Element 的css、js文件 和 Vue.js

```html
<!-- 引入vue资源  -->
<script src="js/vue.js"></script>
<!-- ElementUI资源  -->
<script src="element-ui/lib/index.js"></script>
<link rel="stylesheet" href="element-ui/lib/theme-chalk/index.css">
```

> 创建Vue核心对象. Element 是基于 Vue 的，所以使用Element时必须要创建 Vue 对象

```javascript
<script>
    new Vue({
        el: '#app',
        data() {
            return {

            }
        }
    })
</script>
```

> 官网复制Element组件代码

![image-20220215141838895](https://gitee.com/image_bed/image-bed1/raw/master/img/202202151418046.png)	

> 全部代码

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!-- 引入vue资源  -->
    <script src="js/vue.js"></script>
    <!-- ElementUI资源  -->
    <script src="element-ui/lib/index.js"></script>
    <link rel="stylesheet" href="element-ui/lib/theme-chalk/index.css">
</head>
<body>
    <div id="app">
        <el-row>
            <el-button>默认按钮</el-button>
            <el-button type="primary">主要按钮</el-button>
            <el-button type="success">成功按钮</el-button>
            <el-button type="info">信息按钮</el-button>
            <el-button type="warning">警告按钮</el-button>
            <el-button type="danger">危险按钮</el-button>
        </el-row>

        <el-row>
            <el-button plain>朴素按钮</el-button>
            <el-button type="primary" plain>主要按钮</el-button>
            <el-button type="success" plain>成功按钮</el-button>
            <el-button type="info" plain>信息按钮</el-button>
            <el-button type="warning" plain>警告按钮</el-button>
            <el-button type="danger" plain>危险按钮</el-button>
        </el-row>

        <el-row>
            <el-button round>圆角按钮</el-button>
            <el-button type="primary" round>主要按钮</el-button>
            <el-button type="success" round>成功按钮</el-button>
            <el-button type="info" round>信息按钮</el-button>
            <el-button type="warning" round>警告按钮</el-button>
            <el-button type="danger" round>危险按钮</el-button>
        </el-row>

        <el-row>
            <el-button icon="el-icon-search" circle></el-button>
            <el-button type="primary" icon="el-icon-edit" circle></el-button>
            <el-button type="success" icon="el-icon-check" circle></el-button>
            <el-button type="info" icon="el-icon-message" circle></el-button>
            <el-button type="warning" icon="el-icon-star-off" circle></el-button>
            <el-button type="danger" icon="el-icon-delete" circle></el-button>
        </el-row>
    </div>
    <script>
        new Vue({
            el: '#app',
            data() {
                return {

                }
            }
        })
    </script>
</body>
</html>
```

### 案例

> https://www.jianguoyun.com/p/DUOb_cwQn8jYCBiFxKwE 

