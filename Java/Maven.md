# Maven

## Maven基础

### Maven简介

> - Maven本质上是一个项目管理工具，将项目开发和管理过程抽象成一个项目对象模型(pom(**继承、聚合**))
> - POM(Project Object Model)：项目对象模型

![image-20220218174346172](https://gitee.com/image_bed/image-bed1/raw/master/img/202202181743759.png)	

### Maven的作用

> 项目构建：提供标准的、跨平台的自动化项目构建方式
>
> 依赖管理：方便快捷的管理项目依赖的资源(Jar包)，避免资源之间的版本冲突问题
>
> 统一开发结构：提供标准的、统一的项目结构

![image-20220218174558801](https://gitee.com/image_bed/image-bed1/raw/master/img/202202181746255.png)	

### Maven的下载安装

> 官网下载安装：http://maven.apache.org/download.cgi 

### Maven环境变量配置

> **注意：Maven依赖java，所以需要先配好jdk**

![image-20220218175104452](https://gitee.com/image_bed/image-bed1/raw/master/img/202202181751264.png)	

![image-20220218175148512](https://gitee.com/image_bed/image-bed1/raw/master/img/202202181751996.png)	

> 配置好之后，运行 mvn

### Maven的基础概念

#### 仓库

> 用于存储资源，包含各种jar包

![image-20220224154449101](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241544473.png)	

##### 仓库分类

- 本地仓库：自己电脑上存储资源的仓库，连接远程仓库获取资源
- 远程仓库：非本机电脑上的仓库，为本地仓库提供资源
  - 中央仓库：maven团队维护，存储所有资源的仓库
  - 私服：部门/公司范围内存储资源的仓库，从中央仓库获取资源

##### 私服的作用

- 保存具有版权的资源，包含购买或自主研发的jar
  - 中央仓库的jar都是开源的，不能存储具有版权的资源
- 一定范围内共享资源，仅对内部开放，不对外共享

#### 坐标

> Maven中的坐标用于描述仓库中资源的位置：[Central Repository: (maven.org)](https://repo1.maven.org/maven2/)

##### 坐标的主要组成

- groupId: 定义当前Maven项目隶属组织名称
- artifactId：定义当前Maven项目的名称
- version: 定义当前项目的版本号

##### Maven坐标的作用

> 使用唯一的标识，唯一性定义资源位置，通过该标识可以将资源的识别与下载工作交由机器完成

### 本地仓库配置

> Maven启动后，会自动保存下载的资源到本地仓库

#### 修改仓库的默认位置

> 修改Maven的配置文件：Maven的安装路径下的conf/settings.xml

![image-20211108164159989](Maven.assets/image-20211108164159989.png)

### 远程仓库配置

> Maven默认连接的仓库位置：maven-model-builder-3.6.1.jar\org\apache\maven\model

```xml
<repositories>
    <repository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
      <layout>default</layout>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>
```

### 镜像仓库配置

> 修改Maven的配置文件：Maven的安装路径下的conf/settings.xml
>
> - id: 此镜像的唯一标识符，用来区分不同的mirror元素
> - mirrorOf: 对哪种仓库进行镜像，简单来说就是替代那个仓库
> - name：镜像名称
> - url：镜像url

![image-20211108164444561](Maven.assets/image-20211108164444561.png)	

### 依赖管理

> 依赖是指当前项目运行所需的jar，一个项目可以设置多个依赖

```xml
<!--设置当前项目所依赖的所有jar-->
<dependencies>
    <!-- 设置具体的依赖 -->
	<dependency>
      <!-- 依赖所属群组id -->
	  <groupId>junit</groupId>
      <!-- 依赖所属项目id -->
	  <artifactId>junit</artifactId>
      <!-- 依赖版本 -->
	  <version>4.11</version>
	</dependency>
</dependencies>
```

#### 依赖传递

> 依赖具有传递性

- 直接依赖
  - 在当前项目中通过依赖配置建立的依赖关系
- 间接依赖
  - 如果资源依赖其他资源，当前项目间接依赖其他资源

![image-20220224162657914](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241627703.png)	

#### 依赖传递冲突问题

- 路径优先：当依赖中出现相同的资源的时候，层级越深，优先级越低，层级越浅，优先级越高
- 声明优先：当资源在相同层级被依赖时，配置顺序靠前的覆盖配置顺序靠后的
- 特殊优先：当同级配置了相同资源的不同版本，后配置的覆盖先配置的

![image-20220224170248333](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241702533.png)	

#### 可选依赖

> 对外隐藏当前所依赖的资源 -- 不透明(还是存在的，只是看不到)

```xml
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<version>4.12</version>
	<optional>true</optional>
</dependency>
```

#### 排除依赖

> 主动断开依赖的资源，被排除的资源无需指定版本 --- 不需要

```xml
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<version>4.12</version>
	<exclusions>
		<exclusion>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-core</artifactId>
		</exclusion>
	</exclusions>
</dependency>
```

#### 依赖范围

> 依赖的jar默认情况下可以在项目的任何地方使用，可以通过scope标签设定其作用范围

##### 作用范围

- 主程序范围有效(main文件夹范围内)
- 测试程序范围有效(test文件夹范围内)
- 是否参与打包(package指令范围内)

| scope         | 主代码 | 测试代码 | 打包 | 范例        |
| ------------- | ------ | -------- | ---- | ----------- |
| compile(默认) | Y      | Y        | Y    | log4j       |
| test          |        | Y        |      | junit       |
| provided      | Y      | Y        |      | servlet-api |
| runtime       |        |          | Y    | jdbc        |

##### 依赖范围的传递性

> 带有依赖范围的资源在进行传递时，作用范围将收到影响

![image-20220224172433495](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241724751.png)	

### 生命周期和插件

> Maven构建生命周期描述的是一次构建过程中经历了多少个时间

![image-20220224172539093](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241725859.png)	

#### 项目构建生命周期

- clean: 清理工作
- default：核心工作，例如编译、测试、打包、部署等
- site：产生报告，发布站点等

##### clean生命周期

- pre-clean: 执行一些需要在clean之前完成的工作
- clean: 移除所有上一次构建生成的文件
- post-clean:执行一些需要在clean之后立刻完成的工作

##### default构建生命周期

![image-20220224172925449](https://gitee.com/image_bed/image-bed1/raw/master/img/202202241729266.png)	

##### site构建生命周期

- pre-site: 执行一些需要在生成站点文档之前的工作
- site: 生成项目的站点文档
- post-site:执行一些需要在生成站点文档之后的工作，并且为部署做准备
- site-deploy:将生成的站点文档部署到特定的服务器上

#### 插件

- 插件与生命周期内的阶段绑定，在执行到对应的生命周期时执行对应的插件功能
- 默认maven在各个生命周期上绑定有预设的功能
- 通过插件可以自定义其他功能

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>2.2.1</version>
                <executions>
                    <execution>
                        <goals>
                        	<goal>jar</goal>
                        	<goal>test-jar</goal>
                        </goals>
                        <!-- 当执行到  generate-test-resources 这个生命周期的时候，才会去打jar包 和 test-jar-->
                        <phase>generate-test-resources</phase>
                    </execution>
                </executions>
        </plugin>
    </plugins>
</build>
```

## Maven高级

### 分模块开发与设计

> 将一个项目中的模块拆分出来，分别开发

![image-20220228101015373](https://gitee.com/image_bed/image-bed1/raw/master/img/202202281010523.png)	

### 聚合

> 聚合用于快速构建maven工程，一次性构建多个项目模块

#### 制作方式

> 注意事项：参与聚合操作的模块最终执行顺序与模块之间的依赖关系有关，与配置顺序无关

> 创建一个空模块，打包类型定义为pom

```xml
<packaging>pom</packaging>
```

> 定义当前模块进行构建操作时关联的其他模块名称
>
> 同时编译其它模块，就可以发现**是否有其他模块引用的错误**

```xml
<modules>
    <module>../ssm_controller</module>
    <module>../ssm_service</module>
    <module>../ssm_dao</module>
    <module>../ssm_pojo</module>
</modules>
```

### 继承

#### 作用

> 通过继承可以实现在子工程中沿用父工程中的配置

maven中的继承和java中的继承相似，在子工程中配置继承关系

#### 制作方式

- 在子工程中声明其父工程坐标与对应的位置

```xml
<!---->
<parent>
    <groupId>com.itheima</groupId>
    <artifactId>ssm</artifactId>
    <version>1.0-SNAPSHOT</version>
    <!--pom-->
    <relativePath>../ssm/pom.xml</relativePath>
</parent>
```

- 在父工程中定义依赖管理

```xml
<!---->
<dependencyManagement>
    <!---->
    <dependencies>
        <!--spring-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.1.9.RELEASE</version>
        </dependency>
    <dependencies>
<dependencyManagement>
```

- 在子工程中定义依赖关系，无需声明依赖版本，版本参考父工程中依赖的版本

```xml
<dependencies>
    <!--spring-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
    </dependency>
</dependencies>
```

#### 继承的资源

![image-20220228104207351](https://gitee.com/image_bed/image-bed1/raw/master/img/202202281042802.png)	

### 继承与聚合

- 作用
  - 聚合用于快速构建项目
  - 继承用于快速配置
- 相同点
  - 聚合与继承的pom.xml文件打包方式均为pom，可以将两种关系制作到同一个pom文件中
  - 聚合与继承均属于设计型模块，并无实际的模块内容
- 不同点
  - 聚合是在当前模块中配置关系，可以感知到参与聚合的模块有哪些
  - 继承是在子模块中配置关系，父模块无法感知哪些子模块继承了自己

### 属性

#### 属性类别

- 自定义属性
- 内置属性
- Setting属性
- Java系统属性
- 环境变量属性

#### 自定义属性

> 等同于定义变量，方便统一维护

##### 格式

```xml
<!-- 自定义属性 -->
<properties>
    <spring.version>5.1.9.RELEASE</spring.version>
    <junit.version>4.12</junit.version>
</properties>
```

##### 调用格式

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>${spring.version}</version>
</dependency>
```

#### 内置属性

> 使用Maven的内置属性，快速配置

##### 调用格式

```xml
${basedir}
${version}
```

#### Setting属性

> 使用Maven配置文件setting.xml中的标签属性，用于动态配置

##### 调用格式

```xml
${settings.localRepository}
```

#### Java系统属性

> 读取java系统属性

##### 调用格式

```xml
${user.home}
```

##### 系统属性查询方式

```xml
mvn help:system
```

#### 环境变量属性

> 使用Maven配置文件setting.xml中的标签属性，用于动态配置

##### 调用格式

```xml
${env.JAVA_HOME}
```

##### 环境变量属性查询方式

```xml
mvn help:system
```

### 版本管理

#### 工程版本

##### SNAPSHOT(快照版本)

- 项目开发过程中，为方便团队成员合作，解决模块间相互依赖和时时更新的问题，开发者对每个模块进行构建的时候，输出的临时性版本叫快照版本
- 快照版本会随着开发的进展不断更新

##### RELEASE(发布版本)

- 项目开发到进入里程碑后，向团队外部发布较为**稳定的版本**，这种版本所对应的构件文件是稳定的，即便进行功能的后续开发，也**不会改变当前发布版本内容**，这种版本为**发布版本**

#### 工程版本号约定

> 5.1.9.RELEASE

##### 约定规范

- <主版本>.<次版本>.<增量版本>.<里程碑版本>.
- 主版本：表示项目重大结构的变更，如：spring5相较于spring4的迭代
- 次版本：表示有较大的功能增加和变化，或者全面系统的修复漏洞
- 增量版本：表示有重大漏洞的修复
- 里程碑版本：表明一个版本的里程碑(版本内部)。这样的版本同下一个正式版本相比，相对来说不是很稳定，有待更多的测试

### 资源配置

> 在任意配置文件中加载pom文件中定义的属性

#### 调用格式

```properties
${jdbc.url}	
```

#### 开启配置文件加载pom属性

```xml
<!-- 配置资源文件对应的信息 -->
<resources>
    <resource>
        <!--设定配置文件对应的位置目录，支持使用属性动态设定路径-->
        <directory>${project.basedir}/src/main/resources</directory>
        <!--开启对配置文件的资源加载过滤-->
        <filtering>true</filtering>
    </resource>
</resources>
```

### 多环境开发配置

```xml
<!-- 创建多环境 -->
<profiles>
    <!-- 定义具体的环境：生产环境 -->
    <profile>
        <!-- 定义环境对应的唯一名称 -->
        <id>pro_env</id>
        <!-- 定义环境中专用的属性值 -->
        <properties>
        	<jdbc.url>jdbc:mysql://127.1.1.1:3306/ssm_db</jdbc.url>
        </properties>
        <!-- 设置默认使用这个环境 -->
        <activation>
        	<activeByDefault>true</activeByDefault>
        </activation>
    </profile>
    <!-- 定义具体的环境：开发环境  -->
    <profile>
    	<id>dev_env</id>
        ……
    </profile>
</profiles>
```

#### 加载指定环境

##### 调用格式

```java
mvn 指令 –P 环境定义id // mvn install –P pro_env
```

### 跳过测试

#### 使用指令跳过

> 指令的声明周期必须包含测试环节，例如：install

```java
mvn 指令 –D skipTests
```

#### 使用界面跳过

![image-20220228173426310](https://gitee.com/image_bed/image-bed1/raw/master/img/202202281734862.png)	

#### 使用配置跳过

> 不建议使用

```xml
<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.22.1</version>
    <configuration>
    	<!-- 设置跳过测试 -->
        <skipTests>true</skipTests>
        <!-- 包含指定的测试用例 -->
        <includes> 
        	<include>**/User*Test.java</include>
        </includes>
        <!-- 排除指定的测试用例 -->
        <excludes>
        	<exclude>**/User*TestCase.java</exclude>
        </excludes>
    </configuration>
</plugin>
```

### 私服

#### Nexus私服安装

> 从网上下载zip版本后，直接解压即可

![image-20220301102145957](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011021151.png)	

#### 启动

> 进入bin目录下，执行命令：nexus /run nexus

![image-20220301102515829](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011025306.png)	

#### 配置

> 端口修改

![image-20220301102555510](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011025655.png)	

> 服务器内存配置等

![image-20220301102626622](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011026696.png)	

#### 仓库分类

- 宿主仓库(hosted)
  - 保存无法从中央仓库中获取的资源
    - 自主研发
    - 第三方非开源的项目
- 代理仓库(proxy)
  - 代理远程仓库，通过nexus访问其他公共仓库，例如中央仓库
- 仓库组
  - 将若干个仓库组成一个群组，简化配置
  - 仓库组不能保存资源，属于设计型仓库

![image-20220301103555756](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011035741.png)	

#### 创建宿主仓库

![image-20220301105745804](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011057847.png)	

![image-20220301105813902](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011058727.png)	

#### 添加至项目组

![image-20220301105939258](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011059213.png)	

![image-20220301110002730](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011100406.png)	

#### idea发布至私服

> 原理

![image-20220301110112380](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011101309.png)	

##### 访问私服配置(本地仓库访问私服)

> 配置本地仓库访问私服的权限(setting.xml)

```xml
<servers>
    <!-- release版本的私服权限配置 -->
	<server>
		<id>itcast-release</id>
		<username>admin</username>
		<password>admin</password>
	</server>
	<!-- snapshots版本的私服权限配置 -->
	<server>
		<id>itcast-snapshots</id>
		<username>admin</username>
		<password>admin</password>
	</server>
</servers>
```

> 配置本地仓库来源(setting.xml)

```xml
<mirrors>
	 <mirror>      
      <id>nexus-aliyun</id>    
      <name>nexus-aliyun</name>  
      <url>http://maven.aliyun.com/nexus/content/groups/public</url>    
      <mirrorOf>central</mirrorOf>      
    </mirror>
	
	<!-- 中央仓库的数据从上面的阿里云的私服中获取，其他的从下面的私服中获取 -->
	<mirror>      
      <id>nexus-itcast</id>    
	  <mirrorOf>*</mirrorOf>      
      <url>http://localhost:8081/repository/maven-public/</url>    
    </mirror>
</mirrors>
```

> 上面配置的路径是这个

![image-20220301111135802](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011111773.png)	

##### 访问私服配置(项目工程访问私服)

> 配置当前项目访问私服上传资源的保存位置(pom.xml)

```xml
<distributionManagement>
  <!-- release版本 -->
  <repository>
    <!--通过id名称，去到setting.xml中找到对应的用户名和密码-->
    <id>itcast-release</id>
    <url>http://localhost:8081/repository/itcast-release/</url>
  </repository>
  <!-- snapshots版本 -->
  <snapshotRepository>
    <id>itcast-snapshots</id>
    <url>http://localhost:8081/repository/itcast-snapshots/</url>
  </snapshotRepository>
</distributionManagement>
```

![image-20220301111327512](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011113389.png)	

> 配置完成后，执行命令 mvn deploy

![image-20220301111437161](https://gitee.com/image_bed/image-bed1/raw/master/img/202203011114221.png)	
