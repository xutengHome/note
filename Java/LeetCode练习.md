# LeetCode

## 简单

### 有效的括号

> 栈的使用
>
> [20. 有效的括号 - 力扣（LeetCode） (leetcode-cn.com)](https://leetcode-cn.com/problems/valid-parentheses/)

```java
package com.leetcode.problem.simple.day02;

import org.junit.Test;

import java.util.Stack;

/**
 * <p>有效的括号</p>
 *
 * <a href="https://leetcode-cn.com/problems/valid-parentheses/">有效的括号<a/>
 *
 * 采用栈的方式：
 *      如果没有匹配则入栈，否则则出栈并且新的括号也不入栈
 *
 * @author hyatt 2022/1/18 10:13
 * @version 1.0
 */
public class IsValid {
    public boolean isValid(String s) {
        Stack stack = new Stack();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '(':
                    stack.push(c);
                    break;
                case ')':
                    // 为了防止空栈的情况下 stack.peek() 方法报错
                    if (!stack.isEmpty() && stack.peek().equals('(')) {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
                case '[':
                    stack.push(c);
                    break;
                case ']':
                    if (!stack.isEmpty() && stack.peek().equals('[')) {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
                case '{':
                    stack.push(c);
                    break;
                case '}':
                    if (!stack.isEmpty() && stack.peek().equals('{')) {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
                default:
                    return false;
            }
        }
        return stack.isEmpty();
    }
}
```

![20.gif](https://gitee.com/image_bed/image-bed1/raw/master/img/202201181406623.gif)	

### 合并两个有序列表

> 链表的使用

```java
package com.leetcode.problem.simple.day03;

/**
 * <p>合并两个有序链表</p>
 *
 * <a href="https://leetcode-cn.com/problems/merge-two-sorted-lists/">合并两个有序链表<a/>
 *
 * @author hyatt 2022/1/19 9:51
 * @version 1.0
 */
public class MergeTwoLists {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        return sortListNode(concatListNode(list1, list2));
    }

    /**
     * 连接两个链表
     * 将第二个链表插入到第一个链表的尾部
     *
     * @param list1
     * @param list2
     * @return
     */
    public ListNode concatListNode(ListNode list1, ListNode list2) {
        if (list1 == null) {
            return list2;
        }

        if (list1.next == null) {
            list1.next = list2;
            return list1;
        }

        ListNode head = list1;
        ListNode node = head.next;
        while (node != null) {
            if (node.next == null) {
                node.next = list2;
                break;
            }
            node = node.next;
        }
        return head;
    }

    /**
     * 升序排序
     *
     * @param listNode
     * @return
     */
    public ListNode sortListNode(ListNode listNode) {

        if (listNode == null || listNode.next == null) {
            return listNode;
        }

        ListNode pre = listNode;
        ListNode cur = listNode.next;

        // 辅助节点
        ListNode tempNode = new ListNode(-1);
        tempNode.next = listNode;

        while (cur != null) {
            if (cur.val < pre.val) {
                // 想将cur节点从列表中删除, 然后再把节点插入到合适的位置
                pre.next = cur.next;

                //从前往后找到l2.val>cur.val,然后把cur节点插入到l1和l2之间
                ListNode l1 = tempNode;
                ListNode l2 = tempNode.next;
                while (cur.val > l2.val) {
                    l1 = l2;
                    l2 = l2.next;
                }

                // 插入合适的位置
                cur.next = l2;
                l1.next = cur;

                // 重新调整cur和pre
                cur = pre.next;
            } else {
                pre = cur;
                cur = cur.next;
            }
        }
        return tempNode.next;
    }

    /**
     * 初始化一个ListNode
     *
     * @param array
     * @return
     */
    public static ListNode initLinkedList(int[] array) {
        ListNode head = null, cur = null;
        for (int i = 0; i < array.length; i++) {
            ListNode newNode = new ListNode(array[i]);
            newNode.next = null;
            if (i == 0) {//初始化头节点
                head = newNode;
                cur = head;
            } else {//继续一个个往后插入节点
                cur.next = newNode;
                cur = newNode;
            }
        }
        return head;
    }
}
```

```java
package com.leetcode.problem.simple.day03;

/**
 * <p>类描述</p>
 *
 * @author hyatt 2022/1/19 9:54
 * @version 1.0
 */
public class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                ", next=" + next +
                '}';
    }
}
```

### 删除有序数组中的重复项

> 双数组

```java
package com.leetcode.problem.simple.day04;

/**
 * <p>删除有序数组中的重复项</p>
 *
 * <a href="https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/">删除有序数组中的重复项<a/>
 * <p>
 * 解法: 双指针
 *      1. 当 nums[p] = nums[q] q++
 *      2. 当 nums[p] != nums[q]
 *          1. 将q位置的元素赋值给p+1位置
 *          2. 将q向后移动以为，P向后移动一位
 *		3. 当q大于数组长度的时候结束循环
 *
 * @author hyatt 2022/1/21 10:07
 * @version 1.0
 */
public class RemoveDuplicates {
    public int removeDuplicates(int[] nums) {
        int p = 0;
        int q = p + 1;
        while (q < nums.length) {
            if(nums[p] == nums[q]) {
                q++;
            } else {
                nums[++p]=nums[q++];
            }
        }
        return ++p;
    }
}
```

![1.png](https://gitee.com/image_bed/image-bed1/raw/master/img/202201211049817.png)	
