# JasperReport

> JasperReport是一个强大、灵活的报表生成工具，能够展示丰富的页面内容，并将之转换成PDF，HTML，或者XML格式

## 快速入门

### 模板开发

#### 新建报表

![image-20211229170838076](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291712051.png)	

> 默认A4纸

![image-20211229171218241](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291712915.png)	

![image-20211229171333437](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291713938.png)	

#### 数据源设置

> 新建一个数据源：File -> New -> Data Adapter

![image-20211229171827817](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291718842.png)	

![image-20211229172011664](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291720864.png)	

> 切换到Driver Classpath，选好文件后，test一下，测试成功后，点击finish

![image-20211229172259796](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291723682.png)	

#### 插入一条主sql

![image-20211229172625787](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291726322.png)	

#### **头行结构报表设计**

> 在介绍如何制作头行报表模板之前，先简单介绍一下编辑界面的几个组成部分
>
> - **Title**: 即标题，是一个报表的最顶部的显示内容。一个报表就只有一个Title，也就是打印一个报表时，Title这个区域的内容只会显示一次。一般在Title区域显示文本标题；
> - **Page Header和Page Footer**：顾名思义，Page Header和Page Footer即为页眉页脚，是和报表显示页数有关系的，Page Header和Page Footer的内容会在报表每一页都显示出
> - **Column Header和Column Footer**：Column为列的意思，我们习惯称一个表格的所以Column Header和Column Footer分别为列头和列脚，这两个区域中的内容也会在报表的每页中都显示出来
> - **Summary**:Summary即合计区域，需要注意的是Summary区域的内容只在报表最后一页显示

![image-20211229172840359](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291728048.png)	

##### 参数定义

> 在项目目录下面的parameter，右键create parameter，在右边定义参数名字和参数的表达式，一般参数的表达式：**$P{参数名}**因为jaspersoft支持的是groovy语言

![image-20211229174141402](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291741058.png)		

##### 查询头数据字段

> 右击模板名称--->Dataset and Query

![image-20211229174305709](https://gitee.com/image_bed/image-bed1/raw/master/img/202112291743698.png)	

```mysql
SELECT ooh.header_id
      ,ooh.order_number
      ,ooh.company_id
      ,hoc.company_name
      ,ooh.customer_id
      ,hac.customer_name
      ,ooh.order_date
      ,ooh.order_status
      ,scvt.description order_status_desc
      ,(SELECT SUM(ool.ordered_quantity * ool.unit_selling_price)
        FROM   hap_om_order_lines ool
        WHERE  1 = 1
        AND    ool.header_id = ooh.header_id) order_amount
FROM   hap_om_order_headers ooh
      ,hap_org_companys     hoc
      ,hap_ar_customers     hac
      ,sys_code_b           scb
      ,sys_code_tl          sct
      ,sys_code_value_b     scvb
      ,sys_code_value_tl    scvt
WHERE  1 = 1
AND    ooh.company_id = hoc.company_id
AND    ooh.customer_id = hac.customer_id
AND    scb.code_id = sct.code_id
AND    scb.code_id = scvb.code_id
AND    scvb.code_value_id = scvt.code_value_id
AND    scb.code = 'XXOM_ORDER_STATUS'
AND    scvt.lang = sct.lang
AND    scvt.lang = 'zh_CN'
AND    scvb.value = ooh.order_status
and    ooh.header_id  = $P{poHeaderId} 
GROUP  BY ooh.order_number
         ,hoc.company_name
         ,hac.customer_name
         ,ooh.order_date
         ,scvt.description
ORDER  BY hoc.company_name
         ,ooh.order_number
```

##### 将字段拖到对应的位置

> field下面的字段可以直接当做text控件拖拽，此外jaspersoft支持的是groovy语言，如果字段为null，会把null直接显示出来，可以直接在字段表达式里面直接做判空处理
>
> 例：($F{order_amount}==null) ? "" : $F{order_amount}

![image-20211230103342091](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301033842.png)	

![image-20211230101421033](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301014020.png)		

##### 定义行数据

> 在组件面板把table拖到detail中,会弹框选择dataset作为table的数据来源，可以选择已存在的dataset，也可以选择新建一个：右键项目名称àCreate Dataset，点next继续，然后选择dataset中的所需字段作为行数据

![image-20211230102247855](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301022098.png)

![image-20211230103716873](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301037138.png)	

![image-20211230103746305](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301037901.png)	

![image-20211230104904807](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301049603.png)	

##### 头行关联

> 设置Table的属性，Datasetà>Parameter>add,点击添加参数，选择行dataset已经定义好的参数，参数表达式，选择头行关联字段作为关联条件，这里的例子是选择头数据的HEADER_ID，设置完成后行数据会根据关联的字段去限制数据显示。

> 定义dataSet的参数

![image-20211230105649774](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301056635.png)	

![image-20211230105814018](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301058914.png)	

> 关联头行

![image-20211230105845608](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301058577.png)

![image-20211230110047578](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301100170.png)	

#### 编译生成Jasper文件

> 完成编辑模板文件后，点击编译按钮生成Jasper文件

![image-20211230112051473](https://gitee.com/image_bed/image-bed1/raw/master/img/202201082121440.png)	

![image-20211230112156622](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301121571.png)	

#### 注意

> 凡是涉及到中文字体的地方都需要设置,否则的话中文字体会显示为空

![image-20211230111914386](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301119011.png)

> 设置模板数据源类型

![image-20211230112010046](https://gitee.com/image_bed/image-bed1/raw/master/img/202112301120858.png)	
